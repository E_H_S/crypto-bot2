from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class Bolli(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)
		self.period = period
		self.limits = False

		self.indicators = BotIndicators(orderbook= False,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime)
		
		#Inititalize inticator vectors
		self.mavs1  = np.array([])
		self.B1_upper = np.array([])
		self.B1_lower = np.array([])


		self.B2_upper = np.array([])
		self.B2_lower = np.array([])


		#Extras
		self.mavs2  = np.array([])
		self.mavs3  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None
		self.last_orderbook = pd.DataFrame()
		self.open_value = np.array([])
		self.ATR_switches = np.array([])
		self.ATR_min_list = np.array([])
		self.open_period = np.array([])

		self.count10 = 0
		self.switch10 = False
		self.ATR_switch = False
		self.last_OV_high = 1000000000000000
		self.index = 0
		self.ATR_min = 0

		self.numSimulTrades = 1
		self.margin = True
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		#updating Indicators

		self.mavs1 = np.append(self.mavs1,self.indicators.EMA(self.closes[0:-1],180,append = True))

		B1_upper,B1_lower = self.indicators.Bollingerband(self.closes[0:-1],self.mavs1[0:-1],3,period = 180)
		self.B1_upper = np.append(self.B1_upper,B1_upper)
		self.B1_lower = np.append(self.B1_lower,B1_lower,)

		B2_upper,B2_lower = self.indicators.Bollingerband(self.closes[0:-1],self.mavs1[0:-1],9,period = 180)
		self.B2_upper = np.append(self.B2_upper,B2_upper,)
		self.B2_lower = np.append(self.B2_lower,B2_lower,)

		if len(self.dates) > 2:
			self.open_value = self.indicators.get_open_value(self.dates[-1:])
			#print(self.open_value['max_delta'])
			if len(self.open_value['max_delta'][-1:]) == 1:
				if self.open_value['max_delta'][-1:].item() < - 1000:
					self.open_period = np.append(self.open_period,1)
				else:
					self.open_period = np.append(self.open_period,0)
			else:
				self.open_period = np.append(self.open_period,0)

		else:
			self.open_period = np.append(self.open_period,0)
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 1000
		stoploss = None
		#market_order = 'zero'
		#print(date)
		market_order = False
		# if self.dead_date_start > date.item().replace(tzinfo=None):
		# 	if date.item().replace(tzinfo=None) < self.dead_date_end == False:
				
		
		if self.open_period[-1:] == 1 or self.open_period[-1:] == 0:
			if self.mavs1[-1:] != None and self.B1_lower[-1:] != None and self.B1_upper[-1:] != None and self.B2_lower[-1:] != None and self.B2_upper[-1:] != None:
				if self.highs[-1:] > self.B1_upper[-1:] and self.closes[-2:-1] < self.B1_upper[-1:] :
					action = True
					Long = False
					price = self.B1_upper[-1:]
					#stoploss = self.B2_upper[-1:]
					
							
				if self.lows[-1:] < self.B1_lower[-1:] and self.closes[-2:-1] > self.B1_lower[-1:]:
					
					action = True
					Long = True
					price = self.B1_lower[-1:]
					#stoploss = self.B2_lower[-1:]
		

		if action == True:
			print("ACTION TRUE")
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		
		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		


		if self.highs[-1:] > self.B1_upper[-1:] and  trade.long == True:
		#if self.highs[-1:] > self.mavs1[-1:] and  trade.long == True:
			action = True
			price = self.B1_upper[-1:]
			#price = self.mavs1[-1:]

		
		if self.lows[-1:] < self.B1_lower[-1:] and trade.long == False:
		#if self.lows[-1:] < self.mavs1[-1:] and trade.long == False:
			action = True
			price = self.B1_lower[-1:]
			#price = self.mavs1[-1:]
		
		
		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass
	def calculate_order_book_depth():
		pass




	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.mavs1,self.B1_upper,self.B1_lower,self.B2_upper,self.B2_lower,self.open_value,self.open_period)
