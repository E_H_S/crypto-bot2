from poloniex import poloniex
import urllib.request, urllib.parse, urllib.error, json
import pprint
from Renko_bars import renko_bars
from Mom_object import Mom_object
from botcandlestick import BotCandlestick
import datetime
import time
import pandas as pd
import sqlite3 as sql
import psycopg2


class BotChart(object):
	def __init__(self, exchange, pairs, period, startTime =None,endTime=None,backtest=True,data_set = None,chart_style = 'candlestick',download = False,candle_table = None):


		self.startTime_SYD = startTime
		self.endTime_SYD = endTime
		self.candle_table = candle_table

		self.startTime_UTC = self.startTime_SYD - datetime.timedelta(hours = 11)
		self.endTime_UTC = self.endTime_SYD - datetime.timedelta(hours = 11)
		#self.db_file_path = '../Database_Prep/Database1_copy1.db'
		self.db_file_path = '//Volumes/Samsung_T5/Databaseprep2/Bitmex_backup_db10.db'

		self.pairs = pairs #list of pairs to query
		self.data = {}
		self.exchange = exchange
		self.period = period
		self.last_candle_date = startTime
		self.count = 0
		self.startTime = startTime
		self.endTime = endTime
		self.chart_style = chart_style
		self.renko = renko_bars(period)
		self.mom_object = Mom_object()
		self.renko_bars = {}
		
		if (exchange == "poloniex"):
			self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')

			if backtest == True:

				for pair in self.pairs:
					poloData = self.conn.api_query("returnChartData",{"currencyPair":pair,"start":self.startTime,"end":self.endTime,"period":self.period})
					#print("POLO_DATA", poloData)
					self.data[str(pair)] = self.create_candles(poloData)
					
					if chart_style in ('renko','renko_object'):
						#self.renko(self.data[str(pair)],pair)
						if chart_style == 'renko':
							#self.renko_bars[str(pair)] = self.renko.renko(self.data[str(pair)],pair)
							self.renko_bars = self.renko.renko(self.data[str(pair)],pair)
						elif chart_style == 'renko_object':
							self.multi_renko(pair)



			if download == True:
				df = pd.DataFrame(poloData)
				csv_name = str(self.pairs)+"_"+str(self.period)+'_'+'(' +str(datetime.date.fromtimestamp(self.startTime))+')'+"---"+'('+str(datetime.date.fromtimestamp(self.endTime))+')'+'.csv'
				df.to_csv(csv_name)

			elif backtest == False and data_set != None:
				data = pd.read_csv(data_set)
				#print(data['timestamp'])
				data = data[(data['date']> startTime)]
				data = data[(data['date']< endTime)]
				for pair in self.pairs:
					poloData = data.T.to_dict().values()
					self.data[str(pair)] = self.create_candles(poloData)

					if chart_style == 'renko':
						#self.renko(self.data[str(pair)],pair)
						self.renko_bars = self.renko.renko(self.data[str(pair)],pair)


		for pair in self.pairs:
			if (exchange == "Database1"):
				con = psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432)
				cur = con.cursor()

				with con:
					cur.execute("""
			        SELECT DISTINCT
			        *
			        FROM {}
			        WHERE 1=1
			        and time_stamp > %s
			        and time_stamp < %s
			        order by time_stamp
			        """.format(self.candle_table),(self.startTime_UTC,self.endTime_UTC))
				
				poloData = cur.fetchall()
				print(len(poloData))
				df = pd.DataFrame(poloData)
				#df.columns = ['date','open','close','high','low','volume','time_stamp']
				df.columns = ['time_stamp','open','high','low','close','volume']
				df['datetime_UTC'] = pd.to_datetime(df['time_stamp'])

				df['datetime_syd'] = df['datetime_UTC'] + pd.Timedelta(hours = 11)
				
				poloData = df.T.to_dict().values()
				#print(poloData)
				#time.sleep(15)
				self.data[str(pair)] = self.create_candles(poloData)



	def getPoints(self):
		if self.chart_style == "candlestick":
			return self.data
		elif self.chart_style == 'renko':
			#print(self.renko_bars)
			#print(len(self.data[self.pairs[0]]))
			#print(len(self.renko_bars[self.pairs[0]]))
			return self.renko_bars

	def getCurrentPrice(self):
		currentValues = self.conn.api_query("returnTicker")
		lastPairPrice = {}
		lastPairPrice = currentValues[self.pair]["last"]
		lastAskingPrice = float(currentValues[self.pair]["lowestAsk"])
		return lastPairPrice , lastAskingPrice
	
	def LastestCandle(self,last_candle):
		self.endTime = time.time()+ self.period
		self.startTime = self.endTime - self.period*4
		for pair in self.pairs:
			poloData = self.conn.api_query("returnChartData",{"currencyPair":pair,"start":self.startTime,"end":self.endTime,"period":self.period})

		count = 0
		candles_L = 0
		
		for candles in poloData:
			

			if count == 1:
				new_last_candle = BotCandlestick(self.period,candles['date'],candles['volume'],candles['quoteVolume'],candles['open'],candles['close'],candles['high'],candles['low'],candles['weightedAverage'])
				return new_last_candle
				
			if candles['date'] == last_candle.date:
				count = 1
			
		
	def create_candles(self,candle_dict):
		candles = []
		#print(candle_dict)
		for datum in candle_dict:
			if self.exchange == 'poloniex':
				if (datum['open'] and datum['close'] and datum['high'] and datum['low']):
					candles.append(BotCandlestick(self.period,datum['date'],datum['volume'],datum['quoteVolume'],datum['open'],datum['close'],datum['high'],datum['low'],datum['weightedAverage']))
		
			if self.exchange == 'Database1':
				if (datum['open'] and datum['close'] and datum['high'] and datum['low']):
					candles.append(BotCandlestick(
						period = self.period,
						date =datum['datetime_syd'],
						volume = datum['volume'],
						quoteVolume = None,
						open = datum['open'],
						close = datum['close'],
						high = datum['high'],
						low = datum['low'],
						priceAverage = None))
		


		return candles

	def multi_renko(self,pair):
		momentum_percentages = [0.25,0.5,0.75,1,1.5,2,2.5,3,4,5,6,8,10,12]
		momentums = []
		for x in momentum_percentages:
			renko_bar = self.renko.renko(self.data[str(pair)],pair,percentage=x)
			momentums.append(renko_bar)
		

		print(momentums)

	













	# def renko(self,candles,pair,percentage = 0.5):
	# 	#print("check1")
	# 	self.renko_bars = {}
	# 	self.renko_bars[str(pair)] =[]
	# 	self.count = 0
	# 	self.R_date = 0
	# 	for candle in candles:
	# 		if self.count == 0:
	# 			#print("check3")
	# 			self.R_start_point = candle.priceAverage
	# 			self.R_high = candle.high
	# 			self.R_low = candle.low
	# 			self.R_volume = candle.volume
	# 			self.count +=1
			
			
	# 		#print(candle.close > self.R_start_point*(1+ percentage/100))
	# 		#print(candle.close < self.R_start_point/(1+ percentage/100))

	# 		if candle.open != 0 and candle.close != 0:


	# 			if candle.close > self.R_start_point*(1+ percentage/100):
	# 				renko_bar = self.create_renko_bar(candle,pair,percentage,UP = True)
				
	# 			elif candle.close < self.R_start_point/(1+ percentage/100):
	# 				renko_bar = self.create_renko_bar(candle,pair,percentage,UP = False)
	# 			else:
	# 				self.R_volume += candle.volume
				
	# 			if self.R_high < candle.high:
	# 				self.R_high = candle.high
	# 			elif self.R_low > candle.low:
	# 				self.R_low = candle.low



	# def create_renko_bar(self,candlestick,pair,percentage,UP):
	# 	#print("check2")
	# 	if UP == True:
	# 		close = self.R_start_point*(1+ percentage/100)
	# 		high = close
	# 		low = self.R_start_point
	# 	elif UP == False:
	# 		close = self.R_start_point/(1+ percentage/100)
	# 		low = close
	# 		high = self.R_start_point

	# 	renko_bar = BotCandlestick(self.period,self.R_date,self.R_volume,0,self.R_start_point,close,high,low,close)
	# 	#renko_bar = BotCandlestick(self.period,candlestick.date,self.R_volume,0,self.R_start_point,close,high,low,close)
		
	# 	#print(candlestick.date,self.R_volume,0,self.R_start_point,close,self.R_high,self.R_low,0)
	# 	#print(renko_bar)
	# 	self.renko_bars[str(pair)].append(renko_bar)
	# 	self.R_start_point = close
	# 	#print(self.R_start_point,low)
	# 	self.R_volume  = 0
	# 	self.R_high = close
	# 	self.R_low = close
	# 	self.R_date += 300
		

		




		

