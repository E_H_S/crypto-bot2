import numpy as np
from botcandlestick import BotCandlestick

class renko_bars(object):
	def __init__(self,period):
		self.period = period


	def renko(self,candles,pair,percentage = 1):
		#print("check1")
		self.renko_bars = {}
		self.renko_bars[str(pair)] =[]
		self.count = 0
		self.R_date = 0
		for candle in candles:
			if self.count == 0:
				#print("check3")
				self.R_start_point = candle.priceAverage
				self.R_high = candle.high
				self.R_low = candle.low
				self.R_volume = candle.volume
				self.count +=1
			
			
			#print(candle.close > self.R_start_point*(1+ percentage/100))
			#print(candle.close < self.R_start_point/(1+ percentage/100))

			if candle.open != 0 and candle.close != 0:


				if candle.close > self.R_start_point*(1+ percentage/100) and candle.close < self.R_start_point/(1+ percentage/100):
					self.R_volume += candle.volume
				elif candle.close > self.R_start_point*(1+ percentage/100):
					renko_bar = self.create_renko_bar(candle,pair,percentage,UP = True)
				
				elif candle.close < self.R_start_point/(1+ percentage/100):
					renko_bar = self.create_renko_bar(candle,pair,percentage,UP = False)
				else:
					self.R_volume += candle.volume
				
				if self.R_high < candle.high:
					self.R_high = candle.high
				elif self.R_low > candle.low:
					self.R_low = candle.low

		return self.renko_bars



	def create_renko_bar(self,candlestick,pair,percentage,UP):
		#print("check2")
		if UP == True:
			close = self.R_start_point*(1+ percentage/100)
			high = close
			low = self.R_start_point
		elif UP == False:
			close = self.R_start_point/(1+ percentage/100)
			low = close
			high = self.R_start_point

		renko_bar = BotCandlestick(self.period,self.R_date,self.R_volume,0,self.R_start_point,close,high,low,close)
		#renko_bar = BotCandlestick(self.period,candlestick.date,self.R_volume,0,self.R_start_point,close,high,low,close)
		
		#print(candlestick.date,self.R_volume,0,self.R_start_point,close,self.R_high,self.R_low,0)
		#print(renko_bar)
		self.renko_bars[str(pair)].append(renko_bar)
		self.R_start_point = close
		#print(self.R_start_point,low)
		self.R_volume  = 0
		self.R_high = close
		self.R_low = close
		self.R_date += 300
