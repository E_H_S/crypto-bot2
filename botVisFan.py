import matplotlib.pyplot as plt 
import numpy as np 
import datetime as dt
from matplotlib import style
from mpl_finance import candlestick_ohlc 
import pandas as pd 
import matplotlib.dates as mdates
import time



#import seaborn as sns


class BotVisuals(object):
	def __init__(self): #Takes an input of a strategy log 

		style.use('seaborn')
		self.font_dict1 = {'family':'serif','color':"blue","size":5}
		self.font_dict2 = {'family':'serif','color':"green","size":5}
		self.font_dict3 = {'family':'serif','color':"red","size":5}
		self.ax1 = plt.subplot2grid((10,4),(0,0),rowspan=8,colspan=3)
		#self.fig1 = plt.figure()
		#self.ax1 = self.fig1.add_subplot(1,1,1)
		#self.f2, self.ax1 = plt.subplots(figsize = (10,5))
		
		
	
	def draw(self,StrategyLog,draw_pause = None):
		self.prices = StrategyLog.AveragePrices
		self.pair = StrategyLog.pair

		self.openprices = StrategyLog.openprices
		self.closeprices = StrategyLog.closeprices
		self.highs =  StrategyLog.highs
		self.lows = StrategyLog.lows

		self.mavs2 = StrategyLog.strategy.mavs2
		self.mavs3 = StrategyLog.strategy.mavs3
		self.mavs4 = StrategyLog.strategy.mavs4
		self.mavs5 = StrategyLog.strategy.mavs5
		self.mavs6 = StrategyLog.strategy.mavs6
		self.mavs7 = StrategyLog.strategy.mavs7
		self.mavs8 = StrategyLog.strategy.mavs8
		self.Emavs12 = StrategyLog.strategy.Emavs12




		self.datetimes = StrategyLog.dates
		self.ohlc = pd.DataFrame({"Date":self.datetimes,"Open":self.openprices,"High":self.highs,"Low":self.lows,"Close":self.closeprices})
		self.ohlc = self.ohlc[['Date',"Open","High","Low","Close"]]


		
		if (draw_pause > 0):
			style.use('seaborn')
			plt.ion()
			plt.show()
			plt.ylabel(self.pair)
			plt.xlabel('Datetime')
			plt.xticks(rotation=60)
			plt.axis('tight')

			
			
			self.ax1.plot(self.datetimes,self.prices,color = 'red',marker = '*')
			self.ax1.scatter(self.datetimes,self.openprices,color = 'blue',marker = '_')
			self.ax1.scatter(self.datetimes,self.closeprices,color = 'red',marker = '_')
			self.ax1.scatter(self.datetimes,self.highs,color = 'green',marker = '_')
			self.ax1.scatter(self.datetimes,self.lows,color = 'black',marker = '_')
			self.ax1.plot(self.datetimes, self.mavs ,'b--',marker = '.')
			self.ax1.plot(self.datetimes, self.mavs2 ,'y--',marker = '.')
			self.ax1.plot(self.datetimes, self.Emavs3 ,'k--',marker = '.')
			candlestick_ohlc(self.ax1,self.ohlc.values ,width = 250,colorup = 'green',colordown='red',alpha = 0.5)
			
			plt.draw()
			plt.pause(draw_pause)



		if StrategyLog.strategy.tradeStatus[-1:] in ("OPEN","CLOSED","STOP LOSSED","TARGET HIT"):

			self.tradeMarker(StrategyLog.strategy.trades[-1:])


	def buildChart(self,StrategyLog):

		
		self.ohlc = pd.DataFrame({"Date":self.datetimes,"Open":self.openprices,"High":self.highs,"Low":self.lows,"Close":self.closeprices})
		self.ohlc = self.ohlc[['Date',"Open","High","Low","Close"]]


		#self.datetimes = self.dateconv(StrategyLog.dates)
		self.pair = StrategyLog.pair
		
	
		
		style.use('seaborn')
		plt.xticks(rotation=60)

		self.ax1.plot(self.datetimes,self.prices,color = 'red',marker = '*')
		self.ax1.scatter(self.datetimes,self.openprices,color = 'blue',marker = '_')
		self.ax1.scatter(self.datetimes,self.closeprices,color = 'red',marker = '_')
		self.ax1.scatter(self.datetimes,self.highs,color = 'green',marker = '_')
		self.ax1.scatter(self.datetimes,self.lows,color = 'black',marker = '_')
		
		
		self.ax1.plot(self.datetimes, self.mavs2 ,'y--',marker = '.')
		self.ax1.plot(self.datetimes, self.mavs3 ,'g--',marker = '.')
		self.ax1.plot(self.datetimes, self.mavs4 ,'c--',marker = '.')
		self.ax1.plot(self.datetimes, self.mavs5 ,'b--',marker = '.')
		self.ax1.plot(self.datetimes, self.mavs6 ,'m--',marker = '.')
		self.ax1.plot(self.datetimes, self.mavs7 ,'w--',marker = '.')
		self.ax1.plot(self.datetimes, self.mavs8 ,'k--',marker = '.')
		self.ax1.plot(self.datetimes, self.Emavs12 ,'y',marker = '*')


		candlestick_ohlc(self.ax1,self.ohlc.values ,width = 10000,colorup = 'green',colordown='red',alpha = 0.5)
		#self.ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d-%H'))
		plt.show()
		
		#self.ax1.plot(self.datetimes,self.mavs2,'g--',marker = '.')
		plt.ylabel(str(self.pair)+ " : Price ")
		plt.xlabel('Datetime')
		
		
	def tradeMarker(self,trade):
		for Trade in trade[-1:]: #converting from an list of trade objects to singular trade object

			if Trade.status == "OPEN":
				trade_enter_date = time.mktime(Trade.entryDate.timetuple())
			if Trade.status in ("CLOSED","STOP LOSSED","TARGET HIT"):
				trade_exit_date = time.mktime(Trade.exitDate.timetuple())
				trade_enter_date = time.mktime(Trade.entryDate.timetuple())


			if Trade.status == 'OPEN':
				self.ax1.plot(trade_enter_date,Trade.entryPrice,color = 'cyan' ,marker = 'o',alpha = 0.7)
				#plt.text(date,price,'Opened',fontdict= self.font_dict1)
			elif Trade.status == "CLOSED":
				if Trade.stoplossed == False:
					self.ax1.plot(trade_exit_date,Trade.exitPrice,color = 'magenta' ,marker = 'o',alpha = 0.7)
					self.ax1.plot(trade_enter_date,Trade.entryPrice,color = 'cyan' ,marker = 'o',alpha = 0.7)
					#plt.text(date,price,'Closed',fontdict= self.font_dict2)
			elif Trade.status == "STOP LOSSED":
					self.ax1.plot(trade_exit_date,Trade.exitPrice,color = 'black' ,marker = 'o',alpha = 0.7)
					self.ax1.plot(trade_enter_date,Trade.entryPrice,color = 'cyan' ,marker = 'o',alpha = 0.7)
					#plt.text(date,price,'Closed by stop loss',fontdict= self.font_dict3)
			elif Trade.status == "TARGET HIT":
					self.ax1.plot(trade_exit_date,Trade.exitPrice,color = 'yellow' ,marker = 'o',alpha = 0.7)
					self.ax1.plot(trade_enter_date,Trade.entryPrice,color = 'cyan' ,marker = 'o',alpha = 0.7)
					#plt.text(date,price,'Closed by stop loss',fontdict= self.font_dict3)


		

