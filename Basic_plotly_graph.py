import numpy as np 
import pandas as pd 
import plotly.offline as pyo
import plotly.graph_objs as go

data = pd.read_csv('XBT_instrument_data.csv')
data.columns = ['Date','Open Interest','openValue','Last Price']

data['openValue'] = data['openValue']/100000000


traces=[go.Scatter(
	x = data['Date'],
    y = data['Last Price'],
    mode = 'markers+lines'
),
go.Scatter(
	x = data['Date'],
   	y = data['openValue'],
    mode = 'markers+lines',
    yaxis='y2'
)
]

# layout = go.Layout(
#     title = 'Bitmex Price vs Open Interest'
# )


layout = go.Layout(
    title='Bitmex Price vs Open value',
    yaxis=dict(
        title='XBT Price'
    ),
    yaxis2=dict(
        title='Open value (BTC)',
        titlefont=dict(
            color='rgb(148, 103, 189)'
        ),
        tickfont=dict(
            color='rgb(148, 103, 189)'
        ),
        overlaying='y',
        side='right'
    )
)





fig = go.Figure(data=traces,layout=layout)
pyo.plot(fig, filename='XBT_OI105.html')