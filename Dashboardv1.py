import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_auth
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import bitmex
import datetime
import time

USERNAME_PASSWORD_PAIRS = [['erik', 'getrekt'],['arash', 'getrekt']]
client = bitmex.bitmex(test=False,api_key ='LTt_90UVZJaUYUdhesjTriFl',api_secret='0UWDNbs-04gh1Llj9X8OYHX8zlt1psuCs9WjHH7bQz2evUSv')


app = dash.Dash()
auth = dash_auth.BasicAuth(app,USERNAME_PASSWORD_PAIRS)

app.layout = html.Div([
    "Look back period (minutes)",
    dcc.Input(
        id='number-in',
        value=180,
        style={'fontSize':18}),
    dcc.Dropdown(
    id = 'candlestick-size',
    options=[
        {'label': '1m', 'value': '1m'},
        {'label': '5m', 'value': '5m'},
        {'label': '1h', 'value': '1h'},
        {'label': '1d', 'value': '1d'}
    ],
    value='1m'),
    dcc.Dropdown(
    id = 'Open-metric',
    options=[
        {'label': 'Open Interest', 'value': 'openInterest'},
        {'label': 'Open Value', 'value': 'openValue'}
    ],
    value='openValue'),

    html.Button(
        id='submit',
        n_clicks=0,
        children='Submit',
        style={'fontSize':18}),

    dcc.Graph(id='live-update-graph'),
    html.Div([
    dcc.Textarea(
    placeholder='Enter a value...',
    rows = '1',
    value='5 mins',
    style={'width': '30%','fontSize':30,'color':'blue'}),
    dcc.Textarea(
    
    placeholder='Enter a value...',
    rows = '1',
    value='30 mins',
    style={'width': '30%','fontSize':30,'color':'blue'}),
    dcc.Textarea(
    
    placeholder='Enter a value...',
    rows = '1',
    value='Lookback Period',
    style={'width': '30%','fontSize':30,'color':'blue'}),



    dcc.Textarea(
    id = '5-min-longs',
    placeholder='Enter a value...',
    rows = '1',
    value='This is a TextArea component',
    style={'width': '30%','fontSize':30,'color':'green'}),
    dcc.Textarea(
    id = '30-min-longs',
    placeholder='Enter a value...',
    rows = '1',
    value='This is a TextArea component',
    style={'width': '30%','fontSize':30,'color':'green'}),
    dcc.Textarea(
    id = 'lookback-longs',
    placeholder='Enter a value...',
    rows = '1',
    value='This is a TextArea component',
    style={'width': '30%','fontSize':30,'color':'green'}),



    dcc.Textarea(
    id = '5-min-shorts',
    placeholder='Enter a value...',
    rows = '1',
    value='This is a TextArea component',
    style={'width': '30%','fontSize':30,'color':'red'}),
    dcc.Textarea(
    id = '30-min-shorts',
    placeholder='Enter a value...',
    rows = '1',
    value='This is a TextArea component',
    style={'width': '30%','fontSize':30,'color':'red'}),
    dcc.Textarea(
    id = 'lookback-shorts',
    placeholder='Enter a value...',
    rows = '1',
    value='This is a TextArea component',
    style={'width': '30%','fontSize':30,'color':'red'})
    ]),
    


    #dcc.Graph(id='volume-graph'),

    dcc.Interval(
        id='interval-component',
        interval=10000, # 6000 milliseconds = 6 seconds
        n_intervals=0
    )])



@app.callback(Output('lookback-longs','value'),
              [Input('interval-component', 'n_intervals')])
def update_liquidations(n):
    data = liquidation_data()
    start_time = datetime.datetime.utcnow() - datetime.timedelta(minutes = 5)
    liquids = data[data['timestamp'] > start_time]
    longs = liquids[liquids['side'] == 'Buy']['leavesQty'].sum()
    shorts = liquids[liquids['side'] == 'Sell']['leavesQty'].sum()
    string = str(longs/1000000) + " M Long"
    return string

@app.callback(Output('5-min-longs','value'),
              [Input('interval-component', 'n_intervals'),
              Input('number-in', 'value')])
def update_liquidations(n,lookback):
    data = liquidation_data()
    start_time = datetime.datetime.utcnow() - datetime.timedelta(minutes = int(lookback))
    liquids = data[data['timestamp'] > start_time]
    longs = liquids[liquids['side'] == 'Buy']['leavesQty'].sum()
    shorts = liquids[liquids['side'] == 'Sell']['leavesQty'].sum()
    string = str(longs/1000000) + " M Long"
    return string


@app.callback(Output('30-min-longs','value'),
              [Input('interval-component', 'n_intervals')])
def update_liquidations(n):
    data = liquidation_data()
    start_time = datetime.datetime.utcnow() - datetime.timedelta(minutes = 30)
    liquids = data[data['timestamp'] > start_time]
    longs = liquids[liquids['side'] == 'Buy']['leavesQty'].sum()
    shorts = liquids[liquids['side'] == 'Sell']['leavesQty'].sum()
    string = str(longs/1000000) + " M Long"
    return string


@app.callback(Output('5-min-shorts','value'),
              [Input('interval-component', 'n_intervals')])
def update_liquidations(n):
    data = liquidation_data()
    start_time = datetime.datetime.utcnow() - datetime.timedelta(minutes = 5)
    liquids = data[data['timestamp'] > start_time]
    shorts = liquids[liquids['side'] == 'Sell']['leavesQty'].sum()
    string = str(shorts/1000000) + " M shorts"
    return string

@app.callback(Output('30-min-shorts','value'),
              [Input('interval-component', 'n_intervals')])
def update_liquidations(n):
    data = liquidation_data()
    start_time = datetime.datetime.utcnow() - datetime.timedelta(minutes = 30)
    liquids = data[data['timestamp'] > start_time]
    shorts = liquids[liquids['side'] == 'Sell']['leavesQty'].sum()
    string = str(shorts/1000000) + " M shorts"
    return string

@app.callback(Output('lookback-shorts','value'),
              [Input('interval-component', 'n_intervals'),
              Input('number-in', 'value')])
def update_liquidations(n,lookback):
    data = liquidation_data()
    start_time = datetime.datetime.utcnow() - datetime.timedelta(minutes = int(lookback))
    liquids = data[data['timestamp'] > start_time]
    shorts = liquids[liquids['side'] == 'Sell']['leavesQty'].sum()
    string = str(shorts/1000000) + " M shorts"
    return string








@app.callback(Output('live-update-graph','figure'),
              [Input('interval-component', 'n_intervals')
              ,Input('submit', 'n_clicks')
              ],[State('number-in', 'value'),
              State('candlestick-size','value'),
              State('Open-metric','value')])
def update_graph(n,clicks,lookback,size,metric):
    print(n,lookback,clicks,size,metric)
    
    data,candles = get_data(int(lookback),size)

    graph1 = go.Candlestick(
                        x = candles['timestamp'],
                        open = candles['open'],
                        high = candles['high'],
                        low = candles['low'],
                        close = candles['close'])
    graph2 = go.Scatter(
                        x = data['Date'],
                        y = data[metric],
                        mode = 'lines',
                        yaxis='y2')
    graph3 = go.Bar(
                        x = candles['timestamp'],
                        y = candles['volume'],
                        yaxis = 'y3',
                        opacity = 0.35
                    )


    figure = {'data':[graph1,graph2,graph3],
                   
                    'layout':go.Layout(
                            # autosize=False,
                            # width=1200,
                            # height=1200,
                            # 
                            title='Bitmex Price vs Open value',
                            yaxis=dict(title='XBT Price'),

                            yaxis2=dict(
                            title='Open value(BTC)',
                            titlefont=dict(
                            color='rgb(148, 103, 189)'
                    ),
                            tickfont=dict(
                            color='rgb(148, 103, 189)'
                    ),
                            overlaying='y',
                            side='right'
    ),
                            yaxis3=dict(
                            title='Volume',
                            titlefont=dict(
                            color='rgb(148, 103, 189)'
                    ),
                            tickfont=dict(
                            color='#1f77b4'
                    ),
                            overlaying='y',
                            visible = False,
                            anchor = 'free',
                            side='left'
                            
                            )
                             
)}
    return figure
def get_data(lookback,size):

    if size == '1m':
        s = 1
    elif size == '5m':
        s = 5
    elif size == '1h':
        s = 60
    elif size == '1d':
        s = 1440


    start = datetime.datetime.utcnow() + datetime.timedelta(minutes=-lookback)
    end = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)

    h = 0
    candles_list = pd.DataFrame()
    n = False
    if lookback/s > 500:

        while n == False:
            time.sleep(.1)
            candles = client.Trade.Trade_getBucketed(symbol='XBTUSD',binSize = size, startTime=start,endTime=end,count = 500,start = h,partial = True).result()
            h += 500

            candles = pd.DataFrame(candles[0])
            
            candles_list = pd.concat([candles_list,candles])
            if h > lookback/s:
                n = True
    else:
        candles_list = client.Trade.Trade_getBucketed(symbol='XBTUSD',binSize = size, startTime=start,endTime=end,count = 500,start = h,partial = True).result()
        candles_list = pd.DataFrame(candles_list[0])
        

    data = pd.read_csv('XBT_instrument_data9.csv')
    data.columns = ['Date','openInterest','openValue','Last Price','volume']
    data['openValue'] = data['openValue']/100000000

    data['datetimes'] = pd.to_datetime(data['Date'],format= "%Y-%m-%dT%H:%M:%S.%fZ")
    data = data[data['datetimes'] > start]

    return data,candles_list

def liquidation_data():
    data = pd.read_csv('XBT_Liquidations.csv')
    data['timestamp'] = pd.to_datetime(data['timestamp'])
    return data


if __name__ == '__main__':
    app.run_server()