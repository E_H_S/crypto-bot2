from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
from poloniex import poloniex
import time

#lolololol
#testy test test
#test branch commit


class SMA(object):
	def __init__(self,pair,starting_bull_volume = None,starting_bear_volume= None,live = False,backtest = True):

		#connect to the APi and determin if the script is a backtest or a live trading scenario
		self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')
		self.live = live
		self.bear,self.bull = pair.split("_")
		self.backtest = backtest

		#Initial perameters of this given strategy]
		self.pair = pair
		self.numSimulTrades = 1
		self.indicators = BotIndicators()
		###############
		#To feed information up the chain the data that needs to be atributes
		#the data requirements for BotStratLog are.
		# Initiate trading based vectors
		self.tradeStatus = np.array([])	#TradeStatus is of form "OPENED","IN POSITION",("CLOSED" OR "STOPLOSSED","WAITING"
		self.BullCommisions = np.array([])	#Number of tokens taken by the exchange during a buy order
		self.BearCommisions = np.array([])	#Number of tokens taken by the exchange during a sell order
		self.BullTokens = np.array([starting_bull_volume])		# Number of Bull tokens at the datetime after the Trade and Comission
		self.BearTokens = np.array([starting_bear_volume])		# Number of Bear tokens at the datetime after the Trade and Comission
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.Emavs3 = np.array([])
		self.varience = np.array([])
		self.momentum = np.array([])

		self.rsi = np.array([])
		self.volume = np.array([])

		self.macdb = np.array([])									
		self.macdr = np.array([])
		self.macdd = np.array([])

		self.B_upper = np.array([])
		self.B_lower = np.array([])

		self.openTrades = [] 		#  Initiate a vector of trade objects
		self.trades = []     		#  self.trades's elements are trade objects with the attributes  self.Entry_price, self.Entry_date self.Entry_volume, self.Status, self.Exit_price, self.Exit_date, self.Exit_volume
		self.target_sells = 0
		self.close_sells = 0

		
	def tick(self,candlestick,prices,lows,highs,opens,closes,dates):#candlestick is a singular candlestick, prices is a list of all candlestick price averages

		#updating Indicators
		self.rsi = np.append(self.rsi,self.indicators.RSI(prices,period=14)) #RSI indicator
		self.macdb,self.macdr,self.macdd = self.indicators.MACD(prices) #MACD indicator
		self.varience = np.append(self.varience,self.indicators.avg_range(prices,highs,lows,3))
		self.momentum = np.append(self.momentum,[self.indicators.momentum(closes,period = 4)])

		if self.pair in ("USDT_ETH","USDT_BTC","USDT_XMR"):
			self.mavs = np.append(self.mavs,[self.indicators.movingAverage(prices,4)]) # Creating the self.mavs list for analysis and visualsation later.
			self.Emavs3 = np.append(self.Emavs3,[self.indicators.movingAverage(prices,2)])
		else:
			self.mavs = np.append(self.mavs,[self.indicators.movingAverage(prices,5)])
			self.Emavs3 = np.append(self.Emavs3,[self.indicators.movingAverage(prices,3)])

		if len(self.mavs_delta) == 0:
			self.mavs_delta = np.append(self.mavs_delta,[None])
		else:
			self.mavs_delta = np.append(self.mavs_delta,[self.mavs[-1:]/self.mavs[-2:-1]])
		self.mav2L = 12
		self.mavs2 = np.append(self.mavs2,[self.indicators.EMA(prices,self.mav2L)])
		self.volume = np.append(self.volume,candlestick.volume)
		self.buy_target = None

		############################################################################################################################################################################################################################
		#ALL Live Script !!!
		if self.live == True:
			print("check1")
			balence = self.conn.returnBalances()
			self.BullTokens = np.append(self.BullTokens,balence[self.bull])
			self.BearTokens = np.append(self.BearTokens,balence[self.bear])
		else:
		############################################################################################################################################################################################################################
		#ALL BACK TEST SCRIPT!!!
			self.tradeStatus = np.append(self.tradeStatus,["WAITING"])									#Assume status for this timestamp is waiting unless proven not to be later.
			if len(self.mavs) > 1: 																		#only appends on a tick the 2nd tick onwards to avoid double ups with the first instatiate uses len(mavs) to represent tick index
				self.BullTokens = np.append(self.BullTokens,self.BullTokens[-1:])						#assume Tokens remain the same as last iteration unless trade happens in which case change tokens at that point 
				self.BearTokens = np.append(self.BearTokens,self.BearTokens[-1:])
			self.BullCommisions = np.append(self.BullCommisions,[float(0)])
			self.BearCommisions = np.append(self.BearCommisions,[float(0)])		#Asume no comission fee for this cycle unless unless trade happens in which case we will add comission the end at that point
			
					
		if self.live == True or self.backtest == True:
			self.evaluatePositions(datetime.datetime.fromtimestamp(candlestick.date),prices,candlestick,opens = opens,lows = lows,highs = highs ,closes = closes) 	#evaluate the position


		if self.tradeStatus[-1:] == "IN POSITION":
			self.updateOpenTrades(candlestick,datetime.datetime.fromtimestamp(candlestick.date)) #Evaluate already open positions




	def evaluatePositions(self,date,prices,candlestick,lows=None,highs=None,opens=None,closes=None,dates=None):
		#if self.live == True and len(self.BullTokens)==2:
		#	pass
		#	self.OpenPosition(candlestick,date) #OPEN POSITION!
		#elif self.live == True and len(self.BullTokens)==3:
		#	self.ClosePosition(date,candlestick)#SELL POSITION!
		
		####################################################################################################################################################################################
		# The Strategy Lines BELOW
		####################################################################################################################################################################################
		
		openTrades = []
		for trade in self.trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade)											 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1:] = "IN POSITION" 


				#BUYING CONDITIONS
		if (len(openTrades) < self.numSimulTrades): 								#Checks if the algorithm is allowed to take on another position
			if len(self.BullTokens)> self.mav2L+1: 
				#Buying condition one below
				if self.mavs[-4:-3] > self.mavs[-3:-2] and self.mavs[-2:-1]/self.mavs[-3:-2] > 1 and prices[-2:-1][0] > self.mavs2[-2:-1] and self.mavs_delta[-2:-1] > 1: 
					self.buy_target = closes[-2:-1:][0]/1.002
				
				#elif self.mavs[-4:-3]*1.005 > self.mavs[-3:-2] and self.mavs[-2:-1]/self.mavs[-3:-2] > 1 and self.mavs[-2:-1] > self.mavs2[-2:-1] and self.mavs_delta[-2:-1] > 1:
				#	self.buy_target = closes[-2:-1:][0]/1.002

				#Buying condition two below
				elif  self.mavs[-2:-1]/self.mavs[-3:-2] > 1 and prices[-2:-1][0] > self.mavs2[-2:-1] and self.mavs_delta[-2:-1] > 1 and self.Emavs3[-2:-1] > self.mavs[-2:-1]: 
					self.buy_target = closes[-2:-1:][0]/1.05

				if self.buy_target != None:
					if lows[-1:][0] < self.buy_target:
						self.OpenPosition(self.buy_target,date,Market_buy=False) #OPEN POSITION! #target_Sell = 1.05
						self.buy_target = None
					else:
						self.buy_target = None
	
		#If trade is opened that setting trade status to open
		for trade in self.trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade)											 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1:] = "IN POSITION" 
				
		#Checking for selling conditions in all open trades
		for trade in openTrades:
			if trade.status == "OPEN":
				#selling condition one below
				if self.rsi[-2:-1] > 75: 
					sell_target = closes[-2:-1][0]*1.025
					if candlestick.high > sell_target:
						self.target_sells += 1
						self.ClosePosition(sell_target,date,candlestick,trade,Market_sell = False)#SELL POSITION!

				#elif (self.mavs[-3:-2]-self.mavs[-4:-3]) > (self.mavs[-2:-1]-self.mavs[-3:-2])*2.5 and self.mavs_delta[-2:-1] > 1:
				#	sell_target = closes[-2:-1][0]*1.025
				#	if candlestick.high > sell_target:
				#		self.target_sells += 1
				#		self.ClosePosition(sell_target,date,candlestick,trade,Market_sell = False)#SELL POSITION!

				#selling condition two below
				elif (self.mavs[-2:-1]-self.mavs[-3:-2]) > (self.mavs[-1:]-self.mavs[-2:-1])*4 and candlestick.close/trade.entryPrice < 1 or (self.mavs[-2:-1]-self.mavs[-3:-2]) > (self.mavs[-1:]-self.mavs[-2:-1])*4 and candlestick.close/trade.entryPrice > 1 or self.mavs_delta[-1:] < 1:
					self.ClosePosition(candlestick.close,date,candlestick,trade,Market_sell = True)#SELL POSITION!
					self.close_sells +=1


	def updateOpenTrades(self,candlestick,date):

		for trade in self.trades:
			if (trade.status == "OPEN"):
				
				trade.tick(candlestick.low,date,candlestick.high) #Tick the position to see if it has hit the stoploss
				if trade.tick(candlestick.low,date,candlestick.high) == True: #check to see if it hit the stop loss and if so, close the position as usual
					if trade.target_reached == True:

						self.BearCommisions[-1:] = float(self.BullTokens[-1:])*0.001	#calculate bear Commision and replace last element in the vector with calculated comission
						self.BearTokens[-1:] = (float(self.BullTokens[-1:])*0.999)*(trade.target_sell)
						self.BullTokens[-1:] = 0
						self.tradeStatus[-1:] = "TARGET HIT"
						trade.close('target price hit',date,self.BearTokens[-1:])
					else:
						self.BearCommisions[-1:] = float(self.BullTokens[-1:])*0.002	#calculate bear Commision and replace last element in the vector with calculated comission
						self.BearTokens[-1:] = (float(self.BullTokens[-1:])*0.998)*(trade.stopLoss) #Calculates the number of purchased Bear coins with commision taken into consideration
						self.BullTokens[-1:] = 0
						self.tradeStatus[-1:] = "STOP LOSSED"
						trade.close('stop loss price',date,self.BearTokens[-1:])
						

	def OpenPosition (self,open_price,date,StopLoss = None,target_Sell = None,Trail = None, Market_buy = True,Market_sell = True,Moving_target = False):
		if self.live == True:

			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			time.sleep(1)
			lastPairPrice = float(currentValues[self.pair]["lowestAsk"])
			asking_price = float(round(lastPairPrice*0.998,8))

			amount  = float(self.BearTokens[-1:])*0.99/asking_price
			#orderNumber = self.conn.buy(self.pair,round(lastPairPrice*1.01,8),amount)
			#print(orderNumber)
			time.sleep(2)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			print(trade_hist)
			time.sleep(2)
			balence = self.conn.returnBalances()
			self.BullTokens[-1:] = balence[self.bull]
			self.BearTokens[-1:] = balence[self.bear]
			print(self.BullTokens)
			print(self.BearTokens)

		else:
			if Market_buy == True:

				self.BullTokens[-1:] = (float(self.BearTokens[-1:])*0.998)/open_price #Calculates the number of purchased Bull coins with commision taken into consideration
				self.BearCommisions[-1:] = float(self.BullTokens[-1:])*0.002
				self.BearTokens[-1:] = 0
			else:
				self.BearCommisions[-1:] = float(self.BullTokens[-1:])*0.001
				self.BullTokens[-1:] = (float(self.BearTokens[-1])*0.999)/open_price
				self.BearTokens[-1:] = 0
				
		self.tradeStatus[-1:] = "OPEN"
		self.trades.append(BotTrade(open_price,date,self.BullTokens[-1:],stopLoss = StopLoss,target_sell = target_Sell,trail = Trail )) # Append a BotTrade Object to our trades list in percentage .... StopLoss = 0.02 is 2%

	def ClosePosition (self,sell_price ,date,candlestick,trade = None,Market_sell = True):
		if self.live == True:
			
			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			lastAskingPrice = float(currentValues[self.pair]["lowestAsk"])
			if Market_sell == True:
				asking_price = float(round(lastAskingPrice*0.99,8))
				amount = float(self.BullTokens[-1:])*0.995
			else:
				asking_price = float(round(lastAskingPrice*1.025,8))
				amount = float(self.BullTokens[-1:])*0.995
			#orderNumber = self.conn.sell(self.pair,round(lastAskingPrice*0.99,8),amount)
			#print(orderNumber)
			time.sleep(2)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			time.sleep(2)
			balence = self.conn.returnBalances()

			self.BullTokens[-1:] = balence[self.bull]
			self.BearTokens[-1:] = balence[self.bear]
			print(self.BullTokens)
			print(self.BearTokens)
			


		else:
			if Market_sell == True:
				self.BearCommisions[-1:] = float(self.BullTokens[-1:])*0.002
				self.BearTokens[-1:] = (float(self.BullTokens[-1:])*0.998)*sell_price #Calculates the number of purchased Bear coins with commision taken into consideration
				self.BullTokens[-1:] = 0
				
			else:
				self.BearTokens[-1:] = (float(self.BullTokens[-1:])*0.998)*sell_price #Calculates the number of purchased Bear coins with commision taken into consideration
				self.BullTokens[-1:] = 0
				self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.001	#calculate bear Commision and replace last element in the vector with calculated comission

		trade.close(sell_price,date,self.BearTokens[-1:])
		self.tradeStatus[-1:] = "CLOSED"
	def live_flick(self):
		self.live = True






