from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class SMA_T(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)
		self.period = period

		self.indicators = BotIndicators(orderbook= True,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime)
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.last_orderbook = pd.DataFrame()
		self.open_value = np.array([])

		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []




		self.dead_date_start = datetime.datetime(2019,3,5,20,40,0)
		self.dead_date_end = datetime.datetime(2019,3,6,8,45,0)
		





		self.numSimulTrades = 3
		self.margin = True
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}

		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		
		#updating Indicators

		# order1 = 1500*self.closes[-1:].item() * 2.5 #volumes
		# order2 = 2500*self.closes[-1:].item() * 2.5
		# order3 = 3500*self.closes[-1:].item() * 2.5
		# order4 = 4000*self.closes[-1:].item() * 2.5
		# order5 = 4500*self.closes[-1:].item() * 2.5

		self.last_orderbook = self.indicators.get_orderbook(self.dates[-1:])
		self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,20000000,40000000,3)
		self.buy_price1,self.buy_price2,self.buy_price3 = self.buy_prices['price']
		self.sell_price1,self.sell_price2,self.sell_price3 = self.sell_prices['price']

		self.open_value = self.indicators.get_open_value(self.dates[-1:])

		
		#self.buy_line1.append(self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < order5)]['price'].min())
		#self.sell_line1.append(self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < order5)]['price'].max())



		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3)]

		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)

		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)

		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)


		#self.buy_price = self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < 20000000)]['price'].min()
		#self.sell_price = self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < 20000000)]['price'].max()
		
		

		#self.buy_price1 = self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < order5)]['price'].min()
		#self.buy_price2 = self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < order2)]['price'].min()
		# self.buy_price3 = self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < order3)]['price'].min()
		# self.buy_price4 = self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < order4)]['price'].min()
		# self.buy_price5 = self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < order5)]['price'].min()



		#self.sell_price1 = self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < order5)]['price'].max()
		#self.sell_price2 = self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < order2)]['price'].max()
		# self.sell_price3 = self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < order3)]['price'].max()
		# self.sell_price4 = self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < order4)]['price'].max()
		# self.sell_price5 = self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < order5)]['price'].max()



		
			
	def evaluate_Open(self,bp = None,sp = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = None
		stoploss = None
		market_order = 'zero'
		#print(date)
		#market_order = True
		# if self.dead_date_start > date.item().replace(tzinfo=None):
		# 	if date.item().replace(tzinfo=None) < self.dead_date_end == False:
				
		


		if (self.dead_date_start < date.item().replace(tzinfo=None) and date.item().replace(tzinfo=None) < self.dead_date_end) == False:
			if len(self.dates) > 2:
				if self.highs[-1:] > sp:
					action = True
					Long = False
					price = sp
					stoploss = price*1.03
					
							
				if self.lows[-1:] < bp:
					
					action = True
					Long = True
					price = bp
					stoploss = price/1.03
				
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		
		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		market_order = 'zero'
		#market_order = True
		
		#print(trade.trade_length)
		#print(date)
		if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:
			action = True
			price = avg_open*1.02
					
		if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
			action = True
			price = avg_open/1.02
		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.buy_line1,self.sell_line1,self.open_value,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3)


