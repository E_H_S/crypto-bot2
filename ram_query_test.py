
import sqlite3 as sql 
import pandas as pd
import multiprocessing
import datetime
import numpy as np


print("QUERY TIME")
print('\n')
st = datetime.datetime(2019, 6,6,0,0,0) - datetime.timedelta(hours = 10)
et  = datetime.datetime(2019, 6,7,0,0,0) - datetime.timedelta(hours = 10)
start_UTC = st.strftime("%Y-%m-%d %H:%M:%S.%f")
end_UTC = et.strftime("%Y-%m-%d %H:%M:%S.%f")


t5 = datetime.datetime.utcnow()
# with con:
# 	cur = con.cursor()
# 	cur.execute("""
# 	SELECT DISTINCT
# 	*
# 	FROM {}
# 	WHERE 1=1
# 	and time_stamp > ?
# 	and time_stamp < ?
# 	ORDER BY time_stamp,price ASC
# 	""".format('XBT_orderbook_RAM'),(start_UTC,end_UTC)) #giving 24 hours of past data to begin with

con = sql.connect(':memory:')
with con:
	cur = con.cursor()
	cur.execute("""
	SELECT
	*
	FROM XBT_orderbook_RAM
	WHERE 1=1
 	and time_stamp > ?
 	and time_stamp < ? 
	ORDER BY time_stamp,price ASC
 	""".format('XBT_orderbook_RAM'),(start_UTC,end_UTC))
	
	data = cur.fetchall()
	print(len(data))

t6 = datetime.datetime.utcnow()
print('Total ram query seconds '+str((t6-t5).total_seconds()))