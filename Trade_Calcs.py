from poloniex import poloniex
from bottrade import BotTrade
import numpy as np





class Trade_Calcs(object):
	def __init__(self,pair):
		self.dummy = 0
		self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')
		self.live = False
		self.pair = pair
		self.bear,self.bull = self.pair.split("_")


	def open(self,open_price,date,Long,pair,margin,BullTokens,BearTokens,BullCommisions,BearCommisions,StopLoss = None,target_Sell = None,Trail = None, Market_buy = True,Moving_target = False,amount = None,total = None):
		# print('before open calcs')
		# print(BullTokens[-1:],BearTokens[-1:],BullCommisions[-1:],BearCommisions[-1:])
		
		if self.live == True:

			currentValues = self.conn.api_query("returnTicker") #Get latest asking price
			lastPairPrice = float(currentValues[self.pair]["lowestAsk"])
			asking_price = float(round(lastPairPrice*0.998,8)) # limit order well below the lowest ask to enture a market buy


			if amount != None:
				if Long == True:
					amount  = float(BearTokens[-1])*0.99/asking_price #If amount is not given assume it is the full amount given.
				elif Long == False:
					amount  = float(BullTokens[-1])*0.99/asking_price #If amount is not given assume it is the full amount given.
			
			if Long == True:
				if margin == True:
					pass
					#orderNumber = self.conn.marginBuy(self.pair,round(asking_price,8),amount)
				elif margin == False:
					pass
					#orderNumber = self.conn.sell(pair,round(asking_price,2),amount)
			elif Long == False:
				if margin == True:
					pass
					#orderNumber = self.conn.marginSell(self.pair,round(asking_price,8),amount)
				elif margin == False:
					pass
					#orderNumber = self.conn.buy(pair,round(asking_price,2),amount)
						
			print(orderNumber)
			
			trade_hist = self.conn.returnTradeHistory(self.pair)
			print(trade_hist)
			
			balence = self.conn.returnBalances()
			BullTokens[-1] = balence[self.bull]
			BearTokens[-1] = balence[self.bear]
			print(BullTokens[-5])
			print(BearTokens[-5])

		else:

			if Market_buy == True:
				commision = 0.00055
				#spread = 15/11500
				spread = 0
				com_spread = commision + spread
			elif Market_buy == False:
				commision = -0.00025
				com_spread = commision
			elif Market_buy == 'zero':
				commision = 0
				com_spread = commision

			if amount == None and total == None: #allows for amount to be given. If it is not, then amount is assumed to be the entire acount.
				amount = BullTokens[-1]
				total = BearTokens[-1]
			elif total != None:
				amount = total/open_price
			elif amount != None:
				total = amount*open_price

			# print("amount","total")
			# print(amount,total)
				

			if Long == True:
				#trade_total = total*(1-com_spread)
				#trade_amount = (float(amount)*(1-com_spread))/open_price 
				
				BullCommisions[-1] = float(total/open_price)*com_spread
				BullTokens[-1] = BullTokens[-1] + (float(total)*(1-com_spread))/open_price
				BearTokens[-1] = BearTokens[-1] - total
				
				# print("amount","total")
				# print(amount,total)
				# print(total,open_price,com_spread)
			elif Long == False:
				#print(type(BullTokens[-1:]),type((float(amount)*(1-com_spread))),type(open_price))
				BearCommisions[-1] = float(total)*com_spread
				BullTokens[-1] = BullTokens[-1] - float(total)/open_price
				BearTokens[-1] = BearTokens[-1] + float(total)*(1-com_spread)

				
			

		trade_total = amount - com_spread #Bearvolume
		trade_amount = amount - com_spread #BullVolume


		Trade = BotTrade(open_price,date,stopLoss = StopLoss,target_sell = target_Sell,trail = Trail,Long = Long,total = total)
		# print('after open calcs')
		# print(Trade,BullTokens[-1:],BearTokens[-1:],BullCommisions[-1:],BearCommisions[-1:])
		return Trade,BullTokens,BearTokens,BullCommisions,BearCommisions
	
	def close(self,close_price,date,trade,margin,BullTokens,BearTokens,BullCommisions,BearCommisions,Market_sell = True,amount = None,total = None):
		# print('before close calcs')
		# print(trade,BullTokens[-1:],BearTokens[-1:],BullCommisions[-1:],BearCommisions[-1:])
		
		if self.live == True:

			currentValues = self.conn.api_query("returnTicker") #Get latest asking price
			lowestAsk = float(currentValues[self.pair]["lowestAsk"])
			highestBid = float(currentValues[self.pair]["highestBid"])
			balence = self.conn.returnBalances()
			BullTokens = np.append(BullTokens,balence[self.bull])
			BearTokens = np.append(BearTokens,balence[self.bear])
			trade_hist = self.conn.returnTradeHistory(self.pair)
			last_trade = trade_hist[0]

			if margin == True:
				pass
				#orderNumber = self.conn.closeMarginPosition(self.pair)
			elif margin == False:
				pass
				#orderNumber = self.conn.buy(pair,round(asking_price,2),amount)

			trade.close(asking_price,date,BearTokens[-1])
			print(orderNumber)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			
			print(trade_hist[0])
			total_sum = 0
			for trades in trade_hist:
				if trades == last_trade:
					break
				else:
					total_sum += float(trades['total'])
			if trade.long == True:
				self.USDT_ForPair[-1] += total_sum
			elif trade.long == False:
				self.USDT_ForPair[-1] += total_sum + (trade.entryVolume - total_sum)
		else:


			if Market_sell == True:
				commision = 0.00055
				#spread = 15/11500
				spread = 0
				com_spread = commision + spread
			elif Market_sell == False:
				commision = -0.00025
				com_spread = commision
			elif Market_sell == 'zero':
				commision = 0
				com_spread = commision



			if amount == None and total == None and trade.long == True: #allows for amount to be given. If it is not, then amount is assumed to be the entire acount.
				total = BearTokens[-1]
				amount = BullTokens[-1]
			elif amount == None and total == None and trade.long == False:
				total = BearTokens[-1]
				amount = BullTokens[-1]/(1-com_spread)
			elif total != None:
				amount = total/close_price
			elif amount != None:
				total = amount*close_price

				# print("amount","total")
				# print(amount,total)




			if trade.long == True:
				BullCommisions[-1] = float(total)*com_spread
				BearTokens[-1] = BearTokens[-1] + amount*close_price*(1-com_spread)
				BullTokens[-1] = BullTokens[-1] - float(amount)
				
			elif trade.long == False:
				BearCommisions[-1] = float(amount)*com_spread

				BearTokens[-1] = BearTokens[-1] - (float(abs(amount))*close_price)

				BullTokens[-1] = BullTokens[-1] + (float(abs(amount))*(1-com_spread))
				

				

			
		trade.close(close_price,date)

		# print('after close calcs')
		# print(trade,BullTokens[-1:],BearTokens[-1:],BullCommisions[-1:],BearCommisions[-1:])
	
		return trade,BullTokens,BearTokens,BullCommisions,BearCommisions


