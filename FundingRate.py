from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class FR(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		
		self.period = period
		self.limits = False

		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = True,startTime = startTime ,endTime = endTime)
		
		#Inititalize inticator vectors
		self.upper_band = []
		self.lower_band = []
		self.rates = []
			
		self.numSimulTrades = 1
		self.margin = True

		
	def tick_price_info(self,price_info,trades):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}

		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		self.funding_rate = self.indicators.get_funding_rate(self.dates[-1:])

		#print(self.funding_rate['fundingRate'][-1:])


		if len(self.funding_rate)> 0:
			self.rates.append(self.funding_rate.iloc[-1,0])
			self.upper_band.append(self.funding_rate.iloc[-1,6])
			self.lower_band.append(self.funding_rate.iloc[-1,7])





		#print(self.funding_rate)
		# print('last canlde timestamp ' + str(self.dates[-1:]))
		# print(self.funding_rate)
	
		
			
	def evaluate_Open(self,bp = None,sp = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = None
		stoploss = None
		market_order = 'zero'
		#print(date)
		#market_order = True
		# if self.dead_date_start > date.item().replace(tzinfo=None):
		# 	if date.item().replace(tzinfo=None) < self.dead_date_end == False:
				
		# 6-14-22.   so if x + 2
		# print('\n')
		# print(self.dates[-1:].item().minute)
		# print(type(self.dates[-1:].item()))
		# print('\n')
		if self.dates[-1:].item().minute == 0 and  np.mod(self.dates[-1:].item().hour+2,8) == 0:
			if len(self.funding_rate['fundingRate'][-1:]) > 0 :
				#print("NEW RATE",self.funding_rate['fundingRate'][-1:].item())
				# if 0.001 > self.funding_rate['fundingRate'][-1:].item() > 0.0005:
				# 	action = True
				# 	Long = False
				# 	price = self.closes[-1:]
				# 	#stoploss = price*1.06
					
							
				if  0.003 < self.funding_rate['fundingRate'][-1:].item() < 0.005:
					action = True
					Long = True
					price = self.closes[-1:]
					#stoploss = price/1.06
		
		# if action == True:
		# 	print('OPENED POSITION')
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		
		if trades != None and self.numSimulTrades > 1:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		market_order = 'zero'
		#market_order = True
		
		#print(trade.trade_length)
		#print(date)


		if trade.trade_length == int(2):
			action = True
			price = self.closes[-1:]
					
		if trade.trade_length == int(2):
			action = True
			price = self.closes[-1:]
		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):

		return self.rates,self.upper_band,self.lower_band




