
import pandas as pd


class BotTrade(object):
	def __init__(self,currentPrice,date,stopLoss=None,target_sell = None,trail = None,status = None,Long = True,target2 = None,live_stop = None,margin = True,total = None):
		self.status = "OPEN"
		if status != None:
			self.status = status

		self.total_fiat = total

		self.entryPrice = currentPrice
		self.exitPrice = None
		#self.entryVolume = entryVolume
		self.exitVolume = None
		self.entryDate = date 
		self.exitDate = None
		self.stoplossed = False
		self.trade_length = 1
		self.maxPrice = self.entryPrice
		self.trail = trail
		self.target_reached = None
		self.target_sell = target_sell
		self.long = Long
		self.target2 = target2
		self.live_stop = live_stop
		self.margin = margin
		self.orderbook_depth = None
		self.take_profit_percentage = None

		self.accumulation1 = None
		self.accumulation2 = None
		self.accumulation3 = None

		self.liquidity = None


		self.sharp_ratio = None


		self.stopLoss = stopLoss
		self.momentum = None
		#short points of intrest
		self.lowest_high = None #tuples wth date and price
		#self.failed_high = None	#tuples wth date and price
		self.lowest_low = None	#tuples wth date and price
		self.failed_low = None	#tuples wth date and price

		#long points of intrest
		self.highest_high = None
		self.failed_high = None
		self.highest_low = None
		self.trigger_date = None

		self.entry_ATR = None

		self.time_on_side = 0
		self.time_off_side = 0

		self.returns = []

		self.mins = []
		self.maxes = []

		#if stopLoss != None:
		#	self.stopLoss = currentPrice/ (stopLoss+1)
			
	
	def close(self,closing_price,date):
		if self.status in ("IN POSITION","OPEN"):
			self.status = "CLOSED"		#closing the final status on a trade with either closed for a good exit or stop lossed for a bad exit
		
		

		self.exitPrice = closing_price
		self.exitDate = date
		if self.long == True and self.status == 'CLOSED':
			self.trade_profit = (self.exitPrice - self.entryPrice)/self.entryPrice + 0.0005
		elif self.long == True and self.status == 'STOP LOSSED':
			self.trade_profit = (self.exitPrice - self.entryPrice)/self.entryPrice - 0.0005
		elif self.long == False and self.status == 'CLOSED':
			self.trade_profit = (self.entryPrice - self.exitPrice)/self.entryPrice + 0.0005
		elif self.long == False and self.status == 'STOP LOSSED':
			self.trade_profit = (self.entryPrice - self.exitPrice)/self.entryPrice - 0.0005


		if len(self.returns) > 2:
			self.returns = pd.Series(self.returns)
			self.adjusted_returns = self.returns.diff()/self.returns
			self.std = self.adjusted_returns.std()
			self.sharp_ratio = self.trade_profit/self.std
		else:
			if self.trade_profit > 0:
				self.sharp_ratio = 5
			else:
				self.sharp_ratio = -10


	

		#self.exitVolume = exitVolume
		

	def tick(self,price_info,new_target = None):
		#call this function on open trades. This function should return a boolean to determine if the trade closed itself, and if so the price and date at which it closed.
		


		self.returns.append(price_info['closes'][-1])

		self.candlestickLow = price_info['lows'][-1]
		self.candlestickHigh = price_info['highs'][-1]
		self.date = price_info['dates'][-1:]

		if self.highest_high == None:
			self.highest_high = price_info['highs'][-1]
		elif price_info['highs'][-1] > self.highest_high:
			self.highest_high = price_info['highs'][-1]

		if self.lowest_low == None:
			self.lowest_low = price_info['lows'][-1]
		elif price_info['lows'][-1] < self.lowest_low:
			self.lowest_low = price_info['lows'][-1]

		if self.long == True:
			if self.entryPrice < price_info['closes'][-1]:
				self.time_on_side += 1
			elif self.entryPrice > price_info['closes'][-1]:
				self.time_off_side += 1
		elif self.long == False:
			if self.entryPrice > price_info['closes'][-1]:
				self.time_on_side += 1
			elif self.entryPrice < price_info['closes'][-1]:
				self.time_off_side += 1








		self.trade_length += 1
		closed = False
		price = None
		date = self.date

		if self.target_sell != None:
			closed,price = self.target_sell()

		if self.trail != None:
			trailing_stop()
			

		if self.stopLoss != None:
			closed,price = self.stop_loss()

		return closed,price,date,self.margin
	def stop_loss(self):

		if self.trade_length >2:
			if self.long == True and (self.candlestickLow < self.stopLoss) or self.long == False and (self.candlestickHigh > self.stopLoss):
				self.stoplossed = True
				self.status = "STOP LOSSED"
				self.exitPrice = self.stopLoss
				self.exitDate = self.date
				return True , self.exitPrice

		return False , self.exitPrice
	def trailing_stop(self):

		if self.candlestickHigh > self.maxPrice:
			self.count = 0
			self.maxPrice = self.candlestickHigh

			if self.count > 2:
				self.stopLoss = self.maxPrice/(self.trail +1)
			self.count +=1
	
	def target_sell():
		if new_target != None:
			self.target_sell = new_target
			if (self.candlestickHigh > self.target_sell):
				self.target_reached = True
				self.status = "TARGET HIT"
				self.exitPrice = self.target_sell
				return True,self.exitPrice

