import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt


data = pd.read_csv("USDT_BTC_1800_(2018-05-01)---(2018-08-01).csv")
print(data.head())

current_price = data['averageprice'][-490:-489:]
current_date = data[-490:-489:]
print(current_date)
print("current_price. " +str(current_price))
cr = list(current_price)*10


moving_average_vector = [] #last_value of moving average per index eg [1mv 2mv 3mv 4mv 5mv....]
z = np.linspace(1,10,10)
print(z)
for x in range(1,11):
	mv = data['averageprice'][(-489-x):-489:]
	print(mv,current_price)
	mv = mv.mean()
	moving_average_vector.append(mv)

plt.plot(z,moving_average_vector)
plt.plot(z,cr)
plt.show()