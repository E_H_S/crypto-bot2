import matplotlib.pyplot as plt 
import numpy as np 
import datetime as dt
from matplotlib import style
from mpl_finance import candlestick_ohlc 
import pandas as pd 
import matplotlib.dates as mdates
import time



#import seaborn as sns


class BotVisual(object):
	def __init__(self,live = False,chart_style = 'candlestick'): #Takes an input of a strategy log 

		style.use('seaborn')
		self.font_dict1 = {'family':'serif','color':"blue","size":5}
		self.font_dict2 = {'family':'serif','color':"green","size":5}
		self.font_dict3 = {'family':'serif','color':"red","size":5}
		self.chartstyle = chart_style
		if live == True:
			self.ax1 = plt.subplot2grid((10,4),(0,0),rowspan=10,colspan=4)
		else: 
			self.ax1 = plt.subplot2grid((10,4),(0,0),rowspan=8,colspan=3)
		#self.fig1 = plt.figure()
		#self.ax1 = self.fig1.add_subplot(1,1,1)
		#self.f2, self.ax1 = plt.subplots(figsize = (10,5))
		
		
	
	def draw(self,StrategyLog,draw_pause = None):
		self.prices = StrategyLog.AveragePrices
		self.pair = StrategyLog.pair
		self.period = StrategyLog.period

		self.openprices = StrategyLog.openprices
		self.closeprices = StrategyLog.closeprices
		self.highs =  StrategyLog.highs
		self.lows = StrategyLog.lows
		self.volumes = StrategyLog.volumes
		self.live = StrategyLog.botcontroller.live

		# self.indicators = StrategyLog.botcontroller.strategy.Draw_indicators()

		# self.indicator1 = [None]*len(self.openprices)
		# self.indicator2 = self.indicator1
		# self.indicator3 = self.indicator1

		# if len(self.indicators) == 1:
		# 	print("check19")
		# 	self.indicator1 = self.indicators
		# elif len(self.indicators)== 2:
		# 	self.indicator1,self.indicator2 = self.indicators
		# elif len(self.indicators)== 3:
		# 	self.indicator1,self.indicator2,self.indicator3 = self.indicators



		self.datetimes = StrategyLog.dates

		self.ohlc = pd.DataFrame({"Date":self.datetimes,"Open":self.openprices,"High":self.highs,"Low":self.lows,"Close":self.closeprices})
		self.ohlc = self.ohlc[['Date',"Open","High","Low","Close"]]


		
		if (draw_pause > 0):
			style.use('seaborn')
			plt.ion()
			plt.show()
			plt.ylabel(self.pair)
			plt.xlabel('Datetime')
			plt.xticks(rotation=60)
			plt.axis('tight')

			
			
			self.ax1.plot(self.datetimes,self.prices,color = 'red',marker = '*')
			self.ax1.scatter(self.datetimes,self.openprices,color = 'blue',marker = '_')
			self.ax1.scatter(self.datetimes,self.closeprices,color = 'red',marker = '_')
			self.ax1.scatter(self.datetimes,self.highs,color = 'green',marker = '_')
			self.ax1.scatter(self.datetimes,self.lows,color = 'black',marker = '_')
			self.ax1.plot(self.datetimes, self.mavs ,'b--',marker = '.')
			self.ax1.plot(self.datetimes, self.mavs2 ,'y--',marker = '.')
			self.ax1.plot(self.datetimes, self.Emavs3 ,'k--',marker = '.')
			candlestick_ohlc(self.ax1,self.ohlc.values ,width = self.period*0.8,colorup = 'green',colordown='red',alpha = 0.5)
			
			plt.draw()
			plt.pause(draw_pause)



		#if StrategyLog.strategy.tradeStatus[-1:] in ("OPEN","CLOSED","STOP LOSSED","TARGET HIT"):

			#self.tradeMarker(StrategyLog.strategy.trades[-1:])
			#self.tradeMarker(StrategyLog.strategy.trades)


	def buildChart(self,StrategyLog):

		
		self.ohlc = pd.DataFrame({"Date":self.datetimes,"Open":self.openprices,"High":self.highs,"Low":self.lows,"Close":self.closeprices})
		self.ohlc = self.ohlc[['Date',"Open","High","Low","Close"]]

		self.indicators = StrategyLog.botcontroller.strategy.Draw_indicators()

		self.indicator1 = [None]*len(self.openprices)
		self.indicator2 = self.indicator1
		self.indicator3 = self.indicator1

		if self.indicators != None:
			if len(self.indicators) == len(self.datetimes):
				print("check19")
				self.indicator1 = self.indicators
			elif len(self.indicators)== 2:
				self.indicator1,self.indicator2 = self.indicators
			elif len(self.indicators)== 3:
				self.indicator1,self.indicator2,self.indicator3 = self.indicators
			elif len(self.indicators)== 4:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4 = self.indicators
			#self.datetimes = self.dateconv(StrategyLog.dates)
			self.pair = StrategyLog.pair
		
	
		
		style.use('seaborn')
		plt.xticks(rotation=60)
		#self.ax1.plot(self.datetimes,self.prices,color = 'red',marker = '*')
		self.ax1.scatter(self.datetimes,self.openprices,color = 'blue',marker = '_')
		self.ax1.scatter(self.datetimes,self.closeprices,color = 'red',marker = '_')
		self.ax1.scatter(self.datetimes,self.highs,color = 'green',marker = '_')
		self.ax1.scatter(self.datetimes,self.lows,color = 'black',marker = '_')

	
		
		if self.chartstyle == 'renko':
			candlestick_ohlc(self.ax1,self.ohlc.values ,width = 300,colorup = 'green',colordown='red',alpha = 0.5)
		else:
			self.ax1.plot(self.datetimes, self.indicator1 ,'b--',marker = '.')
			self.ax1.plot(self.datetimes, self.indicator2 ,'y--',marker = '.')
			self.ax1.plot(self.datetimes, self.indicator3 ,'k--',marker = '.')
			#self.ax1.plot(self.datetimes, self.indicator4 ,'k--',marker = '.')
			candlestick_ohlc(self.ax1,self.ohlc.values ,width = self.period*0.8,colorup = 'green',colordown='red',alpha = 0.5)
		

		#self.ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d-%H'))
		#self.tradeMarker(StrategyLog.botcontroller.trades)
		#print(self.indicator1)

		for x in range(len(self.datetimes)):
			date = self.datetimes[x]
			price = (self.closeprices[x]+self.openprices[x])/2
			#text =  str(self.indicator1[x])
			#self.ax1.text(date,price,text)


		self.tradeMarker(StrategyLog.botcontroller.trades)
		plt.show()
		
		#self.ax1.plot(self.datetimes,self.mavs2,'g--',marker = '.')
		plt.ylabel(str(self.pair)+ " : Price ")
		plt.xlabel('Datetime')

		
		
		
	def tradeMarker(self,trade):
		#for Trade in trade[-1:]: #converting from an list of trade objects to singular trade object
		for Trade in trade:

			#if Trade.status == "OPEN":
				#trade_enter_date = time.mktime(Trade.entryDate.timetuple())
		#	if Trade.status in ("CLOSED","STOP LOSSED","TARGET HIT"):
				#print(Trade.exitDate)
				#print(type(Trade.exitDate))
				#trade_exit_date = time.mktime(Trade.exitDate.timetuple())
				#trade_enter_date = time.mktime(Trade.entryDate.timetuple())

			#self.Highs_n_Lows(Trade)


			if Trade.long == True:
				marker = '^'
			elif Trade.long == False:
				marker = 'v'
			if Trade.status == "CLOSED":
				colour = 'magenta'
			elif Trade.status == "STOP LOSSED":
				colour = "black"
			elif Trade.status == "TARGIT HIT":
				colour = "yellow"

			if Trade.exitDate != None and Trade.exitPrice != None:
				self.ax1.plot(Trade.exitDate,Trade.exitPrice,color = colour ,marker = 'o',alpha = 1,markersize=10)
			self.ax1.plot(Trade.entryDate,Trade.entryPrice,color = 'cyan' ,marker = marker,alpha = 1,markersize=10)



			#self.Highs_n_Lows(Trade)

	def Highs_n_Lows(self,trade):

		# print("check19",trade.mins,trade.maxes)
		# for minp,mind in trade.mins:
		# 	#mind = time.mktime(mind.timetuple())
		# 	print(minp,mind)
		# 	self.ax1.plot(mind,minp,color = 'magenta' ,marker = 'v',alpha = 0.7,markersize=12)
		# for maxp,maxd in trade.maxes:
		# 	#maxd = time.mktime(mind.timetuple())
		# 	print(maxp,maxd)
		# 	self.ax1.plot(maxd,maxp,color = 'magenta' ,marker = '^',alpha = 0.7,markersize=12)


		# print('short points of interest')
		# print('lowest low', trade.lowest_low)
		# print('failed low', trade.failed_low)
		# print('lowest high', trade.lowest_high)
		# print('Long points of interest')
		# print('highest_high', trade.highest_high)
		# print('failed_high', trade.failed_high)
		# print('highest_low', trade.highest_low)

		if trade.long == True:
			if trade.highest_high != None:
		 		h1_p,h1_d = trade.highest_high
		 		self.ax1.plot(h1_d,h1_p,color = 'magenta' ,marker = '^',alpha = 1,markersize=12)
			if trade.failed_high != None:
				h1_p,h1_d = trade.failed_high
				self.ax1.plot(h1_d,h1_p,color = 'yellow' ,marker = '^',alpha = 1,markersize=12)
			if trade.highest_low != None:
				h1_p,h1_d = trade.highest_low
				self.ax1.plot(h1_d,h1_p,color = 'yellow' ,marker = 'v',alpha = 1,markersize=12)
		
		elif trade.long == False:
			if trade.lowest_low != None:
				l1_p,l1_d = trade.lowest_low
				self.ax1.plot(l1_d,l1_p,color = 'magenta' ,marker = 'v',alpha = 1,markersize=12)
			if trade.failed_low != None:
				l1_p,l1_d = trade.failed_low
				self.ax1.plot(l1_d,l1_p,color = 'yellow' ,marker = 'v',alpha = 1,markersize=12)
			if trade.lowest_high != None:
				l1_p,l1_d = trade.lowest_high
				self.ax1.plot(l1_d,l1_p,color = 'yellow' ,marker = '^',alpha = 1,markersize=12)









		# # if trade.failed_high != None:
		# # 	h2_p,h2_d = trade.failed_high
		# # 	self.ax1.plot(h2_d,h2_p,color = 'yellow' ,marker = '^',alpha = 1,markersize=12)
		# if trade.Lowest_low != None:
		#  	l1_p,l1_d = trade.Lowest_low
		#  	self.ax1.plot(l1_d,l1_p,color = 'magenta' ,marker = 'v',alpha = 1,markersize=12)
		# if trade.failed_low != None:
		#  	l2_p,l2_d = trade.failed_low
		#  	self.ax1.plot(l2_d,l2_p,color = 'yellow' ,marker = 'v',alpha = 1,markersize=12)
		
		


	
		
		
		





	#def Volume_profile(self,StrategyLog):
	#	plt.figure(7)
	#	plt.bar(self.)


