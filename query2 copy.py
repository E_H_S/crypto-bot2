import sqlite3 as sql
import pandas as pd
import datetime
import plotly.offline as pyo
import plotly.graph_objs as go
import numpy as np



con = sql.connect("Database1_copy1 copy")
cur = con.cursor()

st = datetime.datetime(2019, 3, 1,0,0,0)
et  = datetime.datetime(2019, 3,1,1,0,0)

# st = datetime.datetime.utcnow() - datetime.timedelta(minutes =10)
# et = datetime.datetime.utcnow()

tabletype = 'orderbook'

if tabletype == 'instrument':
	start_UTC = st.strftime("%Y-%m-%dT%H:%M:%S.%fZ") 
	end_UTC = et.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

else:
    start_UTC = st
    end_UTC = et
	#start_UTC = st.strftime("%Y-%m-%d %H:%M:%S.%f") 
	#end_UTC = et.strftime("%Y-%m-%d %H:%M:%S.%f")

print(datetime.datetime.utcnow())

with con:
    cur.execute("""

        SELECT DISTINCT
        *
        FROM candles1m_XBT
        WHERE 1=1
        and time_stamp > ?
        and time_stamp < ?
        ORDER BY time_stamp ASC
        
        """,(start_UTC , end_UTC))

    data = cur.fetchall()

data = pd.DataFrame(data)
print(data)

# data.columns = ['date','open','close','high','low','volume','time_stamp']

# print(data)

# traces = [go.Candlestick(
#                     x = data['time_stamp'],
#                     open = data['open'],
#                     high = data['high'],
#                     low = data['low'],
#                     close = data['close'])]

# print("check")
# layout = go.Layout(title = 'BTC vs Index')

        

        
# fig = go.Figure(data=traces,layout=layout)
# pyo.plot(fig, filename='line3.html')

