from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

#Strategy_on UTC TIMES  [(1.5-2.5),(7.5-11),(17.5 - 19.5)]
		#sydney times.  [(11.5-12.5),(17.5-21),(3.5-5.5)]



		#low volitility [Monday]
		#medium volility[Thursday, Saturday,Sunday]
		#Tuesday-Friday tighten Saturday


# SMA_M uses 3 indicators and will require an imput perameter for each


class P_Spreading_1m(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False,anchored = False,open_value_candles = True,momentum = True,momentum_time_period = 1)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		

		self.ATR = []
		self.ATR_14 = []
		self.stoplosses = []
		self.max_risks = []

		self.ob_index = None

		self.OI_FILTER = []
		self.VPOC = []
		self.max_risk = 0.08

		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []


		
		self.Leverage = 2
		self.mid_line = [None,None]

		if p1 != None:
			self.drop_percent = p1
		else:
			self.drop_percent = 0.032

		if p2 != None:
			self.stoploss = p2
		else:
			self.stoploss = 0.02

		if p3 != None:
			self.clip_size = p3
		else:
			self.clip_size = 1500


		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None

		self.leverages = []

		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.volitilitys =[]
		self.account_size = []
		self.latest_range = []

		self.open_value = np.array([])

		self.middle = None
		self.last_trade_exit_price = 0

		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True
		self.rolling_period = int(60*2)
		self.Num_avg_candles = int(4*5)
		self.momentum_scaler_p = 1


		self.b_1_ready = True
		self.b_2_ready = True
		self.b_3_ready = True

		self.five_m_dates = []

		self.s_1_ready = True
		self.s_2_ready = True
		self.s_3_ready = True

		self.strategy_start = None
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		

		self.ATR_switch = False
		
		self.ATR_min_list = []
		self.ATR_switches = []

		self.numSimulTrades = 3
		self.margin = True

		self.closing_line = []
		self.volume_profile = None

		self.OI_density = []
		self.trigger_date = None
		self.momentum = None
		self.candle_count3 = None
		self.last_range_length = None
		self.avg_length = None
		self.stoploss_count2 = None

		self.time_from_2hour_high = 0
		self.time_from_2hour_low = 0

		self.start_buy_quotes = True
		self.start_sell_quotes = True


		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0
		self.timer4 = 0
		self.timer5 = 0
		self.timer6 = 0
		self.timer7 = 0
		self.timer8 = 0

		self.last_stoplossed_trade = None
		self.second_last_range_length = None
		


		self.count10 = 0
		self.switch10 = False
		self.count50 = None
		self.count60 = None

		self.candle_count = 0
		self.candle_count2 = 0
		self.rolling_candles_counter = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):
		

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.t1 = datetime.datetime.utcnow()

		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		self.t2 = datetime.datetime.utcnow()

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])

		self.tick_indicators(trades)


	

	def tick_indicators(self,trades):
		#updating Indicators
		#print(self.strategy_start,self.dates[-1:])
		self.candle_count +=1
		self.candle_count2 += 1
		self.rolling_candles_counter += 1
		if self.trigger_date is not None:
			self.trigger_date += 1


		if self.candle_count == 1:
			self.two_hour_high = self.highs[-1]
			self.two_hour_low = self.lows[-1]

		if max(self.highs[-24:]) != self.two_hour_high:
			self.two_hour_high = max(self.highs[-24:])
			self.time_from_2hour_high = 0

		if min(self.lows[-24:]) != self.two_hour_low:
			self.two_hour_low = min(self.lows[-24:])
			self.time_from_2hour_low = 0



		self.time_from_2hour_high += 1
		self.time_from_2hour_low += 1


		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
		#if self.count10 > 12*48:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		

		self.t3 = datetime.datetime.utcnow()

		# if len(self.dates)>1:
		# 	self.open_value = self.indicators.get_open_value(self.dates[-2:-1])
		self.ATR.append(self.indicators.avg_range(self.closes,self.highs,self.lows,int(60*24)))
		#self.ATR_14.append(self.indicators.avg_range(self.closes,self.highs,self.lows,288*7))
		if self.candle_count > 1*3:
			self.momentum = (self.indicators.get_momentum(self.dates[-3])/6)*self.momentum_scaler_p
			# print(self.dates[-1])
			# print(self.momentum[-1:])
			# print('\n')
			# time.sleep(5)
			momentum_tweak = -(self.momentum[-1:].item())
		else:
			momentum_tweak = 0
	
		utc_time = self.dates[-1:][0] - datetime.timedelta(hours = 11)
		day = utc_time.weekday()
		hour = utc_time.hour

		if utc_time.minute >= 30:
			minutes = 0.5
		else:
			minutes = 0

		week_num = day*24 + hour + minutes
		#print(week_num)

		self.t4 = datetime.datetime.utcnow()

		#if 1 < week_num < 12 or 24.5 < week_num < 28 or 49 < week_num < 60 or 73 < week_num < 87.5 or 89 < week_num < 93 or 94 < week_num < 98.5 or 99.5 < week_num < 104.5 or 113 < week_num < 140.5 or 144.5 < week_num < 154.5:
		if 0 <= week_num <1.5 or 11.5 < week_num <49.5 or 66.5 < week_num <73.5 or 87 < week_num < 100 or 107 < week_num < 113.5 or 154 < week_num: 
			self.volitility = 'high'
		else:
			self.volitility = 'low'


		
		# if momentum_tweak > 0.015:
		# 	momentum_tweak = 0.015
		# if momentum_tweak < -0.015:
		# 	momentum_tweak = -0.015

		bpc1 = 0.75
		bpc2 = 0.85
		bpc3 = 1

		spc1 = 0.25
		spc2 = 0.15
		spc3 = 0 
			

		self.t5 = datetime.datetime.utcnow()

		if len(self.ATR)> 1:
			if self.ATR[-2] is not None:
				if abs(self.closes[-2] - self.closes[-60])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-60] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'

				elif abs(self.closes[-2] - self.closes[-55])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-55] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-50])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-50] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-45])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-45] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-40])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-40] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-35])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-35] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-30])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-30] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-25])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-25] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-15])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-15] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				elif abs(self.closes[-2] - self.closes[-10])/self.closes[-2] > self.ATR[-2]*self.Num_avg_candles and self.count50 is None:
					self.new_anchor = True
					if self.closes[-2] - self.closes[-10] > 0:
						self.trigger_direction = 'up'
					else:
						self.trigger_direction = 'down'
				else:
					self.new_anchor = False

		

		self.open_trades_check = True
		if len(trades)> 0:
			if trades[-1].status == 'OPEN':
				self.open_trades_check = False



		#if len(self.closes) > 26:
		if len(self.ATR)> 1:
			if self.ATR[-2] is not None:# and self.open_trades_check == True:# and self.ATR[-1] < 0.003:
				#if abs(self.closes[-1] - self.closes[-24])/self.closes[-1] > 0.03:
				if self.new_anchor == True and self.count50 is None and self.trigger_direction == 'up' or self.new_anchor == True and self.count60 is None and self.trigger_direction == 'down':
					self.ATR_switch = True
					if self.last_range_length is not None:
						self.second_last_range_length = int(self.last_range_length)
					self.last_range_length = int(self.candle_count2)


					if self.last_range_length is not None and self.second_last_range_length is not None:
						self.avg_length = int(((self.last_range_length +self.second_last_range_length)/2)*0.7)
						print('AVG LENGTH FOR RANGE')
						print(self.avg_length)

					self.strategy_start = self.dates[-2]


					print(self.dates[-2])
					print("NEW ANCHOR")
					self.stoploss_count = 0
					self.rolling_candles_counter = 2
					self.candle_count2 = 0
					if self.trigger_direction == 'up':
						self.start_sell_quotes = False
						self.count50 = 0
						self.count60 = 0
					elif self.trigger_direction == 'down':
						self.start_buy_quotes = False
						self.count60 = 0
						self.count50 = 0


		if self.count50 is not None:
			self.count50 += 1
			if self.count50 == int(13*5):
				self.count50 = None

		if self.count60 is not None:
			self.count60 += 1
			if self.count60 == int(13*5):
				self.count60 = None


		if self.candle_count > 20:
			#print(len(self.open_value['max_delta_OI0'][-1:]))
			if len(self.open_value['max_delta_OI0'][-1:]) > 0:
				biggest_drop = self.open_value['MAX_MAX_delta_Event_OI'][-1:].item()
				#print(biggest_drop)
				#print(biggest_drop < -self.open_value['high'][-36:].max()*self.drop_percent)
				#print(-self.open_value['high'][-36:].max()*self.drop_percent)
				if biggest_drop < -self.open_value['high'][-36:].max()*self.drop_percent:
					self.candle_count3 = 0
					#print("NEW OI DROP FOUND")

		if self.candle_count3 is not None:
			self.candle_count3 += 1


		
			
			
		self.t6 = datetime.datetime.utcnow()
		
		if len(self.dates)>1:
			#pass
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)
			self.calc_OI_density()


		self.t7 = datetime.datetime.utcnow()

		if len(self.dates)> 10:
			if sum(self.ATR_switches) > 0:
				self.t8 = datetime.datetime.utcnow()

				if self.ATR_switches[-2] == True and self.rolling_candles_counter > 3:# or trades[-1].status == "OPEN" and self.rolling_candles_counter > 3:
					if self.rolling_candles_counter > self.rolling_period:

						self.Rolling_Low = min(self.lows[-self.rolling_period:-1])
						self.Rolling_High = max(self.highs[-self.rolling_period:-1])
						self.Rolling_Spread = (self.Rolling_High - self.Rolling_Low)
					else:

						if self.trigger_direction == 'up':
							self.Rolling_Low = min(self.lows[-(self.rolling_candles_counter-2):-1])
							self.Rolling_High = max(self.highs[-self.rolling_candles_counter:-1])
						elif self.trigger_direction == 'down':
							self.Rolling_Low = min(self.lows[-self.rolling_candles_counter:-1])
							self.Rolling_High = max(self.highs[-(self.rolling_candles_counter-2):-1])
						

						self.Rolling_Spread = (self.Rolling_High - self.Rolling_Low)


					if abs(self.momentum[-1].item())> 0:
						percent = 0.50 - (self.momentum[-1:].item())/4
						#percent = 0.5
						if percent > 0.75:
							percent = 0.75
						elif percent < 0.25:
							percent = 0.25



				else:
					pass
					

				if self.ATR_switches[-2] == True and self.rolling_candles_counter > 3:
					
					
					self.sell_price1 = self.Rolling_Low + self.Rolling_Spread*(1 - spc1)
					self.sell_price2 = self.Rolling_Low + self.Rolling_Spread*(1 - spc2)
					self.sell_price3 = self.Rolling_Low + self.Rolling_Spread*(1 - spc3)
					
					self.stoploss_sell = self.Rolling_High

					self.buy_price1 = self.Rolling_Low + self.Rolling_Spread*(1 - bpc1)
					self.buy_price2 = self.Rolling_Low + self.Rolling_Spread*(1 - bpc2)
					self.buy_price3 = self.Rolling_Low + self.Rolling_Spread*(1 - bpc3)
					
					


					self.stoploss_buy = self.Rolling_Low

					if momentum_tweak < 0:
						#self.VPOC[-1] = self.VPOC[-1]*(1-momentum_tweak)
						self.sell_price1 = self.sell_price1*(1-momentum_tweak)
						self.sell_price2 = self.sell_price2*(1-momentum_tweak)
						self.sell_price3 = self.sell_price3*(1-momentum_tweak)
					elif momentum_tweak > 0:
						#self.VPOC[-1] = self.VPOC[-1]*(1-momentum_tweak)
						self.buy_price1 = self.buy_price1*(1-momentum_tweak)
						self.buy_price2 = self.buy_price2*(1-momentum_tweak)
						self.buy_price3 = self.buy_price3*(1-momentum_tweak)

					self.VPOC.append((self.buy_price1 + self.sell_price1)*(0.5))
				else:
					self.VPOC.append(None)
					self.closing_line.append(None)
					self.buy_price1 = None
					self.buy_price2 = None
					self.buy_price3 = None

					self.sell_price1 = None
					self.sell_price2 = None
					self.sell_price3 = None
			else:
				#print('NONEGG!!')
				self.VPOC.append(None)
				self.closing_line.append(None)
				self.buy_price1 = None
				self.buy_price2 = None
				self.buy_price3 = None

				self.sell_price1 = None
				self.sell_price2 = None
				self.sell_price3 = None
				self.t8 = datetime.datetime.utcnow()
		else:
			self.closing_line.append(None)
			self.VPOC.append(None)
			self.t8 = datetime.datetime.utcnow()


		#self.t8 = datetime.datetime.utcnow()


		# if self.VPOC[-1] == None:
		# 	self.closing_line.append(self.VPOC[-1])
		# else:
		# 	if abs(self.momentum[-1].item())> 0.01:
		# 		self.closing_line.append( (1+(self.momentum[-1].item()/4))*self.VPOC[-1])
		# 		#print("High momentum!")
		# 		#print(self.momentum[-1].item())
		# 	else:
		# 		self.closing_line.append(self.VPOC[-1])
		# 		#print('Low Momentum')





		#print(self.closing_line[-1])
		self.mid_line.append(self.middle)


		if self.volitility == 'low' and self.buy_price1 != None and self.ATR_switch == True:
			self.buy_price1 = self.buy_price1
			self.buy_price2 = self.buy_price2
			self.buy_price3 = self.buy_price3

			self.sell_price1 = self.sell_price1
			self.sell_price2 = self.sell_price2
			self.sell_price3 = self.sell_price3

		# if self.momentum_tweak is not None:
		# 	momentum_scaler = self.momentum_tweak*100*self.ATR[-1]
		# 	print(momentum_scaler)
		# 	if momentum_tweak > 0:
		# 		self.buy_line1.append(self.buy_price1*(1+momentum_tweak))
		# 		self.buy_line2.append(self.buy_price2*(1+momentum_tweak))
		# 		self.buy_line3.append(self.buy_price3*(1+momentum_tweak))
		# 	elif momentum_tweak < 0:
		# 		self.sell_line1.append(self.sell_price1/(1+momentum_tweak))
		# 		self.sell_line2.append(self.sell_price2/(1+momentum_tweak))
		# 		self.sell_line3.append(self.sell_price3/(1+momentum_tweak))
		# else:

		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)
		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)
		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)

		self.buy_line4.append(self.buy_price1)
		self.sell_line4.append(self.sell_price1)
		self.buy_line5.append(self.buy_price2)
		self.sell_line5.append(self.sell_price2)
		self.buy_line6.append(self.buy_price3)
		self.sell_line6.append(self.sell_price3)

		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3)]


		if len(trades)> 2:


			if trades[-1].status == "STOP LOSSED" and trades[-2].status == "STOP LOSSED" and  trades[-3].status == "STOP LOSSED" and self.new_anchor == False and trades[-1].exitPrice != self.last_trade_exit_price:
				self.stoploss_count +=1
				self.last_trade_exit_price = trades[-1].exitPrice
				#if self.stoploss_count > 200:
				self.ATR_switch = False
				print("ATR SWITCH DUE TO STOP LOSS")
				self.last_stoplossed_trade = False
				self.b_1_ready = True
				self.b_2_ready = True
				self.b_3_ready = True
				self.s_1_ready = True
				self.s_2_ready = True
				self.s_3_ready = True
				self.stoploss_count2 = 0







		if self.ATR_switch == False:
			self.ATR_switches.append(0)	
		elif self.ATR_switch == True:
			self.ATR_switches.append(1)
		
		self.ATR_min_list.append(self.ATR_min)
		self.leverages.append(self.Leverage)
		self.stoplosses.append(self.stoploss)
		self.max_risks.append(self.max_risk)



		self.t9 = datetime.datetime.utcnow()


		self.timer1 += (self.t2-self.t1).total_seconds()
		self.timer2 += (self.t3-self.t2).total_seconds()
		self.timer3 += (self.t4-self.t3).total_seconds()
		self.timer4 += (self.t5-self.t4).total_seconds()
		self.timer5 += (self.t6-self.t5).total_seconds()
		self.timer6 += (self.t7-self.t6).total_seconds()
		self.timer7 += (self.t8-self.t7).total_seconds()
		self.timer8 += (self.t9-self.t8).total_seconds()

		# self.OI_accumulation_percentage = 0.04

		# if len(self.open_value) > 3:
		# 	self.OI_accumulation = (self.open_value['close'][-3:-2].item() - self.open_value['low'][-12*6:-3].min())/self.open_value['close'][-3:-2].item()
		# 	#print(OI_accumulation)
		# 	if self.OI_accumulation > self.OI_accumulation_percentage:
		# 		print('TOO MUCH ACCUMULATION')
		# 		print(self.dates[-1])
		# 	if self.OI_accumulation > self.OI_accumulation_percentage:
		# 		self.OI_FILTER.append(1)
		# 	else:
		# 		self.OI_FILTER.append(0)
		# else:
		# 	self.OI_FILTER.append(None)
		
		
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		
		total = self.account_size[-1]*self.Leverage/3



		stoploss = None
		#market_order = 'zero'
		market_order = False

		spread_scaler = 1

		
	

		if self.momentum is not None:
			if bp == self.buy_price1 and self.b_1_ready == False:# or -0.10 > self.momentum[-1:].item()*10:
				bp = None
			elif bp == self.buy_price2 and self.b_2_ready == False:# or -0.10 > self.momentum[-1:].item()*10:
				bp = None
			elif bp == self.buy_price3 and self.b_3_ready == False:# or -0.10 > self.momentum[-1:].item()*10: 
				bp = None
			
			if sp == self.sell_price1 and self.s_1_ready == False:# or 0.10 < self.momentum[-1:].item()*10:
				sp = None
			elif sp == self.sell_price2 and self.s_2_ready == False:# or 0.10 < self.momentum[-1:].item()*10:
				sp = None
			elif sp == self.sell_price3 and self.s_3_ready == False:# or 0.10 < self.momentum[-1:].item()*10:
				sp = None
		else:
			sp = None
			bp = None


		# if self.buy_price2 is not None:
		# 	if bp == self.buy_price1:
		# 		stoploss_percentage = 3*abs((self.buy_price2 - self.sell_price1)/self.buy_price2)# + abs((self.buy_price2 - self.buy_price1)/self.buy_price2)
		# 	elif bp == self.buy_price2:
		# 		stoploss_percentage = 3*abs((self.buy_price2 - self.sell_price1)/self.buy_price2)
		# 	elif bp == self.buy_price3: 
		# 		stoploss_percentage = 3*abs((self.buy_price2 - self.sell_price1)/self.buy_price2)# - abs((self.buy_price2 - self.buy_price3)/self.buy_price2)
			
		# 	if sp == self.sell_price1:
		# 		stoploss_percentage = 3*abs((self.sell_price2 - self.buy_price1)/self.sell_price2)#+ abs((self.sell_price2 - self.sell_price1)/self.sell_price2)
		# 	elif sp == self.sell_price2:
		# 		stoploss_percentage = 3*abs((self.sell_price2 - self.buy_price1)/self.sell_price2)#+abs((self.sell_price2 - self.sell_price1)/self.sell_price2)
		# 	elif sp == self.sell_price3:
		# 		stoploss_percentage = 3*abs((self.sell_price2 - self.buy_price1)/self.sell_price2)# - abs((self.sell_price2 - self.sell_price3)/self.sell_price2)






		






		if self.time_from_2hour_high > 24:
			self.start_sell_quotes = True

		if self.time_from_2hour_low > 24:
			self.start_buy_quotes = True


		# if len(self.OI_density)>0:
		# 	if self.OI_density[-1] is not None:
		# 		if self.OI_density[-1] < 0.85:

		if len(self.dates) > 20 and self.avg_length is not None:# and self.OI_accumulation < self.OI_accumulation_percentage:
			if self.ATR_switch == True and self.candle_count2 > 40 and self.candle_count2 < self.avg_length:
			#if self.ATR_switch == True:
				
				if self.trigger_direction == 'up' and self.candle_count2 < 12*3 and self.start_sell_quotes == True:
				 	pass
				
				# el
				elif sp != None:
					if self.highs[-1] > sp*spread_scaler and self.closes[-2] < sp*spread_scaler and  abs((sp - self.buy_price1)/sp) > 0.005:
						action = True
						Long = False
						price = sp*spread_scaler
						#stoploss_percentage = 3*abs((self.sell_price2 - self.buy_price1)/self.sell_price2)
						stoploss_percentage = 2.5*abs((self.sell_price2 - self.VPOC[-1])/self.sell_price2)
						stoploss = self.sell_price2*(1+stoploss_percentage)
						#stoploss = price*(1+self.stoploss)
				
				if self.trigger_direction == 'down' and self.candle_count2 < 12*3 and self.start_buy_quotes == True:
				  	pass
				
				# el
				elif bp != None:
					if self.lows[-1] < bp/spread_scaler and self.closes[-2] > bp/spread_scaler and abs((bp - self.sell_price1)/bp) > 0.005:
						action = True
						Long = True
						price = bp/spread_scaler
						#stoploss_percentage = 3*abs((self.buy_price2 - self.sell_price1)/self.buy_price2)
						stoploss_percentage = 2.5*abs((self.buy_price2 - self.VPOC[-1])/self.buy_price2)
						stoploss = self.buy_price2/(1+stoploss_percentage)
						#stoploss = price/(1+self.stoploss)



		if action == True:
			if Long == True:
				if bp == self.buy_price1:
					self.b_1_ready = False
				elif bp == self.buy_price2:
					self.b_2_ready = False
				elif bp == self.buy_price3:
					self.b_3_ready = False

			if Long == False:
				if sp == self.sell_price1:
					self.s_1_ready = False
				elif sp == self.sell_price2:
					self.s_2_ready = False
				elif sp == self.sell_price3:
					self.s_3_ready = False


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for Trade in trades:
				avg_open +=Trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#calculate if offside closing positions are needed
		if trade.trade_length < 3:
			trade.exit_strategy2 = False


		# if self.buy_price3 is not None and self.sell_price3 is not None:
		# 	if trade.long == True and (self.lows[-2] - trade.entryPrice)/self.closes[-2] < -0.025:
		# 		trade.exit_strategy2 = True
		# 		print("EXIT STRATEGY 2 ACTIVATED  " ,str(self.dates[-1]))

				
		# 	if trade.long  == False and (self.highs[-2] - trade.entryPrice)/self.closes[-2] > 0.025:
		# 		trade.exit_strategy2 = True
		# 		print("EXIT STRATEGY 2 ACTIVATED  " ,str(self.dates[-1]))






		if self.sell_price1 is not None:
			
			#if self.OI_accumulation < self.OI_accumulation_percentage:
			if trade.long  == False and trade.exit_strategy2 == True and self.closes[-2] < self.VPOC[-1]:
				action = True
				price = self.closes[-2]
				market_order = True
			elif trade.long  == False and trade.exit_strategy2 == True and self.closes[-2] > self.VPOC[-1] and self.lows[-1] < self.VPOC[-1]: 
				action = True
				price = self.VPOC[-1]
				market_order = False

			#elif self.highs[-1] > self.sell_price1 and trade.long == True and trade.trade_length > 1:
			elif self.lows[-1] < self.VPOC[-1] and trade.long == False and trade.trade_length > 1:
				action = True
				market_order = False
				price = self.VPOC[-1]
				#price = self.sell_price1

			# elif self.highs[-1] > max(self.highs[-24:-1:]) and trade.long == False:
			# 	action = True
			# 	market_order = True
			# 	price = max(self.highs[-24:-1:])


			# else:
				
			# 	if self.closes[-2] > self.VPOC[-1]:# and self.closes[-2] > new_sell_price:
			# 		action = True
			# 		price = self.closes[-2]
			# 		market_order = True
			# 	elif self.closes[-2] < self.VPOC[-1] and self.closes[-1] > self.VPOC[-1]:
			# 		action = True
			# 		price = self.VPOC[-1]
			# 		market_order = False

					

					

		if self.buy_price1 is not None:
			#if self.OI_accumulation < self.OI_accumulation_percentage:
			if trade.long  == True and trade.exit_strategy2 == True and self.closes[-2] > self.VPOC[-1]:
				action = True
				price = self.closes[-2]
				market_order = True
			elif trade.long  == True and trade.exit_strategy2 == True and self.closes[-2] < self.VPOC[-1] and self.highs[-1] > self.VPOC[-1]: 
				action = True
				price = self.VPOC[-1]
				market_order = False
			#elif self.lows[-1] < self.buy_price1 and trade.long == False and trade.trade_length > 1:
			elif self.highs[-1] > self.VPOC[-1] and trade.long == True and trade.trade_length > 1:
				action = True
				#price = self.buy_price1
				price = self.VPOC[-1]

			# elif self.lows[-1] < min(self.lows[-24:-1]) and trade.long == True:
			# 	action = True
			# 	market_order = True
			# 	price = min(self.lows[-24:-1])

				#price = self.buy_price1
			# elif self.OI_accumulation > self.OI_accumulation_percentage:

				
			# 	if self.closes[-2] < self.VPOC[-1]:# and self.closes[-2] > new_sell_price:
			# 		action = True
			# 		price = self.closes[-2]
			# 		market_order = True
			# 	elif self.closes[-2] > self.VPOC[-1] and self.closes[-1] < self.VPOC[-1]:
			# 		action = True
			# 		price = self.VPOC[-1]
			# 		market_order = False


		
	

		if action == True:
			self.b_1_ready = True
			self.b_2_ready = True
			self.b_3_ready = True
			self.s_1_ready = True
			self.s_2_ready = True
			self.s_3_ready = True

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def calculate_clip_size(self):
		print('CALCULATING LEVERAGE')
		returns_30 = pd.Series(self.latest_range)
		#print(returns_30)
		returns_30 = (returns_30.diff()/returns_30)
		#print(self.account_size)
		
		#print(len(returns_30[returns_30 != 0]))
		#print(returns_30)
		if len(returns_30[returns_30 != 0]) != 0 and self.ATR_14[-1] is not None:
			print('CALCULATING LEVERAGE part 2')
			mean_returns = returns_30[returns_30 != 0].mean()*100
			risk = returns_30[returns_30 != 0].std()
			SR = mean_returns/risk
			if SR > 10:
				self.max_risk += 0.04 
			elif SR > 5:
				self.max_risk += 0.015

			elif 3 > SR > 5:
				self.max_risk += 0
			elif -5 < SR < 3:
				self.max_risk -= 0.02
			elif SR < -5: 
				self.max_risk -= 0.06



			if self.max_risk > 0.2:
				self.max_risk = 0.2
			elif self.max_risk < 0.04:
				self.max_risk = 0.04


			stoploss_required = (0.04 + self.ATR_14[-1]*8)
			self.stoploss = stoploss_required
			self.Leverage = self.max_risk/stoploss_required
			if self.Leverage > 4:
				self.Leverage = 4
			elif self.Leverage < 0.5:
				self.Leverage = 0.5

			print("STOP_LOSS ",stoploss_required)
			print("MAX RISK ",self.max_risk)
			print("last SR ", SR )
			print("FINAL Leverage",self.Leverage)
		else:
			self.Leverage = 2




	def calc_OI_density(self):

		if self.candle_count3 is not None and self.ATR[-1] is not None:
			if self.candle_count3 > 6:
				max_price = max(self.highs[-self.candle_count3:])
				min_price = min(self.lows[-self.candle_count3:])
				vp_width = ((max_price - min_price)/self.closes[-1])/self.ATR[-1]
				OI_min = self.open_value[-(self.candle_count3+6):]['low'].min()
				OI_cum = ((self.open_value['close'][-1:].item() - OI_min)/self.open_value['close'][-1:].item())*100
				#print(OI_cum)
				#print(vp_width)
				if OI_cum/vp_width < 2:
					self.OI_density.append(OI_cum/vp_width)
				else:
					self.OI_density.append(None)
			else:
				self.OI_density.append(None)
		else:
			self.OI_density.append(None)
		#print(OI_cum)
		#print(vp_width)
		#print(OI_cum/vp_width)
		#print('\n')
	def Draw_indicators(self):

		print("ALL INDICATOR LENGTHS")
		print(len(self.VPOC))
		print(len(self.buy_line1))
		print(len(self.buy_line2))
		print(len(self.buy_line3))
		print(len(self.sell_line1))
		print(len(self.sell_line2))
		print(len(self.sell_line3))
		print(len(self.ATR))
		print(len(self.open_value))
		print(len(self.ATR_switches))
		print(len(self.volitilitys))
		print(len(self.closing_line))
		print(len(self.dates))
		print(self.candle_count)
		print(len(self.five_m_dates))

		#self.open_value = pd.DataFrame({'open':[],'high':[],'low':[],'close':[]})

		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.stoplosses,self.open_value,self.max_risks,self.leverages,self.max_risks,self.five_m_dates)#,self.ATR_min_list)
		return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.ATR_switches,self.OI_FILTER,self.closing_line,self.five_m_dates)#,self.ATR_min_list)
