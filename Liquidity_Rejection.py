from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
import time




class Liquid_Rejection(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		

		self.limits = False
		self.index = 0
		
		
		self.indicators = BotIndicators(orderbook= True,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False,anchored = False,open_value_candles = True,momentum = False)
		
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		

		self.ATR = []

		self.ob_index = None


		self.VPOC = []


		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []
		

		self.mid_line = []
		self.total_buy_side_liquity = []
		self.total_sell_side_liquity = []

		
			
		self.clip_size = 500


		self.prices = []
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None



		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		
		self.last_orderbook = None
		self.open_value = np.array([])

		self.last_prices = None

		self.buy_limit = []
		self.sell_limit = []

		

		self.five_m_dates = []

		self.s_1_ready = True
		self.s_2_ready = True
		self.s_3_ready = True

		self.strategy_start = None
		
		# self._1m_2m_orders = pd.DataFrame({'price':[],'qty':[]})
		# self._2m_5m_orders = pd.DataFrame({'price':[],'qty':[]})
		# self._5m_10m_orders = pd.DataFrame({'price':[],'qty':[]})
		# self._10m_20m_orders = pd.DataFrame({'price':[],'qty':[]})
		# self._20m_50m_orders = pd.DataFrame({'price':[],'qty':[]})


		self._1m_2m_orders = pd.DataFrame({'price':[],'qty':[]})
		self._2m_3m_orders = pd.DataFrame({'price':[],'qty':[]})
		self._3m_4m_orders= pd.DataFrame({'price':[],'qty':[]})
		self._4m_6m_orders = pd.DataFrame({'price':[],'qty':[]})
		self._6m_8m_orders = pd.DataFrame({'price':[],'qty':[]})
		self._8m_10m_orders	= pd.DataFrame({'price':[],'qty':[]})
		self._10m_50m_orders = pd.DataFrame({'price':[],'qty':[]})
		self.total_orders = pd.DataFrame()

		self.last_orderbook_filtered = pd.DataFrame()


		self.daily_VWMA = []


		self.numSimulTrades = 1
		self.margin = True

		self.closing_line = []
		self.account_size = []


		self.stoploss_count = None
		self.candle_count = 0
		self.last_stoplossed_trade = None
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):
		

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.t1 = datetime.datetime.utcnow()


		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		self.t2 = datetime.datetime.utcnow()
		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])

		

		#Update relevent indicators
		if np.mod(self.dates[-1].minute,5) == 0:
			self.five_m_dates.append(self.dates[-1])
			self.tick_indicators(trades)
		#print(trades)
		#if len(trades)> 0:
		#	print(trades[-1:][0].status)

	def tick_indicators(self,trades):
		#updating Indicators
		#print(self.strategy_start,self.dates[-1:])
		self.candle_count +=1


		if len(trades)> 0:
			if trades[-1].status == "STOP LOSSED" and trades[-1] != self.last_stoplossed_trade:
				self.last_stoplossed_trade = trades[-1]
				self.stoploss_count = 0

		if self.stoploss_count is not None:
			self.stoploss_count += 1
			print("STOPLOSS COUNT",self.stoploss_count )

		if self.stoploss_count is not None:
			if self.stoploss_count > 12*12:
				self.stoploss_count = None
				print("STOPLOSS RESET")
				

		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)

		if len(self.closes) > 300:
			self.daily_VWMA.append(sum(self.closes[-144:])/len(self.closes[-144:]))
		else:
			self.daily_VWMA.append(None)

		self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))


		if self.candle_count > 3:
			if self.last_orderbook is not None:
				#self.last_prices = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*300,self.closes[-1]*3500)['price'].values
				self.last_prices = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*550,25000000)['price'].values



				#print(self.last_prices)


			self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)
			# print(self.last_orderbook)
			# print(self.dates[-1])
			# time.sleep(0.2)
			if self.last_orderbook is not None:
				#self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)
				#self._1m_2m_orders = pd.concat([self._1m_2m_orders,self.indicators.return_large_orders(self.last_orderbook,1500000,2000000)])
				#self._2m_3m_orders = pd.concat([self._2m_3m_orders,self.indicators.return_large_orders(self.last_orderbook,2000000,3000000)])
				if self.last_prices is not None:
					orders1 = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*550,self.closes[-1]*600)
					#orders1 = self.indicators.return_large_orders(self.last_orderbook,1000000,self.closes[-1]*700)
					orders2 = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*600,self.closes[-1]*700)
					orders3 = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*700,self.closes[-1]*800)
					orders4 = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*800,self.closes[-1]*1000)
					orders5 = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*1000,self.closes[-1]*3500)

					# orders1 = self.indicators.return_large_orders(self.last_orderbook,3000000,4000000)
					# orders2 = self.indicators.return_large_orders(self.last_orderbook,4000000,6000000)
					# orders3 = self.indicators.return_large_orders(self.last_orderbook,6000000,8000000)
					# orders4 = self.indicators.return_large_orders(self.last_orderbook,8000000,10000000)
					# orders5 = self.indicators.return_large_orders(self.last_orderbook,10000000,25000000)




					total_orders = self.indicators.return_large_orders(self.last_orderbook,self.closes[-1]*550,self.closes[-1]*3500)

					# orders1 = orders1[orders1['price'].isin(self.last_prices)]
					# orders2 = orders2[orders2['price'].isin(self.last_prices)]
					# orders3 = orders3[orders3['price'].isin(self.last_prices)]
					# orders4 = orders4[orders4['price'].isin(self.last_prices)]
					# orders5 = orders5[orders5['price'].isin(self.last_prices)]
					total_orders = total_orders[total_orders['price'].isin(self.last_prices)]

					self._3m_4m_orders = pd.concat([self._3m_4m_orders,orders1])
					self._4m_6m_orders = pd.concat([self._4m_6m_orders,orders2])
					self._6m_8m_orders = pd.concat([self._6m_8m_orders,orders3])
					self._8m_10m_orders = pd.concat([self._8m_10m_orders,orders4])
					self._10m_50m_orders = pd.concat([self._10m_50m_orders,orders5])

					#print(total_orders)

					self.last_orderbook['qty_by_OI'] = self.last_orderbook['qty']/self.open_value['OI_close'][-1:].item()
					self.last_orderbook['density'] = self.last_orderbook['qty_by_OI'].rolling(5,center= True).mean()

					#print(total_orders['density'][0:50])
					#time.sleep(5)

					self.total_buy_side_liquity.append(total_orders[total_orders['side'] == 'Buy']['qty'].sum())
					self.total_sell_side_liquity.append(total_orders[total_orders['side'] == 'Sell']['qty'].sum())

					

					self.last_orderbook_filtered = self.last_orderbook
					self.last_orderbook_filtered = self.last_orderbook_filtered.reset_index(drop = True)

					

					#print(len(self.last_orderbook_filtered))
					self.last_orderbook_filtered.loc[~self.last_orderbook_filtered['price'].isin(total_orders['price'].values),'qty'] = 0

					self.last_orderbook_filtered['counter'] = 0
					self.last_orderbook_filtered.loc[self.last_orderbook_filtered['qty']>0,'counter'] = 1

					# print(self.last_orderbook_filtered[['price','side','counter','qty']][0:50])
					# print(self.last_orderbook_filtered[['price','side','counter','qty']][50:100])
					# print(self.last_orderbook_filtered[['price','side','counter','qty']][100:150])
					# print(self.last_orderbook_filtered[['price','side','counter','qty']][150:200])
					# print('\n')
					# time.sleep(5)



					buy_max_clump = self.clump_func(self.last_orderbook_filtered[self.last_orderbook_filtered['side'] == 'Buy']['counter'].values)
					sell_max_clump = self.clump_func(self.last_orderbook_filtered[self.last_orderbook_filtered['side'] == 'Sell']['counter'].values)

					self.last_orderbook_filtered['qty_behind_buy']  = self.last_orderbook_filtered[self.last_orderbook_filtered['side'] == 'Buy']['qty'].rolling(buy_max_clump).sum()
					self.last_orderbook_filtered['qty_behind_sell'] = self.last_orderbook_filtered[self.last_orderbook_filtered['side'] == 'Sell']['qty'][::-1].rolling(sell_max_clump).sum()

					self.last_orderbook_filtered = self.last_orderbook_filtered.sort_values(by = 'price',ascending=False)

					candleTime = time.mktime(self.dates[-1].timetuple())
					candleTime = datetime.datetime.fromtimestamp(candleTime)

					# if candleTime > datetime.datetime(2019,9,6,0,0,0) and candleTime < datetime.datetime(2019,9,6,12,0,0):
					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][0:50])
					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][50:100])

					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][100:150])
					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][150:200])

					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][200:250])
					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][250:300])

					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][300:350])
					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][350:400])

					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][400:450])
					# 	print(self.last_orderbook_filtered[['price','side','qty','qty_behind_buy','qty_behind_sell']][450:500])
					# 	print(self.closes[-1])
					# 	print(self.closes[-1])
					# 	print(self.closes[-1])
					# 	print('\n')
					# 	print('\n')
					# 	print('\n')


					if buy_max_clump >= 5:
						self.buy_limit.append(self.last_orderbook_filtered[self.last_orderbook_filtered.index == self.last_orderbook_filtered['qty_behind_buy'].idxmax()]['price'].item())
					else:
						self.buy_limit.append(None)

					if sell_max_clump >= 5:
						self.sell_limit.append(self.last_orderbook_filtered[self.last_orderbook_filtered.index == self.last_orderbook_filtered['qty_behind_sell'].idxmax()]['price'].item())
					else:
						self.sell_limit.append(None)

					# print(self.last_orderbook_filtered['counter'][0:50])
					# print(self.last_orderbook_filtered['counter'][-50:-1])


					# print(self.last_orderbook_filtered[['price','qty','qty_behind_buy','qty_behind_sell']][0:50])
					# print(self.last_orderbook_filtered[['price','qty','qty_behind_buy','qty_behind_sell']][-50:-1])
					#print(max_clump)



					#time.sleep(1)
					

					# print(len(self.last_orderbook_filtered))
					# print(len(self.last_orderbook_filtered[~self.last_orderbook_filtered['price'].isin(self.last_prices)]['qty']))
					# print(len(self.last_orderbook_filtered[self.last_orderbook_filtered['price'].isin(self.last_prices)]['qty']))
					# print(self.last_orderbook_filtered[~self.last_orderbook_filtered['price'].isin(self.last_prices)]['qty'])
					# print(self.last_orderbook_filtered[self.last_orderbook_filtered['price'].isin(self.last_prices)]['qty'])
					# print('\n')


					self.total_orders = pd.concat([self.total_orders,total_orders])
				else:
					self.total_buy_side_liquity.append(None)
					self.total_sell_side_liquity.append(None)
					self.sell_limit.append(None)
					self.buy_limit.append(None)
			else:
				self.total_buy_side_liquity.append(None)
				self.total_sell_side_liquity.append(None)
				self.sell_limit.append(None)
				self.buy_limit.append(None)
		else:
			self.total_buy_side_liquity.append(None)
			self.total_sell_side_liquity.append(None)

			self.sell_limit.append(None)
			self.buy_limit.append(None)



		# if len(self.last_orderbook_filtered)>0:
		# 	print(self.last_orderbook_filtered[['price','qty']])
		# 	print(self.last_orderbook_filtered['qty'].max())
		# 	time.sleep(1)

			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		
		total = self.account_size[-1]*5
		stoploss = None
		#market_order = 'zero'
		market_order = False

		spread_scaler = 1

		#print('test!')
		#print(self.buy_limit[-1] is not None)


		if self.daily_VWMA[-1] is not None and self.stoploss_count is None:
			if self.buy_limit[-1] is not None:
				if self.lows[-1] < self.buy_limit[-1]+5:
					Long = True
					action = True
					price = self.buy_limit[-1]+5
					stoploss = price/(1 + (self.ATR[-1]*8))

			if self.sell_limit[-1] is not None:
				if self.highs[-1] > self.sell_limit[-1]-5:
					Long = False
					action = True
					price = self.sell_limit[-1]-5
					stoploss = price*(1 + (self.ATR[-1]*8))




		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False


		if trade.long == True and self.lows[-1] < trade.highest_high/(1 + (self.ATR[-1]*32)):
			action = True
			price = trade.highest_high/(1 + (self.ATR[-1]*32))
			market_order = True


		if trade.long == False and self.highs[-1] > trade.lowest_low*(1 + (self.ATR[-1]*32)):
			action  = True
			price = trade.lowest_low*(1 + (self.ATR[-1]*32))
			market_order = True


		# if trade.long == True and self.highs[-2] < self.daily_VWMA[-1] and self.highs[-1] > self.daily_VWMA[-1]:
		# 	action = True
		# 	price = self.daily_VWMA[-1]


		# elif trade.long == False and self.lows[-2] > self.daily_VWMA[-1] and self.lows[-1] < self.daily_VWMA[-1]:
		# 	action = True
		# 	price = self.daily_VWMA[-1]

	

		if action == True:
			self.b_1_ready = True
			self.b_2_ready = True
			self.b_3_ready = True
			self.s_1_ready = True
			self.s_2_ready = True
			self.s_3_ready = True

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def clump_func(self,clump_list):
		max_clump = 0
		current_clump = 0
		gap = 0
		total_cluster_gap = 0
		start_cluster = 0
		#print(clump_list)
		for x in clump_list:

			if x == 1:
				current_clump +=1
				gap = 0
				if (current_clump) > max_clump:
					max_clump = current_clump
			

			elif current_clump > 0 and x == 0:
				current_clump +=1
				gap +=1
				total_cluster_gap +=1
			
			else:
				gap = 0


			
			if gap >=3 or total_cluster_gap >= 7:
				current_clump = 0
				total_cluster_gap = 0
				gap = 0

		
		#print(max_clump,sum(clump_list))
		return max_clump





	def Draw_indicators(self):
		#self.balence = 
		print(self.total_orders)
		return(self._3m_4m_orders,self._4m_6m_orders,self._6m_8m_orders,self._8m_10m_orders,self._10m_50m_orders,self._10m_50m_orders,self.sell_limit,self.buy_limit,self.ATR)
		#return(self.total_orders,self._3m_4m_orders,self._4m_6m_orders)#,self._6m_8m_orders,self._8m_10m_orders,self._10m_50m_orders,self.total_buy_side_liquity,self.total_sell_side_liquity,)
		#print(self._10m_20m_orders

		#return (self._1m_2m_orders,self._2m_5m_orders,self._5m_10m_orders,self._10m_20m_orders,self._20m_50m_orders)




