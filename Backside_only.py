from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

#Strategy_on UTC TIMES  [(1.5-2.5),(7.5-11),(17.5 - 19.5)]
		#sydney times.  [(11.5-12.5),(17.5-21),(3.5-5.5)]



		#low volitility [Monday]
		#medium volility[Thursday, Saturday,Sunday]
		#Tuesday-Friday tighten Saturday


# SMA_M uses 3 indicators and will require an imput perameter for each


class VPOC_BS(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = True,anchored = True,open_value_candles = True,momentum = True)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None


		self.VPOC = []


		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []
		

		self.mid_line = [None,None]

		self.drop_percent = 0.06
		self.Leverage = 4



		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None



		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.volitilitys =[]

		self.open_value = np.array([])

		self.middle = None


		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True


		self.b_1_ready = False
		self.b_2_ready = False
		self.b_3_ready = False



		self.s_1_ready = False
		self.s_2_ready = False
		self.s_3_ready = False

		self.strategy_start = None
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = np.array([])

		self.ATR_switches = np.array([])

		self.numSimulTrades = 3
		self.margin = True

		self.closing_line = []
		self.account_size = []
		self.latest_range = []
		self.last_stoplossed_trade = None
		
		self.count10 = 0
		self.switch10 = False
		self.trigger_date = None
		self.candle_count = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):
		

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		self.t2 = datetime.datetime.utcnow()

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])

		#Update relevent indicators
		self.tick_indicators(trades)
		#print(trades)
		#if len(trades)> 0:
		#	print(trades[-1:][0].status)

	def tick_indicators(self,trades):
		#updating Indicators
		#print(self.strategy_start,self.dates[-1:])
		self.candle_count +=1

		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
		#if self.count10 > 12*48:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		if self.trigger_date is not None:
			self.trigger_date += 1

		if len(trades)> 0:
			#print(self.last_stoplossed_trade)
			#print(trades[-1])
			if trades[-1].status == "STOP LOSSED" and trades[-1] != self.last_stoplossed_trade and self.trigger_date is not None:
				if self.trigger_date > 6:
					print(trades[-1].entryDate[0])
					#print(self.dates[-2])
					if trades[-1].entryDate[0] == self.dates[-2] or trades[-1].entryDate == self.dates[-3] or trades[-1].entryDate == self.dates[-4]  or trades[-1].entryDate == self.dates[-5] or trades[-1].entryDate == self.dates[-6]:
						self.ATR_switch = False
						print("ATR SWITCH DUE TO STOP LOSS")
						self.last_stoplossed_trade = trades[-1]

		
		#if len(self.dates)>1:
		#	self.open_value = self.indicators.get_open_value(self.dates[-2:-1])
		self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))
		if self.candle_count > 1:
			self.momentum = self.indicators.get_momentum(self.dates[-2])
	
		utc_time = self.dates[-1:][0] - datetime.timedelta(hours = 10)
		day = utc_time.weekday()
		hour = utc_time.hour

		if utc_time.minute >= 30:
			minutes = 0.5
		else:
			minutes = 0

		week_num = day*24 + hour + minutes
		#print(week_num)



		#if 1 < week_num < 12 or 24.5 < week_num < 28 or 49 < week_num < 60 or 73 < week_num < 87.5 or 89 < week_num < 93 or 94 < week_num < 98.5 or 99.5 < week_num < 104.5 or 113 < week_num < 140.5 or 144.5 < week_num < 154.5:
		if 0 <= week_num <1.5 or 11.5 < week_num <49.5 or 66.5 < week_num <73.5 or 87 < week_num < 100 or 107 < week_num < 113.5 or 154 < week_num: 
			self.volitility = 'high'
		else:
			self.volitility = 'low'

		if self.volitility == 'high':
			self.volitilitys.append(1)
			bpc1 = 0.80
			bpc2 = 0.85
			bpc3 = 0.90

			spc1 = 0.2
			spc2 = 0.15
			spc3 = 0.1
		elif self.volitility == 'low':
			self.volitilitys.append(0)
			bpc1 = 0.75
			bpc2 = 0.80
			bpc3 = 0.85

			spc1 = 0.25
			spc2 = 0.2
			spc3 = 0.15



		if self.candle_count > 290:
			if len(self.open_value['max_delta_OI0'][-1:]) > 0:
				biggest_drop = self.open_value['MAX_MAX_delta_Event_OI'][-1:].item()
				
				if biggest_drop < -self.open_value['high'][-36:].max()*self.drop_percent and self.switch10 == False:# and abs(self.closes[-1:]-self.closes[-1:]) > self.closes[-1:]*0.035: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 = True
			
					self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					self.last_OV_high = self.open_value['high'][-36:].max()
					self.ATR_switch = True
					self.trigger_date = 0
					if self.ATR_switches[-1:] == 0:# and trades[-3:-2][0].status == "CLOSED" and trades[-2:-1][0].status == "CLOSED" and trades[-1:][0].status == "CLOSED":
						if len(trades) == 0 or trades[-1].status == "CLOSED":#and trades[-2:-1].status == "CLOSED" and trades[-1:].status == "CLOSED":
							self.strategy_start = self.dates[-1]

					print('\n')
					print("NEW TRIGGER EVENT! " , self.dates[-1])
					print(biggest_drop)
					print("MIN ATR ", self.ATR_min)
					print('\n')

				if self.ATR[-1:] != None:
					if self.ATR_switches[-12*24:].min() == 1:
						if 114 < week_num < 154 or 74 < week_num < 87 or 49 < week_num < 58 or 1 < week_num < 12:
							if self.ATR[-1:] < self.ATR_min*0.8 and self.ATR_switch == True:
								self.ATR_switch = False
								#self.strategy_start = None
								print('\n')
								print('FAILED ATR SWITCH ', self.dates[-1:])
								print('\n')
						else:
							if self.ATR[-1:] < self.ATR_min*1 and self.ATR_switch == True:
								self.ATR_switch = False
								#self.strategy_start = None
								print('\n')
								print('FAILED ATR SWITCH ', self.dates[-1:])
								print('\n')		
					
				if self.open_value['MAX_MAX_delta_Lookback_OI'][-1:].item() > -self.open_value['high'][-36:].max()*self.drop_percent and self.ATR_switch == True:
					self.ATR_switch = False
					#self.strategy_start = None
					print('\n')
					print('5 day OV drop expired! ', self.dates[-1:])
					print('\n')

				if self.open_value['high'][-1].item() > self.last_OV_high + self.open_value['high'][-6*180:].max()*self.drop_percent and self.ATR_switch == True:

					self.ATR_switch = False
					#self.strategy_start = None
					print('\n')
					print('OVER THE OV MAX ', self.dates[-1:])
					print('OV MAX ',self.last_OV_high + self.open_value['high'][-36:].max()*self.drop_percent)
					print("LAST OI ",self.open_value['close'][-1].item())
					print('\n')
			
			

		
		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)




		if len(self.dates)> 10:
			if self.ATR_switches.sum() > 0:
				self.volume_profile = self.indicators.get_anchored_VPOC(self.strategy_start,self.dates[-1])
				if len(self.volume_profile)> 15:
					try:
						self.VPOC.append(self.volume_profile[self.volume_profile['percent']<0.50].index[-1])
					except:
						self.VPOC.append(self.volume_profile[self.volume_profile['percent']>0.50].index[0])



					if abs(self.momentum[-1].item())> 0:
						percent = 0.50 - (self.momentum[-1:].item())*0
						#percent = 0.5
						if percent > 0.7:
							percent = 0.7
						elif percent < 0.3:
							percent = 0.3
						try:
							self.closing_line.append(self.volume_profile[self.volume_profile['percent']<(percent)].index[-1])
						except:
							self.closing_line.append(self.volume_profile[self.volume_profile['percent']>(1-percent)].index[0])
					else:
						try:
							self.closing_line.append(self.volume_profile[self.volume_profile['percent']<0.50].index[-1])
						except:
							self.closing_line.append(self.volume_profile[self.volume_profile['percent']>0.50].index[0])
				else:
					self.VPOC.append(None)
					self.closing_line.append(None)

				#print(self.volume_profile)

				if len(self.volume_profile)> 15:

					#self.V60 = self.volume_profile[self.volume_profile['percent']<0.60].index[-1:][0]
					#self.V40 = self.volume_profile[self.volume_profile['percent']<0.40].index[-1:][0]
					try:
						self.sell_price1 = self.volume_profile[self.volume_profile['percent']<spc1].index[-1:][0]
						self.sell_price2 = self.volume_profile[self.volume_profile['percent']<spc2].index[-1:][0]
						self.sell_price3 = self.volume_profile[self.volume_profile['percent']<spc3].index[-1:][0]
						
						#self.sell_price3 = self.volume_profile.index[0]

						self.buy_price1 = self.volume_profile[self.volume_profile['percent']>bpc1].index[0]
						self.buy_price2 = self.volume_profile[self.volume_profile['percent']>bpc2].index[0]
						self.buy_price3 = self.volume_profile[self.volume_profile['percent']>bpc3].index[0]
						
						#self.buy_price3 = self.volume_profile.index[-1:][0]
					except:
						print(self.volume_profile)
						#self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.2].index[-1:][0]
						try:
							self.sell_price1 = self.volume_profile[self.volume_profile['percent']<spc1].index[-1:][0]
						except:
							self.sell_price1 = self.volume_profile.index[2]

						try:
							self.sell_price2 = self.volume_profile[self.volume_profile['percent']<spc2].index[-1:][0]
						except:
							self.sell_price2 = self.volume_profile.index[1]
						
						self.sell_price3 = self.volume_profile.index[0]

						try:
							self.buy_price1 = self.volume_profile[self.volume_profile['percent']>bpc1].index[0]
						except:
							self.buy_price3 = self.volume_profile.index[-3]
						try:
							self.buy_price2 = self.volume_profile[self.volume_profile['percent']>bpc2].index[0]
						except:
							self.buy_price3 = self.volume_profile.index[-2]

						self.buy_price3 = self.volume_profile.index[-1]

				else:
					pass
			else:
				#print('NONEGG!!')
				self.VPOC.append(None)
				self.closing_line.append(None)
				self.buy_price1 = None
				self.buy_price2 = None
				self.buy_price3 = None

				self.sell_price1 = None
				self.sell_price2 = None
				self.sell_price3 = None
		else:
			self.closing_line.append(None)
			self.VPOC.append(None)





		# if self.VPOC[-1] == None:
		# 	self.closing_line.append(self.VPOC[-1])
		# else:
		# 	if abs(self.momentum[-1].item())> 0.01:
		# 		self.closing_line.append( (1+(self.momentum[-1].item()/4))*self.VPOC[-1])
		# 		#print("High momentum!")
		# 		#print(self.momentum[-1].item())
		# 	else:
		# 		self.closing_line.append(self.VPOC[-1])
		# 		#print('Low Momentum')





		#print(self.closing_line[-1])
		self.mid_line.append(self.middle)


		if self.volitility == 'low' and self.buy_price1 != None and self.ATR_switch == True:
			self.buy_price1 = self.buy_price1
			self.buy_price2 = self.buy_price2
			self.buy_price3 = self.buy_price3

			self.sell_price1 = self.sell_price1
			self.sell_price2 = self.sell_price2
			self.sell_price3 = self.sell_price3


		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)
		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)
		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)

		self.buy_line4.append(self.buy_price1)
		self.sell_line4.append(self.sell_price1)
		self.buy_line5.append(self.buy_price2)
		self.sell_line5.append(self.sell_price2)
		self.buy_line6.append(self.buy_price3)
		self.sell_line6.append(self.sell_price3)

		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3),(self.buy_price4,self.sell_price4),(self.buy_price5,self.sell_price5),(self.buy_price6,self.sell_price6)]





		if self.ATR_switch == False:
			self.ATR_switches = np.append(self.ATR_switches,0)
			
		elif self.ATR_switch == True:
			self.ATR_switches = np.append(self.ATR_switches,1)
		self.ATR_min_list = np.append(self.ATR_min_list,self.ATR_min)
		
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = self.account_size[-1]*self.Leverage/3
		stoploss = None
		#market_order = 'zero'
		market_order = False

		spread_scaler = 1

		if self.volitility == 'high':
			spread_scaler = 1
		elif self.volitility == 'low':
			spread_scaler == 1


		if bp == self.buy_price1 and self.b_1_ready == False:
			bp = None
		elif bp == self.buy_price2 and self.b_2_ready == False:
			bp = None
		elif bp == self.buy_price3 and self.b_3_ready == False:
			bp = None
		
		if sp == self.sell_price1 and self.s_1_ready == False:
			sp = None
		elif sp == self.sell_price2 and self.s_2_ready == False:
			sp = None
		elif sp == self.sell_price3 and self.s_3_ready == False:
			sp = None

		if self.candle_count > 5 and self.sell_price3 != None and self.VPOC[-1] != None:
			if min(self.closes[-6:]) > self.sell_price3:
				self.s_1_ready = True
				self.s_2_ready = True
				self.s_3_ready = True
				#print("SELLS ENABLED ", self.dates[-1])

			elif max(self.closes[-6:]) < self.buy_price3:
				self.b_1_ready = True
				self.b_2_ready = True
				self.b_3_ready = True
				#print("BUYS ENABLED ", self.dates[-1])


			if self.highs[-2] > self.VPOC[-1] and self.lows[-1] < self.VPOC[-1]:
				self.s_1_ready = False
				self.s_2_ready = False
				self.s_3_ready = False
				self.b_1_ready = False
				self.b_2_ready = False
				self.b_3_ready = False
				#print("RESET")
			if self.lows[-2] < self.VPOC[-1] and self.highs[-1] > self.VPOC[-1]:
				self.s_1_ready = False
				self.s_2_ready = False
				self.s_3_ready = False
				self.b_1_ready = False
				self.b_2_ready = False
				self.b_3_ready = False
				#print("RESET")

		
		if len(self.dates) > 2:
			
			if  self.ATR_switch == True and self.ATR_switches[-12*6:].sum() == 12*6:
				
				if sp != None and self.closing_line[-1] != None:
					if self.highs[-1] > sp*spread_scaler and self.closes[-2] < sp*spread_scaler and sp*spread_scaler > self.closing_line[-1]*1.000125:
						action = True
						Long = False
						price = sp*spread_scaler
						stoploss = price*1.0325
						
				if bp != None and self.closing_line[-1] != None:

					if self.lows[-1] < bp/spread_scaler and self.closes[-2] > bp/spread_scaler and bp/spread_scaler < self.closing_line[-1]/1.000125:
						action = True
						Long = True
						price = bp/spread_scaler
						stoploss = price/1.0325



		if action == True:
			if Long == True:
				if bp == self.buy_price1:
					self.b_1_ready = False
				elif bp == self.buy_price2:
					self.b_2_ready = False
				elif bp == self.buy_price3:
					self.b_3_ready = False

			if Long == False:
				if sp == self.sell_price1:
					self.s_1_ready = False
				elif sp == self.sell_price2:
					self.s_2_ready = False
				elif sp == self.sell_price3:
					self.s_3_ready = False


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:


		if self.volitility == 'high':
			#print(self.highs[-1],self.VPOC[-1])
			#if self.highs[-1] > self.VPOC[-1] and trade.long == True:
			if self.highs[-1] > self.closing_line[-1] and trade.long == True:
			#if self.highs[-1:] > self.sell_price1 and trade.long == True:
				action = True
				price = self.closing_line[-1]
				#price = self.sell_price1
		elif self.volitility == 'low':
			#if self.highs[-1:] > self.V60 and trade.long == True:
			if self.highs[-1] > self.closing_line[-1] and trade.long == True:
			#if self.highs[-1] > self.VPOC[-1] and trade.long == True:
				action = True
				#price = self.V60
				price = self.closing_line[-1]
		
					

		if self.volitility == 'high':
			#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
			#if self.lows[-1] < self.VPOC[-1] and trade.long == False:
			if self.lows[-1:] < self.closing_line[-1] and trade.long == False:
				action = True
				price = self.closing_line[-1]
				#price = self.buy_price1
		elif self.volitility == 'low':
			#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
			#if self.lows[-1] < self.VPOC[-1] and trade.long == False:
			if self.lows[-1:] < self.closing_line[-1] and trade.long == False:
				action = True
				price = self.closing_line[-1]
				#price = self.V40

		
	

		if action == True:
			self.b_1_ready = False
			self.b_2_ready = False
			self.b_3_ready = False
			self.s_1_ready = False
			self.s_2_ready = False
			self.s_3_ready = False

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):

		print("ALL INDICATOR LENGTHS")
		print(len(self.VPOC))
		print(len(self.buy_line1))
		print(len(self.buy_line2))
		print(len(self.buy_line3))
		print(len(self.sell_line1))
		print(len(self.sell_line2))
		print(len(self.sell_line3))
		print(len(self.ATR))
		print(len(self.open_value))
		print(len(self.ATR_switches))
		print(len(self.volitilitys))
		print(len(self.closing_line))
		print(len(self.dates))
		print(self.candle_count)




		return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.ATR_switches,self.volitilitys,self.closing_line)#,self.ATR_min_list)



