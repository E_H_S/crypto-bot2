from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time
import operator
import math


#Strategy_on UTC TIMES  [(1.5-2.5),(7.5-11),(17.5 - 19.5)]
		#sydney times.  [(11.5-12.5),(17.5-21),(3.5-5.5)]



		#low volitility [Monday]
		#medium volility[Thursday, Saturday,Sunday]
		#Tuesday-Friday tighten Saturday


# SMA_M uses 3 indicators and will require an imput perameter for each


class Breakouts2(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = False
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False,anchored = False,open_value_candles = True,momentum = False,RSI = False)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = []
		

		self.ATR = []
		self.ATR_ROOT = []


		self.ATR_14 = []
		self.stoplosses = []
		self.max_risks = []

		self.ob_index = None

		self.skew = 0.0005

		self.VPOC = []
		self.max_risk = 0.08

		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []
		
		self.distance_to_entry = 0.02
		self.Leverage = 5

		self.mid_line = [None,None]

		if p1 != None:
			self.drop_percent = p1
		else:
			#self.drop_percent = 0.062
			self.drop_percent = 0.001

		if p2 != None:
			self.stoploss = p2
		else:
			self.stoploss = 0.006

		if p3 != None:
			self.clip_size = p3
		else:
			self.clip_size = 1500


		self.high_low_rolling_period = 1440*5
		self.min_accumulation_time = 1440

		self.volitilitys =[]
		self.account_size = []
		self.latest_range = []

		self.open_value = np.array([])

		self.middle = None

		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = []
		self.ATR_switches = []

		self.numSimulTrades = 1
		self.margin = True

		self.closing_line = []
		self.volume_profile = None

		self.OI_density = []
		self.trigger_date = None

		self.high_index = None
		self.low_index = None


		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0
		self.timer4 = 0
		self.timer5 = 0
		self.timer6 = 0
		self.timer7 = 0
		self.timer8 = 0

		self.last_stoplossed_trade = None


		self.three_day_high_list = []
		self.three_day_low_list = []
		self.seven_day_high_list = []
		self.seven_day_low_list = []

		self.time_from_high = 0
		self.time_from_low = 0

		self.accumulation_since_high = None
		self.accumulation_since_low = None

		self.accumulation_since_high_list = []
		self.accumulation_since_low_list = []

		self.min_Open_Intererest_at_high = None
		self.min_Open_Intererest_at_low = None

		self.max_three_day_accumulation_list = []

		self.ATR_rolling_period = 1440*3
		

		self.five_m_dates = []


		self.count10 = 0
		self.switch10 = False

		self.candle_count = 0
		self.candles_from_trigger = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):
		

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.t1 = datetime.datetime.utcnow()

		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		self.t2 = datetime.datetime.utcnow()

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])		

		#Update relevent indicators
		# if np.mod(self.dates[-1].minute,5) == 0:
		# 	self.five_m_dates.append(self.dates[-1])
		self.tick_indicators(trades)
		

	def tick_indicators(self,trades):


		self.candle_count += 1
		self.candles_from_trigger +=1


		# if self.candle_count > 3:
		# 	self.RSI.append(self.indicators.get_RSI(self.dates[-3]))
		# else:
		# 	self.RSI.append(None)
		if len(self.dates)>3 and np.mod(self.dates[-1].minute,5)-1 == 0:
			self.open_value = self.indicators.get_open_value(self.dates[-3],candle = True)



		if self.candle_count > 120:
			if abs(self.closes[-120] - self.closes[-1])/self.closes[-1] > 0.15:
				self.candles_from_trigger = 0

		if self.candles_from_trigger < self.ATR_rolling_period and self.candles_from_trigger > 120:
			self.ATR.append(self.indicators.avg_range(self.closes,self.highs,self.lows,int(self.candles_from_trigger)))
		else:
			self.ATR.append(self.indicators.avg_range(self.closes,self.highs,self.lows,int(self.ATR_rolling_period)))
		

		if self.ATR[-1] is not None:

			if self.ATR[-1] < 0.0005:
				self.high_low_rolling_period = int(1440*4)
				self.min_accumulation_time = int(1440/2)
				self.stoploss = 0.01
				self.time_before_exit = 240
				self.distance_to_entry = 0.01

			elif self.ATR[-1] < 0.00085:
				self.high_low_rolling_period = int(1440*3)
				self.min_accumulation_time = int(1440/4)
				self.stoploss = 0.008
				self.time_before_exit = 120
				self.distance_to_entry = 0.025


			elif self.ATR[-1] < 0.0015:
				self.high_low_rolling_period = int(1440*1.5)
				self.min_accumulation_time = int(1440/8)
				self.stoploss = 0.006
				self.time_before_exit = 60
				self.distance_to_entry = 0.04


			elif self.ATR[-1] < 0.0025:
				self.high_low_rolling_period = int(1440)
				self.min_accumulation_time = int(180)
				self.stoploss = 0.0045
				self.time_before_exit = 30
				self.distance_to_entry = 0.06
			
			elif self.ATR[-1] < 0.01:
				self.high_low_rolling_period = int(1440/2)
				self.min_accumulation_time = int(180)
				self.stoploss = 0.003
				self.time_before_exit = 15
				self.distance_to_entry = 0.08
		else:
			self.high_low_rolling_period = int(1440*5)
			self.min_accumulation_time = int(1440*2)

		# self.high_low_rolling_period = int(1440/2)
		# self.min_accumulation_time = int(180)
		# self.stoploss = 0.003





		# if self.ATR[-1] is not None:
		# 	self.ATR_ROOT.append((math.log10(self.ATR[-1])))
		# else:
		# 	self.ATR_ROOT.append(None)


		if len(self.dates)> 5:
			self.high_index, self.three_day_high = max(enumerate(self.highs[-self.high_low_rolling_period:-1:]), key=operator.itemgetter(1))
			self.low_index, self.three_day_low = min(enumerate(self.lows[-self.high_low_rolling_period:-1:]), key=operator.itemgetter(1))

			#print(self.high_index,self.low_index)
			#print(self.high_index,int(self.high_index))

			if len(self.highs) < (self.high_low_rolling_period-1):
				high_index_from_end = self.high_index
				low_index_from_end = self.low_index




				self.high_date = self.dates[-high_index_from_end]
				self.low_date = self.dates[-low_index_from_end]
			else:
				length = len(self.highs) - (self.high_low_rolling_period-1)

				high_index_from_end = length + self.high_index
				low_index_from_end = length + self.low_index

				self.high_date = self.dates[high_index_from_end]
				self.low_date = self.dates[low_index_from_end]

			self.seven_day_high = max(self.highs[-20160:-1:])
			self.seven_day_low = min(self.lows[-20160:-1])

		else:
			self.three_day_high = None
			self.three_day_low = None
			self.seven_day_high = None
			self.seven_day_low = None


		# if len(self.dates) > 3:
		# 	self.fifteen_min_low = min(self.lows[-3:])
		# 	self.fifteen_min_high = max(self.highs[-3:])



		self.three_day_high_list.append(self.three_day_high)
		self.three_day_low_list.append(self.three_day_low)
		self.seven_day_high_list.append(self.seven_day_high)
		self.seven_day_low_list.append(self.seven_day_low)

		# if self.high_index is not None and self.low_index is not None and len(self.open_value)> 0:



		# 	if self.three_day_high_list[-2] != self.three_day_high_list[-1]:
		# 		#self.high_index, self.three_day_high = max(enumerate(self.highs[-1440*3:-1:]), key=operator.itemgetter(1))
		# 		#print("NEW HIGH POINT!")


		# 		candleTime = time.mktime(self.high_date.timetuple())
		# 		candleTime = datetime.datetime.fromtimestamp(candleTime)
		# 		#print(self.open_value)
		# 		self.min_Open_Intererest_at_high = self.open_value[(self.open_value.index >= candleTime)]['close'].min()
		# 		print('MIN OI FROM HIGH')
		# 		print(len(self.open_value[(self.open_value.index >= candleTime)]['low']))
		# 		print(self.min_Open_Intererest_at_high)
		# 		#print(self.open_value)
		# 	if self.three_day_low_list[-2] != self.three_day_low_list[-1]:
		# 		#print("NEW LOW POINT!")
		# 		candleTime = time.mktime(self.low_date.timetuple())
		# 		candleTime = datetime.datetime.fromtimestamp(candleTime)
		# 		self.min_Open_Intererest_at_low = self.open_value[(self.open_value.index >= candleTime)]['close'].min()
		# 		print("MIN OI FROM LOW")
		# 		print(len(self.open_value[(self.open_value.index >= candleTime)]['low']))
		# 		print(self.min_Open_Intererest_at_low)

		# 	if self.min_Open_Intererest_at_high is not None and self.min_Open_Intererest_at_low is not None:
		# 		self.accumulation_since_high = self.open_value['close'][-1:].item() - self.min_Open_Intererest_at_high
		# 		self.accumulation_since_low = self.open_value['close'][-1:].item() - self.min_Open_Intererest_at_low


		

		# if len(self.dates) > 12*36:
		# 	#three_days_ago = time.mktime(self.dates[-12*36].timetuple())
		# 	last_high_date = time.mktime(self.high_date.timetuple())
		# 	last_low_date = time.mktime(self.low_date.timetuple())

		# 	last_high_date = datetime.datetime.fromtimestamp(last_high_date)
		# 	last_low_date = datetime.datetime.fromtimestamp(last_low_date)


		# 	self.accumulation_since_high = self.open_value['close'][-1:].item() - self.open_value[self.open_value.index > last_high_date]['low'].min()
		# 	self.accumulation_since_low = self.open_value['close'][-1:].item() - self.open_value[self.open_value.index > last_low_date]['low'].min()

		# 	self.accumulation_since_high_list.append(self.accumulation_since_high)
		# 	self.accumulation_since_low_list.append(self.accumulation_since_low)
		# else:
		# 	self.accumulation_since_high_list.append(None)
		# 	self.accumulation_since_low_list.append(None)

		#updating Indicators
		#print(self.strategy_start,self.dates[-1:])
		
		
		
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		
		total = self.account_size[-1]*self.Leverage

		stoploss = None
		#market_order = 'zero'
		market_order = True

		# if self.candle_count > 10*60:
		# 	if max(self.highs[-60*48:-1]) <  self.three_day_high:
		# 		self.long_target_scaler = 6
		# 	elif max(self.highs[-60*24:-1]) <  self.three_day_high:
		# 		self.long_target_scaler = 3
		# 	elif max(self.highs[-59*12:-1]) <  self.three_day_high:
		# 		self.long_target_scaler = 1.5
		# 	elif max(self.highs[-59*6:-1]) <  self.three_day_high:
		# 		self.long_target_scaler = 1

		# 	if min(self.lows[-60*48:-1]) >  self.three_day_low:
		# 		self.short_target_scaler = 6
		# 	elif min(self.lows[-60*24:-1]) >  self.three_day_low:
		# 		self.short_target_scaler = 3
		# 	elif min(self.lows[-59*6:-1]) >  self.three_day_low:
		# 		self.short_target_scaler = 1.5
		# 	elif min(self.lows[-59*6:-1]) >  self.three_day_low:
		# 		self.short_target_scaler = 1
		


		#self.long_target_scaler = 1
		#self.short_target_scaler = 1


		# if self.accumulation_since_high_list[-1] is not None and self.accumulation_since_low_list[-1] is not None:
		# 	accumulation_high = self.accumulation_since_high_list[-1]/1000000
		
		# 	accumulation_low = self.accumulation_since_low_list[-1]/1000000
		# else:
		# 	accumulation_high = 0
		# 	accumulation_low = 0

		#print(accumulation_low)
		#print(accumulation_high)



		# if accumulation_high  > 100:
		# 	self.time_scaler_high = 2*2
		# elif accumulation_high  > 60:
		# 	self.time_scaler_high = 1.5*2
		# elif accumulation_high  > 40:
		# 	self.time_scaler_high = 1.25*2
		# else:
		# 	self.time_scaler_high = 1*2


		# if accumulation_low  > 100:
		# 	self.time_scaler_low = 2*2
		# elif accumulation_low  > 60:
		# 	self.time_scaler_low = 1.5*2
		# elif accumulation_low  > 40:
		# 	self.time_scaler_low = 1.25*2
		# else:
		# 	self.time_scaler_low = 1*2

		self.time_scaler_low = 1
		self.time_scaler_high = 1


		
		if self.candle_count > 1440*5:
			#if self.highs[-1] > self.three_day_high/(1.00 + self.skew) and max(self.three_day_high_list[-60*6:]) == min(self.three_day_high_list[-60*6:]) or self.highs[-1] > self.seven_day_high/(1.00 + self.skew) and max(self.seven_day_high_list[-60*6:]) == min(self.seven_day_high_list[-60*6:]):# and self.RSI[-1] > 70:
			if self.highs[-1] > self.three_day_high/(1.00 + self.skew) and max(self.highs[-self.min_accumulation_time:-1]) <  self.three_day_high and abs(self.three_day_high/(1.00 + self.skew) - self.closes[-6])/self.closes[-6] < self.distance_to_entry:# and max(self.three_day_high_list[-60*6:]) == min(self.three_day_high_list[-60*6:]) or self.highs[-1] > self.seven_day_high/(1.00 + self.skew) and max(self.seven_day_high_list[-60*6:]) == min(self.seven_day_high_list[-60*6:]):# and self.RSI[-1] > 70:
				action = True
				Long = True
				price = self.three_day_high/(1.00 + self.skew)
				stoploss = price/(1+self.stoploss)
					
			#if self.lows[-1] < self.three_day_low*(1.00 + self.skew) and min(self.three_day_low_list[-60*6:]) == max(self.three_day_low_list[-60*6:]) or self.lows[-1] < self.seven_day_low*(1.00 + self.skew) and min(self.seven_day_low_list[-60*6:]) == max(self.seven_day_low_list[-60*6:]):# and self.RSI[-1] < 30:
			if self.lows[-1] < self.three_day_low*(1.00 + self.skew) and min(self.lows[-self.min_accumulation_time:-1]) >  self.three_day_low and abs(self.three_day_low/(1.00 + self.skew) - self.closes[-6])/self.closes[-6] < self.distance_to_entry:
				action = True
				Long = False
				price = self.three_day_low*(1.00 + self.skew)
				stoploss = price*(1+self.stoploss)
		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for Trade in trades:
				avg_open +=Trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:




		if trade.long == True:
			onside_percentage = (self.closes[-2] - trade.entryPrice)/trade.entryPrice
			time_scaler = self.time_scaler_high
		elif trade.long == False:
			onside_percentage = (trade.entryPrice - self.closes[-2])/trade.entryPrice
			time_scaler = self.time_scaler_low

		
	
		time_scaler = 1
		time_scaler = 1



		if onside_percentage > 0.0 and onside_percentage < 0.005:
			time_before_exit = int(60*time_scaler)
		elif onside_percentage > 0.005 and onside_percentage < 0.01:
			time_before_exit = int(120*time_scaler)
		elif onside_percentage > 0.01 and onside_percentage < 0.02:
			time_before_exit = int(180*time_scaler)
		elif onside_percentage > 0.02 and onside_percentage < 0.04:
			time_before_exit = int(240*time_scaler)
		elif onside_percentage > 0.04 and onside_percentage < 0.08:
			time_before_exit = int(300*time_scaler)
		elif onside_percentage > 0.08 and onside_percentage < 0.016:
			time_before_exit = int(360*time_scaler)
		else:
			time_before_exit = int(30*time_scaler)


		
		time_before_exit = self.time_before_exit
		







		if self.candle_count > 25 and trade.trade_length > 10:
			if trade.long == True and min(self.three_day_high_list[-time_before_exit:]) == max(self.three_day_high_list[-time_before_exit:]):
				action = True
				price = self.closes[-1]
				print('\n')
				print("CLOSED")
				print(time_scaler)
				print(time_before_exit)

			if trade.long == False and min(self.three_day_low_list[-time_before_exit:]) == max(self.three_day_low_list[-time_before_exit:]):
				action = True
				price = self.closes[-1]
				print('\n')
				print("CLOSED")
				print(time_scaler)
				print(time_before_exit)

			
			



			#Hedging Exit
		#print(self.highs[-1])
		#print(trade.entryPrice*((self.stoploss*5)+1))
		# if trade.long == True and self.highs[-1] > trade.entryPrice*((self.stoploss*self.long_target_scaler)+1):# and self.highs[-6] > self.highs[-4] and self.highs[-6] > self.highs[-3] and self.highs[-6] > self.highs[-2]:
		# 	action = True
		# 	price = trade.entryPrice*((self.stoploss*self.long_target_scaler)+1)
		# 	print('scaler and time of close')
		# 	print(self.long_target_scaler,self.dates[-1])
		
		# 	#price = self.sell_price1
		
		# if trade.long == False and self.lows[-1] < trade.entryPrice/((self.stoploss*self.short_target_scaler)+1):# and self.lows[-6] < self.lows[-4] and self.lows[-6] < self.lows[-3] and self.lows[-6] < self.lows[-2]:
		# 	action = True
		# 	price = trade.entryPrice/((self.stoploss*self.short_target_scaler)+1)
		# 	print('scaler and time of close')
		# 	print(self.short_target_scaler,self.dates[-1])

	




		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def Draw_indicators(self):

		
		print(len(self.three_day_high_list))
		print(len(self.three_day_low_list))
		print(len(self.seven_day_high_list))
		print(len(self.seven_day_low_list))

		print(len(self.dates))

		return (self.three_day_high_list,self.three_day_high_list,self.three_day_low_list,self.seven_day_high_list,self.seven_day_low_list,self.ATR,self.ATR_ROOT)#,self.accumulation_since_high_list,self.accumulation_since_low_list,self.RSI,self.open_value)
		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.stoplosses,self.open_value,self.max_risks,self.leverages,self.max_risks,self.five_m_dates)#,self.ATR_min_list)
		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.ATR_switches,self.OI_density,self.closing_line,self.five_m_dates)#,self.ATR_min_list)
