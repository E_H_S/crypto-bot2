import pandas as pd 



trades_df = pd.read_csv('trade_df.csv')
trades_df['long_profit'] = (trades_df[trades_df['long'] == 1]['exit_price'] - trades_df[trades_df['long'] == 1]['entry_price'])/trades_df[trades_df['long'] == 1]['entry_price']
print(trades_df.columns)
trades_df[trades_df['stoplossed'] == 0]['long_profit'] += 0.0005
trades_df[trades_df['stoplossed'] == 1]['long_profit'] -= 0.0005

trades_df['short_profit'] = (trades_df[trades_df['long'] == 0]['entry_price'] - trades_df[trades_df['long'] == 0]['exit_price'])/trades_df[trades_df['long'] == 0]['entry_price']
trades_df[trades_df['stoplossed'] == 0]['short_profit'] += 0.0005
trades_df[trades_df['stoplossed'] == 1]['short_profit'] -= 0.0005

trades_df['long_profit'] = trades_df['long_profit'].fillna(0)
trades_df['short_profit'] = trades_df['short_profit'].fillna(0)


trades_df['final_profit'] = trades_df['long_profit'] + trades_df['short_profit']
trades_df['net_time_on_side'] = trades_df['time_on_side'] - trades_df['time_off_side']
trades_df['net_time_on_side_hours'] = trades_df['net_time_on_side']/12
trades_df['max_pct_off_side_long'] = (trades_df[trades_df['long'] == 1]['lowest_low'] - trades_df[trades_df['long'] == 1]['entry_price'])/trades_df[trades_df['long'] == 1]['entry_price']
trades_df['max_pct_off_side_short'] = (trades_df[trades_df['long'] == 0]['entry_price']- trades_df[trades_df['long'] == 0]['highest_high'])/trades_df[trades_df['long'] == 0]['entry_price']

trades_df['max_pct_off_side_long'] = trades_df['max_pct_off_side_long'].fillna(0)
trades_df['max_pct_off_side_short'] = trades_df['max_pct_off_side_short'].fillna(0)
trades_df['max_pct_off_side'] = trades_df['max_pct_off_side_long'] + trades_df['max_pct_off_side_short']
trades_df['take_profit_percentage_final'] = abs(trades_df['take_profit_percentage'].round(3))
trades_df['entry_ATR_rounded'] = trades_df['entry_ATR'].round(3)





#trades_df['short_profit']

print(trades_df['long_profit'])
print(trades_df['short_profit'])
print(trades_df['final_profit'])

print(trades_df[['entry_ATR_rounded']])

trades_df.to_csv('processed_trades_df1.csv')

#trades['long_profit'] = 
#print(trades_df)