from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

#Strategy_on UTC TIMES  [(1.5-2.5),(7.5-11),(17.5 - 19.5)]
		#sydney times.  [(11.5-12.5),(17.5-21),(3.5-5.5)]



		#low volitility [Monday]
		#medium volility[Thursday, Saturday,Sunday]
		#Tuesday-Friday tighten Saturday


# SMA_M uses 3 indicators and will require an imput perameter for each


class VPOC_ANCHORED_ALWAYS_ON(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = True,anchored = True,open_value_candles = True,momentum = True)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		

		self.ATR = []
		self.ATR_14 = []
		self.stoplosses = []
		self.max_risks = []

		self.ob_index = None


		self.VPOC = []
		self.max_risk = 0.08

		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []
		
		self.Leverage = 0.2


		self.mid_line = [None,None]

		if p1 != None:
			self.drop_percent = p1
		else:
			self.drop_percent = 0.04

			#self.drop_percent = 0.02

		if p2 != None:
			self.stoploss = p2
		else:
			self.stoploss = 0.05

		if p3 != None:
			self.clip_size = p3
		else:
			self.clip_size = 1500


		self.max_accumulation = 0.1
		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None

		self.leverages = []

		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.volitilitys =[]
		self.account_size = []
		self.latest_range = []

		self.open_value = np.array([])

		self.middle = None

		self.rolling_period = 34
		self.ATR_rolling_period = 288

		self.min_initial_take_profit_candles = 0
		self.min_initial_take_profit = 0


		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True



		self.b_1_ready = True
		self.b_2_ready = True
		self.b_3_ready = True

		self.five_m_dates = []

		self.s_1_ready = True
		self.s_2_ready = True
		self.s_3_ready = True

		self.strategy_start = None
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.last_OV_min = None


		self.ATR_switch = False
		
		self.ATR_min_list = []
		self.ATR_switches = []

		self.numSimulTrades = 30000000
		self.margin = True

		self.closing_line = []
		self.volume_profile = None

		self.OI_density = []
		self.trigger_date = None
		self.trigger_dates = []

		self.OI_ACCUMULATED =[]


		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0
		self.timer4 = 0
		self.timer5 = 0
		self.timer6 = 0
		self.timer7 = 0
		self.timer8 = 0

		self.last_stoplossed_trade = None
		


		self.count10 = 0
		self.switch10 = False

		self.candle_count = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):

		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		self.t2 = datetime.datetime.utcnow()

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])



		

		#Update relevent indicators
		if np.mod(self.dates[-1].minute,5) == 0:
			self.five_m_dates.append(self.dates[-1])
			self.tick_indicators(trades)
	

	def tick_indicators(self,trades):
		#updating Indicators
		#print(self.strategy_start,self.dates[-1:])
		self.candle_count +=1
		if self.trigger_date is not None:
			self.trigger_date += 1
			self.trigger_dates.append(self.trigger_date)


		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		

		self.t3 = datetime.datetime.utcnow()
		self.ATR.append(self.indicators.avg_range(self.closes,self.highs,self.lows,int(self.ATR_rolling_period)))

		if len(self.dates)>2:
			self.open_value = self.indicators.get_open_value(self.dates[-3],candle = True)


		if self.ATR[-1] is not None:
			if self.ATR[-1] > 0.004:
				self.drop_percent = 0.075
				self.stoploss = 0.08
			if self.ATR[-1] > 0.003:
				self.drop_percent = 0.065
				self.stoploss = 0.07
			if self.ATR[-1] > 0.002:
				self.drop_percent = 0.055
				self.stoploss = 0.06
			if self.ATR[-1] > 0.001:
				self.drop_percent = 0.045
				self.stoploss = 0.05
			if self.ATR[-1] > 0.00075:
				self.drop_percent = 0.04
				self.stoploss = 0.04
			else:
				self.drop_percent = 0.035
				self.stoploss = 0.03


		if self.candle_count > 2:
			self.momentum = self.indicators.get_momentum(self.dates[-3])
			momentum_tweak = -(self.momentum[-1:].item())*4
		else:
			momentum_tweak = 0

		
		if momentum_tweak > 0.15:
			momentum_tweak = 0.15
		if momentum_tweak < -0.15:
			momentum_tweak = -0.15

	
		bpc1 = 0.74 + momentum_tweak
		bpc2 = 0.80 + momentum_tweak*(0.8)
		bpc3 = 0.86 + momentum_tweak*(2/3)

		spc1 = 0.26 + momentum_tweak
		spc2 = 0.2 + momentum_tweak*(0.8)
		spc3 = 0.14 + momentum_tweak*(2/3)
		


		if self.ATR[-1] is not None:
			if len(self.open_value['max_delta_OI0'][-1:]) > 0:
				biggest_drop = self.open_value['MAX_MAX_delta_Event_OI'][-1:].item()
				
				if biggest_drop < -self.open_value['high'][-36:].max()*self.drop_percent and self.switch10 == False:# and abs(self.closes[-1:]-self.closes[-1:]) > self.closes[-1:]*0.035: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 = True
					#self.calculate_clip_size()
					self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					self.last_OV_high = self.open_value['high'][-36:].max()
					self.last_OV_min = self.open_value['low'][-36:].min()
					self.ATR_switch = True
					self.latest_range = []


					if self.ATR_switches[-1] == 0:# and trades[-3:-2][0].status == "CLOSED" and trades[-2:-1][0].status == "CLOSED" and trades[-1:][0].status == "CLOSED":
						self.trigger_date = 0
						#if len(trades) == 0 or trades[-1].status == "CLOSED":#and trades[-2:-1].status == "CLOSED" and trades[-1:].status == "CLOSED":
						self.strategy_start = self.dates[-1]
					print('\n')
					print("NEW TRIGGER EVENT! " , self.dates[-1])
					print(biggest_drop)
					print("MIN ATR ", self.ATR_min)
					print('\n')

				if self.ATR[-1] != None:
					if min(self.ATR_switches[-12*24:]) == 1:
						if self.ATR[-1] < self.ATR_min*1 and self.ATR_switch == True:
							self.ATR_switch = False
							#self.strategy_start = None
							print('\n')
							print('FAILED ATR SWITCH ', self.dates[-1:])
							print('\n')
					
					
				if self.open_value['MAX_MAX_delta_Lookback_OI'][-1:].item() > -self.open_value['high'][-36:].max()*self.drop_percent and self.ATR_switch == True:
				#if self.open_value['MAX_MAX_delta_Lookback_OI'][-1:].item() > -self.open_value['high'][-36:].max()*1 and self.ATR_switch == True:
					self.ATR_switch = False
					#self.strategy_start = None
					print('\n')
					print('5 day OV drop expired! ', self.dates[-1:])
					print('\n')

				#if self.open_value['high'][-1].item() > self.last_OV_high + self.open_value['high'][-6*180:].max()*self.drop_percent and self.ATR_switch == True:
				#if self.open_value['high'][-1].item() > self.last_OV_high + self.open_value['high'][-6*180:].max()*1 and self.ATR_switch == True:
				if self.last_OV_min is not None:
					if self.open_value['high'][-1].item() > self.last_OV_min*(1+self.max_accumulation) and self.ATR_switch == True:
						self.ATR_switch = False
						#self.strategy_start = None
						print('\n')
						print('OVER THE OV MAX ', self.dates[-1:])
						print('OV MAX ',self.last_OV_high + self.open_value['high'][-36:].max()*self.drop_percent)
						print("LAST OI ",self.open_value['close'][-1].item())
						print('\n')
			
			
		self.t6 = datetime.datetime.utcnow()
		
		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)


		self.t7 = datetime.datetime.utcnow()

		if len(self.dates)> 10:
			
			if self.strategy_start is None:
				self.strategy_start = self.dates[0]
			self.volume_profile = self.indicators.get_anchored_VPOC(self.strategy_start,self.dates[-1],hours = self.rolling_period)
			#self.volume_profile = self.indicators.get_anchored_VPOC(self.dates[0],self.dates[-1],hours = self.rolling_period)



			if len(self.volume_profile)> 15:
				try:
					self.VPOC.append(self.volume_profile[self.volume_profile['percent']<0.50].index[-1])
				except:
					self.VPOC.append(self.volume_profile[self.volume_profile['percent']>0.50].index[0])



				if abs(self.momentum[-1].item())> 0:
					percent = 0.50 - (self.momentum[-1:].item())*10
					#percent = 0.5
					if percent > 0.75:
						percent = 0.75
					elif percent < 0.25:
						percent = 0.25
					try:
						self.closing_line.append(self.volume_profile[self.volume_profile['percent']<(percent)].index[-1])
					except:
						self.closing_line.append(self.volume_profile[self.volume_profile['percent']>(1-percent)].index[0])
				else:
					try:
						self.closing_line.append(self.volume_profile[self.volume_profile['percent']<0.50].index[-1])
					except:
						self.closing_line.append(self.volume_profile[self.volume_profile['percent']>0.50].index[0])
			


				try:
					self.stoploss_buy = self.volume_profile[self.volume_profile['percent']>0.95].index[0]
				except:
					self.stoploss_buy = self.volume_profile[self.volume_profile['percent']].index[0]


				try:
					self.sell_price1 = self.volume_profile[self.volume_profile['percent']<spc1].index[-1:][0]
				except:
					self.sell_price1 = self.volume_profile.index[2]

				try:
					self.sell_price2 = self.volume_profile[self.volume_profile['percent']<spc2].index[-1:][0]
				except:
					self.sell_price2 = self.volume_profile.index[1]
				
				try:
					self.sell_price3 = self.volume_profile[self.volume_profile['percent']<spc3].index[-1:][0]
				except:
					self.sell_price3 = self.volume_profile.index[0]


				try:
					self.stoploss_buy = self.volume_profile[self.volume_profile['percent']>0.95].index[0]
				except:
					self.stoploss_buy =  self.volume_profile.index[-1]

				try:
					self.buy_price1 = self.volume_profile[self.volume_profile['percent']>bpc1].index[0]
				except:
					self.buy_price1 = self.volume_profile.index[-3]
				try:
					self.buy_price2 = self.volume_profile[self.volume_profile['percent']>bpc2].index[0]
				except:
					self.buy_price2 = self.volume_profile.index[-2]

				try:
					self.buy_price3 = self.volume_profile[self.volume_profile['percent']>bpc3].index[0]
				except:
					self.buy_price3 = self.volume_profile.index[-1]


			else:
				#print('NONEGG!!')
				self.VPOC.append(None)
				self.closing_line.append(None)
				self.buy_price1 = None
				self.buy_price2 = None
				self.buy_price3 = None

				self.sell_price1 = None
				self.sell_price2 = None
				self.sell_price3 = None
				self.t8 = datetime.datetime.utcnow()
		else:
			self.closing_line.append(None)
			self.VPOC.append(None)
			self.t8 = datetime.datetime.utcnow()


		self.mid_line.append(self.middle)

		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)
		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)
		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)

		self.buy_line4.append(self.buy_price1)
		self.sell_line4.append(self.sell_price1)
		self.buy_line5.append(self.buy_price2)
		self.sell_line5.append(self.sell_price2)
		self.buy_line6.append(self.buy_price3)
		self.sell_line6.append(self.sell_price3)

		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3)]





		if self.ATR_switch == False:
			self.ATR_switches.append(0)	
		elif self.ATR_switch == True:
			self.ATR_switches.append(1)

		if self.last_OV_min is not None:
			minimum_ov = self.open_value['low'][-self.trigger_date-6:-self.trigger_date+6].min()
			current_accumulation = (self.open_value['close'][-1].item()/minimum_ov) - 1
			self.OI_ACCUMULATED.append(current_accumulation)
		else:
			self.OI_ACCUMULATED.append(None)

		
		self.ATR_min_list.append(self.ATR_min)
		self.leverages.append(self.Leverage)
		self.stoplosses.append(self.stoploss)
		self.max_risks.append(self.max_risk)

		
		
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = self.account_size[-1]*self.Leverage/3
		stoploss = None
		#market_order = 'zero'
		market_order = False
		
		# if bp == self.buy_price1 and self.b_1_ready == False:
		# 	bp = None
		# elif bp == self.buy_price2 and self.b_2_ready == False:
		# 	bp = None
		# elif bp == self.buy_price3 and self.b_3_ready == False:
		# 	bp = None
		
		# if sp == self.sell_price1 and self.s_1_ready == False:
		# 	sp = None
		# elif sp == self.sell_price2 and self.s_2_ready == False:
		# 	sp = None
		# elif sp == self.sell_price3 and self.s_3_ready == False:
		# 	sp = None
	
		
		if len(self.dates) > 2 and self.closing_line[-1] is not None:	
			if sp != None and self.closing_line[-1] != None:
				if self.closes[-2] < sp and self.highs[-1] > sp:
					#print('Leverage', self.Leverage)
					action = True
					Long = False
					price = sp
					stoploss = price*(1+self.stoploss)
			

			if bp != None and self.closing_line[-1] != None:
				if self.lows[-1] < bp and self.closes[-2] > bp:
					#print('Leverage', self.Leverage)
					action = True
					Long = True
					price = bp
					stoploss = price/(1+self.stoploss)



		# if action == True:
		# 	if Long == True:
		# 		if bp == self.buy_price1:
		# 			self.b_1_ready = False
		# 		elif bp == self.buy_price2:
		# 			self.b_2_ready = False
		# 		elif bp == self.buy_price3:
		# 			self.b_3_ready = False

		# 	if Long == False:
		# 		if sp == self.sell_price1:
		# 			self.s_1_ready = False
		# 		elif sp == self.sell_price2:
		# 			self.s_2_ready = False
		# 		elif sp == self.sell_price3:
		# 			self.s_3_ready = False


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
	

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		


		if self.closing_line[-1] is not None:
			if trade.long == True:
				if self.highs[-1] > self.closing_line[-1] and trade.trade_length > 1:
					action = True
					price = self.closing_line[-1]
				
			if trade.long == False and self.closing_line[-1] is not None:
				if self.lows[-1] < self.closing_line[-1] and trade.long == False and trade.trade_length > 1:
					action = True
					price = self.closing_line[-1]
			

		if action == True:
			self.b_1_ready = True
			self.b_2_ready = True
			self.b_3_ready = True
			self.s_1_ready = True
			self.s_2_ready = True
			self.s_3_ready = True

		return action,price,total,date,amount,self.margin,market_order
				
	def Draw_indicators(self):

		print("ALL INDICATOR LENGTHS")
		print(len(self.VPOC))
		print(len(self.buy_line1))
		print(len(self.buy_line2))
		print(len(self.buy_line3))
		print(len(self.sell_line1))
		print(len(self.sell_line2))
		print(len(self.sell_line3))
		print(len(self.ATR))
		print(len(self.open_value))
		print(len(self.ATR_switches))
		print(len(self.volitilitys))
		print(len(self.closing_line))
		print(len(self.dates))
		print(self.candle_count)
		print(len(self.five_m_dates))



		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.stoplosses,self.open_value,self.max_risks,self.leverages,self.max_risks,self.five_m_dates)#,self.ATR_min_list)
		return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.OI_ACCUMULATED,self.OI_density,self.closing_line,self.five_m_dates)#,self.ATR_min_list)

