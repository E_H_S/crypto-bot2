import sqlite3 as sql
import pandas as pd
import datetime
#import plotly.offline as pyo
#import plotly.graph_objs as go
import numpy as np


con = sql.connect("Database1")
cur = con.cursor()

table = 'XBT_instrument'
st = datetime.datetime(2019, 4, 17,3,15,0) - datetime.timedelta(hours = 10)
et  = datetime.datetime(2019, 4, 17,3,20,0) - datetime.timedelta(hours = 10)

start_UTC = st
end_UTC = et

#st = datetime.datetime.utcnow() - datetime.timedelta(hours =10)
#et = datetime.datetime.utcnow()


#start_UTC = st.strftime("%Y-%m-%dT%H:%M:%S.%fZ") 
#end_UTC = et.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
print(datetime.datetime.utcnow())

with con:
    cur.execute("""

        SELECT DISTINCT
        *
        FROM XBT_orderbook
        WHERE 1=1
        and time_stamp > ?
        and time_stamp < ?
        ORDER BY time_stamp ASC
        
        """,(start_UTC , end_UTC))

    data = cur.fetchall()

data = pd.DataFrame(data)
print(data)

