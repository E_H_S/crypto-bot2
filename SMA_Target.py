from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class SMA_M_T(object):
	def __init__(self, period = 14400):

		self.fast = 12
		self.slow = 24
		self.target = 1.15
		self.stoploss = 1.02

		self.period = period

		self.indicators = BotIndicators()
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.mavs3  = np.array([])
		self.RSI = np.array([])

		self.momentum_F = np.array([])
		self.momentum_S = np.array([])

		self.numSimulTrades = 1
		self.margin = True
		
		self.scale = 14400/self.period

		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}

		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		#updating Indicators		
		self.mavs = np.append(self.mavs,[self.indicators.EMA(self.average_prices,int(self.fast*self.scale))])
		self.mavs3 = np.append(self.mavs3,[self.indicators.EMA(self.average_prices,int(6))])
		self.momentum_F = np.append(self.momentum_F,[self.indicators.momentum(self.average_prices,period =int(self.fast*self.scale))])
		self.momentum_S = np.append(self.momentum_S,[self.indicators.momentum(self.average_prices,period =int(self.slow*self.scale))])
		self.RSI = np.append(self.RSI,self.indicators.RSI(self.average_prices))
		if len(self.mavs) < int(self.fast*self.scale) + 2:
			self.mavs_delta = np.append(self.mavs_delta,[None])
		else:
			self.mavs_delta = np.append(self.mavs_delta,[self.mavs[-1:]/self.mavs[-2:-1]])

		self.mav2L = (self.slow*self.scale)

		self.mavs2 = np.append(self.mavs2,[self.indicators.EMA(self.average_prices,int(self.mav2L))])
		
	def evaluate_Open(self):
		action = False
		Long = None
		price = None
		date = int(self.dates[-1:])
		amount = 100
		stoploss = None
		market_Order = True

		 if len(self.dates)> self.longest_indicator+1:
		 	#if self.mavs_delta[-1:] < 1 and self.mavs[-1:] < self.mavs2[-1:] and self.RSI[-1] > 60:
		# 	if self.mavs[-2:-1] < self.mavs2[-2:-1] and self.mavs3[-2:-1] > self.mavs[-2:-1]:
		# 		open_target = self.closes[-2:-1]*1.005
		# 		if self.highs[-1:] > open_target:
		# 			action = True
		# 			Long = False
		# 			price = open_target
		# 			stoploss = price*self.stoploss
		# 			market_Order = False
				
						
		# 	#if self.mavs_delta[-1:] > 1 and self.mavs[-1:] > self.mavs2[-1:] and self.RSI[-1] < 40 :
		# 	if self.mavs[-2:-1] > self.mavs2[-2:-1] and self.mavs3[-2:-1] < self.mavs[-2:-1]:
		# 		open_target = self.closes[-2:-1]/1.005
		# 		if self.lows[-1:] < open_target:
		# 			action = True
		# 			Long = True
		# 			price = open_target
		# 			stoploss = price/self.stoploss
		# 			market_Order = False
				
		return action,Long,price,date,amount,self.margin,stoploss,market_Order
	def evaluate_Close(self,trade):

		action = False
		price = None
		date = int(self.dates[-1:])
		amount = 100
		market_order = True


		if trade.long == False and self.lows[-1:] < trade.entryPrice/self.target:
			action = True
			price = trade.entryPrice/self.target
			market_order = False
					
		if trade.long == True and self.highs[-1:] > trade.entryPrice*self.target:
			action = True
			price = trade.entryPrice*self.target
			market_order = False
		
		return action,price,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.mavs,self.mavs2,self.mavs3)

