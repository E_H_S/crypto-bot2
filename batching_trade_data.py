import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MaxAbsScaler


data = pd.read_csv('USDT_BTC_2016_4H.csv',index_col ='Unnamed: 0')

data.index.names = ['Date']
data.head()
data.tail()
data.plot(x=data.index,y="est_cur_val")
data.info()
train_test_set = data.head(4387)
train_test_set.tail()
holdout_set = data.tail(5404 - 4387)
holdout_set.tail()
print(train_test_set.iloc[10,5])


def create_batches(training_data,batch_size,steps):
    batches = []
    row_length,col_length = np.shape(training_data)
    for i in np.arange(0,row_length,1):
        if training_data.iloc[i,18] == "OPEN":
        
            batch_labels = training_data.iloc[i-(steps-1):i+1,[4,18]]
            batch_labels = np.array(batch_labels['averageprice'])
        
            for k in np.arange(i,row_length,1):
                if training_data.iloc[k,18] == "CLOSED":
                
                    sell_price = training_data.iloc[k,[9,18]]
                    sell_price = np.array(sell_price[0])
                
                    batch_string = batch_labels,sell_price
                   # print(batch_string)
                    batches.append(batch_string)
                    break
    return batches



def scale_batch(batches):
    scaled_batches = []
    for i in np.arange(0,len(batches),1):
        obs,label = batches[i]
        obs = obs.reshape(-1,1)
        label = label.reshape(-1,1)
    
    
    
        scaler = MaxAbsScaler()
        obs = scaler.fit_transform(obs)
        label = scaler.transform(label)
    
        append_me = (obs,label)
        scaled_batches.append(append_me)

    return scaled_batches

def next_batch(batches):
    rand_index = np.random.randint(0,len(batches))
    obs,label = batches[rand_index]
    return obs.reshape(-1, 5, 1) ,label.reshape(-1, 1, 1)
    






batches = create_batches(train_test_set,1,5)
batches
print(np.shape(batches))
scaled_batches = scale_batch(batches)
xt,yt = next_batch(scaled_batches)
print(xt)
print(yt)





