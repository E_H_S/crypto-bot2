import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
import json
import time
import hmac,hashlib
import requests
import datetime
 
def createTimeStamp(datestr, format="%Y-%m-%d %H:%M:%S"):
    return time.mktime(time.strptime(datestr, format))
 
class poloniex:
    def __init__(self, APIKey, Secret):
        self.APIKey = APIKey
        self.Secret = Secret
        self.delay = [2,5,15,30,60,600,7200]
 
    def post_process(self, before):
        after = before
 
        # Add timestamps if there isnt one but is a datetime
        if('return' in after):
            if(isinstance(after['return'], list)):
                for x in range(0, len(after['return'])):
                    if(isinstance(after['return'][x], dict)):
                        if('datetime' in after['return'][x] and 'timestamp' not in after['return'][x]):
                            after['return'][x]['timestamp'] = float(createTimeStamp(after['return'][x]['datetime']))
                           
        return after
 
    def api_query(self, command, req={}):
        received = False
        count = 0
        while received == False:
            try:
                if(command == "returnTicker" or command == "return24Volume"):
                    ret  = requests.post('https://poloniex.com/public?command=' + command,)
                    time.sleep(1)
                    received = True
                    return json.loads(ret.text)

                elif(command == "returnOrderBook"):
                    time.sleep(1)
                    ret  = requests.post('https://poloniex.com/public?command=' + command + '&currencyPair=' + str(req['currencyPair']))
                    received = True
                    return json.loads(ret.text)

                elif(command == "returnMarketTradeHistory"):
                    time.sleep(1)
                    ret = requests.post('https://poloniex.com/public?command=' + "returnTradeHistory" + '&currencyPair=' + str(req['currencyPair']) + '&start=' + str(req['start']) + '&end=' + str(req['end']))
                    received = True
                    return json.loads(ret.text)

                elif(command == "returnChartData"):
                    time.sleep(1)
                    ret = requests.post('https://poloniex.com/public?command=returnChartData&currencyPair=' + str(req['currencyPair']) + '&start=' + str(req['start']) + '&end=' + str(req['end']) + '&period=' + str(req['period']))
                    received = True
                    return json.loads(ret.text)

                else:
                    req['command'] = command
                    req['nonce'] = int(time.time()*1000)
                    post_data = urllib.parse.urlencode(req)
                    sign = hmac.new(bytes(self.Secret,'latin-1'), bytes(post_data,'latin-1'), hashlib.sha512).hexdigest()
                    headers = {
                    'Sign': sign,
                    'Key': self.APIKey
                    }
                    time.sleep(1)
                    ret = requests.post('https://poloniex.com/tradingApi', data=req, headers=headers)
                    jsonRet = json.loads(ret.text)
                    received = True
                    return self.post_process(jsonRet)
            except:
                print("API Call Failed! waiting for " + str(self.delay[count])+ ' Seconds before trying again!')
                time.sleep(self.delay[count])
                count = count+1
            




 
 
    def returnTicker(self):


        retty = self.api_call(self.api_query("returnTicker"))
    
        return self.api_call(self.api_query("returnTicker"))
 
    def return24Volume(self):
        return self.api_call(self.api_query("return24Volume"))
 
    def returnOrderBook (self, currencyPair):
        return self.api_call(self.api_query("returnOrderBook", {'currencyPair': currencyPair}))
 
    def returnMarketTradeHistory (self, currencyPair):
        return self.api_call(self.api_query("returnMarketTradeHistory", {'currencyPair': currencyPair}))
 
 
    # Returns all of your balances.
    # Outputs:
    # {"BTC":"0.59098578","LTC":"3.31117268", ... }
    def returnBalances(self):
        retty = self.api_call(self.api_query('returnBalances'))
        return retty
 
    # Returns your open orders for a given market, specified by the "currencyPair" POST parameter, e.g. "BTC_XCP"
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # Outputs:
    # orderNumber   The order number
    # type          sell or buy
    # rate          Price the order is selling or buying at
    # Amount        Quantity of order
    # total         Total value of order (price * quantity)
    def returnOpenOrders(self,currencyPair):
        retty = self.api_call(self.api_query('returnOpenOrders',{"currencyPair":currencyPair}))
        return retty
 
 
    # Returns your trade history for a given market, specified by the "currencyPair" POST parameter
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # Outputs:
    # date          Date in the form: "2014-02-19 03:44:59"
    # rate          Price the order is selling or buying at
    # amount        Quantity of order
    # total         Total value of order (price * quantity)
    # type          sell or buy
    def returnTradeHistory(self,currencyPair):
        return self.api_call(self.api_query('returnTradeHistory',{"currencyPair":currencyPair}))
 
    # Places a buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is buying at
    # amount        Amount of coins to buy
    # Outputs:
    # orderNumber   The order number
    def buy(self,currencyPair,rate,amount):
        return self.api_call(self.api_query('buy',{"currencyPair":currencyPair,"rate":rate,"amount":amount}))
 
    # Places a sell order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is selling at
    # amount        Amount of coins to sell
    # Outputs:
    # orderNumber   The order number
    def sell(self,currencyPair,rate,amount):
        return self.api_call(self.api_query('sell',{"currencyPair":currencyPair,"rate":rate,"amount":amount}))
 
    # Cancels an order you have placed in a given market. Required POST parameters are "currencyPair" and "orderNumber".
    # Inputs:
    # currencyPair  The curreny pair
    # orderNumber   The order number to cancel
    # Outputs:
    # succes        1 or 0
    def cancel(self,currencyPair,orderNumber):
        return self.api_call(self.api_query('cancelOrder',{"currencyPair":currencyPair,"orderNumber":orderNumber}))
 
    # Immediately places a withdrawal for a given currency, with no email confirmation. In order to use this method, the withdrawal privilege must be enabled for your API key. Required POST parameters are "currency", "amount", and "address". Sample output: {"response":"Withdrew 2398 NXT."}
    # Inputs:
    # currency      The currency to withdraw
    # amount        The amount of this coin to withdraw
    # address       The withdrawal address
    # Outputs:
    # response      Text containing message about the withdrawal
    def withdraw(self, currency, amount, address):
        return self.api_call(self.api_query('withdraw',{"currency":currency, "amount":amount, "address":address}))
    
    def api_call(self,func):
        conection = False
        count = 0
        while conection == False:
            try:
                retty = func
                connection = True
                return retty
            except:
                print("API Call Failed! waiting for " + str(self.delay[count])+ ' Seconds before trying again!')
                time.sleep(self.delay[count])
                count = count+1


    def transferBalance(self, currency, amount,fromAccount, toAccount, confirmed=False):
        """ Transfers funds from one account to another (e.g. from your
        exchange account to your margin account). Required parameters are
        "currency", "amount", "fromAccount", and "toAccount" """
        args = {
            'currency': str(currency).upper(),
            'amount': str(amount),
            'fromAccount': str(fromAccount),
            'toAccount': str(toAccount)
        }
        if confirmed:
            args['confirmed'] = 1
        return self.api_query('transferBalance', args)

    def returnMarginAccountSummary(self):
        """ Returns a summary of your entire margin account. This is the same
        information you will find in the Margin Account section of the Margin
        Trading page, under the Markets list """
        return self.api_query('returnMarginAccountSummary')

    def marginBuy(self, currencyPair, rate, amount, lendingRate=0.02):
        """ Places a margin buy order in a given market. Required parameters are
        "currencyPair", "rate", and "amount". You may optionally specify a
        maximum lending rate using the "lendingRate" parameter (defaults to 2).
        If successful, the method will return the order number and any trades
        immediately resulting from your order. """
        return self.api_query('marginBuy', {
            'currencyPair': str(currencyPair).upper(),
            'rate': str(rate),
            'amount': str(amount),
            'lendingRate': str(lendingRate)
        })

    def marginSell(self, currencyPair, rate, amount, lendingRate=0.02):
        """ Places a margin sell order in a given market. Parameters and output
        are the same as for the marginBuy method. """
        return self.api_query('marginSell', {
            'currencyPair': str(currencyPair).upper(),
            'rate': str(rate),
            'amount': str(amount),
            'lendingRate': str(lendingRate)
        })

    def getMarginPosition(self, currencyPair='all'):
        """ Returns information about your margin position in a given market,
        specified by the "currencyPair" parameter. You may set
        "currencyPair" to "all" if you wish to fetch all of your margin
        positions at once. If you have no margin position in the specified
        market, "type" will be set to "none". "liquidationPrice" is an
        estimate, and does not necessarily represent the price at which an
        actual forced liquidation will occur. If you have no liquidation price,
        the value will be -1. (defaults to 'all')"""
        return self.api_query('getMarginPosition', {
                             'currencyPair': str(currencyPair).upper()})

    def closeMarginPosition(self, currencyPair):
        """ Closes your margin position in a given market (specified by the
        "currencyPair" parameter) using a market order. This call will also
        return success if you do not have an open position in the specified
        market. """
        return self.api_query(
            'closeMarginPosition', {'currencyPair': str(currencyPair).upper()})

    def createLoanOffer(self, currency, amount,
                        lendingRate, autoRenew=0, duration=2):
        """ Creates a loan offer for a given currency. Required parameters are
        "currency", "amount", "lendingRate", "duration" (num of days, defaults
        to 2), "autoRenew" (0 or 1, defaults to 0 'off'). """
        return self.api_query('createLoanOffer', {
            'currency': str(currency).upper(),
            'amount': str(amount),
            'duration': str(duration),
            'autoRenew': str(autoRenew),
            'lendingRate': str(lendingRate)
        })

    def cancelLoanOffer(self, orderNumber):
        """ Cancels a loan offer specified by the "orderNumber" parameter. """
        return self.api_query(
            'cancelLoanOffer', {'orderNumber': str(orderNumber)})

    def returnOpenLoanOffers(self):
        """ Returns your open loan offers for each currency. """
        return self.api_query('returnOpenLoanOffers')

    def returnActiveLoans(self):
        """ Returns your active loans for each currency."""
        return self.api_query('returnActiveLoans')

    def returnLendingHistory(self, start=False, end=False, limit=False):
        """ Returns your lending history within a time range specified by the
        "start" and "end" parameters as UNIX timestamps. "limit" may also
        be specified to limit the number of rows returned. (defaults to the last
        months history)"""
        if not start:
            start = time() - self.MONTH
        if not end:
            end = time()
        args = {'start': str(start), 'end': str(end)}
        if limit:
            args['limit'] = str(limit)
        return self.api_query('returnLendingHistory', args)

    def toggleAutoRenew(self, orderNumber):
        """ Toggles the autoRenew setting on an active loan, specified by the
        "orderNumber" parameter. If successful, "message" will indicate
        the new autoRenew setting. """
        return self.api_query(
            'toggleAutoRenew', {'orderNumber': str(orderNumber)})













