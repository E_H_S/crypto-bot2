import sqlite3 as sql
import pandas as pd
import datetime
import plotly.offline as pyo
import plotly.graph_objs as go
import numpy as np





con = sql.connect("Database1")
cur = con.cursor()

table = 'XBT_instrument'
st 	= datetime.datetime(2019, 3,8,10,0,0)
et  = datetime.datetime(2019, 3,9,11,0,0)

#st = datetime.datetime.utcnow() - datetime.timedelta(minutes =10)
#et = datetime.datetime.utcnow()
start_UTC = st
end_UTC =et
#start_UTC = st.strftime("%Y-%m-%dT%H:%M:%S.%fZ") 
#end_UTC = et.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
print(datetime.datetime.utcnow())

with con:
    cur.execute("""

        SELECT DISTINCT
        *
        FROM XBT_orderbook4
        WHERE 1=1
        and time_stamp > ?
        and time_stamp < ?
        ORDER BY time_stamp,price ASC
        
        """,(start_UTC , end_UTC))

    data = cur.fetchall()

data = pd.DataFrame(data)





print(data[-5:])
dates = data[4].unique()
#data[data[3] == dates[0]].plot.bar(x = 0,y =2 )
rb = np.random.randint(len(dates))


book1 = data[data[4] == dates[rb]]
book1 = book1[(book1[3] < 40000000)]

#book1 = data[data[4] == dates[0]]
book1_b = book1[(book1[1] == 'Buy')&(book1[3] > 20000000)]
book1_s = book1[(book1[1] == 'Sell')&(book1[3] > 20000000)]

book1_b.reset_index()
book1_s.reset_index()

book1_nb =  book1[(book1[3] < 20000000)& (book1[1] == 'Buy')]
book1_ns =  book1[(book1[3] < 20000000)& (book1[1] == 'Sell')]


def most_simple(book1_b,book1_s):
	book1_b = book1_b.reset_index()
	book1_s = book1_s.reset_index()
	# print(book1_b)
	# print(book1_s)
	# print("\n")
	# print("\n")
	max_b = book1_b[2].idxmax()
	max_s = book1_s[2].idxmax()
	# print(max_b,max_s)
	# print(type(book1_b[2].idxmax()))
	# print(len(book1_b))
	# print(len(book1_s))
	max_buy_order = book1_b.iloc[max_b,0]
	max_sell_order = book1_s.iloc[max_s,0]
	# print(max_buy_order)
	# print(max_sell_order)

	buy_price = book1_b.iloc[max_b+1,:]
	sell_price = book1_s.iloc[max_s-1,:]


	return buy_price,sell_price

#bp,sp = most_simple(book1_b,book1_s)




def n_largest_spread(book1_b,book1_s,n):
	book1_b = book1_b.reset_index()
	book1_s = book1_s.reset_index()
	# print(book1_b)
	# print(book1_s)
	# print("\n")
	# print("\n")
	book1_b2 = book1_b.iloc[:-1]
	book1_s2 = book1_s.iloc[1:]
	print(book1_b2)
	print(book1_s2)
	max_b = book1_b2[2].nlargest(n = n)
	max_s = book1_s2[2].nlargest(n = n)
	print(max_b)
	print(max_s)
	# print(max_b,max_s)
	# print(type(book1_b[2].idxmax()))
	# print(len(book1_b))
	# print(len(book1_s))
	#max_buy_order = book1_b.iloc[max_b.index,0]
	#max_sell_order = book1_s.iloc[max_s.index,0]
	# print(max_buy_order)
	# print(max_sell_order)

	buy_price = book1_b.iloc[max_b.index+1,:]
	sell_price = book1_s.iloc[max_s.index-1,:]


	return buy_price,sell_price
 
bp,sp = n_largest_spread(book1_b,book1_s,5)
print("\n")
print("\n")
print(bp)
print(sp)








trace1 = go.Bar(
			x = book1_b[0],
			y = book1_b[2],
			marker = dict(color = 'green')
			)
			
trace2 = go.Bar(
			x = book1_s[0],
			y = book1_s[2],
			marker = dict(color = 'green'))

trace3 = go.Bar(
			x = book1_nb[0],
			y = book1_nb[2],
			marker = dict(color = 'blue')
			)
			#opacity = 0.5)
trace4 = go.Bar(
			x = book1_ns[0],
			y = book1_ns[2],
			marker = dict(color = 'red'))


trace5 = go.Bar(
			x = bp[0],
			y = bp[2],
			marker = dict(color = 'brown'))

trace6 = go.Bar(
			x = sp[0],
			y = sp[2],
			marker = dict(color = 'brown'))


traces = [trace1,trace2,trace3,trace4,trace5,trace6]
print("check")
layout = go.Layout(title = 'BTC vs Index',yaxis2 = dict(side = 'right'))

        

        
fig = go.Figure(data=traces,layout=layout)
pyo.plot(fig, filename='line3.html')




