
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MaxAbsScaler

np.random.seed(7)


data = pd.read_csv('USDT_BTC_2016_4H.csv',index_col ='Unnamed: 0')

data.index.names = ['Date']
train_test_set = data.head(4387)
train_test_set.tail()
holdout_set = data.tail(5404 - 4387)
holdout_set.tail()
print(train_test_set.iloc[10,5])


def create_batches(training_data,steps_before_open):
    batches = []
    row_length,col_length = np.shape(training_data)
    for i in np.arange(0,row_length,1):
        if training_data.iloc[i,18] == "OPEN":
        
            batch_labels = training_data.iloc[i-(steps_before_open-1):i+1,[4,18]]
            batch_labels = np.array(batch_labels['averageprice'])
        
            for k in np.arange(i,row_length,1):
                if training_data.iloc[k,18] == "CLOSED":
                
                    sell_price = training_data.iloc[k,[9,18]]
                    sell_price = np.array(sell_price[0])
                
                    batch_string = batch_labels,sell_price
                   # print(batch_string)
                    batches.append(batch_string)
                    break
    return batches



def scale_batch(batches):
	scaled_obs = []
	scaled_labels = []
	for i in np.arange(0,len(batches),1):
		obs,label = batches[i]
		obs = obs.reshape(-1,1)

		label = label.reshape(-1,1)

		scaler = MaxAbsScaler()
		obs = scaler.fit_transform(obs)
		label = scaler.transform(label)
		if label > 1:
			labeli = 1
		else:
			labeli = 0
		scaled_obs.append(obs)       
		scaled_labels.append(labeli)

	return scaled_obs,scaled_labels

def next_batch(batches):
    rand_index = np.random.randint(0,len(batches))
    obs,label = batches[rand_index]
    return obs.reshape(-1, 5, 1) ,label.reshape(-1, 1, 1)
    






batches = create_batches(train_test_set,5)
obseverations , labels = scale_batch(batches)
obseverations = np.array(obseverations)
#obseverations = np.reshape(obseverations,(5,len(batches)))
obseverations = np.reshape(obseverations,(273,5))
labels = np.ndarray.flatten(np.array(labels))
X_train = obseverations
y_train = labels
print(np.shape(X_train))
print(np.shape(y_train))

test_batch = create_batches(holdout_set,5)
obs_test, labs_test = scale_batch(test_batch)
obs_test = np.reshape(obs_test,(len(obs_test),5))
labs_test = np.ndarray.flatten(np.array(labs_test))
X_test = obs_test
y_test = labs_test



# create the model
#model = Sequential()
#model.add(Embedding(5, 5, input_length=5))
#model.add(LSTM(1000))
#model.add(Dense(1, activation='sigmoid'))
#model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#print(model.summary())
#model.fit(X_train, y_train, epochs=1)
#Final evaluation of the model
#scores = model.evaluate(X_test, y_test, verbose=0)
#print("Accuracy: %.2f%%" % (scores[1]*100))


