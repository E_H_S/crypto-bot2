from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

#Strategy_on UTC TIMES  [(1.5-2.5),(7.5-11),(17.5 - 19.5)]
		#sydney times.  [(11.5-12.5),(17.5-21),(3.5-5.5)]



		#low volitility [Monday]
		#medium volility[Thursday, Saturday,Sunday]
		#Tuesday-Friday tighten Saturday


# SMA_M uses 3 indicators and will require an imput perameter for each


class VPOC_TIME(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = True)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None


		self.VPOC = np.array([None,None]*5)


		self.buy_line1 = [None,None]
		self.sell_line1 = [None,None]
		self.buy_line2 = [None,None]
		self.sell_line2 = [None,None]
		self.buy_line3 = [None,None]
		self.sell_line3 = [None,None]
		self.buy_line4 = [None,None]
		self.sell_line4 = [None,None]
		self.buy_line5 = [None,None]
		self.sell_line5 = [None,None]
		self.buy_line6 = [None,None]
		self.sell_line6 = [None,None]
		

		self.mid_line = [None,None]



		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None



		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.volitilitys = np.array([])

		self.open_value = np.array([])

		self.middle = None


		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True


		self.b_1_ready = True
		self.b_2_ready = True
		self.b_3_ready = True



		self.s_1_ready = True
		self.s_2_ready = True
		self.s_3_ready = True

		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = np.array([])

		self.ATR_switches = np.array([])

		self.numSimulTrades = 3
		self.margin = True

		self.count10 = 0
		self.switch10 = False
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		#updating Indicators



		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		
		#if len(self.dates)>1:
		#	self.open_value = self.indicators.get_open_value(self.dates[-2:-1])
		self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))
		
	
		utc_time = self.dates[-1:][0] - datetime.timedelta(hours = 10)
		day = utc_time.weekday()
		hour = utc_time.hour

		if utc_time.minute >= 30:
			minutes = 0.5
		else:
			minutes = 0

		week_num = day*24 + hour + minutes
		#print(week_num)



		#if 1 < week_num < 12 or 24.5 < week_num < 28 or 49 < week_num < 60 or 73 < week_num < 87.5 or 89 < week_num < 93 or 94 < week_num < 98.5 or 99.5 < week_num < 104.5 or 113 < week_num < 140.5 or 144.5 < week_num < 154.5:
		if 0 <= week_num <1.5 or 11.5 < week_num <49.5 or 66.5 < week_num <73.5 or 87 < week_num < 100 or 107 < week_num < 113.5 or 154 < week_num: 
			self.volitility = 'high'
		else:
			self.volitility = 'low'

		if self.volitility == 'high':
			self.volitilitys = np.append(self.volitilitys,1)
		else:
			self.volitilitys = np.append(self.volitilitys,0)



		if len(self.ATR) > 290:
			if len(self.open_value['max_delta_OI0'][-1:]) > 0:
				biggest_drop = min([self.open_value['max_delta_OI0'][-1:].item(),self.open_value['max_delta_OI1'][-1:].item(),self.open_value['max_delta_OI2'][-1:].item(),self.open_value['max_delta_OI3'][-1:].item(),self.open_value['max_delta_OI4'][-1:].item(),self.open_value['max_delta_OI5'][-1:].item()])

			
				#print(abs(self.closes[-1:]-self.closes[-26:-25]))
				if biggest_drop < -self.open_value['open_interest'][-6*180:].max()*0.06 and self.switch10 == False:# and abs(self.closes[-1:]-self.closes[-1:]) > self.closes[-1:]*0.035: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 == True
					#self.ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					#ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					if self.ATR_switch == False:
						self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					elif self.ATR_switch == True:
						ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
						if ATR_min < self.ATR_min:
							self.ATR_min = ATR_min

					self.last_OV_high = self.open_value['open_interest'][-6*180:].max()
					self.ATR_switch = True

					print('\n')
					print("NEW TRIGGER EVENT! " , self.dates[-1:])
					print(biggest_drop)
					print("MIN ATR ", self.ATR_min)
					print('\n')
					


				if self.ATR[-1:] != None:
					if self.ATR_switches[-12*24:].min() == 1:
						if 114 < week_num < 154 or 74 < week_num < 87 or 49 < week_num < 58 or 1 < week_num < 12:
							if self.ATR[-1:] < self.ATR_min*0.8 and self.ATR_switch == True:
								self.ATR_switch = False
								print('\n')
								print('FAILED ATR SWITCH ', self.dates[-1:])
								print('\n')
						else:
							if self.ATR[-1:] < self.ATR_min*1 and self.ATR_switch == True:
								self.ATR_switch = False
								print('\n')
								print('FAILED ATR SWITCH ', self.dates[-1:])
								print('\n')
						
					
				if self.open_value['max_delta_lookback_OI'][-1:].item() > -self.open_value['open_interest'][-6*180:].max()*0.06 and self.ATR_switch == True:
					self.ATR_switch = False
					print('\n')
					print('5 day OV drop expired! ', self.dates[-1:])
					print('\n')



				if self.open_value['open_interest'][-1].item() > self.last_OV_high + self.open_value['open_interest'][-6*180:].max()*0.06 and self.ATR_switch == True:

					self.ATR_switch = False
					print('\n')
					print('OVER THE OV MAX ', self.dates[-1:])
					print('OV MAX ',self.last_OV_high + self.open_value['open_interest'][-6*180:].max()*0.06)
					print("LAST OI ",self.open_value['open_interest'][-1].item())
					print('\n')
			
			

		
		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2:-1])




		if len(self.dates)> 10:
			self.volume_profile,self.index = self.indicators.get_volume_profile(self.dates[-1:],self.index)
			self.open_value = self.indicators.get_open_value(self.dates[-2:-1])

			if len(self.dates) > 12*24:

				try:
					self.VPOC = np.append(self.VPOC,self.volume_profile[self.volume_profile['percent']<0.50].index[-1:][0])
				except:
					self.VPOC = np.append(self.VPOC,self.volume_profile[self.volume_profile['percent']>0.50].index[0])
				self.V60 = self.volume_profile[self.volume_profile['percent']<0.60].index[-1:][0]
				self.V40 = self.volume_profile[self.volume_profile['percent']<0.40].index[-1:][0]
				try:
					self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.20].index[-1:][0]
					self.sell_price2 = self.volume_profile[self.volume_profile['percent']<0.15].index[-1:][0]
					self.sell_price3 = self.volume_profile[self.volume_profile['percent']<0.10].index[-1:][0]
					
					#self.sell_price3 = self.volume_profile.index[0]

					self.buy_price1 = self.volume_profile[self.volume_profile['percent']>0.80].index[0]
					self.buy_price2 = self.volume_profile[self.volume_profile['percent']>0.85].index[0]
					self.buy_price3 = self.volume_profile[self.volume_profile['percent']>0.90].index[0]
					
					#self.buy_price3 = self.volume_profile.index[-1:][0]
				except:
					print(self.volume_profile)
					#self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.2].index[-1:][0]
					self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.20].index[-1:][0]
					self.sell_price2 = self.volume_profile[self.volume_profile['percent']<0.15].index[-1:][0]
					self.sell_price3 = self.volume_profile.index[0]


					self.buy_price1 = self.volume_profile[self.volume_profile['percent']>0.80].index[0]
					self.buy_price2 = self.volume_profile[self.volume_profile['percent']>0.85].index[0]
					self.buy_price3 = self.volume_profile.index[-1:][0]

			else:
				self.VPOC = np.append(self.VPOC,None)


			
		self.mid_line.append(self.middle)


		if self.volitility == 'low' and self.buy_price1 != None:
			self.buy_price1 = self.buy_price1*1.0035
			self.buy_price2 = self.buy_price2*1.0035
			self.buy_price3 = self.buy_price3*1.0035

			self.sell_price1 = self.sell_price1/1.0035
			self.sell_price2 = self.sell_price2/1.0035
			self.sell_price3 = self.sell_price3/1.0035


		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)
		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)
		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)

		self.buy_line4.append(self.buy_price1)
		self.sell_line4.append(self.sell_price1)
		self.buy_line5.append(self.buy_price2)
		self.sell_line5.append(self.sell_price2)
		self.buy_line6.append(self.buy_price3)
		self.sell_line6.append(self.sell_price3)

		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3),(self.buy_price4,self.sell_price4),(self.buy_price5,self.sell_price5),(self.buy_price6,self.sell_price6)]





		if self.ATR_switch == False:
			self.ATR_switches = np.append(self.ATR_switches,0)
			
		elif self.ATR_switch == True:
			self.ATR_switches = np.append(self.ATR_switches,1)
		self.ATR_min_list = np.append(self.ATR_min_list,self.ATR_min)
		
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 1000
		stoploss = None
		#market_order = 'zero'
		market_order = False

		spread_scaler = 1

		if self.volitility == 'high':
			spread_scaler = 1
		elif self.volitility == 'low':
			spread_scaler == 1


		if bp == self.buy_price1 and self.b_1_ready == False:
			bp = None
		elif bp == self.buy_price2 and self.b_2_ready == False:
			bp = None
		elif bp == self.buy_price3 and self.b_3_ready == False:
			bp = None
		
		if sp == self.sell_price1 and self.s_1_ready == False:
			sp = None
		elif sp == self.sell_price2 and self.s_2_ready == False:
			sp = None
		elif sp == self.sell_price3 and self.s_3_ready == False:
			sp = None
	
		
		if len(self.dates) > 2:
			
			if  self.ATR_switch == True:
				
				if sp != None:
					if self.highs[-1:] > sp*spread_scaler and self.closes[-2:-1] < sp*spread_scaler and sp*spread_scaler > self.VPOC[-1:]*1.000125:
						action = True
						Long = False
						price = sp*spread_scaler
						#stoploss = self.highs[-288*2:].max()*1.02
						
				if bp != None:

					if self.lows[-1:] < bp/spread_scaler and self.closes[-2:-1] > bp/spread_scaler and bp/spread_scaler < self.VPOC[-1:]/1.000125:
						action = True
						Long = True
						price = bp/spread_scaler
						#stoploss = self.lows[-288*2:].min()/1.02



		if action == True:
			if Long == True:
				if bp == self.buy_price1:
					self.b_1_ready = False
				elif bp == self.buy_price2:
					self.b_2_ready = False
				elif bp == self.buy_price3:
					self.b_3_ready = False

			if Long == False:
				if sp == self.sell_price1:
					self.s_1_ready = False
				elif sp == self.sell_price2:
					self.s_2_ready = False
				elif sp == self.sell_price3:
					self.s_3_ready = False


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:


		if self.volitility == 'high':
			if self.highs[-1:] > self.VPOC[-1:] and trade.long == True:
			#if self.highs[-1:] > self.sell_price1 and trade.long == True:
				action = True
				price = self.VPOC[-1:]
				#price = self.sell_price1
		elif self.volitility == 'low':
			#if self.highs[-1:] > self.V60 and trade.long == True:
			if self.highs[-1:] > self.VPOC[-1:] and trade.long == True:
				action = True
				#price = self.V60
				price = self.VPOC[-1:]
		
					

		if self.volitility == 'high':
			#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
			if self.lows[-1:] < self.VPOC[-1:] and trade.long == False:
			#if self.lows[-1:] < self.buy_price1 and trade.long == False:
				action = True
				price = self.VPOC[-1:]
				#price = self.buy_price1
		elif self.volitility == 'low':
			#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
			if self.lows[-1:] < self.VPOC[-1:] and trade.long == False:
			#if self.lows[-1:] < self.V40 and trade.long == False:
				action = True
				price = self.VPOC[-1:]
				#price = self.V40

		
	

		if action == True:
			self.b_1_ready = True
			self.b_2_ready = True
			self.b_3_ready = True
			self.s_1_ready = True
			self.s_2_ready = True
			self.s_3_ready = True

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.ATR_switches,self.volitilitys)#,self.ATR_min_list)



