from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class Exhaustion(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)
		self.period = period
		self.limits = False

		self.indicators = BotIndicators(orderbook= False,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime)
		
		#Inititalize inticator vectors
		self.mavs1  = np.array([])
		self.B1_upper = np.array([])
		self.B1_lower = np.array([])


		self.B2_upper = np.array([])
		self.B2_lower = np.array([])


		#Extras
		self.mavs2  = np.array([])
		self.mavs3  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.last_long_stoploss = datetime.datetime(2018,1,1)
		self.last_short_stoploss = datetime.datetime(2018,1,1)

		self.last_trade = None



		self.ob_index = None
		self.last_orderbook = pd.DataFrame()
		self.open_value = np.array([])
		self.ATR_switches = np.array([])
		self.ATR_min_list = np.array([])
		self.open_period = np.array([])
		self.volume_5 = np.array([])

		self.count10 = 0
		self.switch10 = False
		self.ATR_switch = False
		self.last_OV_high = 1000000000000000
		self.index = 0
		self.ATR_min = 0

		self.numSimulTrades = 1
		self.margin = True
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])
		self.trades = trades

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):


		if len(self.trades)> 0:
			
			self.last_trade = self.trades[-1:][0]
			if self.last_trade.status == "STOP LOSSED" and self.last_trade.long == 1:
				self.last_long_stoploss = self.last_trade.exitDate[0]
				print("NEW STOPLOSS LONG DATE")
			if self.last_trade.status == "STOP LOSSED" and self.last_trade.long == 0:
				self.last_short_stoploss = self.last_trade.exitDate[0]
				print("NEW STOPLOSS SHORT DATE")

			# if self.trades[-1:][0].entryDate != self.last_trade.entryDate:
			# 	print('check555')
			# 	self.last_trade = self.trades[-1:][0]
			# 	if self.last_trade.status == "STOP LOSSED" and self.last_trade.long == 1:
			# 		self.last_long_stoploss = self.last_trade.exitDate
			# 		print("NEW STOPLOSS LONG DATE")
			# 	if self.last_trade.status == "STOP LOSSED" and self.last_trade.long == 0:
			# 		self.last_short_stoploss = self.last_trade.exitDate
			# 		print("NEW STOPLOSS SHORT DATE")






		#updating Indicators
		self.volume_mean = pd.Series(self.volumes).rolling(5,min_periods = 1).mean()

		if np.mod(self.dates[-1:].item().minute,5) == 0:
			five_min_vol = float(self.volumes[-5:].sum())
			self.volume_5 = np.append(self.volume_5,five_min_vol)
		else:
			self.volume_5 = np.append(self.volume_5,None)




		#self.volume_5 =  

		self.volume_STD = pd.Series(self.volumes).rolling(60*24,min_periods = 1).std()

		if len(self.volume_5) > 60*12:
			vols = self.volume_5[-12*24*3:]
			vols = vols[vols != np.array(None)]
			vols = pd.Series(vols)
			B1_upper = vols.astype(float).nlargest(5).mean()
		else:
			B1_upper = None


		#print(B1_upper)
		self.B1_upper = np.append(self.B1_upper,B1_upper)

		self.momentum_small = pd.Series(self.closes).diff(30)
		self.momentum_big = pd.Series(self.closes).diff(12*24*3)

	

		

		
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 3000
		stoploss = None
		#market_order = 'zero'
		#print(date)
		market_order = True
		# if self.dead_date_start > date.item().replace(tzinfo=None):
		# 	if date.item().replace(tzinfo=None) < self.dead_date_end == False:
				
		
		if len(self.volumes)> 60*24*1.1:
			#print(self.dates[-1:])
			# if self.last_short_stoploss == None:
			# 	d = self.dates[-1:].item().tz_convert(None)
			# 	print('NO TZ')
			# else:
			d = self.dates[-1:].item()
			#print('YES TZ')

			#print((d - self.last_short_stoploss).seconds,d,self.last_short_stoploss)
			try:
				if (d - self.last_short_stoploss).total_seconds() > 60*60*24:
					if self.volume_5[self.volume_5 != np.array(None)][-1:] > self.B1_upper[-1:] and self.momentum_small[-1:].item() > 0 and self.volumes[-2:-1]> self.volumes[-1:]:
						action = True
						Long = False
						price = self.closes[-1:]
						stoploss = self.highs[-5:].max() + 25
			except:
				d = self.dates[-1:].item().tz_convert(None)
				if (d - self.last_short_stoploss).total_seconds() > 60*60*24:
					if self.volume_5[self.volume_5 != np.array(None)][-1:] > self.B1_upper[-1:] and self.momentum_small[-1:].item() > 0 and self.volumes[-2:-1]> self.volumes[-1:]:
						action = True
						Long = False
						price = self.closes[-1:]
						stoploss = self.highs[-5:].max() + 25

			
		
			#if self.volumes[-2:-1] > self.volume_mean[-2:-1].item() + 2*self.volume_STD[-2:-1].item() and self.momentum_small[-2:-1].item() < 0 and self.volumes[-2:-1]> self.volumes[-1:] and self.momentum_big[-1:].item() > 0:
		

			# if self.volume_5[self.volume_5 != np.array(None)][-1:] > self.B1_upper[-1:]
			# if self.last_long_stoploss == None:
			# 	dd = self.dates[-1:].item().tz_convert(None)
			# else:
			dd = self.dates[-1:].item()
			try:
				if (dd - self.last_short_stoploss).total_seconds() > 60*60*12:
					if self.volume_5[self.volume_5 != np.array(None)][-1:] > self.B1_upper[-1:] and self.momentum_small[-1:].item() < 0 and self.volumes[-2:-1]> self.volumes[-1:]:
							
						action = True
						Long = True
						price = self.closes[-1:]
						stoploss = self.lows[-5:].min() - 25
			except:
				dd = self.dates[-1:].item().tz_convert(None)
				if (dd - self.last_short_stoploss).total_seconds() > 60*60*12:
					if self.volume_5[self.volume_5 != np.array(None)][-1:] > self.B1_upper[-1:] and self.momentum_small[-1:].item() < 0 and self.volumes[-2:-1]> self.volumes[-1:]:
							
						action = True
						Long = True
						price = self.closes[-1:]
						stoploss = self.lows[-5:].min() - 25

			
		

		if action == True:
			print("ACTION TRUE")
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		
		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		
		if self.highs[-1:] > self.closes[-6:-5]*1.02 and  trade.long == True:
		# if self.highs[-1:] > self.B1_upper[-1:] and  trade.long == True:
			action = True
			#price = self.closes[-1:]
			price = self.closes[-6:-5]*1.02
		elif trade.trade_length > 60*4 and self.volume_5[self.volume_5 != np.array(None)][-1:] > self.B1_upper[-1:] and self.momentum_small[-1:].item() > 0 and self.volumes[-2:-1]> self.volumes[-1:] and  trade.long == True:
			action = True
			#price = self.closes[-1:]
			price = self.closes[-1:]


		if self.lows[-1:] < self.closes[-6:-5]/1.02 and  trade.long == False:
		# if self.highs[-1:] > self.B1_upper[-1:] and  trade.long == True:
			action = True
			#price = self.closes[-1:]
			price = self.closes[-6:-5]/1.02
		elif trade.trade_length > 60*4 and self.volume_5[self.volume_5 != np.array(None)][-1:] > self.B1_upper[-1:] and self.momentum_small[-1:].item() < 0 and self.volumes[-2:-1]> self.volumes[-1:] and  trade.long == False:
			action = True
			#price = self.closes[-1:]
			price = self.closes[-1:]
		
		
		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass
	def calculate_order_book_depth():
		pass




	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.volumes,self.volume_mean,self.B1_upper,self.volume_5)