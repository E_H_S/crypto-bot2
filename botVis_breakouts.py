
import numpy as np 
import datetime as dt
import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd 
import time



#import seaborn as sns


class BotVisual_breakouts(object):
	def __init__(self,live = False,chart_style = 'candlestick'): #Takes an input of a strategy log 

		self.live = live
		

	def buildChart(self,StrategyLog,df = None):
		self.prices = StrategyLog.AveragePrices
		self.pair = StrategyLog.pair
		self.period = StrategyLog.period

		self.openprices = StrategyLog.openprices
		self.closeprices = StrategyLog.closeprices
		self.highs =  StrategyLog.highs
		self.lows = StrategyLog.lows
		self.volumes = StrategyLog.volumes
		self.live = StrategyLog.botcontroller.live
		self.datetimes = StrategyLog.dates
		


		
		self.ohlc = pd.DataFrame({"Date":self.datetimes,"Open":self.openprices,"High":self.highs,"Low":self.lows,"Close":self.closeprices})
		self.ohlc = self.ohlc[['Date',"Open","High","Low","Close"]]
		self.ohlc['time_stamp'] = pd.to_datetime(self.ohlc['Date'],unit='s')




		self.indicators = StrategyLog.botcontroller.strategy.Draw_indicators()

		#print(self.indicators)

		self.indicator1 = [None]*len(self.openprices)
		self.indicator2 = self.indicator1
		self.indicator3 = self.indicator1

		if self.indicators != None:
			if len(self.indicators) == 2 and len(self.indicators[1]) < 1:
				print("check19")
				self.indicator1,dummy = self.indicators
			elif len(self.indicators)== 2:
				self.indicator1,self.indicator2 = self.indicators
			elif len(self.indicators)== 3:
				self.indicator1,self.indicator2,self.indicator3 = self.indicators
			elif len(self.indicators)== 4:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4 = self.indicators
			elif len(self.indicators)== 5:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5 = self.indicators
			elif len(self.indicators)== 6:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6 = self.indicators
			#self.datetimes = self.dateconv(StrategyLog.dates)
			self.pair = StrategyLog.pair

		if self.indicators != None:
			if len(self.indicators)== 7:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7 = self.indicators

			elif len(self.indicators)== 8:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7 ,self.indicator8 = self.indicators
			elif len(self.indicators)== 9:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7 ,self.indicator8,self.indicator9 = self.indicators

			elif len(self.indicators)== 10:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7,self.indicator8,self.indicator9,self.indicator10 = self.indicators

			elif len(self.indicators)== 11:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7,self.indicator8,self.indicator9,self.indicator10,self.indicator11  = self.indicators
			elif len(self.indicators)== 12:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7,self.indicator8,self.indicator9,self.indicator10,self.indicator11,self.indicator12  = self.indicators
			elif len(self.indicators)== 13:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7,self.indicator8,self.indicator9,self.indicator10,self.indicator11,self.indicator12,self.indicator13  = self.indicators
			elif len(self.indicators) == 22:
				self.indicator1,self.indicator2,self.indicator3,self.indicator4,self.indicator5,self.indicator6,self.indicator7,self.indicator8,self.indicator9,self.indicator10,self.indicator11,self.indicator12,self.indicator13,self.indicator14,self.indicator15,self.indicator16,self.indicator17,self.indicator18,self.indicator19,self.indicator20,self.indicator21,self.indicator22  = self.indicators

		#print(self.ohlc)

		#print(self.ohlc['time_stamp'])
		#print(type(self.ohlc['time_stamp'][-1:].value))


		Indicator_x_axis = self.ohlc['time_stamp']
		#Indicator_x_axis = self.indicator13
		orderbook_levels = False


		



		candles = go.Candlestick(
		                    x = self.ohlc['time_stamp'],
		                    open = self.ohlc['Open'],
		                    high = self.ohlc['High'],
		                    low = self.ohlc['Low'],
		                    close = self.ohlc['Close'])

		if self.indicators != None:
			if len(self.indicators) > 0:
				indicator1 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator1,
			                    #yaxis = 'y2',
			                    #opacity = 0.5,
			                    marker = dict(color = 'purple'),
			                    mode = 'lines')

			
			if len(self.indicators) > 1:	
				indicator2 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator2,
			                    #yaxis = 'y2',
			                    marker = dict(color = 'purple'),
			                    mode = 'lines')
					
			if len(self.indicators) > 2:		
				indicator3 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator3,
			                    yaxis = 'y2',
			                    marker = dict(color = 'green'),
			                    mode = 'lines')
				


			if len(self.indicators) > 3:
				indicator4 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator4,
			                    #y = self.indicator4['OV_CORR'],
			                    marker = dict(color = 'teal'),
			                    yaxis='y3',
			                    mode = 'lines')

			if len(self.indicators) > 4:
				indicator5 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator5,
			                    #y = self.indicator4['OV_CORR'],
			                    marker = dict(color = 'blue'),
			                    #yaxis='y3',
			                    mode = 'lines')
				
			if len(self.indicators) > 5:
				indicator6 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator6,
			                    #y = self.indicator4['OV_CORR'],
			                    marker = dict(color = 'blue'),
			                    #yaxis='y3',
			                    mode = 'lines')

			if len(self.indicators) > 6:
				indicator7 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator7,
			                    #y = self.indicator4['OV_CORR'],
			                    marker = dict(color = 'pink'),
			                    yaxis='y4',
			                    mode = 'lines')

			if len(self.indicators) > 7:
				indicator8 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator8,
			                    marker = dict(color = 'red'),
			                    yaxis = 'y4',
			                    mode = 'lines')
				
			if len(self.indicators) > 8:
				indicator9 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator9,
			                    marker = dict(color = 'orange'),
			                    #yaxis = 'y3',
			                    mode = 'lines')

			if len(self.indicators) > 9:
				indicator10 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator10,#.rolling(6*60).mean(),
			                    #y = self.indicator3['OV_CORR'],
			                    marker = dict(color = 'orange'),
			                    #yaxis='y4',
			                    mode = 'lines')

			if len(self.indicators) > 10:
				indicator11 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator11,#.rolling(6*60).mean(),
			                    #y = self.indicator3['OV_CORR'],
			                    marker = dict(color = 'purple'),
			                    yaxis='y4',
			                    mode = 'lines')

			if len(self.indicators) > 11:
				indicator12 = go.Scatter(
			                    x = Indicator_x_axis,
			                    #x = self.indicator13,
			                    y = self.indicator12,#.rolling(6*60).mean(),
			                    #y = self.indicator3['OV_CORR'],
			                    marker = dict(color = 'purple'),
			                    #yaxis='y5',
			                    mode = 'lines')
			if len(self.indicators) > 12:
				indicator13 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator13,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator14 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator14,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator15 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator15,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator16 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator16,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator17 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator17,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator18 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator18,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator19 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator19,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator20 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator20,
			                    marker = dict(color = 'brown'),
			                    mode = 'lines')
				indicator21 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator21,
			                    marker = dict(color = 'black'),
			                    mode = 'lines')
				indicator22 = go.Scatter(
			                    x = Indicator_x_axis,
			                    y = self.indicator22,
			                    marker = dict(color = 'black'),
			                    mode = 'lines')




		#print("check")
		layout = go.Layout(
                    title='Backtest',
                    #'xaxis': {'rangeslider': {'visible': False}},
                    xaxis = dict(rangeslider = dict(visible = False)),
                    yaxis=dict(title='XBT Price',
                        gridcolor = 'black',
                        gridwidth = 1),

                    yaxis2=dict(
                        title='Open value(BTC)',
                        titlefont=dict(
                            color='rgb(148, 103, 189)'),
                        tickfont=dict(
                            color='rgb(148, 103, 189)'),
                        overlaying='y',
                        side='right',
                        showgrid = False),
                    yaxis3=dict(
                        title='ATR',
                        titlefont=dict(
                            color='rgb(148, 103, 189)'),
                        tickfont=dict(
                            color='rgb(148, 103, 189)'),
                        overlaying='y',
                        side='right',
                        showgrid = False),

					yaxis4=dict(
                        title='ATR Switch)',
                        titlefont=dict(
                            color='rgb(148, 103, 189)'),
                        tickfont=dict(
                            color='rgb(148, 103, 189)'),
                        overlaying='y',
                        side='left',
                        #range = [0,5],
                        showgrid = False),

					yaxis5=dict(
                        title='PNL)',
                        titlefont=dict(
                            color='rgb(148, 103, 189)'),
                        tickfont=dict(
                            color='rgb(148, 103, 189)'),
                        overlaying='y',
                        side='left',
                        showgrid = False))
					
		 

		t1,t2,t3,t4 = self.tradeMarker(StrategyLog.botcontroller.trades)
		#print(t1,t2,t3,t4)

		traces = [candles]

		#print(df['est_cur_val'].values)
		indicator_df = go.Scatter(
		                    x = df['date'],
		                    y = df['est_cur_val'].values,
		                    #yaxis = 'y2',
		                    #opacity = 0.3,
		                    marker = dict(color = 'black'),
		                    yaxis = 'y5',
		                    mode = 'lines')

		traces.append(indicator_df)
		
		if self.indicators != None:
			if len(self.indicators) == 2 and self.indicators[1][-1:] == None:
				traces.append(indicator1)
			elif len(self.indicators) == 2 and self.indicators[1] != None:
				traces.append(indicator2)
			elif len(self.indicators) > 2:
				traces = traces +[indicator1,indicator2,indicator3]
			


			if len(self.indicators) > 3:
				traces.append(indicator4)

			if len(self.indicators) > 4:
				traces.append(indicator5)
			if len(self.indicators) > 5:
				traces.append(indicator6)


			if len(self.indicators) > 6:
				traces = traces + [indicator7]
			if len(self.indicators) > 7:
				traces = traces + [indicator8]
			if len(self.indicators) > 8:
				traces = traces + [indicator9]
			if len(self.indicators) > 9:
				traces = traces + [indicator10]
			if len(self.indicators) > 10:
				traces = traces + [indicator11]
			if len(self.indicators) > 11:
				traces = traces + [indicator12]
			if len(self.indicators) > 12:
				traces = traces + [indicator13,indicator14,indicator15,indicator16,indicator17,indicator18,indicator19,indicator20,indicator21,indicator22]



			#if len(self.indicators) == 10:
			#	traces = traces +[indicator8,indicator9,indicator10]

		traces = traces + [t1,t2,t3,t4]
			


		traces = [i for i in traces if i is not None]
		print('traces length', len(traces))


    
		fig = go.Figure(data=traces,layout=layout)
		pyo.plot(fig, filename='line3.html')
		
	
		
		

		
		
		
	def tradeMarker(self,trades):

		# open_longs = {'price':[],"time_stamp": []}
		# open_shorts = {'price':[],"time_stamp": []}
		# close_longs = {'price':[],"time_stamp": []}
		# close_shorts = {'price':[],"time_stamp": []}

		open_longs = []
		open_shorts = []
		close_longs = []
		close_shorts = []

		for trade in trades:
			# if trade.exitDate != 

			trade.entryPrice = float(trade.entryPrice)
			if trade.exitPrice != None:
				trade.exitPrice = float(trade.exitPrice)
			else:
				trade.exitPrice = None


			trade.entryDate = trade.entryDate[0]
			
			if trade.exitDate != None:
				trade.exitDate = trade.exitDate[0]
			else:
				trade.exitDate = None


			if trade.long == True:
				#print(type(trade.entryPrice))
				OL = {'price':trade.entryPrice,'time_stamp':trade.entryDate}
				CL = {'price':trade.exitPrice,'time_stamp':trade.exitDate}
				#print(OL,CL)
				open_longs.append(OL)
				close_longs.append(CL)



				#open_longs['price'].append(trade.entryPrice)
				#open_longs['time_stamp'].append(trade.entryDate)
				# close_longs['price'].append(trade.exitPrice)
				# close_longs['time_stamp'].append(trade.exitDate)
			elif trade.long == False:
				OS = {'price':trade.entryPrice,'time_stamp':trade.entryDate}
				CS = {'price':trade.exitPrice,'time_stamp':trade.exitDate}
				open_shorts.append(OS)
				close_shorts.append(CS)



				# open_shorts['price'].append(trade.entryPrice)
				# open_shorts['time_stamp'].append(trade.entryDate)
				# close_shorts['price'].append(trade.exitPrice)
				# close_shorts['time_stamp'].append(trade.exitDate)


		open_longs = pd.DataFrame(open_longs)
		open_shorts = pd.DataFrame(open_shorts)
		close_longs = pd.DataFrame(close_longs)
		close_shorts = pd.DataFrame(close_shorts)
		
		#print(open_longs)
		ret = []

		if len(open_longs)> 0:
			longs_O = go.Scatter(
				                    x = open_longs['time_stamp'],
				                    y = open_longs['price'],
				                    mode = 'markers',
				                	marker = dict(
				                   		color = 'blue',
				                   		symbol = "triangle-up",
				                   		size = 16,
				                   		opacity = 0.75))
			ret.append(longs_O)
		else:
			ret.append(None)


		if len(close_longs)> 0:
			longs_C = go.Scatter(
				                    x = close_longs['time_stamp'],
				                    y = close_longs['price'],
				                    mode = 'markers',
				                    marker = dict(
				                    	color = 'blue',
				                    	size = 16,
				                    	opacity = 0.5
				                    	))
			ret.append(longs_C)
		else:
			ret.append(None)

		
		if len(open_shorts)> 0:
			shorts_O = go.Scatter(
				                    x = open_shorts['time_stamp'],
				                    y = open_shorts['price'],
				                    mode = 'markers',
				                    marker = dict(
				                    	color = 'black',
				                    	symbol = 'triangle-down',
				                    	size = 16,
				                    	opacity = 0.75))
			ret.append(shorts_O)
		else:
			ret.append(None)



		if len(close_shorts)> 0:
			shorts_C = go.Scatter(
				                    x = close_shorts['time_stamp'],
				                    y = close_shorts['price'],
				                    mode = 'markers',
				                    marker = dict(
				                    	color = 'red',
				                    	size =16,
				                    	opacity = 0.5))
			ret.append(shorts_C)
		else:
			ret.append(None)


		return ret

	def Highs_n_Lows(self,trade):
		pass
	def equity_curve(self,df):




		fig2 = go.Figure(data=traces2,layout=layout2)
		pyo.plot(fig2, filename='equity_curve.html')







