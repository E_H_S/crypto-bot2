

import sqlite3 as sql 
import pandas as pd
import multiprocessing
import datetime
import numpy as np
import time

def Query_orderbooks(table,start_UTC,end_UTC,fp):
	start_UTC = start_UTC.strftime("%Y-%m-%d %H:%M:%S.%f")
	end_UTC = end_UTC.strftime("%Y-%m-%d %H:%M:%S.%f")

	print(start_UTC)
	print(end_UTC)
	print('\n')

	with sql.connect(fp) as con:
		cur = con.cursor()
		cur.execute("""
		SELECT DISTINCT
		*
		FROM {}
		WHERE 1=1
		and time_stamp > ?
		and time_stamp < ?
		ORDER BY time_stamp,price ASC
		""".format(table),(start_UTC,end_UTC)) #giving 24 hours of past data to begin with
		
		data = cur.fetchall()
	
	return data

#['price','side','qty','cum','time_stamp']
def Create_RAM_DB(data):

	con = sql.connect(':memory:')

	with con:
		cur = con.cursor()
		cur.execute("""
			CREATE TABLE IF NOT EXISTS XBT_orderbook_RAM 
			(price real,
			side text,
			qty real,
			cum real,
			time_stamp text);
			""")

	count23 = 0
	with con:
		cur = con.cursor()
		for price,side,size,time_stamp,cum in data:
			count23 += 1
			if np.mod(count23,100000)==0:
				print(count23/len(data))
			#print(row['price'],row['side'],row['size'],row['cum'],row['time_stamp'])
			cur.execute("""
				INSERT INTO XBT_orderbook_RAM
				(price,
				side,
				qty,
				cum,
				time_stamp)
				VALUES (?,?,?,?,?)
				""",(price,side,size,cum,time_stamp))

	return con

def ram_query(con):
	print("QUERY TIME")
	print('\n')
	st = datetime.datetime(2019, 6,1,0,0,0) - datetime.timedelta(hours = 10)
	et  = datetime.datetime(2019, 6,10,0,0,0) - datetime.timedelta(hours = 10)
	start_UTC = st.strftime("%Y-%m-%d %H:%M:%S.%f")
	end_UTC = et.strftime("%Y-%m-%d %H:%M:%S.%f")


	t5 = datetime.datetime.utcnow()
	# with con:
	# 	cur = con.cursor()
	# 	cur.execute("""
	# 	SELECT DISTINCT
	# 	*
	# 	FROM {}
	# 	WHERE 1=1
	# 	and time_stamp > ?
	# 	and time_stamp < ?
	# 	ORDER BY time_stamp,price ASC
	# 	""".format('XBT_orderbook_RAM'),(start_UTC,end_UTC)) #giving 24 hours of past data to begin with
	

	with con:
		cur = con.cursor()
		cur.execute("""
		SELECT
		*
		FROM XBT_orderbook_RAM
		WHERE 1=1
	 	and time_stamp > ?
	 	and time_stamp < ? 
		ORDER BY time_stamp,price ASC
	 	""".format('XBT_orderbook_RAM'),(start_UTC,end_UTC))
		
		data = cur.fetchall()
		print(len(data))

	t6 = datetime.datetime.utcnow()
	print('Total ram query seconds '+str((t6-t5).total_seconds()))




if __name__ == "__main__":

	db_file_path = '../Database_Prep/Database1_copy1'
	table = 'XBT_orderbook_June'
	st = datetime.datetime(2019, 6,1,0,0,0) - datetime.timedelta(hours = 10)
	et  = datetime.datetime(2019, 6,10,0,0,0) - datetime.timedelta(hours = 10)

	t1 = datetime.datetime.utcnow()
	data = Query_orderbooks(table,st,et,db_file_path)
	t2 = datetime.datetime.utcnow()
	print(len(data))
	print('query completed in : '+str((t2-t1).total_seconds()))
	
	t3 = datetime.datetime.utcnow()
	con = Create_RAM_DB(data)
	t4 = datetime.datetime.utcnow()

	ram_query(con)
	print('RAM TABLE CREATED')
	print((t4-t3).total_seconds())











	time.sleep(600)
	# fp_ram = ':memory:'
	# ram_table = 'XBT_orderbook_RAM'
	# t5 = datetime.datetime.utcnow()
	# RAM_DATA =  Query_orderbooks(ram_table,st,et,fp_ram)
	# t6 = datetime.datetime.utcnow()
	# print(len(RAM_DATA))
	# print('Total ram query seconds'+str((t6-t5).total_seconds()))





