from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time
import operator


#Strategy_on UTC TIMES  [(1.5-2.5),(7.5-11),(17.5 - 19.5)]
		#sydney times.  [(11.5-12.5),(17.5-21),(3.5-5.5)]



		#low volitility [Monday]
		#medium volility[Thursday, Saturday,Sunday]
		#Tuesday-Friday tighten Saturday


# SMA_M uses 3 indicators and will require an imput perameter for each


class Breakouts(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = False
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= True,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False,anchored = False,open_value_candles = True,momentum = False,RSI = False,daily_candles = True)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = []
		

		self.ATR = []
		self.ATR_14 = []
		self.stoplosses = []
		self.max_risks = []

		self.ob_index = None

		self.skew = 0.0005

		self.VPOC = []
		self.max_risk = 0.08

		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []
		
		self.Leverage = 2.5

		self.mid_line = [None,None]

		if p1 != None:
			self.drop_percent = p1
		else:
			#self.drop_percent = 0.062
			self.drop_percent = 0.001

		if p2 != None:
			self.stoploss = p2
		else:
			self.stoploss = 0.005

		if p3 != None:
			self.clip_size = p3
		else:
			self.clip_size = 1500



		self.volitilitys =[]
		self.account_size = []
		self.latest_range = []

		self.open_value = np.array([])

		self.middle = None

		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = []
		self.ATR_switches = []

		self.numSimulTrades = 1
		self.margin = True

		self.closing_line = []
		self.volume_profile = None

		self.OI_density = []
		self.trigger_date = None

		self.high_index = None
		self.low_index = None


		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0
		self.timer4 = 0
		self.timer5 = 0
		self.timer6 = 0
		self.timer7 = 0
		self.timer8 = 0

		self.last_stoplossed_trade = None




		#TRADE PERAMETERS
		self.rolling_period = 720
		self.skew = 0.0008
		self.stoploss = 0.003
		self.reach = 0.02
		self.minimum_build_up = int(self.rolling_period/2)
		self.target_bp = 20







		self.three_day_high_list = []
		self.three_day_low_list = []
		self.seven_day_high_list = []
		self.seven_day_low_list = []

		self.time_from_high = 0
		self.time_from_low = 0

		self.accumulation_since_high = None
		self.accumulation_since_low = None

		self.accumulation_since_high_list = []
		self.accumulation_since_low_list = []

		self.min_Open_Intererest_at_high = None
		self.min_Open_Intererest_at_low = None


		


		self.rolling_high = []
		self.rolling_low = []
		self.buy_exits = []
		self.sell_exits = []
		self.rolling_periods = []
		self.volitilitys = []

		self.liquidity_sell_stop = []
		self.liquidity_buy_stop = []

		self.total_buy_side_liquity = []
		self.total_sell_side_liquity = []




		self.last_orderbook = None

		self.five_m_dates = []


		self.count10 = 0
		self.switch10 = False

		self.candle_count = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):
		

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.t1 = datetime.datetime.utcnow()

		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		self.t2 = datetime.datetime.utcnow()

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])		

		#Update relevent indicators
		if np.mod(self.dates[-1].minute-1,5) == 0:
			pass
			self.five_m_dates.append(self.dates[-1])
		self.tick_indicators(trades)
		

	def tick_indicators(self,trades):


		self.candle_count += 1

		if self.candle_count == 1:
			self.volility = self.indicators.get_volitility_ranking(self.dates[-1],first = True)
		else:
			if self.dates[-1].hour == 1 and self.dates[-1].minute == 0:
				self.volility = self.indicators.get_volitility_ranking(self.dates[-1],first = False)
			else:
				pass

		if len(self.dates)>1 and np.mod(self.dates[-1].minute - 1,5)==0:
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)



		if len(self.dates)>2 and np.mod(self.dates[-1].minute - 1,5)==0:
			#self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)
			if self.last_orderbook is not None and len(self.open_value['close'][-2:-1:]) == 1:
				self.calc_liquidity_to_stop_loss()
			else:
				self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)
		elif len(self.dates)>2:
			self.total_buy_side_liquity.append(self.total_buy_side_liquity[-1])
			self.total_sell_side_liquity.append(self.total_sell_side_liquity[-1])
		else:
			self.total_buy_side_liquity.append(None)
			self.total_sell_side_liquity.append(None)


		# 	else:
		# 		self.liquidity_sell_stop.append(None)
		# 		self.liquidity_buy_stop.append(None)
		# else:
		# 	self.liquidity_sell_stop.append(None)
		# 	self.liquidity_buy_stop.append(None)




		self.volitilitys.append(self.volility)
		stop_scaler = 2.5
		min_build_scaler = 6
		self.Leverage = 4


		if self.volitilitys[-1] >= 0.9:
			self.rolling_period = int(1440/2)
			self.skew = 0.0015
			self.stoploss = 0.0015*stop_scaler
			self.reach = 0.08
			self.minimum_build_up = int(self.rolling_period/min_build_scaler)
			self.target_bp = 70
		
		elif self.volitilitys[-1] >= 0.75 and self.volitilitys[-1] < 0.9:
			self.rolling_period = int(1440*1)
			self.skew = 0.001
			self.stoploss = 0.0015*stop_scaler
			self.reach = 0.03
			self.minimum_build_up = int(self.rolling_period/min_build_scaler)
			self.target_bp = 60

		elif self.volitilitys[-1] >= 0.5 and self.volitilitys[-1] < 0.75:
			self.rolling_period = int(1440*1.5)
			self.skew = 0.00
			self.stoploss = 0.0015*stop_scaler
			self.reach = 0.02
			self.minimum_build_up = int(self.rolling_period/min_build_scaler)
			self.target_bp = 55

		elif self.volitilitys[-1] >= 0.25 and self.volitilitys[-1] < 0.5:
			self.rolling_period = int(1440*2)
			self.skew = 0.00
			self.stoploss = 0.0015*stop_scaler
			self.reach = 0.01
			self.minimum_build_up = int(self.rolling_period/min_build_scaler)
			self.target_bp = 50

		elif self.volitilitys[-1] < 0.25:
			self.rolling_period = int(1440*5)
			self.skew = 0.00
			self.stoploss = 0.0015*stop_scaler
			self.reach = 0.005
			self.minimum_build_up = int(self.rolling_period/min_build_scaler)
			self.target_bp = 40



		#print(self.volility)


		if len(self.dates)>5:
			self.rolling_high.append(max(self.highs[-int(self.rolling_period):-1:]))
			self.rolling_low.append(min(self.lows[-int(self.rolling_period):-1:]))
		else:
			self.rolling_high.append(None)
			self.rolling_low.append(None)

		self.rolling_periods.append(self.rolling_period)

		if self.candle_count > 5:
			self.buy_exits.append(self.rolling_high[-1]/(1.00 + self.skew)*((self.target_bp/10000)+1))
			self.sell_exits.append(self.rolling_low[-1]*(1.00 + self.skew)/((self.target_bp/10000)+1))
		else:
			self.buy_exits.append(None)
			self.sell_exits.append(None)
		
		
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		
		total = self.account_size[-1]*self.Leverage

		stoploss = None
		#market_order = 'zero'
		market_order = True


		# print(self.highs[-1])
		# print(-self.minimum_build_up)
		# print(self.rolling_high[-1])
		# print(self.highs)
		# #print(max(self.highs[-self.minimum_build_up:-1]))
		# print(self.highs)


		
		if self.candle_count > self.rolling_period and self.total_sell_side_liquity[-1] is not None and self.total_buy_side_liquity[-1] is not None:
			#print(abs(max(self.closes[-6:-1]) - min(self.closes[-6:-1]))/self.closes[-2])
			if abs(max(self.closes[-6:-1]) - min(self.closes[-6:-1]))/self.closes[-2] < self.reach:


				if self.highs[-1] > self.rolling_high[-1]/(1.00 + self.skew) and max(self.highs[-self.minimum_build_up:-1]) <  self.rolling_high[-1]:
					if self.total_sell_side_liquity[-1] >= 0.05:
						self.Leverage = 0.01
					else:
						self.Leverage = 4
					
					print(self.Leverage,self.dates[-1],self.total_sell_side_liquity[-1])
					action = True
					Long = True
					price = self.rolling_high[-1]/(1.00 + self.skew)
					stoploss = price/(1+self.stoploss)
						
				
				if self.lows[-1] < self.rolling_low[-1]*(1.00 + self.skew) and min(self.lows[-self.minimum_build_up:-1]) >  self.rolling_low[-1]:
					if self.total_buy_side_liquity[-1] >= 0.05:
						self.Leverage = 0.01
					else:
						self.Leverage = 4
					
					print(self.Leverage,self.dates[-1],self.total_buy_side_liquity[-1])
					action = True
					Long = False
					price = self.rolling_low[-1]*(1.00 + self.skew)
					stoploss = price*(1+self.stoploss)
		
		total = self.account_size[-1]*self.Leverage
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for Trade in trades:
				avg_open +=Trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		

		if trade.long == True:
			onside_percentage = (self.closes[-2] - trade.entryPrice)/trade.entryPrice
		elif trade.long == False:
			onside_percentage = (trade.entryPrice - self.closes[-2])/trade.entryPrice


		time_before_exit = 15


		if self.candle_count > 25 and trade.trade_length > 10:
			if trade.long == True and min(self.rolling_high[-time_before_exit:]) == max(self.rolling_high[-time_before_exit:]):
				action = True
				price = self.closes[-1]

			if trade.long == False and min(self.rolling_low[-time_before_exit:]) == max(self.rolling_low[-time_before_exit:]):
				action = True
				price = self.closes[-1]

			
			



			#Hedging Exit
		#print(self.highs[-1])
		#print(trade.entryPrice*((self.stoploss*5)+1))


		# if trade.long == True and self.highs[-1] > trade.entryPrice*((self.target_bp/10000)+1):# and self.highs[-6] > self.highs[-4] and self.highs[-6] > self.highs[-3] and self.highs[-6] > self.highs[-2]:
		# 	action = True
		# 	price = trade.entryPrice*((self.target_bp/10000)+1)
		# 	print('scaler and time of close')
		# 	#print(self.long_target_scaler,self.dates[-1])
		
		# 	#price = self.sell_price1
		
		# if trade.long == False and self.lows[-1] < trade.entryPrice/((self.target_bp/10000)+1):# and self.lows[-6] < self.lows[-4] and self.lows[-6] < self.lows[-3] and self.lows[-6] < self.lows[-2]:
		# 	action = True
		# 	price = trade.entryPrice/((self.target_bp/10000)+1)
		# 	print('scaler and time of close')
		# 	#print(self.short_target_scaler,self.dates[-1])

	


		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def calc_liquidity_to_stop_loss(self):

		if self.candle_count > 3:
			if self.last_orderbook is not None:
				self.last_prices = self.indicators.return_large_orders(self.last_orderbook,1000000,50000000)['price'].values
			try:
				self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)
			except:
				pass
			if self.last_orderbook is not None:
				if self.last_prices is not None and self.rolling_high[-1] is not None and self.rolling_low[-1] is not None:
					total_orders = self.indicators.return_large_orders(self.last_orderbook,1000000,50000000)
					total_orders = total_orders[total_orders['price'].isin(self.last_prices)]
					
					total_orders_sell = total_orders[(total_orders['price'] > self.rolling_high[-1]/(1.00 + self.skew)) & (total_orders['price'] < self.rolling_high[-1]*1.01)]
					total_orders_buy = total_orders[(total_orders['price'] < self.rolling_low[-1]*(1.00 + self.skew)) & (total_orders['price'] > self.rolling_low[-1]/1.01)]

					self.total_buy_side_liquity.append(total_orders_buy[total_orders_buy['side'] == 'Buy']['qty'].sum()/self.open_value['close'][-1:].item())
					self.total_sell_side_liquity.append(total_orders_sell[total_orders_sell['side'] == 'Sell']['qty'].sum()/self.open_value['close'][-1:].item())
					self.five_m_dates.append(self.dates[-1])

					#print(self.total_sell_side_liquity[-1])


				else:
					self.total_buy_side_liquity.append(None)
					self.total_sell_side_liquity.append(None)
			else:
				self.total_buy_side_liquity.append(None)
				self.total_sell_side_liquity.append(None)
		else:
			self.total_buy_side_liquity.append(None)
			self.total_sell_side_liquity.append(None)

	def Draw_indicators(self):

		
		# print(len(self.three_day_high_list))
		# print(len(self.three_day_low_list))
		# print(len(self.seven_day_high_list))
		# print(len(self.seven_day_low_list))

		# print(len(self.dates))


		return (self.rolling_high,self.rolling_low,self.rolling_periods,self.volitilitys,self.buy_exits,self.sell_exits,self.total_buy_side_liquity,self.total_sell_side_liquity,self.five_m_dates)
		#return (self._6_hour_high_list,self._12_hour_high_list,self._18_hour_high_list,self._24_hour_high_list,self._36_hour_high_list,self._48_hour_high_list,self._60_hour_high_list,self._72_hour_high_list,self._96_hour_high_list,self._1_week_high_list,self._2_week_high_list,self._6_hour_low_list,self._12_hour_low_list,self._18_hour_low_list,self._24_hour_low_list,self._36_hour_low_list,self._48_hour_low_list,self._60_hour_low_list,self._72_hour_low_list,self._96_hour_low_list,self._1_week_low_list,self._2_week_low_list)#self.accumulation_since_high_list,self.accumulation_since_low_list,self.RSI)
		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.stoplosses,self.open_value,self.max_risks,self.leverages,self.max_risks,self.five_m_dates)#,self.ATR_min_list)
		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.ATR_switches,self.OI_density,self.closing_line,self.five_m_dates)#,self.ATR_min_list)
