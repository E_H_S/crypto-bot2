from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class ATR_ONLY(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False,momentum = False)
		
		self.ATR_p = 288
		if p1 != None:
			self.ATR_p = p1
		#Inititalize inticator vectors
		
		self.ATR = []
		self.ATR2 = []
		self.ATR3 = []

		self.ob_index = None


		self.VPOC = np.array([None,None]*5)


		self.buy_line1 = [None,None]
		self.sell_line1 = [None,None]
		self.buy_line2 = [None,None]
		self.sell_line2 = [None,None]
		self.buy_line3 = [None,None]
		self.sell_line3 = [None,None]
		self.buy_line4 = [None,None]
		self.sell_line4 = [None,None]
		self.buy_line5 = [None,None]
		self.sell_line5 = [None,None]
		self.buy_line6 = [None,None]
		self.sell_line6 = [None,None]
		

		self.mid_line = [None,None]



		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None



		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0


		self.middle = None


		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True


		self.b_1_ready = False
		self.b_2_ready = False
		self.b_3_ready = False
		self.b_4_ready = False
		self.b_5_ready = False
		self.b_6_ready = False


		self.s_1_ready = False
		self.s_2_ready = False
		self.s_3_ready = False

		self.s_4_ready = False
		self.s_5_ready = False
		self.s_6_ready = False
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = np.array([])

		self.ATR_switches = np.array([])

		self.numSimulTrades = 6
		self.margin = True

		self.count10 = 0
		self.switch10 = False

		self.candle_count = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.t1 = datetime.datetime.utcnow()

		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']

		self.t2 = datetime.datetime.utcnow()

		#Update relevent indicators
		self.tick_indicators(price_info)

	def tick_indicators(self,price_info):
		#updating Indicators
		#pass
		self.candle_count += 1
		#self.momentum = self.indicators.get_momentum(self.dates[-1])



		self.ATR.append(self.indicators.avg_range(price_info['closes'],price_info['highs'],price_info['lows'],288))
		self.ATR2.append(self.indicators.avg_range(price_info['closes'],price_info['highs'],price_info['lows'],288*7))
		self.ATR3.append(self.indicators.avg_range(price_info['closes'],price_info['highs'],price_info['lows'],288*14))
		self.t3 = datetime.datetime.utcnow()
		
		if self.candle_count> 1000:
			x = sum(self.ATR[-100:])
		self.t4 = datetime.datetime.utcnow()

		self.timer1 += (self.t2 - self.t1).total_seconds()
		self.timer2 += (self.t3 - self.t2).total_seconds()
		self.timer3 += (self.t4 - self.t3).total_seconds()

		#self.OV_cutoff_list = 
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		#date = self.dates[-1:]
		date = None
		total = 500
		stoploss = None
		#market_order = 'zero'
		market_order = False


		



		# if len(self.dates) > 2:
		# 	if len(self.open_value['max_delta_OI'])> 0:
		# 		#print(self.open_value['max_delta'])

		# 		if self.open_value['max_delta_OI'][-1:].isnull().item() == False:
		# 			#if self.open_value['max_delta'][-1:].item() < -8000 and self.ATR[-1:] > 0.0009 :
		# 			if  self.ATR_switch == True :
						
		# 				if sp != None:
		# 					if self.highs[-1:] > sp and self.closes[-2:-1] < sp and sp > self.VPOC[-1:]*1.000125:
		# 						action = True
		# 						Long = False
		# 						price = sp
		# 						#stoploss = self.highs[-288*2:].max()*1.02
								
		# 				if bp != None:

		# 					if self.lows[-1:] < bp and self.closes[-2:-1] > bp and bp < self.VPOC[-1:]/1.000125:
		# 						action = True
		# 						Long = True
		# 						price = bp
		# 						#stoploss = self.lows[-288*2:].min()/1.02



		


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		# #print(trade.trade_length)
		# #print(date)

		# #if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:



		# if self.highs[-1:] > self.VPOC[-1:] and trade.long == True:
		# #if self.highs[-1:] > self.sell_price1 and trade.long == True:
		# 	action = True
		# 	price = self.VPOC[-1:]
		# 	#price = self.sell_price1
		
					


		# #if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
		# if self.lows[-1:] < self.VPOC[-1:] and trade.long == False:
		# #if self.lows[-1:] < self.buy_price1 and trade.long == False:
		# 	action = True
		# 	price = self.VPOC[-1:]
		# 	#price = self.buy_price1
		
		


		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		#momentum_indicator = self.momentum['momentum_final']
		#print(momentum_indicator)
		return (self.ATR,self.ATR2,self.ATR3,)#(self.VPOC,self.buy_line2,self.sell_line2,self.buy_line4,self.sell_line4,self.buy_line6,self.sell_line6,self.ATR,self.open_value,self.ATR_switches)#,self.ATR_min_list)






