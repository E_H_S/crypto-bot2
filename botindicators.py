import numpy
import sqlite3 as sql
import pandas as pd
import datetime
import time
import pickle
from multiprocessing import Process, Queue
import psycopg2


class BotIndicators(object):
	def __init__(self,orderbook = False,open_value = False,funding_rate = False,startTime = None,endTime = None,volume_profile = False,anchored = False,pairs = ['XBTUSD'],open_value_candles = False,preped_volume_profiles = False,momentum = False,momentum_time_period = 12,RSI = False,daily_candles = False,VWAP = False):
		self.orderbook = orderbook
		self.open_value = open_value
		self.funding_rate = funding_rate
		self.volume_profile = volume_profile

		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0
		self.timer4 = 0

		

		self.number_of_processes_for_queries = 4


		#self.db_file_path = '../Database_Prep/Database1_copy1.db'
		self.db_file_path = '//Volumes/Samsung_T5/Databaseprep2/Bitmex_backup_db10.db'


		self.startTime_SYD = startTime.replace(tzinfo=None) 
		self.endTime_SYD = endTime.replace(tzinfo=None)

		
		self.startTime_UTC = startTime - datetime.timedelta(hours = 11)
		self.endTime_UTC = endTime - datetime.timedelta(hours = 11)
		



		print(self.startTime_SYD)
		print(self.endTime_SYD)
		print(self.startTime_UTC)
		print(self.endTime_UTC)


		self.orderbook_data = pd.DataFrame()
		self.open_value_data = pd.DataFrame()
		self.funding_rate_data = pd.DataFrame()
		self.pairs = pairs


		self.month_number =  self.startTime_UTC.month
		if self.month_number == 3:
			table_name = 'XBT_orderbook_March_2_5'
		elif self.month_number == 4:
			table_name = 'XBT_orderbook_April_5'
		elif self.month_number == 5:
			table_name = 'XBT_orderbook_May_5'
		elif self.month_number == 6:
			table_name = 'XBT_orderbook_June_10'
		elif self.month_number == 7:
			table_name = 'XBT_orderbook_July_10'
		elif self.month_number == 8:
			table_name = 'XBT_orderbook_Aug_10'
		elif self.month_number == 9:
			table_name = 'XBT_orderbook_Sep_10'
		elif self.month_number == 10:
			table_name = 'XBT_orderbook_Oct_10'


		if self.orderbook == True:
			self.prep_orderbook_segment(table_name)
		if self.open_value == True:
			self.query_open_value_data()
		if self.funding_rate == True:
			self.query_funding_rate()
		if self.volume_profile == True:
			self.query_volume_profile(anchored = anchored)
		if open_value_candles == True:
			self.query_open_interest_5m_candles()
		if preped_volume_profiles == True:
			self.query_pre_made_volume_profiles()
		if momentum == True:
			self.momentum_query(time_period = momentum_time_period)
		if RSI == True:
			self.RSI_query(period = 12)
		if daily_candles == True:
			self.query_daily_candles()
		if VWAP == True:
			self.VWAP()


	def query_pre_made_volume_profiles(self):

		table = 'XBT_Volume_profiles'
		start_UTC = self.startTime_UTC.strftime("%Y-%m-%dT%H:%M:%S.%fZ") 
		end_UTC = self.endTime_UTC.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

		with sql.connect(self.db_file_path) as con:
			cur = con.cursor()
			cur.execute("""
			SELECT DISTINCT
			*
			FROM {}
			WHERE 1=1
			and time_stamp_close > ?
			and time_stamp_close < ?
			ORDER BY time_stamp_close ASC
			""".format(table),(start_UTC,end_UTC,))


		self.volume_profiles_mix =  pd.DataFrame(cur.fetchall())
		print(self.volume_profiles_mix)
		self.volume_profiles_mix.columns = ['avg_price','volume','cum','percent','time_stamp_close']
		print(self.volume_profiles_mix)
		self.volume_profiles_mix['datetimes_UTC'] = pd.to_datetime(self.volume_profiles_mix['time_stamp_close'])
		self.volume_profiles_mix['datetimes_SYD'] = self.volume_profiles_mix['datetimes_UTC'] + pd.Timedelta(hours = 11) + pd.Timedelta(minutes = 5) 
		self.volume_profiles_mix = self.volume_profiles_mix.set_index('datetimes_SYD')
		self.volume_profiles  = []
		dates = self.volume_profiles_mix.index.unique()
		count = 0
		length = len(dates)
		for date in dates:
			count+=1
			if numpy.mod(count,200) == 0:
				print('volume profiles appending.. '+ str(count/length)+'%')
			vp = self.volume_profiles_mix[self.volume_profiles_mix.index == date]
			vp = vp.set_index('avg_price')
			self.volume_profiles.append(vp)

		#print(self.volume_profiles)





	def query_open_interest_5m_candles(self):
		if self.pairs[0] == 'XBTUSD':
			table = 'XBT_OI_5m_candles'
			#table = 'XBT_OI_5m_candles_July_AUG4'
			#table = 'XBT_OI_1m_candles_AUG5'
		elif self.pairs[0] == 'ETHUSD':
			table = 'ETH_OI_5m_candles2'

		orderby1 = 'time_stamp_close'
		orderby2 = ''

		self.OI_5m_candles = self.query(table,self.startTime_UTC,self.endTime_UTC,self.number_of_processes_for_queries,orderby1,orderby2,format2 = "%Y-%m-%dT%H:%M:%S.%fZ")
		

		print('LEN OF RETURN DATA', len(self.OI_5m_candles))
		
		#self.OI_5m_candles.columns = ['OI_open','OI_high','OI_low','OI_close','OV_open','OV_high','OV_low','OV_close','time_stamp_close']
		self.OI_5m_candles.columns = ['time_stamp_close','OI_open','OI_high','OI_low','OI_close']

		print(self.OI_5m_candles['time_stamp_close'].head())
		print(self.OI_5m_candles['time_stamp_close'].tail())





		#self.OI_5m_candles.columns = self.OI_5m_candles.sort_values(by = 'time_stamp_close')
		self.OI_5m_candles['datetimes_UTC'] = pd.to_datetime(self.OI_5m_candles['time_stamp_close'])
		self.OI_5m_candles['datetimes_SYD'] = self.OI_5m_candles['datetimes_UTC'] + pd.Timedelta(hours = 11) + pd.Timedelta(minutes = 5) 
		#print(self.OI_5m_candles)
		
		self.OI_5m_candles = self.OI_5m_candles.set_index('datetimes_SYD')
		#self.OI_5m_candles = self.OI_5m_candles.tz_convert(None)



		#print(self.OI_5m_candles)
		self.OI_5m_candles['datetimes_SYD'] = self.OI_5m_candles.index
		#self.OI_5m_candles['datetimes_SYD'] = self.OI_5m_candles['datetimes_SYD'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
		print(self.OI_5m_candles.index,self.OI_5m_candles['datetimes_SYD'],self.OI_5m_candles['datetimes_UTC'])


		self.OI_5m_candles['delta_OI0'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(3)
		self.OI_5m_candles['delta_OI1'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(4)
		self.OI_5m_candles['delta_OI2'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(5)
		self.OI_5m_candles['delta_OI3'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(6)
		self.OI_5m_candles['delta_OI4'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(7)
		self.OI_5m_candles['delta_OI5'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(8)
		self.OI_5m_candles['delta_OI6'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(9)
		self.OI_5m_candles['delta_OI7'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(10)
		self.OI_5m_candles['delta_OI8'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(11)
		self.OI_5m_candles['delta_OI9'] = self.OI_5m_candles['OI_low'] - self.OI_5m_candles['OI_high'].shift(12)


		self.OI_5m_candles['max_delta_OI0'] = self.OI_5m_candles['delta_OI0'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI1'] = self.OI_5m_candles['delta_OI1'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI2'] = self.OI_5m_candles['delta_OI2'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI3'] = self.OI_5m_candles['delta_OI3'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI4'] = self.OI_5m_candles['delta_OI4'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI5'] = self.OI_5m_candles['delta_OI5'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI6'] = self.OI_5m_candles['delta_OI5'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI7'] = self.OI_5m_candles['delta_OI5'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI8'] = self.OI_5m_candles['delta_OI5'].rolling(12).min()
		self.OI_5m_candles['max_delta_OI9'] = self.OI_5m_candles['delta_OI5'].rolling(12).min()


		# self.OI_5m_candles['delta_OV0'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(3)
		# self.OI_5m_candles['delta_OV1'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(4)
		# self.OI_5m_candles['delta_OV2'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(5)
		# self.OI_5m_candles['delta_OV3'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(6)
		# self.OI_5m_candles['delta_OV4'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(7)
		# self.OI_5m_candles['delta_OV5'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(8)
		# self.OI_5m_candles['delta_OV6'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(9)
		# self.OI_5m_candles['delta_OV7'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(10)
		# self.OI_5m_candles['delta_OV8'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(11)
		# self.OI_5m_candles['delta_OV9'] = self.OI_5m_candles['OV_low'] - self.OI_5m_candles['OV_high'].shift(12)


		# self.OI_5m_candles['max_delta_OV0'] = self.OI_5m_candles['delta_OV0'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV1'] = self.OI_5m_candles['delta_OV1'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV2'] = self.OI_5m_candles['delta_OV2'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV3'] = self.OI_5m_candles['delta_OV3'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV4'] = self.OI_5m_candles['delta_OV4'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV5'] = self.OI_5m_candles['delta_OV5'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV6'] = self.OI_5m_candles['delta_OV5'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV7'] = self.OI_5m_candles['delta_OV5'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV8'] = self.OI_5m_candles['delta_OV5'].rolling(12).min()
		# self.OI_5m_candles['max_delta_OV9'] = self.OI_5m_candles['delta_OV5'].rolling(12).min()


		#print(self.OI_5m_candles)

		self.OI_5m_candles['MAX_MAX_delta_Event_OI'] = self.OI_5m_candles[['max_delta_OI0','max_delta_OI1','max_delta_OI2','max_delta_OI3','max_delta_OI4','max_delta_OI5','max_delta_OI6','max_delta_OI7','max_delta_OI8','max_delta_OI9']].min(axis = 1,skipna = True)
		self.OI_5m_candles['MAX_MAX_delta_Lookback_OI'] = self.OI_5m_candles['MAX_MAX_delta_Event_OI'].rolling(288*5,min_periods = 1).min()

		# self.OI_5m_candles['MAX_MAX_delta_Event_OV'] = self.OI_5m_candles[['max_delta_OV0','max_delta_OV1','max_delta_OV2','max_delta_OV3','max_delta_OV4','max_delta_OV5','max_delta_OV6','max_delta_OV7','max_delta_OV8','max_delta_OV9']].min(axis = 1,skipna = True)
		# self.OI_5m_candles['MAX_MAX_delta_Lookback_OV'] = self.OI_5m_candles['MAX_MAX_delta_Event_OV'].rolling(288*5,min_periods = 1).min()

		#print(self.OI_5m_candles)
		return self.OI_5m_candles
		#print(self.OI_5m_candles)



	def prep_orderbook_segment(self,table):

			#table = 'XBT_orderbook_April'
			#table = 'XBT_orderbook_May_2_5'
			#table = 'XBT_orderbook_June_2_5'
			#table = 'XBT_orderbook_July_2_5'
			#table = 'XBT_orderbook_April_2_5'
			#table = 'XBT_orderbook_July'
			orderby1 = 'time_stamp'
			orderby2 = ',price'

			if table == 'XBT_orderbook_Feb_2_5':
				ST = datetime.datetime(2019,2,1,0,0,0)
				ET = datetime.datetime(2019,3,1,0,0,0)
			elif table == 'XBT_orderbook_March_2_5':
				ST = datetime.datetime(2019,3,1,0,0,0)
				ET = datetime.datetime(2019,4,1,0,0,0)
			elif table == 'XBT_orderbook_April_5':
				ST = datetime.datetime(2019,4,1,0,0,0)
				ET = datetime.datetime(2019,5,1,0,0,0)
			elif table == 'XBT_orderbook_May_5':
				ST = datetime.datetime(2019,5,1,0,0,0)
				ET = datetime.datetime(2019,6,1,0,0,0)
			elif table == 'XBT_orderbook_June_10':
				ST = datetime.datetime(2019,6,1,0,0,0)
				ET = datetime.datetime(2019,7,1,0,0,0)
			elif table == 'XBT_orderbook_July_10':
				ST = datetime.datetime(2019,7,1,0,0,0)
				ET = datetime.datetime(2019,8,1,0,0,0)
			elif table == 'XBT_orderbook_Aug_10':
				ST = datetime.datetime(2019,8,1,0,0,0)
				ET = datetime.datetime(2019,9,1,0,0,0)
			elif table == 'XBT_orderbook_Sep_10':
				ST = datetime.datetime(2019,9,1,0,0,0)
				ET = datetime.datetime(2019,10,1,0,0,0)
			elif table == 'XBT_orderbook_Oct_10':
				ST = datetime.datetime(2019,10,1,0,0,0)
				ET = datetime.datetime(2019,11,1,0,0,0)


			if ST < self.startTime_UTC:
				ST = self.startTime_UTC
			if ET > self.endTime_UTC:
				ET = self.endTime_UTC

			print('Final ST ',ST)
			print('Final ST ',ET)


			orderbook_data = self.query(table,ST,ET,self.number_of_processes_for_queries,orderby1,orderby2)

			if len(orderbook_data) > 0:
				orderbook_data.columns = ['price','side','qty','cum','time_stamp']
				#self.orderbook_data.columns = ['price','side','qty','time_stamp','cum']

			#orderbook_data = orderbook_data.sort_values(by = 'time_stamp')

			try:
				orderbook_data = orderbook_data.sort_values(by = 'time_stamp')
				orderbook_data['datetimes_UTC'] = pd.to_datetime(orderbook_data['time_stamp'])
				orderbook_data['datetimes_SYD'] = orderbook_data['datetimes_UTC'] + pd.Timedelta(hours = 11)
			except:
				print("No orderbook data for this time period")

			orderbook_data = orderbook_data.set_index('datetimes_SYD',drop = True)
			orderbook_data['price'] = orderbook_data['price'].astype(float)
			orderbook_data['datetimes_SYD'] = orderbook_data['datetimes_UTC'] + pd.Timedelta(hours = 11)
			orderbook_time_stamps =  pd.unique(orderbook_data['time_stamp'])
			orderbook_datetimes =  pd.unique(orderbook_data.index)
			orderbook_data['qty2'] = orderbook_data['qty']

			orderbook_data.loc[orderbook_data.qty>8000000,'qty2'] = 0

			#print(self.orderbook_datetimes)
			print("num or df's ", len(orderbook_datetimes))

			self.books = []
			count = 0

			for time_stamp in orderbook_datetimes:
				if numpy.mod(count,100) == 0:
					print(count)

				df = orderbook_data[orderbook_data.index == time_stamp]

				self.books.append(df)
				count += 1
			self.orderbook_data = orderbook_data
			self.orderbook_datetimes = orderbook_datetimes
			self.orderbook_time_stamps = orderbook_time_stamps
			#print(self.books)
			
			

	def query_open_value_data(self):

		self.open_value_pairs = {}
		for pair in self.pairs:
			if self.open_value == True:
				if pair == 'XBTUSD':
					table = 'XBT_instrument'
				elif pair == 'ETHUSD':
					table = 'ETH_instrument'

				start_UTC = self.startTime_UTC.strftime("%Y-%m-%dT%H:%M:%S.%fZ") 
				end_UTC = self.endTime_UTC.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
				print(self.startTime_UTC,self.endTime_UTC)
				print(start_UTC,end_UTC)

				#with sql.connect(self.db_file_path) as con:
				with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
					cur = con.cursor()
					cur.execute("""
					SELECT DISTINCT
					*
					FROM {}
					WHERE 1=1
					and time_stamp > ?
					and time_stamp < ?
					ORDER BY time_stamp ASC
					""".format(table),(start_UTC,end_UTC,))
					self.open_value_data = cur.fetchall()
					self.open_value_data = pd.DataFrame(self.open_value_data)

					if pair == 'XBTUSD':
						self.open_value_data.columns = ['time_stamp','open_interest','open_value','XBT_price','volume']
					elif pair == 'ETHUSD':
						self.open_value_data.columns = ['time_stamp','open_interest','open_value','ETH_price','volume','XBT_price']

				self.open_value_data['datetimes_UTC'] = pd.to_datetime(self.open_value_data['time_stamp'])

				self.open_value_data['datetimes_SYD'] = self.open_value_data['datetimes_UTC'] + pd.Timedelta(hours = 11)
				self.open_value_data['open_value'] = self.open_value_data['open_value']/100000000
				self.open_value_data['ov_diff'] = self.open_value_data['open_value'].diff()
				self.open_value_data = self.open_value_data.set_index('datetimes_SYD')
				self.open_value_data = self.open_value_data.tz_convert(None)
				self.open_value_data['datetimes_SYD'] = self.open_value_data.index
				self.open_value_data['datetimes_SYD'] = self.open_value_data['datetimes_SYD'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")



				self.open_value_data['delta_OI0'] = self.open_value_data['open_interest'].diff(6*25)
				self.open_value_data['delta_OI1'] = self.open_value_data['open_interest'].diff(6*20)
				self.open_value_data['delta_OI2'] = self.open_value_data['open_interest'].diff(6*15)
				self.open_value_data['delta_OI3'] = self.open_value_data['open_interest'].diff(6*10)
				self.open_value_data['delta_OI4'] = self.open_value_data['open_interest'].diff(6*5)
				self.open_value_data['delta_OI5'] = self.open_value_data['open_interest'].diff(6*3)
				self.open_value_data['delta_OI6'] = self.open_value_data['open_interest'].diff(6*30)
				self.open_value_data['delta_OI7'] = self.open_value_data['open_interest'].diff(6*40)
				self.open_value_data['delta_OI8'] = self.open_value_data['open_interest'].diff(6*50)
				self.open_value_data['delta_OI9'] = self.open_value_data['open_interest'].diff(6*60)


				self.open_value_data['max_delta_OI0'] = self.open_value_data['delta_OI0'].rolling(6*60).min()
				self.open_value_data['max_delta_OI1'] = self.open_value_data['delta_OI1'].rolling(6*60).min()
				self.open_value_data['max_delta_OI2'] = self.open_value_data['delta_OI2'].rolling(6*60).min()
				self.open_value_data['max_delta_OI3'] = self.open_value_data['delta_OI3'].rolling(6*60).min()
				self.open_value_data['max_delta_OI4'] = self.open_value_data['delta_OI4'].rolling(6*60).min()
				self.open_value_data['max_delta_OI5'] = self.open_value_data['delta_OI5'].rolling(6*60).min()
				self.open_value_data['max_delta_OI6'] = self.open_value_data['delta_OI5'].rolling(6*60).min()
				self.open_value_data['max_delta_OI7'] = self.open_value_data['delta_OI5'].rolling(6*60).min()
				self.open_value_data['max_delta_OI8'] = self.open_value_data['delta_OI5'].rolling(6*60).min()
				self.open_value_data['max_delta_OI9'] = self.open_value_data['delta_OI5'].rolling(6*60).min()




				self.open_value_data['max_delta_lookback_OI'] = self.open_value_data['delta_OI0'].rolling(6*60*24*5).min()


				self.open_value_data['delta'] = self.open_value_data['open_value'].diff(6*30)
				self.open_value_data['max_delta'] = self.open_value_data['delta'].rolling(6*60).min()
				self.open_value_data['max_delta_lookback'] = self.open_value_data['delta'].rolling(6*60*24*5).min()

				#self.open_value_data_np = np.array(self.open_value_data['delta'])
				self.open_value_pairs[pair] = self.open_value_data




	def query_funding_rate(self):

		self.funding_rate_pairs = {}
		for pair in self.pairs:
			if self.funding_rate == True:
				if pair == 'XBTUSD':
					table = 'XBT_FR'
				elif pair == 'ETHUSD':
					table = 'ETH_FR'


				start_UTC = self.startTime_UTC.strftime("%Y-%m-%dT%H:%M:%S.%fZ") 
				end_UTC = self.endTime_UTC.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
				

				#with sql.connect(self.db_file_path) as con:
				with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
					cur = con.cursor()
					cur.execute("""
					SELECT DISTINCT
					*
					FROM {}
					WHERE 1=1
					and time_stamp > ?
					and time_stamp < ?
					ORDER BY time_stamp ASC
					""".format(table),(start_UTC,end_UTC,))
					
					self.funding_rate_data = cur.fetchall()
					self.funding_rate_data = pd.DataFrame(self.funding_rate_data)

					#if pair == 'XBTUSD':
					self.funding_rate_data.columns = ['fundingRate','dailyFundingRate','time_stamp']
					
				self.funding_rate_data['datetimes_UTC'] = pd.to_datetime(self.funding_rate_data['time_stamp'])
				self.funding_rate_data['datetimes_SYD'] = self.funding_rate_data['datetimes_UTC'] + pd.Timedelta(hours = 11)
				self.funding_rate_data = self.funding_rate_data.set_index('datetimes_SYD')
				self.funding_rate_data = self.funding_rate_data.tz_convert(None)
				self.funding_rate_data['datetimes_SYD'] = self.funding_rate_data.index
				self.funding_rate_data['datetimes_SYD'] = self.funding_rate_data['datetimes_SYD'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
				
				self.funding_rate_pairs[pair] = self.funding_rate_data
				print(self.funding_rate_data)
				


	def query_volume_profile(self,anchored = False):
		self.volume_profile_pairs = {}
		self.one_m_candles_pairs = {}

		for pair in self.pairs:
			if self.volume_profile == True:
				if pair == 'XBTUSD':
					table = 'XBT_1m_candles'
					#table = 'candles1m_XBT4'
				elif pair == 'ETHUSD':
					table = 'candles1m_ETH'

				#start_UTC = self.startTime_UTC.strftime("%Y-%m-%d %H:%M:%S.%fZ") 
				#end_UTC = self.endTime_UTC.strftime("%Y-%m-%d %H:%M:%S.%fZ")
				
				#with sql.connect(self.db_file_path) as con:
				with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
					cur = con.cursor()
					cur.execute("""
					SELECT DISTINCT
					*
					FROM {}
					WHERE 1=1
					and time_stamp > %s
					and time_stamp < %s
					ORDER BY time_stamp ASC
					""".format(table),(self.startTime_UTC,self.endTime_UTC,))
					
					self.one_m_candles = cur.fetchall()
				
					
				self.one_m_candles = pd.DataFrame(self.one_m_candles)
				self.one_m_candles.columns = ['time_stamp','open','close','high','low','volume']
				self.one_m_candles['candle_num'] = self.one_m_candles.index
					

				self.one_m_candles['datetimes_UTC'] = pd.to_datetime(self.one_m_candles['time_stamp'])
				self.one_m_candles['datetimes_SYD'] = self.one_m_candles['datetimes_UTC'] + pd.Timedelta(hours = 11) 
				self.one_m_candles = self.one_m_candles.set_index('datetimes_SYD')
				#self.one_m_candles = self.one_m_candles.tz_convert(None)
				self.one_m_candles['datetimes_SYD'] = self.one_m_candles.index
				self.total_candles = len(self.one_m_candles)
				

				self.one_m_candles['avg_price'] = (self.one_m_candles['open'] + self.one_m_candles['close'] + self.one_m_candles['high'] + self.one_m_candles['low'])/4
				if self.pairs[0] == 'XBTUSD':
					self.one_m_candles['avg_price'] = self.one_m_candles['avg_price'].round(0)
				elif self.pairs[0] == 'ETHUSD':
					self.one_m_candles['avg_price'] = self.one_m_candles['avg_price'].round(1)


				#print(self.one_m_candles.head())
				self.one_m_candles['datetimes_SYD'] = self.one_m_candles['datetimes_SYD'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
				#print(self.one_m_candles)
				self.one_m_candles_pairs[pair] = self.one_m_candles
				if anchored == False:
					self.volume_profiles = self.split_tasks(self.create_volume_profiles,self.startTime_SYD,self.endTime_SYD,4)
					#self.volume_profile_pairs[pair] = self.create_volume_profiles(self.startTime_SYD,self.endTime_SYD)

			

	def query_daily_candles(self):

		#table = 'candles1d_XBT'
		table = 'XBT_1d_candles'
		#with sql.connect(self.db_file_path) as con:
		with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
			cur = con.cursor()
			cur.execute("""
			SELECT DISTINCT
			*
			FROM {}
			WHERE 1=1
			and time_stamp > %s
			and time_stamp < %s
			ORDER BY time_stamp ASC
			""".format(table),(datetime.datetime(2018,1,1),datetime.datetime(2019,12,1),))

			self.one_d_candles = cur.fetchall()

		self.one_d_candles = pd.DataFrame(self.one_d_candles)
		self.one_d_candles.columns = ['date','open','close','high','low','volume','time_stamp']
		self.one_d_candles['candle_num'] = self.one_d_candles.index
			

		self.one_d_candles['datetimes_UTC'] = pd.to_datetime(self.one_d_candles['time_stamp'])
		self.one_d_candles['datetimes_SYD'] = self.one_d_candles['datetimes_UTC'] + pd.Timedelta(hours = 11) 
		self.one_d_candles = self.one_d_candles.set_index('datetimes_SYD')
		self.one_d_candles = self.one_d_candles.tz_convert(None)
		self.one_d_candles['datetimes_SYD'] = self.one_d_candles.index

		

	def movingAverage(self, dataPoints, period):
		if (len(dataPoints) > 1):
			return sum(dataPoints[-period:]) / float(len(dataPoints[-period:]))
		else:
			return dataPoints[0]

	def momentum_query(self,time_period = 24):# Also takes time_periods (1,2,6,12,24)

		if self.pairs[0] == 'XBTUSD':

			table = 'XBT_5m_candles'
			#table = 'candles5m_XBT'
			#table = 'candles5m_XBT3'
			#table = 'candles1m_XBT4'
		elif self.pairs[0] == 'ETHUSD':
			#table = 'candles5m_ETH'
			table = 'ETH_5m_candles'
		
		#with sql.connect(self.db_file_path) as con:
		with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
			cur = con.cursor()
			cur.execute("""
			SELECT DISTINCT
			*
			FROM {}
			WHERE 1=1
			and time_stamp > %s
			and time_stamp < %s
			ORDER BY time_stamp ASC
			""".format(table),(self.startTime_UTC,self.endTime_UTC,))
			
		
		self.five_m_candles = pd.DataFrame(cur.fetchall())
		self.five_m_candles.columns  = ['time_stamp','open','close','high','low','volume']

		self.five_m_candles['datetimes_UTC'] = pd.to_datetime(self.five_m_candles['time_stamp'])
		
		self.five_m_candles['datetimes_SYD'] = self.five_m_candles['datetimes_UTC'] + pd.Timedelta(hours = 11)
		

		self.five_m_candles = self.five_m_candles.set_index('datetimes_SYD')
		#self.five_m_candles = self.five_m_candles.tz_convert(None)
		self.five_m_candles['avg_price'] = (self.five_m_candles['open'] + self.five_m_candles['close'] + self.five_m_candles['high'] + self.five_m_candles['low'])/4
		
		if time_period == 1:
			k =3*5
			n = [k,k*2,k*3,k*4,k*5,k*6,k*7,k*8,k*9,k*10,k*11,k*12,k*13,k*14,k*15,k*16,k*17,k*18,k*19,k*20,k*21,k*22,k*23,k*24]
			#n = [1,2,3,4,5,6,7,8,9,10,11,12]
		else:

			if time_period == 24: #48 hour momentum
				k = 24
			elif time_period == 12: #24 hour momentum
				k = 12
			elif time_period == 6: #12 hour momentum
				k = 6
			elif time_period == 3: #6 hour momentum
				k = 3
			
			n = [k,k*2,k*3,k*4,k*5,k*6,k*7,k*8,k*9,k*10,k*11,k*12,k*13,k*14,k*15,k*16,k*17,k*18,k*19,k*20,k*21,k*22,k*23,k*24]
		
		self.five_m_candles['momentum1'] = (self.five_m_candles['avg_price'].diff(n[0]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum2'] = (self.five_m_candles['avg_price'].diff(n[1]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum3'] = (self.five_m_candles['avg_price'].diff(n[2]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum4'] = (self.five_m_candles['avg_price'].diff(n[3]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum5'] = (self.five_m_candles['avg_price'].diff(n[4]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum6'] = (self.five_m_candles['avg_price'].diff(n[5]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum7'] = (self.five_m_candles['avg_price'].diff(n[6]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum8'] = (self.five_m_candles['avg_price'].diff(n[7]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum9'] = (self.five_m_candles['avg_price'].diff(n[8]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum10'] = (self.five_m_candles['avg_price'].diff(n[9]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum11'] = (self.five_m_candles['avg_price'].diff(n[10]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum12'] = (self.five_m_candles['avg_price'].diff(n[11]))/self.five_m_candles['avg_price']
		#if time_period != 1:
		self.five_m_candles['momentum13'] = (self.five_m_candles['avg_price'].diff(n[12]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum14'] = (self.five_m_candles['avg_price'].diff(n[13]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum15'] = (self.five_m_candles['avg_price'].diff(n[14]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum16'] = (self.five_m_candles['avg_price'].diff(n[15]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum17'] = (self.five_m_candles['avg_price'].diff(n[16]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum18'] = (self.five_m_candles['avg_price'].diff(n[17]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum19'] = (self.five_m_candles['avg_price'].diff(n[18]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum20'] = (self.five_m_candles['avg_price'].diff(n[19]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum21'] = (self.five_m_candles['avg_price'].diff(n[20]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum22'] = (self.five_m_candles['avg_price'].diff(n[21]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum23'] = (self.five_m_candles['avg_price'].diff(n[22]))/self.five_m_candles['avg_price']
		self.five_m_candles['momentum24'] = (self.five_m_candles['avg_price'].diff(n[23]))/self.five_m_candles['avg_price']

		self.five_m_candles['momentum_final'] = self.five_m_candles[['momentum1','momentum2','momentum3','momentum4','momentum5','momentum6','momentum7','momentum8','momentum9','momentum10','momentum11','momentum12','momentum13','momentum14','momentum15','momentum16','momentum17','momentum18','momentum19','momentum20','momentum21','momentum22','momentum23','momentum24']].mean(axis = 1,skipna = True)
		

		# else:
		self.five_m_candles['momentum_final_small'] = self.five_m_candles[['momentum1','momentum2','momentum3','momentum4','momentum5','momentum6','momentum7','momentum8','momentum9','momentum10','momentum11','momentum12']].mean(axis = 1,skipna = True)
			
	def RSI_query(self,period = 12):# Also takes time_periods (1,2,6,12,24)

		if self.pairs[0] == 'XBTUSD':
			table = 'XBT_5m_candles'
			#table = 'candles5m_XBT3'
			#table = 'candles1m_XBT4'
		elif self.pairs[0] == 'ETHUSD':
			table = 'candles5m_ETH'
		
		#with sql.connect(self.db_file_path) as con:
		with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
			cur = con.cursor()
			cur.execute("""
			SELECT DISTINCT
			*
			FROM {}
			WHERE 1=1
			and time_stamp > ?
			and time_stamp < ?
			ORDER BY time_stamp ASC
			""".format(table),(self.startTime_UTC,self.endTime_UTC,))
			
		
		self.five_m_candles_RSI = pd.DataFrame(cur.fetchall())
		self.five_m_candles_RSI.columns  = ['date','open','close','high','low','volume','time_stamp']

		self.five_m_candles_RSI['datetimes_UTC'] = pd.to_datetime(self.five_m_candles_RSI['time_stamp'])
		
		self.five_m_candles_RSI['datetimes_SYD'] = self.five_m_candles_RSI['datetimes_UTC'] + pd.Timedelta(hours = 11)
		

		self.five_m_candles_RSI = self.five_m_candles_RSI.set_index('datetimes_SYD')
		self.five_m_candles_RSI = self.five_m_candles_RSI.tz_convert(None)

		self.five_m_candles_RSI['diff1'] = self.five_m_candles_RSI['close'].diff()
		self.five_m_candles_RSI['posi_diffs'] = self.five_m_candles_RSI[self.five_m_candles_RSI['diff1'] > 0]['diff1']
		self.five_m_candles_RSI['posi_diffs'] = self.five_m_candles_RSI['posi_diffs'].fillna(0)
		self.five_m_candles_RSI['neg_diffs'] = self.five_m_candles_RSI[self.five_m_candles_RSI['diff1'] < 0]['diff1']
		self.five_m_candles_RSI['neg_diffs'] = self.five_m_candles_RSI['neg_diffs'].fillna(0)
		self.five_m_candles_RSI['neg_diffs'] = self.five_m_candles_RSI['neg_diffs']*-1
		self.five_m_candles_RSI['posi_sum'] = self.five_m_candles_RSI['posi_diffs'].rolling(period).sum()
		self.five_m_candles_RSI['neg_sum'] = self.five_m_candles_RSI['neg_diffs'].rolling(period).sum()
		self.five_m_candles_RSI['ratio'] = self.five_m_candles_RSI['posi_sum']/self.five_m_candles_RSI['neg_sum']
		self.five_m_candles_RSI['RSI'] = 100 - 100/(1+self.five_m_candles_RSI['ratio'])

		print(self.five_m_candles_RSI)

	def VWAP(self,period = 288):# Also takes time_periods (1,2,6,12,24)

		if self.pairs[0] == 'XBTUSD':
			#table = 'candles5m_XBT'
			#table = 'candles5m_XBT3'
			table = 'XBT_5m_candles'
		elif self.pairs[0] == 'ETHUSD':
			table = 'candles5m_ETH'
		
		#with sql.connect(self.db_file_path) as con:
		with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
			cur = con.cursor()
			cur.execute("""
			SELECT DISTINCT
			*
			FROM {}
			WHERE 1=1
			and time_stamp > ?
			and time_stamp < ?
			ORDER BY time_stamp ASC
			""".format(table),(self.startTime_UTC,self.endTime_UTC,))
		self.one_m_candles_vwap = pd.DataFrame(cur.fetchall())
		self.one_m_candles_vwap.columns  = ['date','open','close','high','low','volume','time_stamp']

		self.one_m_candles_vwap['datetimes_UTC'] = pd.to_datetime(self.one_m_candles_vwap['time_stamp'])

		
		self.one_m_candles_vwap['datetimes_SYD'] = self.one_m_candles_vwap['datetimes_UTC'] + pd.Timedelta(hours = 11)
		

		self.one_m_candles_vwap = self.one_m_candles_vwap.set_index('datetimes_SYD')
		self.one_m_candles_vwap = self.one_m_candles_vwap.tz_convert(None)

		self.one_m_candles_vwap['price x volume'] = self.one_m_candles_vwap['close']*self.one_m_candles_vwap['volume']

		self.one_m_candles_vwap['total_volume_period'] = self.one_m_candles_vwap['volume'].rolling(period).sum()
		self.one_m_candles_vwap['price x volume avg'] = self.one_m_candles_vwap['price x volume'].rolling(period).sum()

		self.one_m_candles_vwap['vwap'] = self.one_m_candles_vwap['price x volume avg']/self.one_m_candles_vwap['total_volume_period']
		
		self.VWAP_LIST = list(self.one_m_candles_vwap['vwap'])
		self.VWAP_LIST_dates = list(self.one_m_candles_vwap['time_stamp'])



	def get_VWAP(self):
		return self.VWAP_LIST.pop(0),self.VWAP_LIST_dates.pop(0)

	def get_ATR(self,first = False,period = 1440):
		if first == True:
			self.one_m_candles_vwap['ATR'] = (self.one_m_candles_vwap['high'] - self.one_m_candles_vwap['low'])/((self.one_m_candles_vwap['high'] + self.one_m_candles_vwap['low'])/2)
			self.one_m_candles_vwap['ATR_mean'] = self.one_m_candles_vwap['ATR'].rolling(period).mean()
			self.ATR_LIST =  list(self.one_m_candles_vwap['ATR_mean'])

		return self.ATR_LIST.pop(0)

	def get_RSI(self,candleTime):
		candleTime = candleTime + datetime.timedelta(minutes = 5) #- datetime.timedelta(hours = 1)
		candleTime = time.mktime(candleTime.timetuple())
		candleTime = datetime.datetime.fromtimestamp(candleTime)

		data = self.five_m_candles_RSI[(self.five_m_candles_RSI.index <= candleTime)]['RSI'][-1:].item()

		return data

	def get_momentum(self,candleTime,small = False):

		candleTime = candleTime
		candleTime = time.mktime(candleTime.timetuple())
		candleTime = datetime.datetime.fromtimestamp(candleTime)
		if small == False:
			data = self.five_m_candles[(self.five_m_candles.index <= candleTime)]['momentum_final']#['momentum_final']
		elif small == True:
			data = self.five_m_candles[(self.five_m_candles.index <= candleTime)]['momentum_final_1m']#['momentum_final']
			
		return data


	def momentum(self):

		if (len(dataPoints) > period -1):
			return dataPoints[-1] * 100 / dataPoints[-period]

	def EMA(self, prices, period , append = True):
		
		if len(prices) > period:
			x = numpy.asarray(prices)
			weights = None
			weights = numpy.exp(numpy.linspace(-1., 0., period))
			weights /= weights.sum()

			a = numpy.convolve(x, weights, mode='full')[:len(x)]
			a[:period] = a[period]
			if append == True:
				return a[-1:]
			else:
				return a
		else:
			return None
	
			
	def MACD(self, prices, nslow=26, nfast=12):
		if len(prices) > 26:
			emaslow = self.EMA(prices, nslow, append = False)
			emafast = self.EMA(prices, nfast,append = False)
			blue = emafast-emaslow
			red = self.EMA(blue,9,append = False)
			return blue, red, blue - red
		else:
			return None,None,None

	def RSI (self, prices, period=14):
		prices = numpy.array(prices)
		deltas = numpy.diff(prices)
		seed = deltas[:period+1]
		up = seed[seed >= 0].sum()/period
		down = -seed[seed < 0].sum()/period
		rs = up/down
		rsi = numpy.zeros_like(prices)
		rsi[:period] = 100. - 100./(1. + rs)
 
		for i in range(period, len(prices)):
			delta = deltas[i-1]
			if delta >0:
				upval = delta
				downval = 0.
			else:
				upval = 0.
				downval = -delta

			up = (up*(period - 1) + upval)/period
			down = (down*(period - 1) + downval)/period
			rs = up/down
			rsi[i] = 100. - 100./(1. + rs)
		if len(prices) > period:
			return rsi[-1]
		else:
			return 50 # output a neutral amount until enough prices in list to calculate RSI


	def RSI(self,candle_date,period):
		pass

	def RSI_SIG (self, prices,period = 14):
		pass
	def Bollingerband(self,prices,movingAverage,num_sdv,period = 20):
		if len(prices) < period or movingAverage[-1:] == None:
			return None,None
		else:
			sdv = numpy.std(prices[-period:])
			upper_band = movingAverage[-1:] + num_sdv*sdv
			lower_band = movingAverage[-1:] - num_sdv*sdv
			return upper_band,lower_band
	def avg_range(self,prices,highs,lows,period):
		Highs = numpy.array(highs[-period:])
		Lows = numpy.array(lows[-period:])
		Prices = numpy.array(prices[-period:])
		if len(highs) > period:
			varience = (Highs - Lows)/Prices
			Mean_V =  numpy.mean(varience)
			return Mean_V
		else:
			return None
	def std(self,prices,period):
		if len(prices) < period:
			return None
		else:
			sdv = numpy.std(prices[-period:])
			return sdv
	def MA_integral(self,prices,highest_period):
			if len(prices) < highest_period:
				return None
			else:
				price = prices[-1:][0]
				indicator = None
				movingAverage_vector = numpy.array([])
				for x in range(1,highest_period):
					mv = numpy.array(prices[-x:]).mean()
					movingAverage_vector = numpy.append(movingAverage_vector,mv)

				count = 0
				last = None
				indicator = 0
				for x in movingAverage_vector:
					if count == 0:
						last = x

					elif count == 1:
						if x < last:
							Type = 'Bullish'
							indicator += 1
						else:
							Type = 'Bearish'
							indicator -= 1
					else:
						if Type == 'Bullish':
							if x < last:
								indicator += 1
							else:
								return indicator
						elif Type == 'Bearish':
							if x > last:
								indicator -= 1
							else: 
								return indicator
					count += 1

			return indicator
	def volume_rank(self,volume,period = 100):
		volume_rank = numpy.array([])
		if len(volume) < period+1:
			#volume_rank = numpy.append(volume_rank,None)
			return None
		else: 
			
			rank = sum(volume[-1:]>volume[-period:])/period
			rank = int(rank*100)
			return rank

	def sig_levels(self,price_dict,close_n3,open_n3,close_n2,open_n2,close_n1,open_n1,close,open):
	
		x = str(close_n1) in price_dict.keys()
		x2 = str(open_n1) in price_dict.keys()
		x3 = str(close) in price_dict.keys()
		


		if x == False:
			price_dict[str(close_n1)] = 0
		
		if x2 == False:
			price_dict[str(open_n1)] = 0
			
		
		if x3 == False:
			price_dict[str(close)] = 0
			

		
		if close_n3 < close_n2 and close_n1 == open_n2 and close > open_n1:		#long bounce
			price_dict[str(close_n2)] = price_dict[str(close_n2)]+ 1

		elif close_n3 > close_n2 and close_n1 == open_n2 and close > open_n1:
			price_dict[str(close_n2)] = price_dict[str(close_n2)]+ 1
			
		else:
			price_dict[str(close_n2)] = 0
		

		return price_dict

	def get_orderbook(self,candleTime,last_index = None):
		
		#print(candleTime)
		candleTime = candleTime
		last = candleTime#- datetime.timedelta(hours = 1)
		last = time.mktime(last.timetuple())
		last = datetime.datetime.fromtimestamp(last)
		#print(last)
		if last < datetime.datetime(2019,4,7,5,0,0):
			last = last - datetime.timedelta(minutes = 60)


		if (last - datetime.timedelta(hours = 11)).month != self.month_number:

			self.month_number =  last.month
			if self.month_number == 3:
				table_name = 'XBT_orderbook_March_5'
			elif self.month_number == 4:
				table_name = 'XBT_orderbook_April_5'
			elif self.month_number == 5:
				table_name = 'XBT_orderbook_May_5'
			elif self.month_number == 6:
				table_name = 'XBT_orderbook_June_10'
			elif self.month_number == 7:
				table_name = 'XBT_orderbook_July_10'
			elif self.month_number == 8:
				table_name = 'XBT_orderbook_Aug_10'
			elif self.month_number == 9:
				table_name = 'XBT_orderbook_Sep_10'
			elif self.month_number == 10:
				table_name = 'XBT_orderbook_Oct_10'

			self.prep_orderbook_segment(table_name)
			last_index = None



		if last_index == None:
			try:
				last_timestamp =  pd.unique(self.orderbook_data[self.orderbook_data['datetimes_SYD'] < last]['time_stamp'])[-1]
				data = self.orderbook_data[self.orderbook_data['time_stamp'] == last_timestamp].sort_values(by = 'price')
				index = len(pd.unique(self.orderbook_data[self.orderbook_data['datetimes_SYD'] < last]['time_stamp'])) - 1
				return data,index
			except:
				return None,None

		elif last_index != None:
			x = 1
			y = 0
			while y == 0: 
				next_datetime = self.orderbook_datetimes[last_index+x]
				next_datetime = str(next_datetime)[0:-3]
				next_datetime = datetime.datetime.strptime(next_datetime,"%Y-%m-%dT%H:%M:%S.%f")
				if next_datetime > last:
					index = last_index+x-1
					next_time_stamp = self.orderbook_time_stamps[index]
					#data = self.orderbook_data[self.orderbook_data['time_stamp'] == next_time_stamp]
					data = self.books[index].sort_values(by = 'price')
					y = 1
					#print(data)
					#time.sleep(5)
					return data,index
				x +=1



	def get_open_value(self,candleTime,candle = False):
		# candleTime = candleTime - datetime.timedelta(hours = 1)
		# candleTime = time.mktime(candleTime.timetuple())
		# candleTime = datetime.datetime.fromtimestamp(candleTime)
		# print(str(candleTime))
		# candleTime = datetime.datetime.strptime(str(candleTime),'%')
		#print(self.open_value_data['datetimes'])

		#data = self.open_value_data[(candleTime - datetime.timedelta(minutes = 5) < self.open_value_data.index)& (self.open_value_data.index < candleTime)]
		
		if candle == False:
			candleTime = candleTime #- datetime.timedelta(hours = 1)
			candleTime = time.mktime(candleTime.timetuple())
			candleTime = datetime.datetime.fromtimestamp(candleTime)
			data = self.open_value_data[(self.open_value_data.index < candleTime)]
		elif candle == True:
			candleTime = candleTime #- datetime.timedelta(hours = 1)
			candleTime = time.mktime(candleTime.timetuple())
			candleTime = datetime.datetime.fromtimestamp(candleTime)
			data = self.OI_5m_candles[(self.OI_5m_candles.index <= candleTime)]
		
		return data


	def n_largest_spread(self,book,start_depth,end_depth,n,cum2 = False):
		#print(book)
		

		cum = 'cum'
		qty = 'qty'
		if cum2 == True:
			cum = 'cum2'
			qty = 'qty2'
			#print(df)
			book.sort_values(by='price',ascending=False)
			book = book.reset_index(drop = True)
			#print(df)
			#df.sort_values(by='price',ascending=False)
			#df['ten'] = 0
			book['a15'] = book[book['side'] == 'Buy']['qty2'][::-1].cumsum() 
			book['a16'] = book[book['side'] == 'Sell']['qty2'].cumsum()
			book['cum2'] = book[['a15','a16']].sum(axis = 1)


		#print(book)
		book1 = book[(book[cum] < end_depth)]
		#print(book1)
		#print(book.loc[90:105,'cum2'])
		book1_b = book1[(book1['side'] == 'Buy')&(book1[cum] > start_depth)]
		book1_s = book1[(book1['side'] == 'Sell')&(book1[cum] > start_depth)]

		#print(book1_b)
		#print(book1_s)



		book1_b = book1_b.reset_index(drop = True)
		book1_s = book1_s.reset_index(drop = True)
		book1_b2 = book1_b.iloc[:-1]
		book1_s2 = book1_s.iloc[1:]
		max_b = book1_b2[qty].nlargest(n = n)
		max_s = book1_s2[qty].nlargest(n = n)
		buy_price = book1_b.iloc[max_b.index+1,:].sort_values(by = 'price', ascending = False)
		sell_price = book1_s.iloc[max_s.index-1,:].sort_values(by = 'price', ascending = True)

		#print(buy_price,sell_price)
		return buy_price,sell_price

	def VPOC_price_picker(self,book,price_min,price_max,side):
		#print(book)
	
		#print('\n')
		#print("VPOC FUNCTION START")

		#print("PRICE_MIN",price_min)
		#print("PRICE_MAX",price_max)


		#print("FIRST BOOK")
		#print(book)
		book = book[(book['price'] >= price_min) & (book['price'] <= price_max)]
		#print(book)

		book = book.reset_index(drop = True)
		
		if len(book)>4:
			if side == 'Buy':
				book2 = book.iloc[:-1]
				wall = book2['qty'].nlargest(n = 1)
				price = book.iloc[wall.index+1,:].sort_values(by = 'price', ascending = False)['price'].item()
			elif side == 'Sell':
				book2 = book.iloc[1:]
				wall = book2['qty'].nlargest(n = 1)
				price = book.iloc[wall.index-1,:].sort_values(by = 'price', ascending = True)['price'].item()
		else:
			price = int((price_max+price_min)/2)

		
		#print('final price ',price)
		return price


	def get_funding_rate(self,candleTime):
		candleTime = candleTime.item()
		last = candleTime  + datetime.timedelta(minutes = 1)
		last = time.mktime(last.timetuple())
		last = datetime.datetime.fromtimestamp(last)
		#print(str(candleTime))
		#print(last)
		# candleTime = datetime.datetime.strptime(str(candleTime),'%')
		#print(self.open_value_data['datetimes'])
		self.funding_rate_data['2M_STD'] = self.funding_rate_data.fundingRate.rolling(3*30).std()
		self.funding_rate_data['upper_band'] = 0.0001 + 2*self.funding_rate_data['2M_STD']
		self.funding_rate_data['lower_band'] = 0.0001 - 2*self.funding_rate_data['2M_STD']
		
		data = self.funding_rate_data[self.funding_rate_data.index < last]
		#print(data)



		return data

	def Open_value_rolling_corr(self,n):


		print('check 15')
		self.open_value_correlation = self.open_value_data.resample('1T',label = 'right',closed = 'right').mean()
		#self.open_value_correlation['OV_CORR'] = pd.rolling_corr(self.open_value_correlation['XBT_price'],self.open_value_correlation['open_value'],60)
		self.open_value_correlation['OV_CORR'] = self.open_value_correlation['XBT_price'].rolling(n).corr(self.open_value_correlation['open_value'])
		#print(self.open_value_correlation)


	def get_volitility_ranking(self,candleTime,first = False):

		candleTime = time.mktime(candleTime.timetuple())
		candleTime = datetime.datetime.fromtimestamp(candleTime)

		if first == True:
			self.daily_atr_distrobution()

		ret = self.one_d_candles[self.one_d_candles.index < candleTime ]['ATR_percentile_3_day_avg'][-1:].item()
		return ret



	

	def daily_atr_distrobution(self):
		self.one_d_candles['ATR'] = (self.one_d_candles['high'] - self.one_d_candles['low'])/((self.one_d_candles['high'] + self.one_d_candles['low'])/2)
		self.one_d_candles['ATR_rank'] = self.one_d_candles['ATR'].rolling(100000000,min_periods = 2).apply(lambda x: pd.Series(x).rank()[-1:].item())

		#print(self.one_d_candles[['ATR','ATR_rank','candle_num']][0:50])
		#ss
		self.one_d_candles['ATR_percentile'] = self.one_d_candles['ATR_rank']/(self.one_d_candles['candle_num']+1)

		self.one_d_candles['ATR_percentile_3_day_avg'] = self.one_d_candles['ATR_percentile'].rolling(3).mean()

		print(self.one_d_candles['ATR_percentile_3_day_avg'])









	def open_value_mvavg(self,n):
		self.open_value_data['moving_average'] = self.open_value_data['open_interest'].rolling(n).mean()

	def get_ov_corr(self,candleTime):
		candleTime = candleTime.item()- datetime.timedelta(hours = 1)
		candleTime = time.mktime(candleTime.timetuple())
		candleTime = datetime.datetime.fromtimestamp(candleTime)
		#print(candleTime)

		data = self.open_value_correlation[self.open_value_correlation.index < candleTime]
		return data
	
	def create_volume_profiles(self,startTime_SYD,endTime_SYD,q,index):

		self.volume_profiles = []
		self.volume_profile_base = 1
		self.last_base = 1

		#self.one_m_candles['c'] = 0.1 + (0.9/self.total_candles)*self.one_m_candles['candle_num']
		self.one_m_candles['c'] = 1 #numpy.exp(range(len(self.one_m_candles)))
		self.one_m_candles['time_weighted_volume'] = self.one_m_candles['volume']*self.one_m_candles['c']
		#print(self.one_m_candles['time_weighted_volume'],self.one_m_candles['c'])

		t = startTime_SYD
		#total_minutes = (self.endTime_SYD - self.startTime_SYD).total_minutes()
		#scaler_add = 0.5/(total_minutes/5)
		count = 0
		while t < endTime_SYD:
			count+=1
			if numpy.mod(5000,count) == 0:
				print("Proccess Num : "+ str(index)+' '+str(round((t-startTime_SYD).total_seconds()/(endTime_SYD-startTime_SYD).total_seconds(),3)))
				print('\n')

			#print(t)
			df = self.one_m_candles[self.one_m_candles.index < t]# and ((self.one_m_candles.index > t - datetime.timedelta(hours = 48)))]
			df = df[df.index > t - datetime.timedelta(hours = 24)]
	
			vp = df[['avg_price','time_weighted_volume']].groupby(['avg_price']).sum()

			vp = vp.sort_values(by = 'avg_price',ascending = False)
			vp['cum'] = vp['time_weighted_volume'].cumsum()
			vp['percent'] = vp['cum']/vp['time_weighted_volume'].sum()
			self.volume_profiles.append(vp)
			self.last_base = int(self.volume_profile_base)
			t = t + datetime.timedelta(minutes = 5)

		data = {'data':self.volume_profiles,'key':index}
		q.put(data)
		print("VP Proccess Num : "+str(index)+' Finished!')



	def get_volume_profile(self,candleTime,last_index = None):
		candleTime = candleTime
		last = candleTime  #- datetime.timedelta(hours = 1)
		last = time.mktime(last.timetuple())
		last = datetime.datetime.fromtimestamp(last)

		if last_index == None:
			index = 0
			#print('index',index)
			return self.volume_profiles[index],index
		else:
			index = last_index + 1

		#print('index',index)
		#print(len(self.volume_profiles))
		#print(len(self.volume_profiles[index]))
		return self.volume_profiles[index],index

	def get_anchored_VPOC(self,strategy_on_start_time,candleTime,hours = 24):

		t1 = datetime.datetime.utcnow()

		candleTime = candleTime
		last = candleTime  - datetime.timedelta(hours = 1)
		last = time.mktime(last.timetuple())
		last = datetime.datetime.fromtimestamp(last)

		first = strategy_on_start_time
		first = time.mktime(first.timetuple())
		first = datetime.datetime.fromtimestamp(first)
		total_seconds_in = (first - last).total_seconds()

		t2 = datetime.datetime.utcnow()

		if abs(total_seconds_in) < 60*60*(hours - 1.5):
			#print(total_seconds_in/60*60)
			#print(first,last)
			candles = self.one_m_candles[(self.one_m_candles.index >= first) & (self.one_m_candles.index <= last)]
			
			#print("less then 24 hours")
		else:
			candles = self.one_m_candles[(self.one_m_candles.index >= last - datetime.timedelta(hours = hours))& (self.one_m_candles.index <= last) ]
			#print("more then 24 hours")
		#print(candles[-5:])
		t3 = datetime.datetime.utcnow()
		#if self.pairs[0] == 'XBTUSD':
		vp = candles[['avg_price','volume']].groupby(['avg_price']).sum()
		vp = vp.sort_values(by = 'avg_price',ascending = False)
		vp['cum'] = vp['volume'].cumsum()
		vp['percent'] = vp['cum']/vp['volume'].sum()
		#elif self.pairs[0] == 'ETHUSD':

		t4 = datetime.datetime.utcnow()
		self.timer1 += (t2-t1).total_seconds()
		self.timer2 += (t3-t2).total_seconds()
		self.timer3 += (t4-t3).total_seconds()


		return vp

	def ETH_VP_round(self):
		pass

	def custom_round(self,x,base):
		ret = int(base * round(float(x)/base))
		return ret

	# def positive_sum(self,x):
	# 	ret = 0
	# 	if x > 0:
	# 		ret = 


	def OBD_predict(self,features):

		X = self.scaler.transform(features)
		#X = X
		
		OBD = self.OBD_MODEL.predict(X)
		#print(OBD)
		return OBD

	def return_large_orders(self,book,min_s,max_s):
		book = book[(book['qty'] > min_s) & (book['qty'] < max_s)]
		return book[['price','qty','side']]

	def filter_large_levels(self,previous_books):

		book_1,book_2,book_3,book_4 =  previous_books

		if book_1 is not None and book_2 is not None and book_3 is not None and book_4 is not None:
			prices1 = set(book_1['price'].values)
			prices2 = set(book_2['price'].values)
			prices3 = set(book_3['price'].values)
			prices4 = set(book_4['price'].values)

			set_list = [prices2,prices3,prices4]

			final_prices = prices1

			for s in set_list:
				final_prices.intersection_update(s)

			final_prices = list(final_prices)

			orderbook_levels = book_1[book_1['price'].isin(final_prices)]
			return orderbook_levels
		else:
			return None


	
	def query(self,table,st,et,num_proccesses,orderby1,orderby2,format2 = "%Y-%m-%d %H:%M:%S.%f"):

		def execute_query(table,start_UTC,end_UTC,q,orderby1,orderby2,index,format2 = "%Y-%m-%d %H:%M:%S.%f"):

			start_UTC = start_UTC.strftime(format2)
			end_UTC = end_UTC.strftime(format2)

			print(start_UTC)
			print(end_UTC)
			print('\n')

			#with sql.connect(self.db_file_path) as con:
			with psycopg2.connect(database = "postgres",user = 'Bitmex',password = 'Getrekt!',host = 'database-1.com87m7lu0tb.ap-southeast-2.rds.amazonaws.com',port = 5432) as con:
				cur = con.cursor()
				cur.execute("""
				SELECT DISTINCT
				*
				FROM {}
				WHERE 1=1
				and {} > %s
				and {} < %s
				ORDER BY {}{} ASC
				""".format(table,orderby1,orderby1,orderby1,orderby2),(start_UTC,end_UTC)) #giving 24 hours of past data to begin with
				data = pd.DataFrame(cur.fetchall())
				ret = {'data':data,'key':index}
				
			q.put(ret)
			print("data queued")



		# Initiate Proccesses
		processes = []
		q = Queue()
		total_seconds = (et - st).total_seconds()
		t1 = (st)
		t2 = (st + datetime.timedelta(seconds = int(total_seconds/num_proccesses)))
		
		if num_proccesses > 1:
			t3 = (st + datetime.timedelta(seconds = 2*int(total_seconds/num_proccesses)))
			processes.append(Process(target = execute_query,args = (table,t1,t2,q,orderby1,orderby2,0,format2)))
			processes.append(Process(target = execute_query,args = (table,t2,t3,q,orderby1,orderby2,1,format2)))
		if num_proccesses > 2:
			t4 = (st + datetime.timedelta(seconds = 3*int(total_seconds/num_proccesses)))
			processes.append(Process(target = execute_query,args = (table,t3,t4,q,orderby1,orderby2,2,format2)))
		if num_proccesses > 3:
			t5 = (st + datetime.timedelta(seconds = 4*int(total_seconds/num_proccesses)))
			processes.append(Process(target = execute_query,args = (table,t4,t5,q,orderby1,orderby2,3,format2)))



		print(t1,t2,t3,t4,t5)

		for p in processes:
			p.start()

		final_data = []
		while len(final_data) < num_proccesses:
			final_data.append(q.get())
			#print("DATA FETCHED")
			time.sleep(0.01)

		count = 0
		for p in processes:
			count+=1
			p.join()
			print('Process ',str(count) + ' Joined')


		if num_proccesses == 4:
			x1,x2,x3,x4 = final_data
			x_list = [x1,x2,x3,x4]
			df_list = []

			for n in range(4):
				for x in x_list:
					if x['key'] == n:
						df_list.append(x['data'])


			data = pd.concat(df_list)
			data = data.reset_index(drop = True)
			print(len(data))
			#print("Final DF!")
			#print(data)
		#print(df1,df2,df3,df4)
		return data      	
        	


	def split_tasks(self,target_function,st,et,num_proccesses):
		processes = []
		q = Queue()
		total_seconds = (et - st).total_seconds()
		t1 = (st)
		t2 = (st + datetime.timedelta(seconds = int(total_seconds/num_proccesses)))
		
		if num_proccesses > 1:
			t3 = (st + datetime.timedelta(seconds = 2*int(total_seconds/num_proccesses)))
			processes.append(Process(target = target_function,args = (t1,t2,q,0)))
			processes.append(Process(target = target_function,args = (t2,t3,q,1)))
		if num_proccesses > 2:
			t4 = (st + datetime.timedelta(seconds = 3*int(total_seconds/num_proccesses)))
			processes.append(Process(target = target_function,args = (t3,t4,q,2)))
		if num_proccesses > 3:
			t5 = (st + datetime.timedelta(seconds = 4*int(total_seconds/num_proccesses)))
			processes.append(Process(target = target_function,args = (t4,t5,q,3)))



		print(t1,t2,t3,t4,t5)

		for p in processes:
			p.start()

		final_data = []
		while len(final_data) < num_proccesses:
			final_data.append(q.get())
			#print("DATA FETCHED")
			time.sleep(0.01)

		count = 0
		for p in processes:
			count+=1
			p.join()
			print('Process ',str(count) + ' Joined')


		if num_proccesses == 4:
			x1,x2,x3,x4 = final_data
			x_list = [x1,x2,x3,x4]
			df_list = []

			for n in range(4):
				for x in x_list:
					if x['key'] == n:
						df_list = df_list + x['data']


			data = df_list
		return data  





