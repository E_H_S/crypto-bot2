from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
from poloniex import poloniex
import time

#lolololol
#testy test test
#test branch commit


class SMAV(object):
	def __init__(self,pair,starting_bull_volume = None,starting_bear_volume= None,live = False,backtest = True):

		#connect to the APi and determin if the script is a backtest or a live trading scenario
		self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')
		self.live = live
		self.bear,self.bull = pair.split("_")
		self.backtest = backtest

		#Initial perameters of this given strategy]
		self.pair = pair
		self.numSimulTrades = 1
		self.indicators = BotIndicators()
		###############
		#To feed information up the chain the data that needs to be atributes
		#the data requirements for BotStratLog are.
		# Initiate trading based vectors
		self.tradeStatus = np.array([])	#TradeStatus is of form "OPENED","IN POSITION",("CLOSED" OR "STOPLOSSED","WAITING"
		self.BullCommisions = np.array([])	#Number of tokens taken by the exchange during a buy order
		self.BearCommisions = np.array([])	#Number of tokens taken by the exchange during a sell order
		self.BullTokens = np.array([starting_bull_volume])		# Number of Bull tokens at the datetime after the Trade and Comission
		self.BearTokens = np.array([starting_bear_volume])		# Number of Bear tokens at the datetime after the Trade and Comission
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs2  = np.array([])
		self.Emavs3 = np.array([])

		self.rsi = np.array([])
		self.volume = np.array([])

		self.macdb = np.array([])#Because these will eventually be stored in a dataframe they cannot be tuples and must each have their own list
		self.macdr = np.array([])
		self.macdd = np.array([])

		self.openTrades = [] 		#  Initiate a vector of trade objects
		self.trades = []     		#  self.trades's elements are trade objects with the attributes  self.Entry_price, self.Entry_date self.Entry_volume, self.Status, self.Exit_price, self.Exit_date, self.Exit_volume

		
	def tick(self,candlestick,prices):#candlestick is a singular candlestick, prices is a list of all candlestick price averages
		

		#updating Indicators
		self.rsi = np.append(self.rsi,self.indicators.RSI(prices)) #RSI indicator
		self.macdb,self.macdr,self.macdd = self.indicators.MACD(prices) #MACD indicator
		self.mavs = np.append(self.mavs,[self.indicators.movingAverage(prices,6)]) # Creating the self.mavs list for analysis and visualsation later.
		self.mavs2 = np.append(self.mavs2,[self.indicators.EMA(prices,12)])
		self.Emavs3 = np.append(self.Emavs3,[self.indicators.EMA(prices,5)])	
		self.volume = np.append(self.volume,candlestick.volume)	

		############################################################################################################################################################################################################################
		#ALL Live Script !!!
		if self.live == True:
			print("check1")
			balence = self.conn.returnBalances()
			self.BullTokens = np.append(self.BullTokens,balence[self.bull])
			self.BearTokens = np.append(self.BearTokens,balence[self.bear])
		else:
		############################################################################################################################################################################################################################
		#ALL BACK TEST SCRIPT!!!
			self.tradeStatus = np.append(self.tradeStatus,["WAITING"])									#Assume status for this timestamp is waiting unless proven not to be later.
			if len(self.mavs) > 1: 																		#only appends on a tick the 2nd tick onwards to avoid double ups with the first instatiate uses len(mavs) to represent tick index
				self.BullTokens = np.append(self.BullTokens,self.BullTokens[-1:])						#assume Tokens remain the same as last iteration unless trade happens in which case change tokens at that point 
				self.BearTokens = np.append(self.BearTokens,self.BearTokens[-1:])
			self.BullCommisions = np.append(self.BullCommisions,[float(0)])
			self.BearCommisions = np.append(self.BearCommisions,[float(0)])		#Asume no comission fee for this cycle unless unless trade happens in which case we will add comission the end at that point
			
					
		if self.live == True or self.backtest == True:
			self.evaluatePositions(datetime.datetime.fromtimestamp(candlestick.date),prices,candlestick) 	#evaluate the position


		if self.tradeStatus[-1:] == "IN POSITION":
			self.updateOpenTrades(candlestick,datetime.datetime.fromtimestamp(candlestick.date)) #Evaluate already open positions

	def evaluatePositions(self,date,prices,candlestick):
		#if self.live == True and len(self.BullTokens)==2:
		#	pass
		#	self.OpenPosition(candlestick,date) #OPEN POSITION!
		#elif self.live == True and len(self.BullTokens)==3:
		#	self.ClosePosition(date,candlestick)#SELL POSITION!
		
		####################################################################################################################################################################################
		# The Strategy Lines BELOW
		####################################################################################################################################################################################
		openTrades = []																# Searches the trades vector for 
		for trade in self.trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade)											 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1:] = "IN POSITION" 


		if (len(openTrades) < self.numSimulTrades): 								#Checks if the algorithm is allowed to take on another position
			if len(self.BullTokens)> 12: 
				if  self.mavs[-1:]-self.mavs[-2:-1] > 0 and self.mavs[-3:-2] > self.mavs[-2:-1] and self.mavs[-4:-3]> self.mavs[-3:-2] and self.mavs[-5:-4] > self.mavs[-4:-3]:  #and  candlestick.close > self.mavs2[-1:] #and self.rsi[-1:] < 80 :# and self.volume[-1:] > self.volume[-2:-1]:
					self.OpenPosition(candlestick,date,stopLoss = 0.07) #OPEN POSITION!
					

	
		#Momentum opener		
		for trade in openTrades:
			if (self.mavs[-2:-1]-self.mavs[-3:-2]) > (self.mavs[-1:]-self.mavs[-2:-1])*4 and (self.mavs[-2:-1]-self.mavs[-3:-2]) > 0:#and (self.mavs[-1:]-self.mavs[-2:-1])>0 :
				self.ClosePosition(date,candlestick,trade)#SELL POSITION!


	def updateOpenTrades(self,candlestick,date):
		for trade in self.trades:
			if (trade.status == "OPEN"):
				trade.tick(candlestick.low,date) #Tick the position to see if it has hit the stoploss
				if trade.tick(candlestick.low,date) == True: #check to see if it hit the stop loss and if so, close the position as usual
					
					self.BullTokens[-1:] = 0
					self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.998)*(candlestick.close/1.01) #Calculates the number of purchased Bear coins with commision taken into consideration
					self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.002	#calculate bear Commision and replace last element in the vector with calculated comission
					self.tradeStatus[-1:] = "STOP LOSSED"
					trade.close('stop loss price',date,self.BearTokens[-1:])
					



	def OpenPosition (self,candlestick,date,stopLoss = None):
		if self.live == True:

			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			time.sleep(1)
			lastPairPrice = float(currentValues[self.pair]["lowestAsk"])
			asking_price = float(round(lastPairPrice*1.01,8))

			amount  = float(self.BearTokens[-1:])*0.99/asking_price
			orderNumber = self.conn.buy(self.pair,round(lastPairPrice*1.01,8),amount)
			print(orderNumber)
			time.sleep(2)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			time.sleep(2)
			balence = self.conn.returnBalances()
			self.BullTokens[-1:] = balence[self.bull]
			self.BearTokens[-1:] = balence[self.bear]
			print(self.BullTokens)
			print(self.BearTokens)

		else:
			self.BearTokens[-1:] = 0
			self.BullTokens[-1:] = (float(self.BearTokens[-2:-1])*0.998)/candlestick.close #Calculates the number of purchased Bull coins with commision taken into consideration
			self.BullCommisions[-1:] = float(self.BearTokens[-2:-1])*0.002	#calculate bull Commision and replace last element in the vector with calculated comission
			
		self.tradeStatus[-1:] = "OPEN"
		self.trades.append(BotTrade(candlestick.close,date,self.BullTokens[-1:],stopLoss)) # Append a BotTrade Object to our trades list in percentage .... StopLoss = 0.02 is 2%






	def ClosePosition (self,date,candlestick,trade = None):
		if self.live == True:

			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			lastAskingPrice = float(currentValues[self.pair]["lowestAsk"])
			asking_price = float(round(lastAskingPrice*0.99,8))
			amount = float(self.BullTokens[-1:])*0.995
			orderNumber = self.conn.sell(self.pair,round(lastAskingPrice*0.99,8),amount)
			print(orderNumber)
			time.sleep(2)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			time.sleep(2)
			balence = self.conn.returnBalances()

			self.BullTokens[-1:] = balence[self.bull]
			self.BearTokens[-1:] = balence[self.bear]
			print(self.BullTokens)
			print(self.BearTokens)

		else:
			self.BullTokens[-1:] = 0
			self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.998)*candlestick.close #Calculates the number of purchased Bear coins with commision taken into consideration
			self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.002	#calculate bear Commision and replace last element in the vector with calculated comission
			
		trade.close(candlestick.close,date,self.BearTokens[-1:])
		self.tradeStatus[-1:] = "CLOSED"
	def live_flick(self):
		self.live = True



