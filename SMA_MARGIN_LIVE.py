from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
from poloniex import poloniex
import time

#lolololol
#testy test test
#test branch commit


class SMA_LIVE_M(object):
	def __init__(self,pair,starting_bull_volume = None,starting_bear_volume= None,live = False,backtest = True,Initial_trading_volume = None):

		#connect to the APi and determin if the script is a backtest or a live trading scenario
		self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')
		self.live = live
		self.bear,self.bull = pair.split("_")
		self.backtest = backtest

		#Initial perameters of this given strategy]
		self.pair = pair
		self.numSimulTrades = 1
		self.indicators = BotIndicators()
		###############
		#To feed information up the chain the data that needs to be atributes
		#the data requirements for BotStratLog are.
		# Initiate trading based vectors
		self.tradeStatus = np.array([])	#TradeStatus is of form "OPENED","IN POSITION",("CLOSED" OR "STOPLOSSED","WAITING"

		self.BullCommisions = np.array([])	#Number of tokens taken by the exchange during a buy order
		self.BearCommisions = np.array([])	#Number of tokens taken by the exchange during a sell order
		self.USDT_ForPair = np.array([Initial_trading_volume])		#Number of Bull tokens allocated to this pair
		self.BullTokens = np.array([starting_bull_volume])		# Number of Bull tokens at the datetime after the Trade and Comission
		self.BearTokens = np.array([starting_bear_volume])		# Number of Bear tokens at the datetime after the Trade and Comission
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.Emavs3 = np.array([])
		self.varience = np.array([])

		self.rsi = np.array([])
		self.volume = np.array([])

		self.macdb = np.array([])									
		self.macdr = np.array([])
		self.macdd = np.array([])

		self.B_upper = np.array([])
		self.B_lower = np.array([])

		self.openTrades = [] 		#  Initiate a vector of trade objects
		self.trades = []     		#  self.trades's elements are trade objects with the attributes  self.Entry_price, self.Entry_date self.Entry_volume, self.Status, self.Exit_price, self.Exit_date, self.Exit_volume
		self.target_sells = 0
		self.close_sells = 0

		
	def tick(self,candlestick,prices,lows,highs,opens,closes,dates):#candlestick is a singular candlestick, prices is a list of all candlestick price averages

		#updating Indicators
		self.rsi = np.append(self.rsi,self.indicators.RSI(prices)) #RSI indicator
		self.macdb,self.macdr,self.macdd = self.indicators.MACD(prices) #MACD indicator
		self.varience = np.append(self.varience,self.indicators.avg_range(prices,highs,lows,3))

		
		self.mavs = np.append(self.mavs,[self.indicators.movingAverage(prices,5)])
		self.Emavs3 = np.append(self.Emavs3,[self.indicators.movingAverage(prices,3)])

		if len(self.mavs_delta) == 0:
			self.mavs_delta = np.append(self.mavs_delta,[None])
		else:
			self.mavs_delta = np.append(self.mavs_delta,[self.mavs[-1:]/self.mavs[-2:-1]])

		self.mavs2 = np.append(self.mavs2,[self.indicators.EMA(prices,12)])
		self.volume = np.append(self.volume,candlestick.volume)
		self.buy_target = None



		
		############################################################################################################################################################################################################################
		#ALL Live Script !!!
		if self.live == True:
			self.UpdateOpenOrders()
			balence = self.conn.returnBalances()
			self.BullTokens = np.append(self.BullTokens,balence[self.bull])
			self.BearTokens = np.append(self.BearTokens,balence[self.bear])
			print("Last 5 Bull tokens : " + str(self.BullTokens[-5:]))
			print("Last 5 Bear tokens : " +  str(self.BearTokens[-5:]))
			print("Last 5 BTC_totals tokens : " +  str(self.USDT_ForPair[-5:]))

		else:
		############################################################################################################################################################################################################################
		#ALL BACK TEST SCRIPT!!!
			self.tradeStatus = np.append(self.tradeStatus,["WAITING"])									#Assume status for this timestamp is waiting unless proven not to be later.
			if len(self.mavs) > 1: 																		#only appends on a tick the 2nd tick onwards to avoid double ups with the first instatiate uses len(mavs) to represent tick index
				self.BullTokens = np.append(self.BullTokens,self.BullTokens[-1:])						#assume Tokens remain the same as last iteration unless trade happens in which case change tokens at that point 
				self.BearTokens = np.append(self.BearTokens,self.BearTokens[-1:])
				self.USDT_ForPair = np.append(self.USDT_ForPair,self.USDT_ForPair[-1:])
			self.BullCommisions = np.append(self.BullCommisions,[float(0)])
			self.BearCommisions = np.append(self.BearCommisions,[float(0)])		#Asume no comission fee for this cycle unless unless trade happens in which case we will add comission the end at that point
			
					
		if self.live == True or self.backtest == True:
			self.evaluatePositions(datetime.datetime.fromtimestamp(candlestick.date),prices,candlestick,opens = opens,lows = lows,highs = highs ,closes = closes) 	#evaluate the position


		if self.tradeStatus[-1:] == "IN POSITION":
			self.updateOpenTrades(candlestick,datetime.datetime.fromtimestamp(candlestick.date)) #Evaluate already open positions



	def evaluatePositions(self,date,prices,candlestick,lows=None,highs=None,opens=None,closes=None,dates=None):
		

		openTrades = []
		for trade in self.trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade)											 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1:] = "IN POSITION" 


		for trade in openTrades:
			if trade.status == "OPEN" and trade.long == False:
				if (self.mavs[-2:-1]-self.mavs[-3:-2]) < (self.mavs[-1:]-self.mavs[-2:-1])*4:
					self.Close_Margin_Trade(date,candlestick,trade,Market_sell = True)
		for trade in openTrades:
			if trade.status == "OPEN" and trade.long == True:
				if (self.mavs[-2:-1]-self.mavs[-3:-2]) > (self.mavs[-1:]-self.mavs[-2:-1])*4:
					self.Close_Margin_Trade(date,candlestick,trade,Market_sell = True)


		if (len(openTrades) < self.numSimulTrades):
			if len(self.BullTokens)> 15 and self.live == True: 
				if self.mavs[-3:-2] > self.mavs[-2:-1] and self.mavs[-1:]/self.mavs[-2:-1] > 1 and prices[-1:][0] > self.mavs2[-1:] and self.mavs_delta[-1:] > 1:
					self.Margin_Open(date,Long = True,Market_buy = True ,leverage = 1 )
					

		if (len(openTrades) < self.numSimulTrades):
			if len(self.BullTokens)> 15 and self.live == True: 
				if self.mavs[-3:-2] < self.mavs[-2:-1] and self.mavs[-1:]/self.mavs[-2:-1] < 1 and prices[-1:][0] < self.mavs2[-1:] and self.mavs_delta[-1:] < 1:
					self.Margin_Open(date,Long = False,Market_buy = True ,leverage = 1)
					

	def UpdateOpenOrders(self):
		for trade in self.trades:
			if (trade.status in ("BUY ORDER","SELL ORDER")):
				if trade.status == "BUY ORDER":
					open_orders = self.conn.returnOpenOrders(self.pair)
					trade_hist = self.conn.returnTradeHistory(self.pair)
					print("OPEN ORDERS")
					print(open_orders)
					if len(open_orders) == 0:
						self.tradeStatus[-1:] = "IN POSITION"
						trade.status = "OPEN"
						print("BUY ORDER FILLED")
						print(trade_hist[-10:])
						total_sum = 0
						for trades in trade_hist:
							if trades['type'] == 'buy':
								total_sum += float(trades['total'])
							elif trades['type'] == 'sell':
								break
							print(total_sum)
						self.USDT_ForPair =  np.append(self.USDT_ForPair,self.USDT_ForPair[-1:] - total_sum)
					elif len(open_orders) > 0 and self.buy_order != open_orders:
						total_sum = 0
						for orders in open_orders:
							self.tradeStatus[-1:] = "IN POSITION"
							trade.status = "OPEN"
							print("PARTIAL FILL AND OPENED")
							self.conn.cancel(self.pair,orders["orderNumber"])
							print(trade_hist[-10:])
							for trades in trade_hist:
								if trades['type'] == 'buy':
									total_sum += float(trades['total'])
									print(total_sum)
								elif trades['type'] == 'sell':
									break
							self.USDT_ForPair =  np.append(self.USDT_ForPair,self.USDT_ForPair[-1:] - total_sum)
							self.buy_order = None
					else:
						for orders in open_orders:
							self.conn.cancel(self.pair,orders["orderNumber"])
						self.tradeStatus[-1:] = "WAITING"
						trade.status = "NO FILL"
						print("BUY ORDER CANCELED")
						self.USDT_ForPair =  np.append(self.USDT_ForPair,self.USDT_ForPair[-1:])
				elif trade.status == "SELL ORDER":
					open_orders = self.conn.returnOpenOrders(self.pair)
					trade_hist = self.conn.returnTradeHistory(self.pair)
					if len(open_orders) == 0:
						self.tradeStatus[-1:] = "CLOSED"
						trade.status = "CLOSED"
						print("SELL ORDER FILLED")
						total_sum = 0
						for trades in trade_hist:
							if trades['type'] == 'sell':
								total_sum += float(trades['total'])
							elif trades['type'] == 'buy':
								break
						self.USDT_ForPair =  np.append(self.USDT_ForPair,total_sum)
					else:
						for orders in open_orders:
							self.conn.cancel(self.pair,orders["orderNumber"])
							self.tradeStatus[-1:] = "IN POSITION"
							trade.status = "OPEN"
							print("SELL ORDER CANCELED")
							self.USDT_ForPair =  np.append(self.USDT_ForPair,self.USDT_ForPair[-1:])
			elif trade.status in ("OPEN","IN POSITION"):
				self.USDT_ForPair =  np.append(self.USDT_ForPair,self.USDT_ForPair[-1:])


	def updateOpenTrades(self,candlestick,date):

		for trade in self.trades:
			if (trade.status == "OPEN"):
				trade.tick(candlestick.low,date,candlestick.high) #Tick the position to see if it has hit the stoploss

	def Margin_Open (self,date,StopLoss = None,target_Sell = None,Trail = None, Market_buy = True,Market_sell = True,Moving_target = False,Long = True):
		if self.live == True:
			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			lowestAsk = float(currentValues[self.pair]["lowestAsk"])
			highestBid = float(currentValues[self.pair]["highestBid"])
			last = float(currentValues[self.pair]["last"])
			balence = self.conn.returnBalances()
			self.BullTokens = np.append(self.BullTokens,balence[self.bull])
			self.BearTokens = np.append(self.BearTokens,balence[self.bear])

			if Long == True:
				asking_price = float(round(last*1.01,8))
				amount  = float(self.USDT_ForPair[-1:])/asking_price
				orderNumber = self.conn.marginBuy(self.pair,round(asking_price,8),amount)
			elif Long == False:
				asking_price = float(round(last/1.01,8))
				amount  = float(self.USDT_ForPair[-1:])/asking_price
				orderNumber = self.conn.marginSell(self.pair,round(asking_price,8),amount)

			self.buy_order = self.conn.returnOpenOrders(self.pair)
			print(self.buy_order)
			
			self.tradeStatus[-1:] = "OPEN"
			self.trades.append(BotTrade(asking_price,date,self.BullTokens[-1:],stopLoss = StopLoss,target_sell = target_Sell,trail = Trail)) # Append a BotTrade Object to our trades list in percentage .... StopLoss = 0.02 is 2%

	def Close_Margin_Trade(self,date,candlestick,trade = None,Market_sell = True):
		if self.live == True:
			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			lowestAsk = float(currentValues[self.pair]["lowestAsk"])
			highestBid = float(currentValues[self.pair]["highestBid"])
			balence = self.conn.returnBalances()
			self.BullTokens = np.append(self.BullTokens,balence[self.bull])
			self.BearTokens = np.append(self.BearTokens,balence[self.bear])

			orderNumber = self.conn.closeMarginPosition(self.pair)
			trade.close(asking_price,date,self.BearTokens[-1:])
			print(orderNumber)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			print(trade_hist[0])
			self.tradeStatus[-1:] = "CLOSED"
			total_sum = 0
			for trades in trade_hist:
				if trades['type'] == 'sell':
					total_sum += float(trades['total'])
				elif trades['type'] == 'buy':
					break
			self.USDT_ForPair[-1:] += total_sum


	def live_flick(self):
		self.live = True

