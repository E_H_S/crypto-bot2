from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class Momentum(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)
		self.period = period
		self.limits = False

		self.indicators = BotIndicators(orderbook= False,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime)
		
		#Inititalize inticator vectors
		self.mavs1  = np.array([])
		self.mavs2  = np.array([])
		self.mavs3  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None
		self.last_orderbook = pd.DataFrame()
		self.open_value = np.array([])
		self.ATR_switches = np.array([])
		self.ATR_min_list = np.array([])

		self.count10 = 0
		self.switch10 = False
		self.ATR_switch = False
		self.last_OV_high = 1000000000000000
		self.index = 0
		self.ATR_min = 0

		self.numSimulTrades = 1
		self.margin = True
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		
		#updating Indicators
		self.mavs1 = np.append(self.mavs1,self.indicators.EMA(self.closes,24,append = True))
		self.mavs2 = np.append(self.mavs2,self.indicators.EMA(self.closes,24*3,append = True))
		self.mavs3 = np.append(self.mavs3,self.indicators.EMA(self.closes,24*9,append = True))
		self.mavs4 = np.append(self.mavs3,self.indicators.EMA(self.closes,24*27,append = True))
		
		#self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))

		self.ATR = pd.Series((self.highs - self.lows)/self.closes).rolling(288).mean()

		# print('\n')
		# print(self.mavs1)
		# print(self.mavs2)
		# print(self.mavs3)
		#time.sleep(3)

		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		

		self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))
		#self.ATR = pd.Series()
		
	


		if len(self.ATR) > 290:
			if len(self.open_value['max_delta'][-1:]) > 0:

				if self.open_value['max_delta'][-1:].item() < -8000 and self.switch10 == False: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 == True

					self.last_OV_high = self.open_value['open_value'][-6*120].max()
					self.ATR_switch = True
					self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					print('\n')
					print("NEW TRIGGER EVENT!")
					print(self.open_value['max_delta'][-1:].item())
					print("MIN ATR ", self.ATR_min)
					print('\n')
					
				if self.ATR[-1:] != None:
					if self.ATR[-1:] < self.ATR_min*1 and self.ATR_switch == True:
						self.ATR_switch = False
						print('\n')
						print('FAILED ATR SWITCH')
						print('\n')
						
					
				if self.open_value['max_delta_lookback'][-1:].item() > -8000 and self.ATR_switch == True:
					self.ATR_switch = False
					print('\n')
					print('5 day OV drop expired!')
					print('\n')



				if self.open_value['open_value'][-1].item() > self.last_OV_high + 6000 and self.ATR_switch == True:

					self.ATR_switch = False
					print('\n')
					print('OVER THE OV MAX')
					print('OV MAX ',self.last_OV_high + 6000)
					print("LAST OV ",self.open_value['open_value'][-1].item())
					print('\n')


		if len(self.dates) > 2:
			self.open_value = self.indicators.get_open_value(self.dates[-1:])

		if self.ATR_switch == False:
			self.ATR_switches = np.append(self.ATR_switches,0)
			
		elif self.ATR_switch == True:
			self.ATR_switches = np.append(self.ATR_switches,1)
		self.ATR_min_list = np.append(self.ATR_min_list,self.ATR_min)


		
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 1000
		stoploss = None
		#market_order = 'zero'
		#print(date)
		market_order = True
		# if self.dead_date_start > date.item().replace(tzinfo=None):
		# 	if date.item().replace(tzinfo=None) < self.dead_date_end == False:
				
		#print(bp,sp)


		#if (self.dead_date_start < date.item().replace(tzinfo=None) and date.item().replace(tzinfo=None) < self.dead_date_end) == False:
			#print('check555')

		if len(self.dates) > 2:
			if len(self.open_value['max_delta'])> 0:
				#print(self.open_value['max_delta'])

				if self.open_value['max_delta'][-1:].isnull().item() == False:
					#if self.open_value['max_delta'][-1:].item() < -8000 and self.ATR[-1:] > 0.0009 :
					if  self.ATR_switch == False :
						#pass

						if self.mavs1[-1:] != None and self.mavs2[-1:] != None and self.mavs3[-1:] != None and self.mavs4[-1:] != None:
							if self.mavs1[-1:] < self.mavs2[-1:] < self.mavs3[-1:] < self.mavs4[-1:] :
								action = True
								Long = False
								price = self.closes[-1:]
								stoploss = price*1.05
								
										
							if self.mavs1[-1:] > self.mavs2[-1:] > self.mavs3[-1:] > self.mavs4[-1:]:
								
								action = True
								Long = True
								price = self.closes[-1:]
								stoploss = price/1.05
		

		if action == True:
			print("ACTION TRUE")
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		
		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = True
		
		


		if self.mavs1[-1:] < self.mavs3[-1:] and  trade.long == True:
			action = True
			price = self.closes[-1:]
		
					


		
		if self.mavs1[-1:] > self.mavs3[-1:] and trade.long == False:
			action = True
			price = self.closes[-1:]
		
		
		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass
	def calculate_order_book_depth():
		pass




	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.mavs1,self.mavs2,self.mavs3,self.open_value,self.ATR,self.ATR_switches,self.ATR_min_list)

		#(self.buy_line1,self.sell_line1,self.open_value,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3)

