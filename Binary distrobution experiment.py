import numpy as np
import matplotlib.pyplot as plt



s_list = []
itrations = 10000
bet_size = 1
bet_risk_win = 0.025
bet_risk_loss = 0.025
edge = 0.503
maker = -0.00025
#maker = 0.001
taker = 0.0015
#taker = 0.004
#win_scaler = (1 + bet_risk_win)*(1-maker)**2
#lose_scaler = (1-bet_risk_loss)*(1-taker)*(1-maker)
win_scaler = (1 * (1+bet_risk_win))#*(1-maker)**2)
lose_scaler = (1/(1+bet_risk_loss))#*(1-taker)*(1-maker)
print(win_scaler)
print(lose_scaler)

for y in range(itrations):

	#mu, sigma = 1.005, 0.05 # mean and standard deviation
	number_of_trades = 3250 #times 2 for BTC and ETH contracts


	s = np.random.random(number_of_trades)
	s2 = []

	for x in s:
		if x < edge:
			x = 1
		else:
			x = 0
		s2.append(x)



	final_scaler = 1
	for x in s2:
		if x == 1:
			#final_scaler = final_scaler*win_scaler
			final_scaler = (final_scaler*(1-bet_size))+(final_scaler*bet_size)*win_scaler
		else:
			#final_scaler = final_scaler*lose_scaler
			final_scaler = (final_scaler*(1-bet_size))+(final_scaler*bet_size)*lose_scaler

	#print(final_scaler)

	s_list.append(final_scaler)

	#if y&100 == 0:
	#	print(y)

	#plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi))*np.exp( - (bins - mu)**2 / (2 * sigma**2) ),linewidth=2, color='r')
	#plt.show()
#print(s_list)


#abs(mu - np.mean(s)) < 0.01
#abs(sigma - np.std(s, ddof=1)) < 0.01
#print(s_list)
s_list = np.array(s_list)
count, bins, ignored = plt.hist(s_list,100, (0,5))
avg = np.mean(s_list)
median = np.median(s_list)
over_one = sum(s_list>1)/itrations
s_list_std = np.std(s_list)
twenty_fith = np.percentile(s_list,25)
seventy_fith = np.percentile(s_list,75)

print('Std of the final scaler ' +str(s_list_std))
print("over one " +str(over_one))
print("average final payoff "+  str(avg))
print("\n")
print('25 percentile = '+str(twenty_fith))
print("median result " + str(median))
print('75 percentile = '+str(seventy_fith))
#plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi))*np.exp( - (bins - mu)**2 / (2 * sigma**2) ),linewidth=2, color='r')
plt.show()