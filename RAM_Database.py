

import sqlite3 as sql 
import pandas as pd
import multiprocessing
import datetime
import numpy as np
import time

#The purpose of the RAM Database class is to store a segment of the database in RAM memory alowing for rapid
#Access for large backtesting parameter sweeps.

class RAM_DATABASE(object):
	def __init__(self): #Takes an input of a strategy log

	self.table_names = ['XBT_orderbook_RAM_April_1','XBT_orderbook_RAM_April_2','XBT_orderbook_RAM_April_3',
	'XBT_orderbook_RAM_May_1','XBT_orderbook_RAM_May_2','XBT_orderbook_RAM_May_3',
	'XBT_orderbook_RAM_June_1','XBT_orderbook_RAM_June_2','XBT_orderbook_RAM_June_3',
	'XBT_orderbook_RAM_July_1']
 

	self.UTC_start_times = [datetime.datetime(2019,4,1,0,0,0),datetime.datetime(2019,4,10,0,0,0),datetime.datetime(2019,4,20,0,0,0),datetime.datetime(2019,5,1,0,0,0),datetime.datetime(2019,5,10,0,0,0),datetime.datetime(2019,5,20,0,0,0),datetime.datetime(2019,6,1,0,0,0),datetime.datetime(2019,6,10,0,0,0),datetime.datetime(2019,6,20,0,0,0),datetime.datetime(2019,7,1,0,0,0)]
	self.UTC_end_times = [datetime.datetime(2019,4,10,0,0,0),datetime.datetime(2019,4,20,0,0,0),datetime.datetime(2019,5,1,0,0,0),datetime.datetime(2019,5,10,0,0,0),datetime.datetime(2019,5,20,0,0,0),datetime.datetime(2019,6,1,0,0,0),datetime.datetime(2019,6,10,0,0,0),datetime.datetime(2019,6,20,0,0,0),datetime.datetime(2019,7,1,0,0,0),datetime.datetime(2019,7,10,0,0,0)]
	count = 0
	for table,st,et in zip(self.table_names,self.UTC_start_times,self.UTC_end_times):
		data = self.Query_orderbooks('XBT_orderbook',st,et)

		if count == 0:
			self.con = Create_RAM_DB(table,data)
		else:
			self.con = Create_RAM_DB(table,data,self.con)




	def Query_orderbooks(table,start_UTC,end_UTC,fp):
		start_UTC = start_UTC.strftime("%Y-%m-%d %H:%M:%S.%f")
		end_UTC = end_UTC.strftime("%Y-%m-%d %H:%M:%S.%f")

		print(start_UTC)
		print(end_UTC)
		print('\n')

		with sql.connect(fp) as con:
			cur = con.cursor()
			cur.execute("""
			SELECT DISTINCT
			*
			FROM {}
			WHERE 1=1
			and time_stamp > ?
			and time_stamp < ?
			ORDER BY time_stamp,price ASC
			""".format(table),(start_UTC,end_UTC)) #giving 24 hours of past data to begin with
			
			data = cur.fetchall()

		return data


	def Create_RAM_DB(table,data,con_existing = None):

		if con_existing != None:
			con = con_existing

		con = sql.connect(':memory:')

		with con:
			cur = con.cursor()
			cur.execute("""
				CREATE TABLE IF NOT EXISTS {} 
				(price real,
				side text,
				qty real,
				cum real,
				time_stamp text);
				""".format(table))

		count23 = 0
		with con:
			cur = con.cursor()
			for price,side,size,time_stamp,cum in data:
				count23 += 1
				if np.mod(count23,100000)==0:
					print(count23/len(data))
				#print(row['price'],row['side'],row['size'],row['cum'],row['time_stamp'])
				cur.execute("""
					INSERT INTO {}
					(price,
					side,
					qty,
					cum,
					time_stamp)
					VALUES (?,?,?,?,?)
					""".format(table),(price,side,size,cum,time_stamp))

		return con














