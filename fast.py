from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
from poloniex import poloniex
import time
from math import pi

#lolololol
#testy test test
#test branch commit


class FAST(object):
	def __init__(self,pair,starting_bull_volume = None,starting_bear_volume= None,live = False,backtest = True):

		#connect to the APi and determin if the script is a backtest or a live trading scenario
		self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')
		self.live = live
		self.bear,self.bull = pair.split("_")
		self.backtest = backtest

		#Initial perameters of this given strategy]
		self.pair = pair
		self.numSimulTrades = 1
		self.indicators = BotIndicators()
		###############
		#To feed information up the chain the data that needs to be atributes
		#the data requirements for BotStratLog are.
		# Initiate trading based vectors
		self.tradeStatus = np.array([])	#TradeStatus is of form "OPENED","IN POSITION",("CLOSED" OR "STOPLOSSED","WAITING"
		self.BullCommisions = np.array([])	#Number of tokens taken by the exchange during a buy order
		self.BearCommisions = np.array([])	#Number of tokens taken by the exchange during a sell order
		self.BullTokens = np.array([starting_bull_volume])		# Number of Bull tokens at the datetime after the Trade and Comission
		self.BearTokens = np.array([starting_bear_volume])		# Number of Bear tokens at the datetime after the Trade and Comission
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs2  = np.array([])
		self.Emavs3 = np.array([])

		self.rsi = np.array([])
		self.volume = np.array([])
		self.B_upper = np.array([])
		self.B_lower = np.array([])
		self.B_upper2 = np.array([])
		self.B_lower2 = np.array([])





		self.macdb = np.array([])#Because these will eventually be stored in a dataframe they cannot be tuples and must each have their own list
		self.macdr = np.array([])
		self.macdd = np.array([])

		self.openTrades = [] 		#  Initiate a vector of trade objects
		self.trades = []     		#  self.trades's elements are trade objects with the attributes  self.Entry_price, self.Entry_date self.Entry_volume, self.Status, self.Exit_price, self.Exit_date, self.Exit_volume

		
	def tick(self,candlestick,prices,lows,highs,opens,closes):#candlestick is a singular candlestick, prices is a list of all candlestick price averages
		
		#updating Indicators
		self.rsi = np.append(self.rsi,self.indicators.RSI(prices)) #RSI indicator
		self.macdb,self.macdr,self.macdd = self.indicators.MACD(prices) #MACD indicator
		self.mavs = np.append(self.mavs,[self.indicators.movingAverage(prices,80)]) # Creating the self.mavs list for analysis and visualsation later.
		self.mavs2 = np.append(self.mavs2,[self.indicators.movingAverage(prices,5)])
		self.Emavs3 = np.append(self.Emavs3,[self.indicators.EMA(prices,5)])	
		self.volume = np.append(self.volume,candlestick.volume)

		self.B_upperA,self.B_lowerA = self.indicators.Bollingerband(prices,self.mavs,5.5,period = 80)
		self.B_upperA2,self.B_lowerA2 = self.indicators.Bollingerband(prices,self.mavs,1.5,period = 80)


		self.B_upper = np.append(self.B_upper,self.B_upperA)
		self.B_lower = np.append(self.B_lower,self.B_lowerA)
		self.B_upper2 = np.append(self.B_upper2,self.B_upperA2)
		self.B_lower2 = np.append(self.B_lower2,self.B_lowerA2)
		






		############################################################################################################################################################################################################################
		#ALL Live Script !!!
		if self.live == True:
			print("check1")
			balence = self.conn.returnBalances()
			self.BullTokens = np.append(self.BullTokens,balence[self.bull])
			self.BearTokens = np.append(self.BearTokens,balence[self.bear])
		else:
		############################################################################################################################################################################################################################
		#ALL BACK TEST SCRIPT!!!
			self.tradeStatus = np.append(self.tradeStatus,["WAITING"])									#Assume status for this timestamp is waiting unless proven not to be later.
			if len(self.mavs) > 1: 																		#only appends on a tick the 2nd tick onwards to avoid double ups with the first instatiate uses len(mavs) to represent tick index
				self.BullTokens = np.append(self.BullTokens,self.BullTokens[-1:])						#assume Tokens remain the same as last iteration unless trade happens in which case change tokens at that point 
				self.BearTokens = np.append(self.BearTokens,self.BearTokens[-1:])
			self.BullCommisions = np.append(self.BullCommisions,[float(0)])
			self.BearCommisions = np.append(self.BearCommisions,[float(0)])		#Asume no comission fee for this cycle unless unless trade happens in which case we will add comission the end at that point
			
					
		if self.live == True or self.backtest == True:
			self.evaluatePositions(datetime.datetime.fromtimestamp(candlestick.date),prices,candlestick,lows,highs,opens,closes) 	#evaluate the position


		if self.tradeStatus[-1:] == "IN POSITION":
			self.updateOpenTrades(candlestick,datetime.datetime.fromtimestamp(candlestick.date)) #Evaluate already open positions

	def evaluatePositions(self,date,prices,candlestick,lows,highs,opens,closes):
		#if self.live == True and len(self.BullTokens)==2:
		#	pass
		#	self.OpenPosition(candlestick,date) #OPEN POSITION!
		#elif self.live == True and len(self.BullTokens)==3:
		#	self.ClosePosition(date,candlestick)#SELL POSITION!
		
		####################################################################################################################################################################################
		# The Strategy Lines BELOW
		####################################################################################################################################################################################
		openTrades = []																# Searches the trades vector for 
		for trade in self.trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade)											 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1:] = "IN POSITION" 


		if (len(openTrades) < self.numSimulTrades): 								#Checks if the algorithm is allowed to take on another position
			if len(self.BullTokens)> 83:
				if candlestick.high > self.B_upper[-4:-3] and self.B_upper[-1:]> self.B_upper[-2:-1] and prices[-1:] > prices[-2:-1] and 1.1 > self.B_upper[-1:]/self.B_lower[-1:] > 1.04 and self.B_upper[-2:-1]/self.B_lower[-2:-1] > 1.04 and self.B_upper[-3:-2]/self.B_lower[-3:-2] > 1.04 and self.B_upper[-4:-3]/self.B_lower[-4:-3] > 1.04 and candlestick.close < 1.05 * self.mavs2[-7:-6]:# and sum(self.volume[-3:]) > sum(self.volume[-10:])*0.6:# and : #and  candlestick.priceAverage/min(prices[-15:]) < 1.025  : # and closes[-2:-1][0]/opens[-2:-1][0] < 1.003: #and self.B_upper[-1:]/self.B_lower[-1:] < 1.04:
				#if candlestick.low < self.B_lower2[-1:] and self.B_upper[-1:]/self.B_lower[-1:] < 1.025 and lows[-2:-1] > self.B_lower2[-1:]: #and np.arctan(float(self.mavs[-1:]-self.mavs[-2:-1])) > 0 
					if self.pair == "USDT_ETH":
						self.OpenPosition(candlestick.close,date,StopLoss = 0.0325, target_Sell = 1.0325*self.B_upper[-3:-2]) #OPEN POSITION!
					elif self.pair == "USDT_BTC":
						self.OpenPosition(candlestick.close,date,StopLoss = 0.0225, target_Sell = 1.0225*self.B_upper[-3:-2])
					elif self.pair == "USDT_BCH":
						self.OpenPosition(candlestick.close,date,StopLoss = 0.0425, target_Sell = 1.0425*self.B_upper[-3:-2])
					elif self.pair == "USDT_ETC":
						self.OpenPosition(candlestick.close,date,StopLoss = 0.0425, target_Sell = 1.0425*self.B_upper[-3:-2])
					elif self.pair == "USDT_XRP":
						self.OpenPosition(candlestick.close,date,StopLoss = 0.0425, target_Sell = 1.0425*self.B_upper[-3:-2])

					

	
		#Momentum opener		
		for trade in openTrades:
			if candlestick.priceAverage < self.B_upper2[-1:]:# or trade.trade_length > 120 and  self.mavs2[-2:-1]>self.mavs2[-1:]:
	 			pass
				#self.ClosePosition(date,candlestick,trade)#SELL POSITION!


	def updateOpenTrades(self,candlestick,date):
		for trade in self.trades:
			if (trade.status == "OPEN"):
				#New_Target = None
				trade.tick(candlestick.low,date,candlestickHigh = candlestick.high) #Tick the position to see if it has hit the stoploss with new target
				if trade.tick(candlestick.low,date,candlestick.high) == True: #check to see if it hit the stop loss and if so, close the position as usual
					if trade.target_reached == True:
						self.BullTokens[-1:] = 0
						self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.999)*(trade.target_sell) #Calculates the number of purchased Bear coins with commision taken into consideration
						self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.001	#calculate bear Commision and replace last element in the vector with calculated comission
						#print(self.BullTokens)
						#print(self.BullTokens)



						self.tradeStatus[-1:] = "TARGET HIT"
						trade.close('target price hit',date,self.BearTokens[-1:])
					else:
						self.BullTokens[-1:] = 0
						self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.998)*(trade.stopLoss) #Calculates the number of purchased Bear coins with commision taken into consideration
						self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.002	#calculate bear Commision and replace last element in the vector with calculated comission
						self.tradeStatus[-1:] = "STOP LOSSED"
						trade.close('stop loss price',date,self.BearTokens[-1:])
						



	def OpenPosition (self,open_price,date,StopLoss = None,target_Sell = None,Trail = None, Market_buy = True,Market_sell = True,Moving_target = False):
		if self.live == True:

			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			time.sleep(1)
			lastPairPrice = float(currentValues[self.pair]["lowestAsk"])
			asking_price = float(round(lastPairPrice*1.01,8))

			amount  = float(self.BearTokens[-1:])*0.99/asking_price
			orderNumber = self.conn.buy(self.pair,round(lastPairPrice*1.01,8),amount)
			print(orderNumber)
			time.sleep(2)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			time.sleep(2)
			balence = self.conn.returnBalances()
			self.BullTokens[-1:] = balence[self.bull]
			self.BearTokens[-1:] = balence[self.bear]
			print(self.BullTokens)
			print(self.BearTokens)

		else:
			self.BearTokens[-1:] = 0
			if Market_buy == True:
				self.BullTokens[-1:] = (float(self.BearTokens[-2:-1])*0.998)/open_price #Calculates the number of purchased Bull coins with commision taken into consideration
				self.BullCommisions[-1:] = float(self.BearTokens[-2:-1])*0.002	#calculate bull Commision and replace last element in the vector with calculated comission
			else:
				self.BullTokens[-1:] = (float(self.BearTokens[-2:-1])*0.999)/open_price
				self.BullCommisions[-1:] = float(self.BearTokens[-2:-1])*0.001	
			
		self.tradeStatus[-1:] = "OPEN"
		self.trades.append(BotTrade(open_price,date,self.BullTokens[-1:],stopLoss = StopLoss,target_sell = target_Sell,trail = Trail )) # Append a BotTrade Object to our trades list in percentage .... StopLoss = 0.02 is 2%






	def ClosePosition (self,date,candlestick,trade = None,Market_sell = True):
		if self.live == True:

			#Get latest asking price
			currentValues = self.conn.api_query("returnTicker")
			lastAskingPrice = float(currentValues[self.pair]["lowestAsk"])
			asking_price = float(round(lastAskingPrice*0.99,8))
			amount = float(self.BullTokens[-1:])*0.995
			orderNumber = self.conn.sell(self.pair,round(lastAskingPrice*0.99,8),amount)
			print(orderNumber)
			time.sleep(2)
			trade_hist = self.conn.returnTradeHistory(self.pair)
			time.sleep(2)
			balence = self.conn.returnBalances()

			self.BullTokens[-1:] = balence[self.bull]
			self.BearTokens[-1:] = balence[self.bear]
			print(self.BullTokens)
			print(self.BearTokens)

		else:
			self.BullTokens[-1:] = 0

			if Market_sell == True:
				self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.998)*candlestick.close #Calculates the number of purchased Bear coins with commision taken into consideration
				self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.002	#calculate bear Commision and replace last element in the vector with calculated comission
			else:
				self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.999)*candlestick.close
				self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.001


			
		trade.close(candlestick.close,date,self.BearTokens[-1:])
		self.tradeStatus[-1:] = "CLOSED"
	def live_flick(self):
		self.live = True


