from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class FISHY(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)
		self.period = period
		self.limits = True

		self.indicators = BotIndicators(orderbook= True,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime)
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None
		self.last_orderbook = pd.DataFrame()
		self.open_value = np.array([])

		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.OBD_predict_list = []

		self.dead_date_start = datetime.datetime(2019,4,7,0,0,0)
		self.dead_date_end = datetime.datetime(2019,4,7,6,0,0)
		

		self.numSimulTrades = 3
		self.margin = True
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		
		#updating Indicators


		self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1:],self.ob_index)



		if len(self.dates) > 2:
			self.open_value = self.indicators.get_open_value(self.dates[-1:])


		# if len(self.open_value) > 1450 and len(self.closes) > 10:
		# 	# print('\n')
		# 	# print('check22')
		# 	# print(self.open_value['open_value'][-1:])
		# 	# print(self.open_value['open_value'][-6:-5:])
		# 	feature0 = self.open_value['open_value'][-1:].item()
		# 	feature1 = abs(self.open_value['open_value'][-1:].item() - self.open_value['open_value'][-6:-5:].item())
		# 	# print('feature1', feature1)
		# 	# print('\n')


		# 	feature2 = self.open_value['open_interest'][-1:].item()
		# 	feature3 = self.closes[-2:-1] - self.closes[-5:-4]
		# 	feature4 = self.open_value['open_value'][-1:].item()
		# 	feature5 = self.closes[-2:-1] - self.closes[-14:-13]
		# 	feature6 =	abs(self.open_value['open_value'][-1:].item() - self.open_value['open_value'][-90:-89:].item())
		# 	feature7 = self.volumes[-2:-1]
		# 	feature8 = self.open_value['open_value'][-1:].item() - self.open_value['open_value'][-360:-359:].item()
		# 	feature9 = abs(self.open_value['open_interest'][-1:].item() - self.open_value['open_interest'][-6:-5:].item())
		# 	feature10 = self.open_value['open_value'][-1:].item() - self.open_value['open_value'][-1440:-1439:].item()

		# 	#print(feature1,feature2,feature3,feature4,feature5,feature6,feature7)

		# 	features = np.array([feature0,feature1,feature2,feature3,feature4,feature5,feature6,feature7,feature8,feature9,feature10]).reshape(1,-1)
		# 	#print(features)

			
		# 	self.OBD_predict = self.indicators.OBD_predict(features)[0]
		# 	self.OBD_predict_list.append(self.OBD_predict)
		# 	if np.mod(len(self.dates),100) == 0:
		# 		print(max(self.OBD_predict_list))
		# 	#print(self.OBD_predict)

		# 	#self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,40000000 ,60000000,3)
		# 	if self.OBD_predict > 30000000:
		# 		print('\n')
		# 		print("Greater then 40M!")
		# 		print(self.OBD_predict)
		# 		self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,self.OBD_predict ,self.OBD_predict*1.5,3)
		# 	else:
		# 		self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,30000000 ,60000000,3)
		
		# else:
		self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,40000000 ,70000000,3)
		try:
			self.buy_price1,self.buy_price2,self.buy_price3 = self.buy_prices['price']
			self.sell_price1,self.sell_price2,self.sell_price3 = self.sell_prices['price']
		except:
			pass
			#print(self.last_orderbook[['price','cum','datetimes_SYD']])
		

		self.open_value = self.indicators.get_open_value(self.dates[-1:])

		
		#self.buy_line1.append(self.last_orderbook[(self.last_orderbook['side']=='Buy') & (self.last_orderbook['cum'] < order5)]['price'].min())
		#self.sell_line1.append(self.last_orderbook[(self.last_orderbook['side']=='Sell') & (self.last_orderbook['cum'] < order5)]['price'].max())



		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3)]

		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)

		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)

		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)

			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 1000
		stoploss = None
		#market_order = 'zero'
		#print(date)
		market_order = False
		# if self.dead_date_start > date.item().replace(tzinfo=None):
		# 	if date.item().replace(tzinfo=None) < self.dead_date_end == False:
				
		#print(bp,sp)


		#if (self.dead_date_start < date.item().replace(tzinfo=None) and date.item().replace(tzinfo=None) < self.dead_date_end) == False:
			#print('check555')
		if len(self.dates) > 2:
			if self.highs[-1:] > sp and sp > self.closes[-2:-1]*1.005:
				action = True
				Long = False
				price = sp
				stoploss = price*1.05
				
						
			if self.lows[-1:] < bp and bp < self.closes[-2:-1]/1.005:
				
				action = True
				Long = True
				price = bp
				stoploss = price/1.05
		

		if action == True:
			print("ACTION TRUE")
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		
		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:



		if trade.trade_length == 7 and  trade.long == True:
			action = True
			price = self.closes[-1:]
		elif self.highs[-1:] > self.sell_price1 and trade.long == True:
			action = True
			price = self.sell_price1
		#elif trade.trade_length == 2 and trade.long == True and (self.closes[-1:] - trade.entryPrice) < (self.opens[-2:-1] - trade.entryPrice)/2:
		#	action = True
		#	price = self.closes[-1:]
					


		#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
		if trade.trade_length == 7 and trade.long == False:
			action = True
			price = self.closes[-1:]
		elif self.lows[-1:] < self.buy_price1 and trade.long == False:
			action = True
			price = self.buy_price1
		# elif trade.trade_length == 2 and trade.long == False and (trade.entryPrice - self.closes[-1:]) < (trade.entryPrice - self.opens[-2:-1])/2:
		# 	action = True
		# 	price = self.closes[-1:]

		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass
	def calculate_order_book_depth():
		pass




	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.buy_line1,self.sell_line1,self.open_value,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3)


