from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class Wicks_small(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= True,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False,open_value_candles = True)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None


		self.VPOC = np.array([None,None])
		self.vp_percentage =np.array([])

		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None


		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []
		

		self.mid_line = []

		self.prices = [(None,None),(None,None),(None,None)]
		
		if p1 is not None:
			self.p1 = p1
		else:
			self.p1 = 10000000

		if p2 is not None:
			self.p2 = p2
		else:
			p2 = None


		if p3 is not None:
			self.p3 = p3
		else:
			self.p3 = None


		self.drop_percent = 0.06
		self.middle = None

		self.OI_ACCUMULATED1 = []
		self.OI_ACCUMULATED2 = []
		self.OI_ACCUMULATED3 = []
		self.OI_ACCUMULATED_T =[]
		self.OV_min_list = []


		self.candle_count = 0
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = np.array([])
		self.Min_depths = []

		self.ATR_switches = np.array([])

		self.numSimulTrades = 3
		self.margin = True

		self.count10 = 0
		self.switch10 = False



		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0
		self.timer4 = 0
		self.timer5 = 0
		self.timer6 = 0
		self.timer7 = 0
		self.timer8 = 0

		self.account_size = []

		self.avg_entry = None



		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		#self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])

		#Update relevent indicators
		self.tick_indicators(trades)

	def tick_indicators(self,trades):

		self.avg_entry = None

		self.t1 = datetime.datetime.now()
		self.candle_count += 1
		#updating Indicators
		self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))
		self.t2 = datetime.datetime.now()


		self.t3 = datetime.datetime.now()
		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)


		self.calc_accumulations()
		self.t4 = datetime.datetime.now()



		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')



		# if self.candle_count > 290:
		# 	if len(self.open_value['max_delta_OI0'][-1:]) > 0:
		# 		biggest_drop_OI = self.open_value['MAX_MAX_delta_Event_OI'][-1:].item()
		# 		biggest_drop_OV = self.open_value['MAX_MAX_delta_Event_OV'][-1:].item()
		# 		#print(biggest_drop_OV)
		# 		#print(-self.open_value['OV_high'][-36:].max()*self.drop_percent)
		# 		#if biggest_drop_OI < -self.open_value['OI_high'][-36:].max()*self.drop_percent and self.switch10 == False:# and abs(self.closes[-1:]-self.closes[-1:]) > self.closes[-1:]*0.035: #and self.open_value['max_delta'][-1:].item() != self.max_value:
		# 		if biggest_drop_OV < -self.open_value['OV_high'][-36:].max()*self.drop_percent and self.switch10 == False or biggest_drop_OI < -self.open_value['OI_high'][-36:].max()*self.drop_percent and self.switch10 == False:
		# 			self.switch10 = True
			
		# 			self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
		# 			self.last_OV_high = self.open_value['OI_high'][-36:].max()
		# 			self.last_OV_min = self.open_value['OI_low'][-36:].min()
					

		# 			self.ATR_switch = True
		# 			if self.ATR_switches[-1:] == 0:# and trades[-3:-2][0].status == "CLOSED" and trades[-2:-1][0].status == "CLOSED" and trades[-1:][0].status == "CLOSED":
		# 				if len(trades) == 0 or trades[-1].status == "CLOSED":#and trades[-2:-1].status == "CLOSED" and trades[-1:].status == "CLOSED":
		# 					self.strategy_start = self.dates[-1]
		# 					self.OV_min_list.append(self.last_OV_min)
		# 			print('\n')
		# 			print("NEW TRIGGER EVENT! " , self.dates[-1])
		# 			#print(biggest_drop)
		# 			print("MIN ATR ", self.ATR_min)
		# 			print('\n')

		# 		if self.ATR[-1:] != None:
		# 			if self.ATR_switches[-12*24:].min() == 1:					
		# 				if self.ATR[-1:] < self.ATR_min*0.9 and self.ATR_switch == True:
		# 					self.ATR_switch = False
		# 					#self.strategy_start = None
		# 					print('\n')
		# 					print('FAILED ATR SWITCH ', self.dates[-1:])
		# 					print('\n')		
					
		# 		if self.open_value['MAX_MAX_delta_Lookback_OI'][-1:].item() > -self.open_value['OI_high'][-36:].max()*self.drop_percent and self.ATR_switch == True:
		# 			self.ATR_switch = False
		# 			#self.strategy_start = None
		# 			print('\n')
		# 			print('5 day OV drop expired! ', self.dates[-1:])
		# 			print('\n')

		# 		if self.open_value['OI_high'][-1].item() > self.last_OV_high + self.open_value['OI_high'][-6*180:].max()*self.drop_percent and self.ATR_switch == True:

		# 			self.ATR_switch = False
		# 			#self.strategy_start = None
		# 			print('\n')
		# 			print('OVER THE OV MAX ', self.dates[-1:])
		# 			print('OV MAX ',self.last_OV_high + self.open_value['OI_high'][-36:].max()*self.drop_percent)
		# 			print("LAST OI ",self.open_value['OI_close'][-1].item())
		# 			print('\n')
		
		# self.t5 = datetime.datetime.now()

		self.ATR_switch = True


		if self.ATR_switch == True and self.ATR[-1] is not None and self.open_value['OI_close'][-1:].item() is not None:
			#self.min_depth = self.p1 + self.p2*self.ATR[-1] + self.p3*self.open_value['OI_close'][-1:].item()

			self.min_depth = self.p1 + self.p2*self.ATR[-1]*2 + self.p3*self.open_value['OI_close'][-1:].item()


			#self.min_depth = 20000000
			self.max_depth = self.min_depth*2.25
			# print('\n')
			# print(self.p1)
			# print(self.p2*self.ATR[-1])
			# print(self.p3*self.open_value['close'][-1:].item())


			# print(round(self.min_depth/1000000,1))
			# time.sleep(1)
		else:
			self.min_depth = 20000000
			self.max_depth = self.min_depth*2.25


		

		if self.candle_count > 1:
			trade_not_closed = False
			if len(trades) > 0:
				if trades[-1].status == "OPEN":# or trades[-2].status == "OPEN" or trades[-3].status == "OPEN":#
					trade_not_closed = True
					#print("TRADE NOT CLOSED == TRUE!")


			if self.ATR_switch == True or trade_not_closed == True :
		#if self.dates[-2] != None:
			
				self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)
				#self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],None)
				self.t6 = datetime.datetime.now()
				#if self.candle_count > 290:
					# print(self.dates[-1]," Candle Date")
					# print(self.last_orderbook, " Orderbook Date")
					# print('\n')
					# time.sleep(1)

				#self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-2:-1],self.ob_index)
				if self.last_orderbook is not None:
					self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,self.min_depth ,self.max_depth,3,cum2= True)
					#try:

					buy_prices = self.buy_prices['price']
					sell_prices = self.sell_prices['price']

					if len(buy_prices) == 3 and len(sell_prices) == 3:
						self.buy_price1,self.buy_price2,self.buy_price3 = buy_prices
						self.sell_price1,self.sell_price2,self.sell_price3 = sell_prices
					else:

						self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,self.min_depth ,self.min_depth*3.5,3,cum2= True)
						buy_prices = self.buy_prices['price']
						sell_prices = self.sell_prices['price']






						print(buy_prices)
						print(sell_prices)
						print(self.last_orderbook)
						print(self.min_depth)
						print(self.max_depth)
						print(self.last_orderbook[['side','price','qty2','cum']][75:125])
						
					
					# except:
					# 	print("Book Skipped")
					# 	self.buy_price1 = None
					# 	self.buy_price2 = None
					# 	self.buy_price3 = None
					# 	self.sell_price1 = None
					# 	self.sell_price2 = None
					# 	self.sell_price3 = None

						#print(self.last_orderbook[['price','qty2','cum']][0:50])
						#print(self.last_orderbook[['price','qty2','cum']][-50:])
						#time.sleep(1)
			else:
				self.buy_price1 = None
				self.buy_price2 = None
				self.buy_price3 = None
				self.sell_price1 = None
				self.sell_price2 = None
				self.sell_price3 = None

			self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3)]
			self.t7 = datetime.datetime.now()
		else:
			self.t6 = datetime.datetime.now()
			self.t7 = datetime.datetime.now()
			self.buy_price1 = None
			self.buy_price2 = None
			self.buy_price3 = None
			self.sell_price1 = None
			self.sell_price2 = None
			self.sell_price3 = None
		


		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)

		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)

		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)


		if self.ATR_switch == False:
			self.ATR_switches = np.append(self.ATR_switches,0)
			
		elif self.ATR_switch == True:
			self.ATR_switches = np.append(self.ATR_switches,1)
		self.ATR_min_list = np.append(self.ATR_min_list,self.ATR_min)
		self.Min_depths.append(self.min_depth)

		self.t8 = datetime.datetime.now()

		# self.timer1 += (self.t2-self.t1).total_seconds()
		# self.timer2 += (self.t3-self.t2).total_seconds()
		# self.timer3 += (self.t4-self.t3).total_seconds()
		# self.timer4 += (self.t5-self.t4).total_seconds()
		# self.timer5 += (self.t6-self.t5).total_seconds()
		# self.timer6 += (self.t7-self.t6).total_seconds()
		# self.timer7 += (self.t8-self.t7).total_seconds()
			
			
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		self.Leverage = 6
		total = self.account_size[-1]*self.Leverage/3



		stoploss = None
		#market_order = 'zero'
		market_order = False



		#print("CHECK1")
		if sp != None and self.closes[-2] != None and self.ATR_switch == True and sum(self.ATR_switches[-12:]) == 12:
			#print("CHECK2")
			#if self.highs[-1:] > sp and self.closes[-2:-1] < sp and  0.20 < self.vp_percentage[-2:-1] < 0.80 and  0.20 < self.vp_percentage[-3:-2] < 0.80 and  0.20 < self.vp_percentage[-4:-3] < 0.80 and  0.20 < self.vp_percentage[-5:-4] < 0.80 and  0.2 < self.vp_percentage[-6:-5] < 0.80 and sp > self.closes[-2:-1]*1.0035: 
			if self.highs[-1] > sp and self.closes[-2] < sp and sp > self.closes[-2]*1.001: 
			
				action = True
				Long = False
				price = sp
				#stoploss = self.sell_price2*1.01
				
		if bp != None and self.closes[-2] != None and self.ATR_switch == True and sum(self.ATR_switches[-12:]) == 12:
			#print("CHECK3")
			if self.lows[-1] < bp and self.closes[-2] > bp  and bp < self.closes[-2]/1.001:
				action = True
				Long = True
				price = bp
				#stoploss = self.buy_price2/1.01


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):


		if self.closes[-2] > trade.entryPrice and trade.long == True or self.closes[-2] < trade.entryPrice and trade.long == False: #position is onside
			factor1 = [2,2.5,3]
			factor2 = [1.25,1.25,1.25]
		else:#position is offside
			factor1 = [3,4,5]
			factor2 = [1.5,1.5,1.5]


		if self.avg_entry == None:
			open_trades = []
			entries = []

			for trade in trades:
				if trade.status == 'OPEN':
					open_trades.append(trade)
			
			if len(open_trades) > 0:
				for o_trade in open_trades:
					entries.append(o_trade.entryPrice)

				self.avg_entry = sum(entries)/len(open_trades)



		#try:
		#b_p,s_p = self.indicators.n_largest_spread(self.last_orderbook,self.min_depth/factor1[0] ,self.min_depth/factor2[0],1,cum2= True)

		if self.last_orderbook is not None:
			b_p,s_p = self.indicators.n_largest_spread(self.last_orderbook,self.min_depth/5 ,self.min_depth/1.5,1,cum2= True)



		try:
			bp1= b_p['price'].item()
			sp1 = s_p['price'].item()
		except:
			try:
				b_p,s_p = self.indicators.n_largest_spread(self.last_orderbook,self.min_depth/5 ,self.min_depth*1.5,1,cum2= True)
				bp1= b_p['price'].item()
				sp1 = s_p['price'].item()
			except:
				bp1 = None
				sp1 = None
			


			# print(b_p)
			# print(s_p)
			# print(self.min_depth/factor1[0])
			# print(self.min_depth/factor2[0])

			
		# except:
		# 	print('NO ORDERS RETURNED')
		# 	#print(self.last_orderbook['cum2'])
		# 	ssss
		# 	try:
		# 		b_p,s_p = self.indicators.n_largest_spread(self.last_orderbook,self.min_depth/factor1[1] ,self.min_depth/factor2[1],1,cum2= True)
		# 		bp1= b_p['price'].item()
		# 		sp1 = s_p['price'].item()
		# 	except:
		# 		print('NO ORDERS RETURNED')
		# 		try:
		# 			print('NO ORDERS RETURNED')
		# 			b_p,s_p = self.indicators.n_largest_spread(self.last_orderbook,self.min_depth/factor1[2],self.min_depth/factor2[2],1,cum2= True)
		# 			bp1= b_p['price'].item()
		# 			sp1 = s_p['price'].item()
		# 		except:
		# 			bp1 = None
		# 			sp1 = None





	
		#print(bp1,sp1)
		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:



		#if self.highs[-1:] > self.VPOC[-1:] and trade.long == True:
		#if trade.trade_length == 19 and trade.long == True:
		if bp1 is not None and sp1 is not None:
			if self.highs[-1] > sp1  and trade.long == True:
			#if self.highs[-1:] > self.sell_price1 and trade.long == True:
				action = True
				price = sp1
				#price = trade.entryPrice*1.02
				#price = self.sell_price1
			
						


			#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
			#if self.lows[-1:] < self.VPOC[-1:] and trade.long == False:
			if self.lows[-1] < bp1 and trade.long == False:
			#if trade.trade_length == 19 and trade.long == False:
			#if self.lows[-1:] < trade.entryPrice/1.01 and trade.long == False:
				action = True
				price = bp1
				#price = trade.entryPrice/1.02
				#price = self.buy_price1


		if self.avg_entry is not None:
			if trade.long == True and self.lows[-1] < self.avg_entry/1.02:
				action = True
				price = self.avg_entry/1.02
				market_order = True

			if trade.long == False and self.highs[-1] > self.avg_entry*1.02:
				action = True
				price = self.avg_entry*1.02
				market_order = True

		
		

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def calc_accumulations(self):

		self.switch10 = False
		if self.switch10 == False:

			if len(self.OV_min_list)> 0:
				minimum_ov = self.OV_min_list[-1]
				current_accumulation = (self.open_value['OI_close'][-1].item()/minimum_ov) - 1
				self.OI_ACCUMULATED1.append(current_accumulation)
			else:
				self.OI_ACCUMULATED1.append(None)

			if len(self.OV_min_list)> 1:
				#minimum_ov = self.open_value['low'][-self.trigger_date-6:-self.trigger_date+6].min()
				minimum_ov = self.OV_min_list[-2]
				current_accumulation = (self.open_value['OI_close'][-1].item()/minimum_ov) - 1
				self.OI_ACCUMULATED2.append(current_accumulation)
			else:
				self.OI_ACCUMULATED2.append(None)

			if len(self.OV_min_list)> 2:
				#minimum_ov = self.open_value['low'][-self.trigger_date-6:-self.trigger_date+6].min()
				minimum_ov = self.OV_min_list[-3]
				current_accumulation = (self.open_value['OI_close'][-1].item()/minimum_ov) - 1
				self.OI_ACCUMULATED3.append(current_accumulation)
			else:
				self.OI_ACCUMULATED3.append(None)
		else:
			self.OI_ACCUMULATED1.append(None)
			self.OI_ACCUMULATED2.append(None)
			self.OI_ACCUMULATED3.append(None)

		if self.OI_ACCUMULATED1[-1] is not None and self.OI_ACCUMULATED2[-1] is not None and self.OI_ACCUMULATED3[-1] is not None:
			append = (self.OI_ACCUMULATED1[-1] + self.OI_ACCUMULATED2[-1]*0.5 + self.OI_ACCUMULATED3[-1]*0.25)/1.75
			self.OI_ACCUMULATED_T.append(append)
		else:
			self.OI_ACCUMULATED_T.append(None)

	def scale_out():
		pass
	def Draw_indicators(self):
		print(len(self.buy_line1))
		print(len(self.buy_line2))
		print(len(self.buy_line3))
		print(len(self.sell_line1))
		print(len(self.sell_line2))
		print(len(self.sell_line3))
		print(len(self.ATR))
		print(len(self.open_value))
		print(len(self.ATR_switches))


		print(self.OI_ACCUMULATED_T)


		#self.vp_percentage = np.append(self.vp_percentage[self.vp_percentage == None],1 - self.vp_percentage[self.vp_percentage != None] )
		return (self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.ATR,self.open_value,self.ATR_switches,self.Min_depths,self.OI_ACCUMULATED_T)
		#(self.vp_percentage,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.Min_depths)
		#(self.VPOC,self.buy_line2,self.sell_line2,self.buy_line4,self.sell_line4,self.buy_line6,self.sell_line6,self.ATR,self.open_value,self.ATR_switches)#,self.ATR_min_list)

