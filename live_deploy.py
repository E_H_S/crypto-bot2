import sys, getopt
from botchart import BotChart
from BotStratLog import BotTradingLog
from botcandlestick import BotCandlestick
from botVis import BotVisual
from poloniex import poloniex
from Small_MA import SMA
import datetime
import time
import schedule



class Live(object):
	def __init__(self):


		
		#Input perameters ##
		self.exchange = "poloniex"
		self.pair = "USDT_ETH"
		self.bear,self.bull = self.pair.split('_')
		self.period = 14400 #[300,900,1800,7200,14400,86400]
		self.endTime = time.time()
		self.timedelta_seconds = self.period *20.5
		self.startTime = self.endTime - self.timedelta_seconds
		self.draw_pause = float(0.001)
		self.live = False
		self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')

		self.balence = self.conn.returnBalances()
		
		self.Initital_BullVolume = self.balence[self.bull]
		self.Initital_BearVolume = self.balence[self.bear]
		



		self.strategy = SMA(self.pair,self.Initital_BullVolume,self.Initital_BearVolume,backtest = False)
		self.chart = BotChart(self.exchange,self.pair,self.period,self.startTime,self.endTime,backtest = True)

		self.TradingLog = BotTradingLog(self.pair,self.period,self.strategy)
		print("HISTORICAL BACKTEST DATA")
	
		for candlestick in self.chart.getPoints()[0:-1]:#First query the past 20 candlesticks for indicators
			self.TradingLog.tick(candlestick)
			print(str(datetime.datetime.fromtimestamp(candlestick.date))+' : Close price : '+str(candlestick.close))
	
		self.TradingLog.strategy.live_flick()	#Flick stategy script to Live
		dt = datetime.datetime.fromtimestamp(time.time())
		if self.period == 300:
			dt = dt - datetime.timedelta(minutes=dt.minute % 5, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 4)
		elif self.period == 900:
			dt = dt - datetime.timedelta(minutes=dt.minute % 15, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 14)
		elif self.period == 1800:
			dt = dt - datetime.timedelta(minutes=dt.minute % 30, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 29)	
		elif self.period == 7200:
			dt = dt - datetime.timedelta(hours = dt.hour % 2, minutes=dt.minute, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 119)
		elif self.period == 14400:
			dt = dt - datetime.timedelta(hours = dt.hour % 4, minutes=dt.minute, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(hours = 3 ,minutes = 50)
		elif self.period == 86400:
			dt = dt - datetime.timedelta(hours = dt.hour % 24, minutes=dt.minute, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(hours = 23 ,minutes = 59)
		
		dt = str(dt.time())[0:-3]
		print(dt)
		schedule.every().day.at(dt).do(self.pacing).tag('once-only')
		print("LIVE FRESH DATA")

	
	def pacing(self):
		schedule.every(self.period).seconds.do(self.live_data).tag('tradeing')
		schedule.clear('once-only')
		self.live_data()

	def live_data(self):
		last_candle = self.chart.LastestCandle()
		print('Time of candlestick query :  ' +str(datetime.datetime.fromtimestamp(time.time())))
		self.TradingLog.tick(last_candle)
		print('Candlestick datetime ; ' + str(datetime.datetime.fromtimestamp(last_candle.date)))
		print("Close  :  "+str(last_candle.close)+ " : EMA : " +str(self.TradingLog.strategy.mavs2[-1:]) + " : MAV : " + str(self.TradingLog.strategy.mavs[-1:]))



if __name__ == "__main__":
	live_trade = Live()
	while True:
		schedule.run_pending()
		time.sleep(1)






