import sys, getopt
from botchart import BotChart
from BotStratLog import BotTradingLog
from botcandlestick import BotCandlestick
from botVis import BotVisual
from poloniex import poloniex
from SMAlive import SMA_LIVE
from SmaMarginLive import SMA_LIVE_M
from Momentum_margin_reverse_live import M_M_R_LIVE
import datetime
import time
import schedule


class Live(object):
	def __init__(self,argv):

		#Input perameters ##
		#chart conditions
		self.exchange = "poloniex"
		self.pairs = ["USDT_BTC"]
		self.bear,self.bull = self.pairs[0].split('_')
		self.period = 300
		self.endTime = time.time()
		
		#self.delay = 1

		#Other conditions
		self.draw_pause = float(0.001)
		self.live = False
		self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')

		#Trading volume conditions
		self.balence = self.conn.returnBalances()
		self.Initital_BullVolume = self.balence[self.bull]
		self.Initital_BearVolume = self.balence[self.bear]
		self.initital_trading_amount = float(round(0.021,4))
		
		#inititalising classes
		self.Graph = BotVisual(live = True)
		self.strategy = {}
		self.TradingLog = {}

		#Terminal inputs
		try:
			opts, args = getopt.getopt(argv,"hp:c:d:",["period=","currency=","delay="])
		except getopt.GetoptError:
			print('trading-bot.py -p <period length> -c <currency pair>')
			sys.exit(2)

		for opt, arg in opts:
			if opt in ("-p", "--period"):
				if (int(arg) in [300,900,1800,7200,14400,86400]):
					self.period = int(arg)
				else:
					print('Poloniex requires periods in 300,900,1800,7200,14400, or 86400 second increments')
					sys.exit(2)
			elif opt in ("-c", "--currency"):
				self.pairs = [arg]
			elif opt in ("-d", "--delay"):
				self.delay = float(arg)

		self.bear,self.bull = self.pairs[0].split('_')
		self.endTime = time.time()
		self.timedelta_seconds = self.period *50.5
		self.startTime = self.endTime - self.timedelta_seconds


		
		self.chart = BotChart(self.exchange,self.pairs,self.period,self.startTime,self.endTime,backtest = True)
		for pair in self.pairs:
			
			self.strategy[str(pair)] = M_M_R_LIVE(pair,self.Initital_BullVolume,self.Initital_BearVolume,live = False,backtest = True,Initial_trading_volume = self.initital_trading_amount)
			self.TradingLog[str(pair)] = BotTradingLog(pair,self.period,self.strategy[str(pair)])	
		print("HISTORICAL BACKTEST DATA")
	


		candles_allpairs = self.chart.getPoints()

		for pair in self.pairs:
			for candlestick in candles_allpairs[str(pair)][:-1:]:#First query the past 20 candlesticks for indicators
				self.TradingLog[str(pair)].tick(candlestick)
				self.Graph.draw(self.TradingLog[str(pair)],self.draw_pause)
				print(str(datetime.datetime.fromtimestamp(candlestick.date))+' : Close price : '+str(candlestick.close))
				self.last_candle = candlestick
	
		self.TradingLog[str(self.pairs[0])].strategy.live_flick()	#Flick stategy script to Live

		dt = self.start_time()
		print(dt)
		schedule.every().day.at(dt).do(self.pacing).tag('once-only')
		print("LIVE FRESH DATA")
		

	
	def pacing(self):
		schedule.every(self.period).seconds.do(self.live_data).tag('tradeing')
		schedule.clear('once-only')
		self.live_data()

	def live_data(self):
		print("\n")
		query_time = time.time()
		print('Time of candlestick query :  ' +str(datetime.datetime.fromtimestamp(query_time)))
		last_candle = self.chart.LastestCandle(self.last_candle)
		if last_candle.open == 0:
			last_candle = self.TradingLog[str(self.pairs[0])].candlesticks[-1:][0]
			last_candle.date = last_candle.date+self.period
			last_candle.open = last_candle.priceAverage
			last_candle.close = last_candle.priceAverage


		print('Candlestick datetime ; ' + str(datetime.datetime.fromtimestamp(last_candle.date)))
		for pair in self.pairs:
			self.TradingLog[str(pair)].tick(last_candle)
			self.Graph.draw(self.TradingLog[str(pair)],self.draw_pause)

			if pair in ("BTC_XMR","BTC_DASH","BTC_MAID","BTC_FCT"):	
				print("Close  :  "+str(last_candle.close)+ " : EMA : " +str(self.TradingLog[str(self.pairs[0])].strategy.mavs2[-1:]) + " : MAV : " + str(self.TradingLog[str(self.pairs[0])].strategy.mavs[-1:])+ " :MAV_DELTA " + str(self.TradingLog[str(self.pairs[0])].strategy.mavs_delta[-1:]) + " MOMENTUM : " + str(self.TradingLog[str(self.pairs[0])].strategy.momentum[-1:]))
			else:
				print("Close  :  "+str(last_candle.close)+ " : EMA : " +str(self.TradingLog[str(self.pairs[0])].strategy.mavs2[-1:]) + " : MAV : " + str(self.TradingLog[str(self.pairs[0])].strategy.mavs[-1:])+ " :MAV_DELTA " + str(self.TradingLog[str(self.pairs[0])].strategy.mavs_delta[-1:]))
		
		self.last_candle = last_candle

		if query_time > (last_candle.date + self.period - 120):
			self.reset_pacing()


	def reset_pacing(self):
		schedule.clear('tradeing')
		dt = self.start_time()
		print("query time reset to " + str(dt))
		schedule.every().day.at(dt).do(self.pacing).tag('once-only')



	def start_time(self):
		dt = datetime.datetime.fromtimestamp(time.time())
		if self.period == 300:
			dt = dt - datetime.timedelta(minutes=dt.minute % 5, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 5 - self.delay)
		elif self.period == 900:
			dt = dt - datetime.timedelta(minutes=dt.minute % 15, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 15 - self.delay)
		elif self.period == 1800:
			dt = dt - datetime.timedelta(minutes=dt.minute % 30, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 30- self.delay)	
		elif self.period == 7200:
			dt = dt - datetime.timedelta(hours = dt.hour % 2, minutes=dt.minute, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(minutes = 120 - self.delay)
		elif self.period == 14400:
			dt = dt - datetime.timedelta(hours = dt.hour % 4, minutes=dt.minute, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(hours = 3 ,minutes = 60 - self.delay)
		elif self.period == 86400:
			dt = dt - datetime.timedelta(hours = dt.hour % 24, minutes=dt.minute, seconds=dt.second,microseconds=dt.microsecond) +  datetime.timedelta(hours = 24 ,minutes = 60 - self.delay)
		
		dt = str(dt.time())[0:-3]
		return dt



	

if __name__ == "__main__":
	live_trade = Live(sys.argv[1:])
	
	while True:
		schedule.run_pending()
		time.sleep(5)



