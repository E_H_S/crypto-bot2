import numpy as np
import pandas as pd
import datetime

def datetime_chop(x):
	return x[37:-51]

data = pd.read_csv('self_data.csv')
trades = pd.read_csv('self_trades.csv')
OI_XBT = pd.read_csv('self_OI_XBT.csv')
OI_ETH = pd.read_csv('self_OI_ETH.csv')
FR_XBT = pd.read_csv('self_FR_XBT.csv')
FR_ETH = pd.read_csv('self_FR_ETH.csv')
XBT_book_metrics = pd.read_csv('self_XBT_book_metrics.csv')
ETH_book_metrics = pd.read_csv('self_ETH_book_metrics.csv')
XBT_vps = pd.read_csv('self_XBT_vps.csv')
ETH_vps = pd.read_csv('self_ETH_vps.csv')


print(data.info())
print(trades.info())
print(OI_XBT.info())
print(OI_ETH.info())
print(FR_XBT.info())
print(FR_ETH.info())
print(XBT_book_metrics.info())
print(ETH_book_metrics.info())
print(XBT_vps.info())
print(XBT_vps.head())
print(XBT_vps.tail())
print(ETH_vps['datetimes_SYD'])


trades['lookback-date'] = pd.to_datetime(trades['entry_date']) - pd.Timedelta(minutes = 5)

trades['lookback-date'] = trades['lookback-date'].astype(str)

print(trades['entry_date'])
print(trades['lookback-date'])



#trades['lookback-date'] = trades['lookback-date'].astype(str)



print('\n')
print('\n')
print('\n')
print(data['date'][0])
print(trades['entry_date'][0])
print('\n')
print(OI_XBT['datetimes_SYD'][0])
print(OI_ETH['datetimes_SYD'][0])

print('\n')
#FR_ETH['datetimes_SYD'] = pd.to_datetime(FR_ETH['datetimes_SYD'])

#FR_XBT['datetimes_SYD'] = pd.to_datetime(FR_XBT['datetimes_SYD'])


print(FR_ETH['datetimes_SYD'][0])
print(FR_XBT['datetimes_SYD'][0])
print(type(FR_ETH['datetimes_SYD'][0]))

print('\n')
print(XBT_book_metrics['datetimes_SYD'][0])
print(ETH_book_metrics['datetimes_SYD'][0])
print('\n')

print(XBT_vps['datetimes_SYD'][0])
print(ETH_vps['datetimes_SYD'][0])
print('\n')
print('\n')
print(XBT_vps['datetimes_SYD'][2][37:-51])



#Prep the vps
XBT_vps['datetimes_SYD2'] = XBT_vps['datetimes_SYD'].apply(lambda x: datetime_chop(x))
XBT_vps = XBT_vps[1::]

ETH_vps['datetimes_SYD2'] = ETH_vps['datetimes_SYD'].apply(lambda x: datetime_chop(x))
ETH_vps = XBT_vps[1::]





df = pd.merge(trades,data,left_on = 'lookback-date',right_on = 'date')


OI_XBT2 = pd.DataFrame()
XBT_OI_indexes = []
ETH_OI_indexes = []

FR_XBT_indexes = []
FR_ETH_indexes = []

XBT_book_metrics_indexes = []
ETH_book_metrics_indexes = []

XBT_vps_indexes = []
ETH_vps_indexes = []

print(type(FR_XBT['datetimes_SYD'][0]),type(df['entry_date'][0]),type(OI_XBT['datetimes_SYD'][0]),type(XBT_vps['datetimes_SYD'][1]))

#print(XBT_vps['datetimes_SYD'])

#XBT_vps['datetimes_SYD'] = pd.to_datetime(XBT_vps['datetimes_SYD']).dt.strftime("%Y-%m-%d %H:%M:%S.%f")
print("\n")
print("\n")
#print(XBT_vps['datetimes_SYD'][3400][37:-51])
#print(XBT_vps['datetimes_SYD'][0][28:-51])
#XBT_vps['datetimes_SYD'] = XBT_vps['datetimes_SYD'][3400][37:-51]
#XBT_vps['datetimes_SYD'] = XBT_vps['datetimes_SYD'][37:-51]

print("\n")
print(df['lookback-date'][0])
print("\n")
#print(XBT_vps['datetimes_SYD'][2] < df['lookback-date'][0])
#FR_XBT['datetimes_next'] = FR_XBT['datetimes_SYD'] + pd.Timedelta(hours = 10)



#print()
for index,row in df.iterrows():
	print('\n')
	print(row['lookback-date'])


	XBT_OI_indexes.append(OI_XBT[OI_XBT['datetimes_SYD'] < row['lookback-date']].iloc[-1:,:].index.item())
	ETH_OI_indexes.append(OI_ETH[OI_ETH['datetimes_SYD'] < row['lookback-date']].iloc[-1:,:].index.item())


	try:
		i = FR_XBT[FR_XBT['datetimes_SYD'] < row['lookback-date']].iloc[-1:,:].index.item()
		if i+1 < len(FR_XBT):
			FR_XBT_indexes.append(i+1)
		else:
			R_XBT_indexes.append(i)

		#print(FR_XBT.iloc[FR_XBT_indexes[-1:],:])
	except:
		FR_XBT_indexes.append(1)

	try:
		i = FR_ETH[FR_ETH['datetimes_SYD'] < row['lookback-date']].iloc[-1:,:].index.item()
		if i+1 < len(FR_ETH):
			FR_ETH_indexes.append(i+1)
		else:
			FR_ETH_indexes.append(i)


	except:
		FR_ETH_indexes.append(1)


	try:
		XBT_book_metrics_indexes.append(XBT_book_metrics[XBT_book_metrics['datetimes_SYD'] < row['lookback-date']].iloc[-1:,:].index.item())
		#ETH_book_metrics_indexes.append(ETH_book_metrics[ETH_book_metrics['datetimes_SYD'] < row['lookback-date']].iloc[-1:,:].index.item())
	except:
		pass

	try:
		ETH_book_metrics_indexes.append(ETH_book_metrics[ETH_book_metrics['datetimes_SYD'] < row['lookback-date']].iloc[-1:,:].index.item())
	except:
		pass

	
	XBT_vps_indexes.append(XBT_vps[XBT_vps['datetimes_SYD2'] < row['lookback-date']].iloc[-1:,:].index.item())
	ETH_vps_indexes.append(ETH_vps[ETH_vps['datetimes_SYD2'] < row['lookback-date']].iloc[-1:,:].index.item())
	





print(FR_XBT_indexes)
print(FR_ETH_indexes)
print(len(FR_XBT))
print(len(FR_ETH))



#Creating the DataFrames
OI_XBT2 = OI_XBT.iloc[XBT_OI_indexes,:].reset_index()
OI_ETH2 = OI_ETH.iloc[ETH_OI_indexes,:].reset_index()
FR_XBT2 = FR_XBT.iloc[FR_XBT_indexes,:].reset_index()
FR_ETH2 = FR_ETH.iloc[FR_ETH_indexes,:].reset_index()
XBT_book_metrics2 = XBT_book_metrics.iloc[XBT_book_metrics_indexes,:].reset_index()
ETH_book_metrics2 = ETH_book_metrics.iloc[ETH_book_metrics_indexes,:].reset_index()
XBT_vps2 = XBT_vps.iloc[XBT_vps_indexes,:].reset_index()
ETH_vps2 = ETH_vps.iloc[ETH_vps_indexes,:].reset_index()









#df = pd.concat([df,OI_XBT2,OI_ETH2,FR_XBT2,FR_ETH2,XBT_book_metrics2,ETH_book_metrics2,XBT_vps2,ETH_vps2],axis =1)
df = pd.merge(df,OI_XBT2,left_index = True, right_index = True,how = 'left')
df = pd.merge(df,OI_ETH2,left_index = True, right_index = True,how = 'left')
df = pd.merge(df,FR_XBT2,left_index = True, right_index = True,how = 'left')
df = pd.merge(df,FR_ETH2,left_index = True, right_index = True,how = 'left')
df = pd.merge(df,XBT_book_metrics2,left_index = True, right_index = True,how = 'left')
df = pd.merge(df,ETH_book_metrics2,left_index = True, right_index = True,how = 'left')
df = pd.merge(df,XBT_vps2,left_index = True, right_index = True,how = 'left')
df = pd.merge(df,ETH_vps2,left_index = True, right_index = True,how = 'left')



print(df['ov_diff_x'])
print(df.info())

#df.to_csv('Post_pump_candles3.csv')
print(len(df))
print(len(XBT_OI_indexes))
print(len(ETH_OI_indexes))
print('\n')

print(len(FR_XBT_indexes))
print(len(FR_ETH_indexes))
print('\n')
print(len(ETH_book_metrics_indexes))
print(len(XBT_book_metrics_indexes))
print('\n')
print(len(XBT_vps_indexes))
print(len(ETH_vps_indexes))


#print(OI_XBT2)



