from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class renko_range(object):
	def __init__(self, period = 14400):

		self.fast = 5
		self.slow = 42
		self.target = 1.15
		self.stoploss = 1.02

		self.period = period

		self.indicators = BotIndicators()
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.mavs3  = np.array([])
		self.RSI = np.array([])
		self.volume_rank = np.array([])

		self.momentum_F = np.array([])
		self.momentum_S = np.array([])

		self.numSimulTrades = 1
		self.margin = True
		
		self.scale = 14400/self.period

		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closesprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}

		self.weighted_average = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		self.volume_rank = np.append(self.volume_rank,self.indicators.volume_rank(self.volumes))
		#print(self.volume_rank[-1:])
		
	def evaluate_Open(self):
		action = False
		Long = None
		price = None
		date = int(self.dates[-1:])
		total = None
		stoploss = None
		#market_Order = 'zero'
		market_Order = True


		
			
		if self.closes[-1:] < self.opens[-1:] and self.closes[-2:-1:] > self.opens[-2:-1:] and self.closes[-3:-2:] < self.opens[-3:-2:]:
			action = True
			Long = False
			price = self.closes[-1:]
			stoploss = None
			if market_Order != 'zero':
				market_Order = True
				
						
			
		if self.closes[-1:] > self.opens[-1:] and self.closes[-2:-1:] < self.opens[-2:-1:] and self.closes[-3:-2:] > self.opens[-3:-2:]:
			action = True
			Long = True
			price = self.closes[-1:]
			stoploss = None
			if market_Order != 'zero':
				market_Order = True
			
				
		return action,Long,price,date,total,self.margin,stoploss,market_Order
	def evaluate_Close(self,trade):

		action = False
		price = None
		date = int(self.dates[-1:])
		total = None
		market_order = True
		#market_order = 'zero'


		if trade.long == False and self.closes[-1:] < self.opens[-1:]:
			action = True
			price = self.closes[-1:]
			if market_order != 'zero':
				market_order = True
			
		elif trade.long == False and self.closes[-1:] > self.opens[-1:]:
			action = True
			price = self.closes[-1:]
			if market_order != 'zero':
				market_order = True
			
					
		if trade.long == True and self.closes[-1:] > self.opens[-1:]:
			action = True
			price = self.closes[-1:]
			if market_order != 'zero':
				market_order = True
			
		elif trade.long == True and self.closes[-1:] < self.opens[-1:]:
			action = True
			price = self.closes[-1:]
			if market_order != 'zero':
				market_order = True
			
		
		return action,price,total,date,total,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		return self.volume_rank
