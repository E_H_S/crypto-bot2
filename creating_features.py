import numpy as np
import pandas as pd 
from pprint import pprint
import datetime as dt
from botVis import BotVisual 
import datetime



class CF(object):
	def __init__(self):
		self.df = pd.DataFrame()
		self.pairs = ["XBTUSD","ETHUSD"]
	

	def create_DataFrame(self,data,trades,OI,Orderbooks,FR,vp,one_m_candles):
		self.data = data
		self.trades = trades
		self.OI = OI
		self.Orderbooks = Orderbooks
		self.FR = FR
		self.vp = vp
		self.XBT_1m_candles = one_m_candles['XBTUSD']
		self.ETH_1m_candles = one_m_candles['ETHUSD']








		for pair in self.pairs:

			print(data.info())
			print(trades.info())
			print(OI[pair].info())
			print(OI[pair].info())
			print(FR[pair].info())


		self.price_action_features()
		self.Trade_Features()
		self.Open_value_features()
		self.Funding_Rate_features()
		self.orderbooks_features_prep()
		self.VP_features_prep()
		self.combine_features()


	def price_action_features(self):

		#Momentum

		print("STARTING PRICE ACTION FEATURES")
		print(datetime.datetime.now())
		self.data['5_M_momentum'] = self.data['closeprice'].diff(1)
		self.data ['15_M_momentum'] = self.data['closeprice'].diff(3)
		self.data ['1_H_momentum'] = self.data['closeprice'].diff(12)
		self.data ['2_H_momentum'] = self.data['closeprice'].diff(24)
		self.data ['4_H_momentum'] = self.data['closeprice'].diff(48)
		self.data ['24_H_momentum'] = self.data['closeprice'].diff(12*24)
		self.data ['1_W_momentum'] = self.data['closeprice'].diff(12*24*7)


		#Price Volitility
		self.data['1_H_volitility'] = self.data['closeprice'].rolling(12).std()
		self.data['2_H_volitility'] = self.data['closeprice'].rolling(12*2).std()
		self.data['4_H_volitility'] = self.data['closeprice'].rolling(12*4).std()
		self.data['8_H_volitility'] = self.data['closeprice'].rolling(12*8).std()
		self.data['1_D_volitility'] = self.data['closeprice'].rolling(12*24).std()
		self.data['2_D_volitility'] = self.data['closeprice'].rolling(12*48).std()

		#Total volume
		self.data['5_M_volume'] = self.data['volume'].rolling(1).sum()
		self.data['15_M_volume'] = self.data['volume'].rolling(3).sum()
		self.data['30_M_volume'] = self.data['volume'].rolling(6).sum()
		self.data['1_H_volume'] = self.data['volume'].rolling(12).sum()
		self.data['2_H_volume'] = self.data['volume'].rolling(24).sum()
		self.data['4_H_volume'] = self.data['volume'].rolling(48).sum()

		

		print(self.data)
		self.data.to_csv('self_data.csv')

		print(datetime.datetime.now())
		print("FINISHED price action features")

	def Trade_Features(self):
		self.trades['DayOfWeek'] = self.trades['entry_date'].dt.dayofweek
		self.trades['HourOfDay'] = self.trades['entry_date'].dt.hour
		self.trades['Hours_To_Next_Funding'] = self.trades['HourOfDay'].apply(lambda x:self.time_till_funding(x))
		self.trades.to_csv('self_trades.csv')
		print(self.trades)

	def Open_value_features(self):

		print("OV FEATURES")
		print(datetime.datetime.now())

		for pair in self.pairs:
			if pair == 'XBTUSD':
				self.OI_XBT = self.OI[pair]
				#self.OI_XBT.columns = ['time_stamp','open_interest','open_value','XBT_price','volume']
			elif pair == 'ETHUSD':
				self.OI_ETH = self.OI[pair]
				#self.OI.columns = ['time_stamp','open_interest','open_value','ETH_price','volume','XBT_price']

	

		self.OI_XBT['OV_1M_cum'] = self.OI_XBT['open_value'].diff(6)
		self.OI_XBT['OV_5M_cum'] = self.OI_XBT['open_value'].diff(6*5)
		self.OI_XBT['OV_15M_cum'] = self.OI_XBT['open_value'].diff(6*15)
		self.OI_XBT['OV_1H_cum'] = self.OI_XBT['open_value'].diff(6*60)
		self.OI_XBT['OV_4H_cum'] = self.OI_XBT['open_value'].diff(6*60*4)
		self.OI_XBT['OV_24H_cum'] = self.OI_XBT['open_value'].diff(6*60*24)

		self.OI_XBT['OI_1M_cum'] = self.OI_XBT['open_interest'].diff(6)
		self.OI_XBT['OI_5M_cum'] = self.OI_XBT['open_interest'].diff(6*5)
		self.OI_XBT['OI_15M_cum'] = self.OI_XBT['open_interest'].diff(6*15)
		self.OI_XBT['OI_1H_cum'] = self.OI_XBT['open_interest'].diff(6*60)
		self.OI_XBT['OI_4H_cum'] = self.OI_XBT['open_interest'].diff(6*60*4)
		self.OI_XBT['OI_24H_cum'] = self.OI_XBT['open_interest'].diff(6*60*24)

		self.OI_XBT['OV_1M_diff'] = abs(self.OI_XBT['open_value'].diff()).rolling(6).sum()
		self.OI_XBT['OV_5M_diff'] = abs(self.OI_XBT['open_value'].diff()).rolling(6*5).sum()
		self.OI_XBT['OV_15M_diff'] = abs(self.OI_XBT['open_value'].diff()).rolling(6*15).sum()
		self.OI_XBT['OV_1H_diff'] = abs(self.OI_XBT['open_value'].diff()).rolling(6*60).sum()
		self.OI_XBT['OV_4H_diff'] = abs(self.OI_XBT['open_value'].diff()).rolling(6*60*4).sum()
		self.OI_XBT['OV_24H_diff'] = abs(self.OI_XBT['open_value'].diff()).rolling(6*60*24).sum()

		self.OI_XBT['OI_1M_diff'] = abs(self.OI_XBT['open_interest'].diff()).rolling(6).sum()
		self.OI_XBT['OI_5M_diff'] = abs(self.OI_XBT['open_interest'].diff()).rolling(6*5).sum()
		self.OI_XBT['OI_15M_diff'] = abs(self.OI_XBT['open_interest'].diff()).rolling(6*15).sum()
		self.OI_XBT['OI_1H_diff'] = abs(self.OI_XBT['open_interest'].diff()).rolling(6*60).sum()
		self.OI_XBT['OI_4H_diff'] = abs(self.OI_XBT['open_interest'].diff()).rolling(6*60*4).sum()
		self.OI_XBT['OI_24H_diff'] = abs(self.OI_XBT['open_interest'].diff()).rolling(6*60*24).sum()


		self.OI_ETH['OV_1M_cum'] = self.OI_ETH['open_value'].diff(6)
		self.OI_ETH['OV_5M_cum'] = self.OI_ETH['open_value'].diff(6*5)
		self.OI_ETH['OV_15M_cum'] = self.OI_ETH['open_value'].diff(6*15)
		self.OI_ETH['OV_1H_cum'] = self.OI_ETH['open_value'].diff(6*60)
		self.OI_ETH['OV_4H_cum'] = self.OI_ETH['open_value'].diff(6*60*4)
		self.OI_ETH['OV_24H_cum'] = self.OI_ETH['open_value'].diff(6*60*24)

		self.OI_ETH['OI_1M_cum'] = self.OI_ETH['open_interest'].diff(6)
		self.OI_ETH['OI_5M_cum'] = self.OI_ETH['open_interest'].diff(6*5)
		self.OI_ETH['OI_15M_cum'] = self.OI_ETH['open_interest'].diff(6*15)
		self.OI_ETH['OI_1H_cum'] = self.OI_ETH['open_interest'].diff(6*60)
		self.OI_ETH['OI_4H_cum'] = self.OI_ETH['open_interest'].diff(6*60*4)
		self.OI_ETH['OI_24H_cum'] = self.OI_ETH['open_interest'].diff(6*60*24)

		self.OI_ETH['OV_1M_diff'] = abs(self.OI_ETH['open_value'].diff()).rolling(6).sum()
		self.OI_ETH['OV_5M_diff'] = abs(self.OI_ETH['open_value'].diff()).rolling(6*5).sum()
		self.OI_ETH['OV_15M_diff'] = abs(self.OI_ETH['open_value'].diff()).rolling(6*15).sum()
		self.OI_ETH['OV_1H_diff'] = abs(self.OI_ETH['open_value'].diff()).rolling(6*60).sum()
		self.OI_ETH['OV_4H_diff'] = abs(self.OI_ETH['open_value'].diff()).rolling(6*60*4).sum()
		self.OI_ETH['OV_24H_diff'] = abs(self.OI_ETH['open_value'].diff()).rolling(6*60*24).sum()

		self.OI_ETH['OI_1M_diff'] = abs(self.OI_ETH['open_interest'].diff()).rolling(6).sum()
		self.OI_ETH['OI_5M_diff'] = abs(self.OI_ETH['open_interest'].diff()).rolling(6*5).sum()
		self.OI_ETH['OI_15M_diff'] = abs(self.OI_ETH['open_interest'].diff()).rolling(6*15).sum()
		self.OI_ETH['OI_1H_diff'] = abs(self.OI_ETH['open_interest'].diff()).rolling(6*60).sum()
		self.OI_ETH['OI_4H_diff'] = abs(self.OI_ETH['open_interest'].diff()).rolling(6*60*4).sum()
		self.OI_ETH['OI_24H_diff'] = abs(self.OI_ETH['open_interest'].diff()).rolling(6*60*24).sum()









		self.OI_XBT.to_csv('self_OI_XBT.csv')
		self.OI_ETH.to_csv('self_OI_ETH.csv')




		print("OV FEATURES FINISHED")
		print(datetime.datetime.now())

	def Funding_Rate_features(self):
		self.FR_XBT = self.FR['XBTUSD']
		self.FR_ETH = self.FR['ETHUSD']
		self.FR_XBT.to_csv('self_FR_XBT.csv')
		self.FR_ETH.to_csv('self_FR_ETH.csv')


	def orderbooks_features_prep(self):

		print("Orderbook FEATURES")
		print(datetime.datetime.now())

		
		self.XBT_book_metrics = {'datetimes_SYD':[],'ratio_1':[],'ratio_2':[],'ratio_3':[],'ratio_4':[],'ratio_5':[],'ratio_6':[]}
		self.ETH_book_metrics = {'datetimes_SYD':[],'ratio_1':[],'ratio_2':[],'ratio_3':[],'ratio_4':[],'ratio_5':[],'ratio_6':[]}
		self.XBT_books = self.Orderbooks['XBTUSD']
		self.ETH_books = self.Orderbooks['ETHUSD']


		for data in self.XBT_books:
			if len(data) > 1995:
				self.XBT_book_metrics['ratio_1'].append((data.iloc[999+25,4]- data.iloc[999-25,4])/min([data.iloc[999+25,4],data.iloc[999-25,4]]))
				self.XBT_book_metrics['ratio_2'].append(((data.iloc[999+50,4] - data.iloc[999+25,4]) - (data.iloc[999-50,4] -data.iloc[999-25,4]))/min([data.iloc[999+50,4] - data.iloc[999+25,4],data.iloc[999-50,4] -data.iloc[999-25,4]]))
				self.XBT_book_metrics['ratio_3'].append(((data.iloc[999+100,4] - data.iloc[999+50,4]) - (data.iloc[999-100,4] -data.iloc[999-50,4]))/min([data.iloc[999+100,4] - data.iloc[999+50,4],data.iloc[999-100,4] -data.iloc[999-50,4]]))
				self.XBT_book_metrics['ratio_4'].append(((data.iloc[999+200,4] - data.iloc[999+100,4]) - (data.iloc[999-200,4] -data.iloc[999-100,4]))/min([data.iloc[999+200,4] - data.iloc[999+100,4],data.iloc[999-200,4] -data.iloc[999-100,4]]))
				self.XBT_book_metrics['ratio_5'].append(((data.iloc[999+400,4] - data.iloc[999+200,4]) - (data.iloc[999-400,4] -data.iloc[999-200,4]))/min([data.iloc[999+400,4] - data.iloc[999+200,4],data.iloc[999-400,4] -data.iloc[999-200,4]]))
				self.XBT_book_metrics['ratio_6'].append(((data.iloc[999+800,4] - data.iloc[999+400,4]) - (data.iloc[999-800,4] -data.iloc[999-400,4]))/min([data.iloc[999+800,4] - data.iloc[999+400,4],data.iloc[999-800,4] -data.iloc[999-400,4]]))
				self.XBT_book_metrics['datetimes_SYD'].append(data['datetimes_SYD'][0])

		for data in self.ETH_books:
			if len(data) > 1995:
				self.ETH_book_metrics['ratio_1'].append((data.iloc[999+25,4]- data.iloc[999-25,4])/min([data.iloc[999+25,4],data.iloc[999-25,4]]))
				self.ETH_book_metrics['ratio_2'].append(((data.iloc[999+50,4] - data.iloc[999+25,4]) - (data.iloc[999-50,4] -data.iloc[999-25,4]))/min([data.iloc[999+50,4] - data.iloc[999+25,4],data.iloc[999-50,4] -data.iloc[999-25,4]]))
				self.ETH_book_metrics['ratio_3'].append(((data.iloc[999+100,4] - data.iloc[999+50,4]) - (data.iloc[999-100,4] -data.iloc[999-50,4]))/min([data.iloc[999+100,4] - data.iloc[999+50,4],data.iloc[999-100,4] -data.iloc[999-50,4]]))
				self.ETH_book_metrics['ratio_4'].append(((data.iloc[999+200,4] - data.iloc[999+100,4]) - (data.iloc[999-200,4] -data.iloc[999-100,4]))/min([data.iloc[999+200,4] - data.iloc[999+100,4],data.iloc[999-200,4] -data.iloc[999-100,4]]))
				self.ETH_book_metrics['ratio_5'].append(((data.iloc[999+400,4] - data.iloc[999+200,4]) - (data.iloc[999-400,4] -data.iloc[999-200,4]))/min([data.iloc[999+400,4] - data.iloc[999+200,4],data.iloc[999-400,4] -data.iloc[999-200,4]]))
				self.ETH_book_metrics['ratio_6'].append(((data.iloc[999+800,4] - data.iloc[999+400,4]) - (data.iloc[999-800,4] -data.iloc[999-400,4]))/min([data.iloc[999+800,4] - data.iloc[999+400,4],data.iloc[999-800,4] -data.iloc[999-400,4]]))
				self.ETH_book_metrics['datetimes_SYD'].append(data['datetimes_SYD'][0])


		#Funding Rate ['fundingRate','dailyFundingRate','time_stamp']

		#self.FR['Minutes_To_Next_Funding'] = self.FR['UTC_datetimes']
		self.XBT_book_metrics = pd.DataFrame(self.XBT_book_metrics)
		self.ETH_book_metrics = pd.DataFrame(self.ETH_book_metrics)

		self.XBT_book_metrics.to_csv('self_XBT_book_metrics.csv')
		self.ETH_book_metrics.to_csv('self_ETH_book_metrics.csv')

		print("Orderbook FEATURES FINISHED!")
		print(datetime.datetime.now())
		#price_retracement
	def VP_features_prep(self):

		self.XBT_vps = {'datetimes_SYD':[],'current_percentile': []}
		self.ETH_vps = {'datetimes_SYD':[],'current_percentile': []}


		print(len(self.XBT_1m_candles))
		print(len(self.ETH_1m_candles))

		data = self.XBT_1m_candles
		for x in range(len(data)):
			if x < 1440:
				candles = data[0:x]
			else:
				candles = data[x-1440:x]

			vp = candles[['close','volume']].groupby(['close']).sum()
			vp = vp.sort_values(by = 'close',ascending = False)
			vp['cum'] = vp['volume'].cumsum()
			vp['percent'] = vp['cum']/vp['volume'].sum()
			#print(vp)
			#bprint(candles[-1:]['close'])
			if len(vp)>0 and len(candles)>0:
				self.XBT_vps['current_percentile'].append(vp[vp.index == candles[-1:]['close'].values[0]]['percent'].values[0])
				self.XBT_vps['datetimes_SYD'].append(candles[-1:]['datetimes_SYD'])
	    


		data = self.ETH_1m_candles
		for x in range(len(data)):
			if x < 1440:
				candles = data[0:x]
			else:
				candles = data[x-1440:x]

			vp = candles[['close','volume']].groupby(['close']).sum()
			vp = vp.sort_values(by = 'close',ascending = False)
			vp['cum'] = vp['volume'].cumsum()
			vp['percent'] = vp['cum']/vp['volume'].sum()

			if len(vp)>0 and len(candles)>0:
				self.ETH_vps['current_percentile'].append(vp[vp.index == candles[-1:]['close'].values[0]]['percent'].values[0])
				self.ETH_vps['datetimes_SYD'].append(candles[-1:]['datetimes_SYD'])


		self.XBT_vps = pd.DataFrame(self.XBT_vps)
		self.ETH_vps = pd.DataFrame(self.ETH_vps)

		self.XBT_vps.to_csv('self_XBT_vps.csv')
		self.ETH_vps.to_csv('self_ETH_vps.csv')

		print(self.XBT_vps)
		print(self.ETH_vps)


	def combine_features(self):
		pass

		#self.df = pd.merge(self.trades,self.data,left_on = 'entry_date', right_index = True,how = 'left')
		#self.df = pd.merge(self.df,self.)
		#print(self.df)


	def time_till_funding(self,hour):

		ret = 8 - np.mod(hour+4,8)
		return ret







		#print(target)



