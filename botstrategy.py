from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 


class NoobyStrat(object):
	def __init__(self,starting_bull_volume = None,starting_bear_volume = None):
		
		#Initial perameters of this given strategy
		self.numSimulTrades = 1
		self.indicators = BotIndicators()
		###############
		#To feed information up the chain the data that needs to be atributes
		#the data requirements for BotStratLog are.
		# Initiate trading based vectors
		self.tradeStatus = np.array([])	#TradeStatus is of form "OPENED","IN POSITION",("CLOSED" OR "STOPLOSSED","WAITING"

		self.BullCommisions = np.array([])	#Number of tokens taken by the exchange during a buy order

		self.BearCommisions = np.array([])	#Number of tokens taken by the exchange during a sell order

		self.BullTokens = np.array([float(starting_bull_volume)])		# Number of Bull tokens at the datetime after the Trade and Comission
		

		self.BearTokens = np.array([float(starting_bear_volume)])		# Number of Bear tokens at the datetime after the Trade and Comission
		

		#Also need to calculate this sucker
		self.mavs  = np.array([])

		self.openTrades = [] 		#  Initiate a vector of trade objects
		self.trades = []     		#  self.trades's elements are trade objects with the attributes  self.Entry_price, self.Entry_date self.Entry_volume, self.Status, self.Exit_price, self.Exit_date, self.Exit_volume

		
	def tick(self,candlestick,prices):#candlestick is a singular candlestick, prices is a list of all candlestick price averages
		self.tradeStatus = np.append(self.tradeStatus,["WAITING"])# Assume status for this timestamp is waiting unless proven not to be later.
		


		self.mavs = np.append(self.mavs,[self.indicators.movingAverage(prices,10)]) # Creating the self.mavs list for analysis and visualsation later.
		if len(self.mavs) > 1: #only appends on a tick the 2nd tick onwards to avoid double ups with the first instatiate uses len(mavs) to represent tick index
			
			self.BullTokens = np.append(self.BullTokens,self.BullTokens[-1:])
			self.BearTokens = np.append(self.BearTokens,self.BearTokens[-1:])
							 #assume Tokens remain the same as last iteration unless trade happens in which case change tokens at that point 
		
		self.BullCommisions = np.append(self.BullCommisions,[float(0)])
		self.BearCommisions = np.append(self.BearCommisions,[float(0)])	#Asume no comission fee for this cycle unless unless trade happens in which case we will add comission the end at that point

		
		
		self.evaluatePositions(datetime.datetime.fromtimestamp(candlestick.date),prices,candlestick) #evaluate the position
		
		self.updateOpenTrades(candlestick,datetime.datetime.fromtimestamp(candlestick.date)) #Evaluate already open positions
		


	def evaluatePositions(self,date,prices,candlestick):
		
		####################################################################################################################################################################################
		# The Strategy Lines BELOW
		####################################################################################################################################################################################
		# Searches the trades vector for 
		openTrades = []
		for trade in self.trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade) #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1:] = "IN POSITION" 


		

		if (len(openTrades) < self.numSimulTrades): #Checks if the algorithm is allowed to take on another position
			if (candlestick.priceAverage < self.indicators.movingAverage(prices,10)): # When the candlestick price Average is less than the 10 point moving average, Initiate a open position
				self.OpenPosition(candlestick,date) #OPEN POSITION!

	
				
		for trade in openTrades:
			if (candlestick.priceAverage > self.indicators.movingAverage(prices,10)): #checks to see if the position now meets closing conditions
				self.ClosePosition(date,candlestick,trade)#SELL POSITION!
		
		####################################################################################################################################################################################
		# The Strategy Lines ABOVE
		####################################################################################################################################################################################


	def updateOpenTrades(self,candlestick,date):
		for trade in self.trades:
			if (trade.status == "OPEN"):
				trade.tick(candlestick.priceAverage,date) #Tick the position to see if it has hit the stoploss
				if trade.tick(candlestick.priceAverage,date) == True: #check to see if it hit the stop loss and if so, close the position as usual
					
					self.BullTokens[-1:] = 0
					self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.998)*candlestick.priceAverage #Calculates the number of purchased Bear coins with commision taken into consideration
					self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.002	#calculate bear Commision and replace last element in the vector with calculated comission
					self.tradeStatus[-1:] = "STOP LOSSED"
					trade.close(candlestick.priceAverage,date,self.BearTokens)



	def OpenPosition (self,candlestick,date):
		self.BearTokens[-1:] = 0
		self.BullTokens[-1:] = (float(self.BearTokens[-2:-1])*0.998)/candlestick.priceAverage #Calculates the number of purchased Bull coins with commision taken into consideration
		self.BullCommisions[-1:] = float(self.BearTokens[-2:-1])*0.002	#calculate bull Commision and replace last element in the vector with calculated comission
		self.tradeStatus[-1:] = "OPEN"
		self.trades.append(BotTrade(candlestick.priceAverage,date,self.BullTokens[-1:],stopLoss=.001)) # Append a BotTrade Object to our trades list

	def ClosePosition (self,date,candlestick,trade):
		self.BullTokens[-1:] = 0
		self.BearTokens[-1:] = (float(self.BullTokens[-2:-1])*0.998)*candlestick.priceAverage #Calculates the number of purchased Bear coins with commision taken into consideration
		self.BearCommisions[-1:] = float(self.BullTokens[-2:-1])*0.002	#calculate bear Commision and replace last element in the vector with calculated comission
		self.tradeStatus[-1:] = "CLOSED"
		trade.close(candlestick.priceAverage,date,self.BearTokens[-1:])















