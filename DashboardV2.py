import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import bitmex
import datetime


client = bitmex.bitmex(test=False,api_key ='LTt_90UVZJaUYUdhesjTriFl',api_secret='0UWDNbs-04gh1Llj9X8OYHX8zlt1psuCs9WjHH7bQz2evUSv')


app = dash.Dash()

app.layout = html.Div([
    "Look back period (minutes)",
    dcc.Input(
        id='number-in',
        value=18,
        style={'fontSize':18}),
    html.Button(
        id='submit',
        n_clicks=0,
        children='Submit',
        style={'fontSize':18}),

    dcc.Checklist(
    id = 'indicators',
    options=[
        {'label': 'VWAP', 'value': 'VWAP'},
        {'label': 'Open Value', 'value': 'openValue'},
        {'label': 'Open Interest', 'value': 'openInterest'}
    ],
    values=['MTL', 'SF']
    ),



    dcc.Graph(id='live-update-graph'),
    dcc.Graph(id='volume-graph'),

    dcc.Interval(
        id='interval-component',
        interval=10000, # 6000 milliseconds = 6 seconds
        n_intervals=0
    )])


@app.callback(Output('volume-graph','figure'),
              [Input('interval-component', 'n_intervals')
              ])
def update_vol_graph(n):
    
    data,candles = get_data()
    
    figure = {'data':[
                    go.Bar(
                        x = candles['timestamp'],
                        y = candles['volume'], 
                    )
],
                    'layout':go.Layout(
                            title='volume',
                            
)}
    return figure




@app.callback(Output('live-update-graph','figure'),
              [Input('interval-component', 'n_intervals')
              #,Input('number-in', 'value')
              ])
def update_graph(n):
    
    data,candles = get_data()




    figure = {'data':[
                    go.Candlestick(
                        x = candles['timestamp'],
                        open = candles['open'],
                        high = candles['high'],
                        low = candles['low'],
                        close = candles['close']
                        
                        
                    ),
                    go.Scatter(
                        x = data['Date'],
                        y = data['openValue'],
                        mode = 'lines',
                        yaxis='y2'
                     )
],
                    'layout':go.Layout(
                            title='Bitmex Price vs Open value',
                            yaxis=dict(
                            title='XBT Price'
                    ),
                            yaxis2=dict(
                            title='Open value(BTC)',
                            titlefont=dict(
                            color='rgb(148, 103, 189)'
                    ),
                            tickfont=dict(
                            color='rgb(148, 103, 189)'
                    ),
                            overlaying='y',
                            side='right'
    )
)}
    return figure
def get_data():
    data = pd.read_csv('XBT_instrument_data9.csv')
    data.columns = ['Date','Open Interest','openValue','Last Price','volume']
    data['openValue'] = data['openValue']/100000000
    start = data['Date'][0]
    f = "%Y-%m-%dT%H:%M:%S.%fZ"
    start = datetime.datetime.strptime(start,f)
    #start = datetime.datetime.utcnow() + datetime.timedelta(minutes=-60)
    end = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)

   
    candles = client.Trade.Trade_getBucketed(symbol='XBTUSD',binSize = '1m', startTime=start,endTime=end,count = 500,partial = True).result()
    
    candles = pd.DataFrame(candles[0])

    return data,candles
if __name__ == '__main__':
    app.run_server()