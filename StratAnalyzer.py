
import numpy as np
import pandas as pd 
from pprint import pprint
import datetime as dt
from botVis import BotVisual 
import datetime



class Analyzer(object):
	def __init__(self,BotTradingLog,pair,starttime=None,endtime=None):
		self.starttime = starttime
		self.endtime = endtime
		self.BotTradingLog = BotTradingLog
		self.pair = pair



		

		# turn all model atributes into a single dictionary to make a dataframe out of
		self.datadict = {'date':BotTradingLog.dates,"openprice":BotTradingLog.openprices,
		"closeprice":BotTradingLog.closeprices,"averageprice":BotTradingLog.AveragePrices,
		"high":BotTradingLog.highs,"low":BotTradingLog.lows,"volume":BotTradingLog.volumes,
		"quotevolumes":BotTradingLog.quoteVolumes,"starttime":BotTradingLog.startTime,
		"tradestatus":BotTradingLog.botcontroller.tradeStatus,"bullcommision":BotTradingLog.botcontroller.BullCommisions,
		"bearcommision":BotTradingLog.botcontroller.BearCommisions,"bullt":BotTradingLog.botcontroller.BullTokens,
		"beart":BotTradingLog.botcontroller.BearTokens}


		# ,"moving_average_price":BotTradingLog.strategy.mavs,'mavs2':BotTradingLog.strategy.mavs2,"RSI":BotTradingLog.strategy.RSI
		# ,'Momentum_F':BotTradingLog.strategy.momentum_F,'Momentum_S':BotTradingLog.strategy.momentum_S}

		#"MACDD":BotTradingLog.strategy.macdd,"MACDB":BotTradingLog.strategy.macdb,"MACDR":BotTradingLog.strategy.macdr,"RSI":BotTradingLog.strategy.rsi,"mavs_delta":BotTradingLog.strategy.mavs_delta
#optional indicators
		#"ma_int":BotTradingLog.strategy.ma_int
		#,'Momentum':BotTradingLog.strategy.momentum}
		
		#optional indicators
		
		#self.target_sells = BotTradingLog.strategy.target_sells
		#self.close_sells = BotTradingLog.strategy.close_sells
		



		#print(self.datadict)
		self.df = pd.DataFrame(self.datadict)# turn off of the stategy log information into a neat pandas dataframe for analysis
		#print(self.df)
		
		#dateconv = np.vectorize(dt.datetime.fromtimestamp)
		
		self.df.index = self.df["date"] #converting the index to datetimes time series

		#print(self.df["bullt"])
		#print(self.df["closeprice"])
		#print(self.df["beart"])
		self.df["est_cur_val"] = self.df["bullt"]*self.df["closeprice"] +self.df["beart"]
		self.df["hold_return"] = self.df["closeprice"]/self.df["closeprice"][0]

		
		self.risk = self.df["est_cur_val"].std()


		#self.df['est_cur_val_shift1'] = self.df["est_cur_val"].diff()
		
		self.df['adjusted_returns'] = (self.df["est_cur_val"].diff()/self.df["est_cur_val"])
		self.mean_returns = self.df[self.df['adjusted_returns'] != 0]['adjusted_returns'].mean()*100
		self.risk2 = self.df[self.df['adjusted_returns'] != 0]['adjusted_returns'].std()

		self.Ulcer_risk2 = self.Ulcer_std(self.df['adjusted_returns'])

		#print(self.df['adjusted_returns'])
		print(self.mean_returns,'mean_returns')
		print(self.risk2,'Risk')
		print(self.Ulcer_risk2)
		



		#self.index_risk = self.df['closeprice'].std()
		self.df['closes_percentage_diff'] = (self.df['closeprice'].diff()/self.df['closeprice'])
		self.index_risk = self.df['closes_percentage_diff'].std()
		self.index_mean = self.df['closes_percentage_diff'].mean() * 100





		self.join_df = self.df[['closeprice','tradestatus','est_cur_val',"hold_return"]]
		self.new_cols = []
		for col in list(self.join_df.columns.values):
			self.new_cols.append(self.pair + '_' + col)
		self.join_df.columns = self.new_cols
		#print(list(self.join_df.columns.values))
		print(self.df)

		#Trade statistics 
		self.trades = BotTradingLog.botcontroller.trades
		self.status = np.array([])
		self.entryPrice = np.array([])
		self.exitPrice = np.array([])
		self.entryVolume = np.array([])
		self.exitVolume = np.array([])
		self.entryDate = np.array([])
		self.exitDate = np.array([])
		self.stoplossed = np.array([])
		self.stopLoss = np.array([])
		self.market_exit = np.array([])
		self.long = np.array([])
		self.entry_depth =  np.array([])


		self.take_profit_percentage = np.array([])
		self.trade_length = np.array([])
		self.highest_high = np.array([])
		self.lowest_low = np.array([])
		self.entry_ATR = np.array([])
		self.time_on_side = np.array([])
		self.time_off_side = np.array([])
		self.trigger_date  = np.array([])
		self.trade_sharp_ratio = np.array([])
		self.accumulation1 = np.array([])
		self.accumulation2 = np.array([])
		self.accumulation3 = np.array([])
		self.liquidity = np.array([])




		for trades in self.trades:
			if trades.status == "CLOSED" or trades.status =="STOP LOSSED" or  trades.status == "TARGET HIT":
				#print(trades.entryPrice,trades.exitPrice)
				self.status = np.append(self.status,trades.status)
				self.entryPrice = np.append(self.entryPrice,trades.entryPrice)
				self.exitPrice = np.append(self.exitPrice,trades.exitPrice)
				#self.entryVolume = np.append(self.entryVolume,trades.entryVolume)
				#self.exitVolume = np.append(self.exitVolume,trades.exitVolume)
				self.entryDate = np.append(self.entryDate,trades.entryDate)
				self.exitDate = np.append(self.exitDate,trades.exitDate)
				self.stoplossed = np.append(self.stoplossed,trades.stoplossed)
				self.stopLoss = np.append(self.stopLoss,trades.stopLoss)
				self.long = np.append(self.long,trades.long)
				self.entry_depth = np.append(self.entry_depth,trades.orderbook_depth)


				self.take_profit_percentage = np.append(self.take_profit_percentage,trades.take_profit_percentage)
				self.trade_length = np.append(self.trade_length,trades.trade_length)
				self.highest_high = np.append(self.highest_high,trades.highest_high)
				self.lowest_low = np.append(self.lowest_low,trades.lowest_low)
				self.entry_ATR = np.append(self.entry_ATR,trades.entry_ATR)
				self.time_on_side = np.append(self.time_on_side,trades.time_on_side)
				self.time_off_side = np.append(self.time_off_side,trades.time_off_side)
				self.trigger_date = np.append(self.trigger_date,trades.trigger_date)
				self.trade_sharp_ratio = np.append(self.trade_sharp_ratio,trades.sharp_ratio)
				self.accumulation1 = np.append(self.accumulation1,trades.accumulation1)
				self.accumulation2 = np.append(self.accumulation2,trades.accumulation2)
				self.accumulation3 = np.append(self.accumulation3,trades.accumulation3)
				self.liquidity = np.append(self.liquidity,trades.liquidity)







				


		
		



		#convert trades info into a dictionary for pandas dataframe format
		self.trades_dict = {}
		self.trades_dict['status'] = self.status
		self.trades_dict['entry_price'] = self.entryPrice
		self.trades_dict['exit_price'] = self.exitPrice
		self.trades_dict['entryDate'] = self.entryDate
		self.trades_dict['exitDate'] = self.exitDate
		self.trades_dict['stoplossed'] = self.stoplossed
		self.trades_dict['stopLoss'] = self.stopLoss
		self.trades_dict['long'] = self.long
		self.trades_dict['entry_depth'] = self.entry_depth
		self.trades_dict['take_profit_percentage'] = self.take_profit_percentage
		self.trades_dict['trade_length'] = self.trade_length
		self.trades_dict['highest_high'] = self.highest_high
		self.trades_dict['lowest_low'] = self.lowest_low
		self.trades_dict['entry_ATR'] = self.entry_ATR
		self.trades_dict['time_on_side'] = self.time_on_side
		self.trades_dict['time_off_side'] = self.time_off_side
		self.trades_dict['trigger_date'] = self.trigger_date
		self.trades_dict['sharp_ratio'] = self.trade_sharp_ratio
		self.trades_dict['accumulation1'] = self.accumulation1
		self.trades_dict['accumulation2'] = self.accumulation2
		self.trades_dict['accumulation3'] = self.accumulation3
		self.trades_dict['liquidity'] = self.liquidity



		#print(self.trades_dict)
		self.df_trades = pd.DataFrame(self.trades_dict)

		#self.df_trades['long_profit'] = (self.df_trades[self.df_trades['long'] == 1]['exit_price'] - self.df_trades[self.df_trades['long'] == 1]['entry_price'])/self.df_trades[self.df_trades['long'] == 1]['entry_price']
		#self.df_trades[(self.df_trades['long'] == 1 & self.df_trades['stop_lossed'] == 1)]['long_profit'] 
		#self.df_trades['short_profit'] = (self.df_trades[self.df_trades['long'] == 0]['entry_price'] - self.df_trades[self.df_trades['long'] == 0]['exit_price'])/self.df_trades[self.df_trades['long'] == 10['entry_price']





		self.df_trades.to_csv('VPOC_TRADES_July_Sep.csv')
		print(self.df_trades)
		print(self.df_trades['entryDate'])
		



		#self.df_trades['price difference'] = (self.df_trades['exit_price']-(self.df_trades['entry_price']*1.0044))/(self.df_trades['entry_price'])
		#self.df_trades['price difference'][self.df_trades['long']== False] = ((self.df_trades['entry_price']/1.0044) - self.df_trades['exit_price'])/(self.df_trades['entry_price'])
		self.df_trades['price difference'] = (self.df_trades['exit_price']/(self.df_trades['entry_price']))
		self.df_trades['price difference'][self.df_trades['long']== False] = ((self.df_trades['entry_price'] - self.df_trades['exit_price'])/self.df_trades['entry_price'])+1
	

		#calculating summary statistics
		self.market_difference = round(float(self.df['closeprice'][-1:]/self.df['closeprice'][0]),4)
		self.final_return = round(float((self.df['est_cur_val'][-1:]/ self.df['est_cur_val'][0])*100),2)
		self.start_value = float(self.df['est_cur_val'][0])
		self.end_value = float(self.df['est_cur_val'][-1:])
		self.total_opens = sum(self.df['tradestatus']=="OPEN" )
		self.total_closed = sum(self.df['tradestatus']=="CLOSED")
		self.total_stoplossed = sum(self.df['tradestatus']=="STOP LOSSED")

		#self.total_commisiion = sum(sum(abs(self.df['bullcommision'])) + sum(abs(self.df['bearcommision']))*self.df['averageprice'][-1:])
		#print(type(self.df['bullcommision'].sum()),type((self.df['bearcommision']*self.df['closeprice'][-1:]).sum()))
		#print(self.df['bullcommision'].sum())
		self.total_commisiion = self.df['bullcommision'].sum().item() + (self.df['bearcommision'].sum()*self.df['closeprice'][-1:].item())
		#print(self.df['bullcommision'])
		#print(self.df['bearcommision'])


		self.num_positive_trades = sum(self.df_trades['price difference']>1)
		self.num_negative_trades = sum(self.df_trades['price difference']<1)
		self.average_positve_trade = round(pd.DataFrame.mean(self.df_trades['price difference'][self.df_trades['price difference']>1]),4)
		self.average_negative_trade = round(pd.DataFrame.mean(self.df_trades['price difference'][self.df_trades['price difference']<1]),4)

		if self.risk2 != 0:
			self.sharp_ratio = round(self.mean_returns/self.risk2,3)
		if self.Ulcer_risk2 != 0:
			self.ulcer_index = round(self.mean_returns/self.Ulcer_risk2,3)


		self.edge_before_commision = self.df_trades['price difference'].mean()
		#print(self.df_trades[['price difference']].values.flatten())
		#Trade_percentages = self.df_trades[['price difference']].values.flatten()
		#print(Trade_percentages)
		#print(type(self.df_trades['price difference'].values),self.df_trades['price difference'].values.shape)


		#self.edge_p_2_5, self.edge_p_50, self.edge_p_97_5 = self.Bootstrap(self.df_trades[['price difference']].values.flatten())




		if self.total_stoplossed > 0:
			self.close_pct = self.total_closed/(self.total_closed+self.total_stoplossed)
			self.stoplossed_pct = self.total_stoplossed/(self.total_closed+self.total_stoplossed)



		#set up axis
		#self.ax1 = plt.subplot2grid((7,1),(0,0),rowspan=4,colspan=1) #price
		# self.ax2 = plt.subplot2grid((10,4),(8,0),rowspan=2,colspan=3) #RSI
		# #self.ax3 = plt.subplot2grid((10,4),(9,0),rowspan=1,colspan=3) #macD
		# self.ax4 = plt.subplot2grid((10,4),(0,3),rowspan=5,colspan=1) #value
		# self.ax5 = plt.subplot2grid((10,4),(5,3),rowspan=5,colspan=1) #trade histogram


	def full_analysis(self):
		self.trade_charts()
		self.Summary_stats()
		self.chart_value()
		self.chart_indicators()
		csv_name = str(self.BotTradingLog.pair)+"_"+str(self.BotTradingLog.period)+'_'+'(' +str(self.starttime)+')'+"---"+'('+str(self.endtime)+')'+'.csv'
		csv_name_trades = str('Trades_'+self.BotTradingLog.pair)+"_"+str(self.BotTradingLog.period)+'_'+'(' +str(self.starttime)+')'+"---"+'('+str(self.endtime)+')'+'.csv'
		#self.df.to_csv(csv_name)

		csv_name_trades = 'Wicks11-' + str(self.starttime)+'.csv'
		self.df_trades.to_csv(csv_name_trades)
		if len(self.df_trades['price difference'].values) > 0 :
			self.Bootstrap_analysis()


		return self.df,self.df_trades
		#print(self.df[['date','est_cur_val']])
		#print(self.df_trades[['entry_date','entry_price','exit_date','exit_price','long','price difference']])
		#print(self.df)
		#print(self.df_trades)
		
		
		
		
	def Summary_stats(self):

		print("SUUMMARY STASTICS OF STRATEGY PERFORMANCE ON "+ str(self.pair))
		print("overall performance of the strategy over the time period was :"+str(self.final_return)+"%")
		if self.risk != 0:
			#print("Sharp Ratio " + str(round(self.final_return/self.risk,2)))
			print("Sharp Ratio " + str(self.sharp_ratio))
			print("Ulcer Index " + str(self.ulcer_index))


		print("Hoddle Sharp Ratio " + str(round(self.index_mean/self.index_risk,3)))

		print("The market performance for over this time was :" +str(self.market_difference)+"X")
		print("starting value : " + str(self.start_value)+ " ending value : "+str(self.end_value))
		print("\n")
		print("Number of opened positions : " + str(self.total_opens))
		print("number of positive positions taken : " +str(self.num_positive_trades)+" With average return : "+str(self.average_positve_trade)+"X")
		print("number of negative positions taken : " +str(self.num_negative_trades)+" With average return : "+str(self.average_negative_trade)+"X")
		print("Total edge before comission is : "+ str(self.edge_before_commision))
		print("\n")
		#print("Total edge p values : p(2.5) = " + str(self.edge_p_2_5)+' p(50) = '+ str(self.edge_p_50)+' p(97.5) = '+str(self.edge_p_97_5))
		#print("Number of sells that reach target : " +str(self.target_sells))
		#print("Number of closed sells : " +str(self.close_sells))
		print("Total comission paid in bulltokens: " + str(self.total_commisiion) + "With percentage " + str(round(self.total_commisiion/self.df['beart'][0]*100,2)) + '%')
		#print(self.df_trades)
		
		


		if self.total_stoplossed > 0:
			print("Number of succesful exits : " + str(self.total_closed)+ str(" with percentage : ") + str(round(self.close_pct,2))+ "%")
			print("Number of stopped lossed exits : " + str(self.total_stoplossed)+ str(" with percentage : ") + str(round(self.stoplossed_pct,2))+"%")
			print("Number of succesful exits : " + str(self.total_closed)+ str(" with percentage : ") + str(round(self.close_pct,2))+ "%")
			
		
		#print(self.df.head())
		#print(self.df.tail())
		#print(self.df_trades[self.df_trades['price difference']>0])
		#print(self.df_trades[self.df_trades['price difference']<0])
	
	def chart_indicators(self):#ALL plottings
		pass

	def chart_value(self): #value of account 

		pass


	def trade_charts(self):
		
		pass

	def Bootstrap_analysis(self):
		self.edge_p_2_5, self.edge_p_50, self.edge_p_97_5 = self.Bootstrap(self.df_trades[['price difference']].values.flatten())
		print("Total edge p values : p(2.5) = " + str(self.edge_p_2_5)+' p(50) = '+ str(self.edge_p_50)+' p(97.5) = '+str(self.edge_p_97_5))

	
	def Bootstrap(self,og_samples):
		#print(type(og_samples))
		#print(og_samples)
		
		test_stat_vector = []
		for x in range(1000):
			#print("check1")
			#print(OG_sample1,OG_sample1.shape)
			BS_sample = np.random.choice(og_samples,size = og_samples.shape,replace = True)
			#print("check2")
			
			test_stat_vector.append(BS_sample.mean())

		test_stat_vector = np.array(test_stat_vector)

		p_2_5 = np.percentile(test_stat_vector,2.5)
		p_50 = np.percentile(test_stat_vector,50)
		p_97_5 = np.percentile(test_stat_vector,97.5)

		return p_2_5,p_50,p_97_5

	def Ulcer_std(self,returns):
		#print(returns)

		mean = returns.mean()
		#print('mean ',mean)
		#print('\n')
		#diff = returns.diff()/returns
		total_residual = 0
		for residual in returns:
			if residual < 0:
				#print(residual)
				#print(mean)
				total_residual = total_residual + ((residual - mean)*(residual - mean))
				#print(total_residual)
				#print('\n')
		
		#print(total_residual)
		ulcer_std = (total_residual/len(returns))**0.5


		return ulcer_std  







		
		


	







		

	