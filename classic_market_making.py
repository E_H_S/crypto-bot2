from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class classic_market_maker(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		#self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False,momentum = False,VWAP = False)
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = True,anchored = True,open_value_candles = False ,momentum = False)
		
		self.ATR_p = 288
		if p1 != None:
			self.ATR_p = p1
		#Inititalize inticator vectors
		
		self.ATR = []
		self.ATR2 = []
		self.ATR3 = []

		self.ob_index = None


		


		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		
		self.b_1_ready = True
		self.b_2_ready = True
		self.b_3_ready = True


		self.s_1_ready = True
		self.s_2_ready = True
		self.s_3_ready = True


		self.VPOC = []



		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None



		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0


		self.middle = None


	
		self.skew = 0
		self.increment = 0.003

		self.rolling_period = 288
		
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = np.array([])

		self.ATR_switches = np.array([])

		self.numSimulTrades = 99999999
		self.margin = True

		self.count10 = 0
		self.switch10 = False

		self.candle_count = 0

		self.ATR_rolling_period = 288

		self.VWAP = []

		self.account_size = []
		self.latest_range = []
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.t1 = datetime.datetime.utcnow()

		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])

		self.t2 = datetime.datetime.utcnow()


		if np.mod(self.dates[-1].minute,5) == 0:
			self.tick_indicators(trades)
	

	def tick_indicators(self,trades):
		#updating Indicators
		self.candle_count += 1
		

		if self.candle_count > 1: 
			self.buy_price1 = self.closes[-2]/1.005
			self.buy_price2 = self.closes[-2]/1.01
			self.buy_price3 = self.closes[-2]/1.015

			self.sell_price1 = self.closes[-2]*1.005
			self.sell_price2 = self.closes[-2]*1.01
			self.sell_price3 = self.closes[-2]*1.015
		else:
			self.buy_price1 = None
			self.buy_price2 = None
			self.buy_price3 = None

			self.sell_price1 = None
			self.sell_price2 = None
			self.sell_price3 = None
			

		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)
		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)
		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)


		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3)]
		#print(self.buy_line1)
		#time.sleep(1)

		
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		#date = None
		total = 50
		stoploss = None
		#market_order = 'zero'
		market_order = False

		if bp == self.buy_price1 and self.b_1_ready == False:
			bp = None
		elif bp == self.buy_price2 and self.b_2_ready == False:
			bp = None
		elif bp == self.buy_price3 and self.b_3_ready == False:
			bp = None
		
		if sp == self.sell_price1 and self.s_1_ready == False:
			sp = None
		elif sp == self.sell_price2 and self.s_2_ready == False:
			sp = None
		elif sp == self.sell_price3 and self.s_3_ready == False:
			sp = None


		#if 1 == 1:
		if len(self.dates) > 2:
			if sp != None:
				if self.highs[-1] > sp and self.closes[-2] < sp:
					action = True
					Long = False
					price = sp
					#stoploss = price*1.003
					
			if bp != None:
				if self.lows[-1] < bp and self.closes[-2] > bp:
					action = True
					Long = True
					price = bp
					#stoploss = self.lows[-288*2:].min()/1.02


		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):

		action = False
		price = None
		date = self.dates[-1:]
		total = 50
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#if 1 == 1:
		if trade.long == True:
			if self.highs[-1] > trade.entryPrice*(1+self.increment):
				action = True
				price = trade.entryPrice*(1+self.increment)
				
		elif trade.long == False:
			if self.lows[-1] < trade.entryPrice/(1+self.increment):
				action = True
				price = trade.entryPrice/(1+self.increment)

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		#momentum_indicator = self.momentum['momentum_final']
		#print(momentum_indicator)
		return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3)
		#return (self.VWAP,self.buy_line1,self.sell_line1,self.buy_line4,self.sell_line4, self.buy_line7,self.sell_line7)