from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class Range(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = False
		
		self.indicators = BotIndicators(orderbook= False,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime)
		self.indicators.Open_value_rolling_corr(60*6)

		self.indicators.open_value_mvavg(int(6*60*6))

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])

		self.ob_index = None
		self.last_orderbook = pd.DataFrame()
		self.open_value = np.array([])

		self.last_value = np.array([])

		
		

		self.numSimulTrades = 1
		self.margin = True
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}

		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		
		#updating Indicators


		self.open_value = self.indicators.get_open_value(self.dates[-1:])
		self.open_value_corr = self.indicators.get_ov_corr(self.dates[-1:])
		# print(self.dates[-1:])
		# # print(self.open_value[-1:])
		# print(self.open_value_corr.index[-1:])

		#time.sleep(10)



			
	def evaluate_Open(self,bp = None,sp = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 1000
		stoploss = None
		market_order = 'zero'
		#print(date)
		#market_order = False
		# if self.dead_date_start > date.item().replace(tzinfo=None):
		# 	if date.item().replace(tzinfo=None) < self.dead_date_end == False:
				
		


		#if (self.dead_date_start < date.item().replace(tzinfo=None) and date.item().replace(tzinfo=None) < self.dead_date_end) == False:



		if len(self.dates) > 6*15 +5:
			# print(self.open_value_corr['OV_CORR'][-2:-1][0])
			# print(self.open_value_corr['OV_CORR'][-1:][0])
			# print(self.open_value['moving_average'][-1:])
			
			if self.open_value_corr['OV_CORR'][-6:-5][0] < 0.25  and self.open_value_corr['OV_CORR'][-1:][0] > 0.25 and (self.open_value['moving_average'][-1:][0] - self.open_value['moving_average'][-2:-1][0]) < 0:
				action = True
				Long = False
				price = self.closes[-1:]
			elif self.open_value_corr['OV_CORR'][-6:-5][0] > -0.25  and self.open_value_corr['OV_CORR'][-1:][0] < -0.25 and (self.open_value['moving_average'][-1:][0] - self.open_value['moving_average'][-2:-1][0]) < 0:
				action = True
				Long = True
				price = self.closes[-1:]
				
						
			if self.open_value_corr['OV_CORR'][-6:-5][0] < 0.25  and self.open_value_corr['OV_CORR'][-1:][0] > 0.25 and (self.open_value['moving_average'][-1:][0] - self.open_value['moving_average'][-2:-1][0]) > 0:
				action = True
				Long = True
				price = self.closes[-1:]
			elif self.open_value_corr['OV_CORR'][-6:-5][0] > -0.25  and self.open_value_corr['OV_CORR'][-1:][0] < -0.25 and (self.open_value['moving_average'][-1:][0] - self.open_value['moving_average'][-2:-1][0]) > 0:
				action = True
				Long = False
				price = self.closes[-1:]
				#stoploss = price/1.015
		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		market_order = 'zero'
		#market_order = False
		
		



		if self.open_value_corr['OV_CORR'][-6:-5][0] > 0.25  and self.open_value_corr['OV_CORR'][-1:][0] < 0.25 and trade.long == True: 
			action = True
			price = self.closes[-1:]
		# elif self.highs[-1:] > self.sell_price1 and trade.long == True:
		# 	action = True
		# 	price = self.sell_price1
					


		
		if self.open_value_corr['OV_CORR'][-6:-5][0] > 0.25  and self.open_value_corr['OV_CORR'][-1:][0] < 0.25 and trade.long == False:
			action = True
			price = self.closes[-1:]
		# elif self.lows[-1:] < self.buy_price1 and trade.long == False:
		# 	action = True
		# 	price = self.buy_price1
		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		return (None,None,self.open_value,self.open_value_corr)