
from Strategies import Strategies
from Bot_controller import BotController
#from poloniex import poloniex
import numpy as np







class BotTradingLog(object):
	def __init__(self,pair,period,strategy , starting_bull_volume = None,starting_bear_volume= None,live = False,backtest = True):

		#Initiate the the strategy which this this log record
		self.period = period 
		self.pair = pair
		self.strategy = strategy # This is noobyStrat for now
		#self.conn = poloniex('G3CH4OLX-IZNGQXQS-NIITU83S-6JPYGX0N','fd1d4dc8fc421b402c876377f55267a667ab7e81c6cfff49ec896a2a50208f97084f421d375e4ea9fa193d0f16618ee3f8b9913bce6f3666324401fdfa21184d')
		self.live = live
		self.backtest = backtest
		self.botcontroller = BotController(pair,strategy,live,backtest,starting_bull_volume = starting_bull_volume ,starting_bear_volume = starting_bear_volume)
		

		#Initiate empty vectores for Candlestick data to fill, date,current price etc... 
		self.dates = []
		self.openprices = []
		self.closeprices = [] # Needed for Momentum Indicator
		self.AveragePrices = [] #This price is considered the average price and is what the trading strategy conisders the "Current price" at any given timestamp
		self.highs = []
		self.lows = []
		self.volumes = [] #Total Trading volume for the Candlestick 
		self.quoteVolumes = []
		self.startTime = [] # For Live Running only
		self.current = []	#Current price, for Live Running only
		self.candlesticks = []

		#intiate empty vectors for strategy performance
		# self.BullTokens = np.array([starting_bull_volume])		# Number of Bull tokens at the datetime after the Trade and Comission
		# self.BearTokens = np.array([starting_bear_volume])	
		# self.tradeStatus = np.array([])							#TradeStatus is of form "OPENED","IN POSITION",("CLOSED" OR "STOPLOSSED","WAITING"
		# self.BullCommisions = np.array([])						#Number of tokens taken by the exchange during a buy order
		# self.BearCommisions = np.array([])

		#list of trades
		self.orders = []		#Initiate a vector of order objects
		self.trades = [] 		#  Initiate a vector of trade objects
		


	def tick(self,candlestick):

		#Append all new candlestick informaton to the class
		self.candlesticks.append(candlestick)
		self.dates.append(candlestick.date)
		self.openprices.append(candlestick.open)
		self.closeprices.append(candlestick.close)
		self.AveragePrices.append(candlestick.priceAverage)
		self.highs.append(candlestick.high)
		self.lows.append(candlestick.low)
		self.volumes.append(candlestick.volume)
		self.quoteVolumes.append(candlestick.quoteVolume)
		self.startTime.append(candlestick.startTime)
		self.current.append(candlestick.current)


		self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.AveragePrices,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		


		# Call Upon the strategy class to make descisions and execute actions if need be. We will store the information here to visualise and analyse later.
		#self.strategy.tick_macro(candlestick,self.AveragePrices,self.lows,self.highs,self.openprices,self.closeprices,self.dates)
		
		self.botcontroller.tick(self.price_info)
		
		


		













	
		


		
