import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt




class Compiler(object):
	def __init__(self,results):
		#Make a master DataFrame

		self.count = 0
		for pairs in list(results.keys()):
			if self.count == 0:
				self.MDF = results[pairs].join_df
				self.count += 1
			else:
				new_df = results[pairs].join_df
				self.MDF = pd.merge(self.MDF,new_df,left_index=True, right_index=True)
		self.strategy_returns_list = []
		self.index_list = []
		for names in self.MDF.columns.values:
			if 'est_cur_val' in names:
				self.strategy_returns_list.append(names)
			elif 'hold_return' in names:
				self.index_list.append(names)


		self.MDF['PF_RETURN'] = self.MDF[self.strategy_returns_list].mean(axis =1)
		self.MDF['INDEX_RETURN'] =self.MDF[self.index_list].mean(axis = 1)
		self.MDF['PF_RETURN'] = self.MDF['PF_RETURN']/self.MDF['PF_RETURN'][0]
		print("TOTAL INDEX RETURN = " + str(self.MDF['INDEX_RETURN'][-1:]))
		print("TOTAL PORTFOLIO RETURN = " + str(self.MDF['PF_RETURN'][-1:]))
		plt.figure(3)
		plt.plot(self.MDF.index,self.MDF['PF_RETURN'],self.MDF['INDEX_RETURN'])
		plt.show(3)
				
		#print(self.MDF[self.MDF.index.month==5])
		self.Alpha_beta()
		self.Sharp_ratio()
		self.Max_draw_down()
	
	def Alpha_beta(self):
		# PLot(index returns vs potfolio returns by month)
		#Alpha and beta is a regression of the strategy returns as a function of the asset returns
		self.monthly_index_returns = []
		self.monthly_portfolio_returns = []
		for x in range(1,13):
			month = self.MDF[self.MDF.index.month==x]
			if len(month)> 0:
				self.monthly_index_returns.append(float(month['INDEX_RETURN'][-1:]/month['INDEX_RETURN'][0]))
				self.monthly_portfolio_returns.append(float(month['PF_RETURN'][-1:]/month['PF_RETURN'][0]))

		m,b = np.polyfit(self.monthly_index_returns,self.monthly_portfolio_returns,1)
		x = np.linspace(0,2,21)
		y = m*x + b
		print("Beta = " +str(b))
		alpha = round((m+b-1)*100,3)
		print("Alpha = "+ str(alpha) + "%")
		plt.figure(4)
		plt.plot(x,y,color = 'red')
		plt.scatter(self.monthly_index_returns,self.monthly_portfolio_returns,marker = "*")
		plt.show(4)

	def Sharp_ratio(self):
		self.index_risk_SD = self.MDF['INDEX_RETURN'].std()
		self.pf_risk_SD = self.MDF['PF_RETURN'].std()
		self.index_SR = self.MDF['INDEX_RETURN'][-1:]/float(self.index_risk_SD)
		self.pf_SR = (self.MDF['PF_RETURN'][-1:]/self.pf_risk_SD)
		print("Sharp_ratio of INDEX  = " + str(self.index_SR.values))
		print("Sharp_ratio of Portfolio  = " + str(self.pf_SR.values))

	def UCF(self):
		pass
	def Max_draw_down(self):
		self.PF_draw_back = 0
		self.INDEX_draw_back = 0

		count = 0
		for x in self.MDF['PF_RETURN']:
			if count == 0:
				z = x
				count+=1
			elif x < z:
				self.PF_draw_back += z-x
			z=x



		count2 = 0
		for x2 in self.MDF['INDEX_RETURN']:
			if count2 == 0:
				z2 = x2
				count2+=1
			elif x2 < z2:
				self.INDEX_draw_back += z2-x2
			z2=x2
		
		self.PF_draw_back =round(self.PF_draw_back,3)
		self.INDEX_draw_back = round(self.INDEX_draw_back,3)
		print("Total Draw back of INDEX = " + str(self.INDEX_draw_back))
		print("Total Draw back of Portfolio = " + str(self.PF_draw_back))
		
			
	def Correlation(self):
		pass 




			




		




		
		

