from SMA_MARGIN import SMA_M
from Trade_Calcs import Trade_Calcs
import numpy as np
import datetime


class BotController(object):
	def __init__(self,pair,strategy,live,backtest,starting_bull_volume = None,starting_bear_volume= None):
		self.pair = pair
		self.trade_calcs = Trade_Calcs(pair)
		self.strategy = strategy
		self.live = live
		self.backtest = backtest
		self.numSimulTrades = self.strategy.numSimulTrades
		self.trades = []
		self.orders = []
		self.tradeStatus = np.array([])

		#intiate empty vectors for strategy performance
		#self.BullTokens = np.array([starting_bull_volume])		# Number of Bull tokens at the datetime after the Trade and Comission
		#self.BearTokens = np.array([starting_bear_volume])

		self.BullTokens = [starting_bull_volume]
		self.BearTokens = [starting_bear_volume]


		#self.tradeStatus = np.array([])							#TradeStatus is of form "OPENED","IN POSITION",("CLOSED" OR "STOPLOSSED","WAITING"
		self.tradeStatus = []
		#self.BullCommisions = np.array([])						#Number of tokens taken by the exchange during a buy order
		#self.BearCommisions = np.array([])

		self.BullCommisions = []					#Number of tokens taken by the exchange during a buy order
		self.BearCommisions = []

		self.strategy_total_time = 0
		self.trade_controller_time = 0

		self.count = 0
	
	def tick(self,price_info):

		
		#t0 = datetime.datetime.now()
		#self.tradeStatus = np.append(self.tradeStatus,"WAITING")
		self.tradeStatus.append("WAITING")
		if self.live == True:
			balence = self.conn.returnBalances()
			self.BullTokens = np.append(self.BullTokens,balence[self.bull])
			self.BearTokens = np.append(self.BearTokens,balence[self.bear])
		else:
			if len(self.tradeStatus) > 1: 																		#only appends on a tick the 2nd tick onwards to avoid double ups with the first instatiate uses len(mavs) to represent tick index
				#self.BullTokens = np.append(self.BullTokens,self.BullTokens[-1:])						#assume Tokens remain the same as last iteration unless trade happens in which case change tokens at that point 
				#self.BearTokens = np.append(self.BearTokens,self.BearTokens[-1:])
				self.BullTokens.append(self.BullTokens[-1])						#assume Tokens remain the same as last iteration unless trade happens in which case change tokens at that point 
				self.BearTokens.append(self.BearTokens[-1])
				#print(self.BullTokens)


			#self.BullCommisions = np.append(self.BullCommisions,[float(0)])
			#self.BearCommisions = np.append(self.BearCommisions,[float(0)])
			self.BullCommisions.append(float(0))
			self.BearCommisions.append(float(0))		
		
		#t1 = datetime.datetime.now()
		#self.strategy_total_time += (t1-t0).total_seconds()


		t0 = datetime.datetime.now()
		self.strategy.tick_price_info(price_info,self.trades,self.BullTokens,self.BearTokens)
		t1 = datetime.datetime.now()
		self.strategy_total_time += (t1-t0).total_seconds()
		#print(self.price_info)
		

		t2 = datetime.datetime.now()
		if self.live == True or self.backtest == True:
			self.evaluate_orders(self.orders)
			#self.evaluate_trades(self.trades,price_info)

			self.evaluate_position(self.trades,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions)
			self.evaluate_trades(self.trades,price_info)
		t3 = datetime.datetime.now()
		self.trade_controller_time += (t3-t2).total_seconds()


		#self.evaluate_trades(self.trades,price_info)

		#print(self.BullTokens[-1:] , self.BearTokens[-1:])
		#print('\n')
	def evaluate_orders(self,orders):
		pass


	def evaluate_trades(self,trades,price_info):
		for trade in trades:
			if (trade.status == "OPEN"):
				closed,price,date,margin = trade.tick(price_info) #Tick the position to see if it has hit the stoploss
				if closed == True:
					#print(self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions)
					trade,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions = self.trade_calcs.close(price,date,trade,margin,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions,Market_sell = True,total = None)
					print(trade.total_fiat)
					#print(self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions)

	def evaluate_position(self,trades,BullTokens,BearTokens,BullCommisions,BearCommisions):

		openTrades = []
		for trade in trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade)											 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1] = "IN POSITION" 

		
		for trade in openTrades:
			if trade.status == "OPEN":
				action,price,total,date,amount,margin,market_order = self.strategy.evaluate_Close(trade,openTrades)
				
				if action == True:
					trade,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions = self.trade_calcs.close(price,date,trade,margin,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions,Market_sell = market_order,total = total )
					self.tradeStatus[-1] = "CLOSED"

		
		openTrades = []
		for trade in trades:
			if (trade.status == "OPEN"):
				openTrades.append(trade)										 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
				self.tradeStatus[-1] = "IN POSITION"


		
		if (len(openTrades) < self.strategy.numSimulTrades) and self.tradeStatus[-1] != "CLOSED":
			if self.strategy.limits == True:
				
				# longs = 0
				# shorts = 0
				# if len(openTrades) > 0:
				# 	for trade in openTrades:
				# 		if trade.long == True:
				# 			longs +=1
				# 		elif trade.longs == False:
				# 			shorts +=1
				# index = max([longs,shorts])

				for bp,sp in self.strategy.prices:
					openTrades = []
					for trade in trades:
						if (trade.status == "OPEN"):
							openTrades.append(trade)										 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
							self.tradeStatus[-1] = "IN POSITION"

					if (len(openTrades) < self.strategy.numSimulTrades):
					#print(bp,sp)
						action,Long,price,date,total,margin,stoploss,market_Order = self.strategy.evaluate_Open(bp,sp,openTrades)
						if action == True:
							#print("OPEN")
							trade,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions = self.trade_calcs.open(price,date,Long,self.pair,margin,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions,StopLoss = stoploss,Market_buy = market_Order,total = total )
							self.trades.append(trade)
							self.tradeStatus[-1] = "OPEN"
			else:
				action,Long,price,date,total,margin,stoploss,market_Order = self.strategy.evaluate_Open()
				if action == True:
					print("OPEN")
					trade,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions = self.trade_calcs.open(price,date,Long,self.pair,margin,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions,StopLoss = stoploss,Market_buy = market_Order,total = total )
					self.trades.append(trade)
					self.tradeStatus[-1] = "OPEN"


		#FOR BREAKOUTS HEDGING STRATEGY ONLY

		
		# openTrades = []
		# for trade in trades:
		# 	if (trade.status == "OPEN"):
		# 		openTrades.append(trade)											 #appendeds the self attributed trades vector to the open_trades vector that is reset with every cycle of this forloop
		# 		self.tradeStatus[-1] = "IN POSITION" 

		
		# for trade in openTrades:
		# 	if trade.status == "OPEN":
		# 		action,price,total,date,amount,margin,market_order = self.strategy.evaluate_Close(trade,openTrades)
				
		# 		if action == True:
		# 			trade,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions = self.trade_calcs.close(price,date,trade,margin,self.BullTokens,self.BearTokens,self.BullCommisions,self.BearCommisions,Market_sell = market_order,total = total )
		# 			self.tradeStatus[-1] = "CLOSED"

		





