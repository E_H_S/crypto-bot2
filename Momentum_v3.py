from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class VPOC_MOMENTUM(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1=None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = True,anchored = True,open_value_candles = True,preped_volume_profiles = False)
		
		self.open_value = pd.DataFrame()
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])


		self.buy_side_switch = True
		self.sell_side_switch = True

		self.ATR = []

		self.account_size = []
		self.latest_range = []
		self.leverages = []

		self.ob_index = None

		self.Leverage = 2
		self.VPOC = [None,None]*5


		self.buy_line1 = [None,None]
		self.sell_line1 = [None,None]
		self.buy_line2 = [None,None]
		self.sell_line2 = [None,None]
		self.buy_line3 = [None,None]
		self.sell_line3 = [None,None]
		self.buy_line4 = [None,None]
		self.sell_line4 = [None,None]
		self.buy_line5 = [None,None]
		self.sell_line5 = [None,None]
		self.buy_line6 = [None,None]
		self.sell_line6 = [None,None]
		

		self.mid_line = [None,None]



		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None



		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.percent_drop = 0.062
		self.ATR_Scaler = 0.95

		self.last_OV_low = None


		self.middle = None
		self.candle_count = 0


		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True


		self.b_1_ready = True
		self.b_2_ready = True
		self.b_3_ready = True



		self.s_1_ready = True
		self.s_2_ready = True
		self.s_3_ready = True

		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = []

		self.ATR_switches = []

		self.numSimulTrades = 1
		self.margin = True

		self.count10 = 0
		self.switch10 = False

		self.First_enter = False

		self.timer1 = 0

		self.timer2 = 0

		self.timer3 = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		#updating Indicators
		self.candle_count +=1

		#t1 = datetime.datetime.utcnow()

		if self.switch10 == True:
			self.count10 +=1

		#if self.count10 > 12*48:
		if self.count10 > 12*3:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		t1 = datetime.datetime.utcnow()

		if self.candle_count > 1:
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)
			#print(self.open_value.columns)
			if self.last_OV_low is not None:
				self.range_accumulation = self.open_value['OI_close'][-1:].item()/1000000 - self.last_OV_low/1000000


		self.ATR.append(self.indicators.avg_range(self.closes,self.highs,self.lows,288))
		
	


		if self.candle_count > 290:
			if len(self.open_value['max_delta_OI0'][-1:]) > 0:
				#biggest_drop = min([self.open_value['max_delta_OI0'][-1:].item(),self.open_value['max_delta_OI1'][-1:].item(),self.open_value['max_delta_OI2'][-1:].item(),self.open_value['max_delta_OI3'][-1:].item(),self.open_value['max_delta_OI4'][-1:].item(),self.open_value['max_delta_OI5'][-1:].item()])
				biggest_drop = self.open_value['MAX_MAX_delta_Event_OI'][-1:].item()
			
				
				if biggest_drop < - self.open_value['OI_high'][-36:].max()*self.percent_drop and self.switch10 == False:# and abs(self.closes[-1:]-self.closes[-7:-6])>self.closes[-1:]*0.02: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 = True
					self.count10 = 0
					self.ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					self.First_enter = True
					self.last_OV_low = self.open_value['OI_low'][-36:].min()
					#ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					

					# if self.ATR_switch == False:
					# 	self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					# elif self.ATR_switch == True:
					# 	ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					# 	if ATR_min < self.ATR_min:
					# 		self.ATR_min = ATR_min

					self.last_OV_high = self.open_value['OI_high'][-36:].max()
					self.ATR_switch = True
					
					print('\n')
					print("NEW TRIGGER EVENT! " , self.dates[-1:])
					print(biggest_drop)
					print("MIN ATR ", self.ATR_min)
					print('\n')
					


				if self.ATR[-1:] != None:
					if self.ATR_switches[-12*24:].min() == 1: 
						if self.ATR[-1:] < self.ATR_min*self.ATR_Scaler and self.ATR_switch == True:
							self.ATR_switch = False
							print('\n')
							print('FAILED ATR SWITCH ', self.dates[-1:])
							print('\n')
							#self.calculate_clip_size()
							self.latest_range = []
						
					
				if self.open_value['MAX_MAX_delta_Lookback_OI'][-1:].item() > -self.open_value['OI_high'][-36:].max()*self.percent_drop and self.ATR_switch == True:
					self.ATR_switch = False
					print('\n')
					print('5 day OV drop expired! ', self.dates[-1:])
					print('\n')
					#self.calculate_clip_size()
					self.latest_range = []



				if self.open_value['OI_high'][-1].item() > self.last_OV_high + self.open_value['OI_high'][-36:].max()*self.percent_drop and self.ATR_switch == True:

					self.ATR_switch = False
					print('\n')
					print('OVER THE OV MAX ', self.dates[-1:])
					print('OV MAX ',self.last_OV_high + self.open_value['OI_high'][-36:].max()*self.percent_drop)
					print("LAST OI ",self.open_value['OI_close'][-1].item())
					print('\n')
					self.calculate_clip_size()
					self.latest_range = []
				
		#t2 = datetime.datetime.utcnow()
		
		

		t2 = datetime.datetime.utcnow()
		if self.candle_count > 10:
			#self.volume_profile,self.index = self.indicators.get_anchored_(self.dates[-1],self.index)
			self.volume_profile= self.indicators.get_anchored_VPOC(self.dates[0],self.dates[-1],hours = 12)
			
			#t1 = datetime.datetime.utcnow()
			if self.candle_count > 12*24:

				try:
					self.VPOC = np.append(self.VPOC,self.volume_profile[self.volume_profile['percent']<0.50].index[-1:][0])
				except:
					self.VPOC = np.append(self.VPOC,self.volume_profile[self.volume_profile['percent']>0.50].index[0])
					
				try:
					#t1 = datetime.datetime.utcnow()
					self.sell_price1 = self.volume_profile.index[self.volume_profile['percent']<0.3][-1:][0]
					self.sell_price2 = self.volume_profile.index[self.volume_profile['percent']<0.25][-1:][0]
					self.sell_price3 = self.volume_profile.index[self.volume_profile['percent']<0.2][-1:][0]

					
					#self.sell_price3 = self.volume_profile.index[0]

					self.buy_price1 = self.volume_profile.index[self.volume_profile['percent']>0.70][0]
					self.buy_price2 = self.volume_profile.index[self.volume_profile['percent']>0.75][0]
					self.buy_price3 = self.volume_profile.index[self.volume_profile['percent']>0.8][0]


					#t2 = datetime.datetime.utcnow()
					#self.buy_price3 = self.volume_profile.index[-1:][0]
				except:
					#print(self.volume_profile)
					#self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.2].index[-1:][0]
					

					self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.3].index[-1:][0]
					self.sell_price2 = self.volume_profile[self.volume_profile['percent']<0.25].index[-1:][0]
					self.sell_price3 = self.volume_profile.index[0]


					self.buy_price1 = self.volume_profile[self.volume_profile['percent']>0.7].index[0]
					self.buy_price2 = self.volume_profile[self.volume_profile['percent']>0.75].index[0]
					self.buy_price3 = self.volume_profile.index[-1:][0]

		

			else:
				self.VPOC = np.append(self.VPOC,None)



		# if self.buy_price1 != None:
		# 	self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)

		# 	if self.last_orderbook is not None:
		# 		#self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-1],self.ob_index)
		# 		#self._1m_2m_orders = pd.concat([self._1m_2m_orders,self.indicators.return_large_orders(self.last_orderbook,1500000,2000000)])
		# 		self.levels = self.indicators.return_large_orders(self.last_orderbook,1000000,50000000)
		# 		self.previous_levels.append(self.levels)
		# 		self.previous_levels.pop(0)
		# 		#print(type(self.levels))
		# 		self.levels_filtered = self.indicators.filter_large_levels(self.previous_levels)
		# 		if self.levels_filtered is not None:
		# 			self.large_orderbook_levels = pd.concat([self.large_orderbook_levels,self.levels_filtered])
		# 			prices_distance = (self.stoploss_sell - self.stoploss_buy)*0.35
		# 			#print(prices_distance)
		# 			#print(self.stoploss_sell,upper_price)
		# 			self.upper_price = self.stoploss_sell + prices_distance
		# 			self.lower_price = self.stoploss_buy - prices_distance
		# 			#print(self.stoploss_sell,upper_price)
		# 			#print(self.stoploss_buy,lower_price)
		# 			#print("check222")
		# 			#print(self.levels_filtered)
		# 			self.levels_final =  self.levels_filtered[(self.levels_filtered['price'] <= self.upper_price) & (self.levels_filtered['price'] >= self.lower_price)]

		# 			#print(self.levels_final)
		# 			#print()
		# 			upper_value = round(self.levels_final[(self.levels_final['side'] == 'Sell')& (self.levels_final['price'] > self.sell_price1_final)]['qty'].sum()/1000000,2)#/self.range_accumulation
		# 			lower_value = round(self.levels_final[(self.levels_final['side'] == 'Buy')& (self.levels_final['price'] < self.buy_price1_final)]['qty'].sum()/1000000,2)#/self.range_accumulation
		# 			#print(upper_value,lower_value)
		# 			self.upper_thickness.append(upper_value)
		# 			self.lower_thickness.append(lower_value)
		# 		else:
		# 			self.upper_thickness.append(None)
		# 			self.lower_thickness.append(None)
		# 	else:
		# 		self.upper_thickness.append(None)
		# 		self.lower_thickness.append(None)









		#t2 = datetime.datetime.utcnow()
		t3 = datetime.datetime.utcnow()
			
		self.mid_line.append(self.middle)

		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)
		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)
		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)

		self.buy_line4.append(self.buy_price1)
		self.sell_line4.append(self.sell_price1)
		self.buy_line5.append(self.buy_price2)
		self.sell_line5.append(self.sell_price2)
		self.buy_line6.append(self.buy_price3)
		self.sell_line6.append(self.sell_price3)

		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3),(self.buy_price4,self.sell_price4),(self.buy_price5,self.sell_price5),(self.buy_price6,self.sell_price6)]
				

		if self.ATR_switch == False:
			self.ATR_switches = np.append(self.ATR_switches,0)
			
		elif self.ATR_switch == True:
			self.ATR_switches = np.append(self.ATR_switches,1)
		self.ATR_min_list = np.append(self.ATR_min_list,self.ATR_min)
		#self.OV_cutoff_list = 
		t4 = datetime.datetime.utcnow()
		self.leverages.append(self.Leverage)

		self.timer1 += (t2-t1).total_seconds()
		self.timer2 += (t3-t2).total_seconds()
		self.timer3 += (t4-t3).total_seconds()

	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = self.account_size[-1]*self.Leverage/3
		stoploss = None
		#market_order = 'zero'
		market_order = False


		if bp == self.buy_price1 and self.b_1_ready == False:
			bp = None
		elif bp == self.buy_price2 and self.b_2_ready == False:
			bp = None
		elif bp == self.buy_price3 and self.b_3_ready == False:
			bp = None
		
		if sp == self.sell_price1 and self.s_1_ready == False:
			sp = None
		elif sp == self.sell_price2 and self.s_2_ready == False:
			sp = None
		elif sp == self.sell_price3 and self.s_3_ready == False:
			sp = None
	
		#self.ATR_switch = True
		if self.candle_count > 12*24 and self.First_enter == True:

			if self.closes[-1] > self.VPOC[-1] and self.closes[-2] < self.VPOC[-1]:
				self.sell_side_switch = True
			elif self.closes[-1] < self.VPOC[-1] and self.closes[-2] > self.VPOC[-1]:
				self.buy_side_switch = True






			if len(self.open_value['max_delta_OI0'])> 0:
				#print(self.open_value['max_delta'])

				if self.open_value['max_delta_OI0'][-1:].isnull().item() == False:
					#if self.open_value['max_delta'][-1:].item() < -8000 and self.ATR[-1:] > 0.0009 :
					if  self.ATR_switch == False :
						
						
						#if max(self.closes[-26:-2:]) < self.VPOC[-1] and self.closes[-1] > self.VPOC[-1]:
						#if max(self.closes[-4:-2]) < self.buy_price3 and self.closes[-5] > self.buy_price3 and self.lows[-1] < self.closes[-2]:# and self.highs[-1] > self.closes[-2] :
						if max(self.closes[-4:-1]) < self.buy_price3 and self.closes[-5] > self.buy_price3 and self.lows[-1] < self.closes[-2] and self.sell_side_switch == True:# and self.highs[-1] > self.closes[-2] :
							action = True
							Long = False
							market_order = False
							price = self.closes[-2]
							self.sell_side_switch = False
							#stoploss = self.highs[-288*2:].max()*1.02
								
						#if bp != None:

						#if min(self.closes[-26:-2:]) > self.VPOC[-1] and self.closes[-1] < self.VPOC[-1]:
						#if min(self.closes[-4:-2]) > self.sell_price3 and self.closes[-5] < self.sell_price3 and self.highs[-1] > self.closes[-2]: #and self.lows[-1] < self.closes[-2]:
						if min(self.closes[-4:-1]) > self.sell_price3 and self.closes[-5] < self.sell_price3 and self.highs[-1] > self.closes[-2] and self.buy_side_switch == True: #and self.lows[-1] < self.closes[-2]:
							action = True
							Long = True
							market_order = False
							price = self.closes[-2]
							self.buy_side_switch = False
							#stoploss = self.lows[-288*2:].min()/1.02



		if action == True:
			if Long == True:
				if bp == self.buy_price1:
					self.b_1_ready = False
				elif bp == self.buy_price2:
					self.b_2_ready = False
				elif bp == self.buy_price3:
					self.b_3_ready = False

			if Long == False:
				if sp == self.sell_price1:
					self.s_1_ready = False
				elif sp == self.sell_price2:
					self.s_2_ready = False
				elif sp == self.sell_price3:
					self.s_3_ready = False


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = True

		if trade.trade_length < 3:
			trade.start_timer2 = False
			if trade.long == True:
				trade.timer_2_target = max(self.highs[-12*24:])
			elif trade.long == False:	
				trade.timer_2_target = min(self.lows[-12*24:])

			trade.timer_1 = 0

		if trade.start_timer2 == False:
			if trade.long == True and self.closes[-2] > trade.timer_2_target:
				trade.start_timer2 = True
				trade.highest_price = self.highs[-1]
				trade.time_from_high = 0
				print('Trade timer 2 started!')
			elif trade.long == False and self.closes[-2] < trade.timer_2_target:
				trade.start_timer2 = True
				trade.lowest_price = self.lows[-1]
				trade.time_from_low = 0
				print('Trade timer 2 started!')
			else:
				trade.timer_1 += 1



		if trade.start_timer2 == True:
			if trade.long == True:
				if self.highs[-1] > trade.highest_price:#*(1+(3*self.ATR[-1])):
					trade.highest_price = self.highs[-1]
					trade.time_from_high = 0
				else:
					trade.time_from_high += 1
					#print('Time from high  ',trade.time_from_high)



			if trade.long == False:
				if self.lows[-1] < trade.lowest_price:#/(1+(3*self.ATR[-1])):
					trade.lowest_price = self.lows[-1]
					trade.time_from_low = 0
				else:
					trade.time_from_low += 1
					#print('Time from low  ',trade.time_from_low)



		if trade.start_timer2 == True:
			if trade.long == True:
				if trade.time_from_high > 12*2:
					action = True
					price = self.closes[-1]


			if trade.long == False:
				if trade.time_from_low > 12*2:
					action = True
					price = self.closes[-1]

		elif trade.start_timer2 == False:
			print('checking timer 1')
			print(trade.timer_1)
			if trade.long == True and trade.timer_1 > 12*3:
				action = True
				price = self.closes[-1]

			if trade.long == False and trade.timer_1 > 12*3:
				action = True
				price = self.closes[-1]





		
		#if trade.long == True and self.closes[-1] > trade.entryPrice or trade.long == False and self.closes[-1] < trade.entryPrice:
		


		if max(self.closes[-2:]) < self.buy_price1 and trade.long == True and self.closes[-1]< trade.entryPrice:
			action = True
			price = self.closes[-1]
			#price = self.buy_price1


		if min(self.closes[-2:]) > self.sell_price1 and trade.long == False and self.closes[-1] > trade.entryPrice:
			action = True
			price = self.closes[-1]
			#price = self.buy_price1
		






		# #else:
		# 	if max(self.closes[-2:]) < self.buy_price1 and trade.long == True:
		# 		action = True
		# 		price = self.closes[-1]
		# 		#price = self.buy_price1


		# 	if min(self.closes[-2:]) > self.sell_price1  and trade.long == False:
		# 		action = True
		# 		price = self.closes[-1]
		# 		#price = self.buy_price1

		
	

		if action == True:
			self.b_1_ready = True
			self.b_2_ready = True
			self.b_3_ready = True
			self.s_1_ready = True
			self.s_2_ready = True
			self.s_3_ready = True

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def calculate_clip_size(self):
		print('CALCULATING LEVERAGE')
		returns_30 = pd.Series(self.latest_range)
		#print(returns_30)
		returns_30 = (returns_30.diff()/returns_30)
		#print(self.account_size)
		
		#print(len(returns_30[returns_30 != 0]))
		#print(returns_30)
		if len(returns_30[returns_30 != 0]) != 0:
			print('CALCULATING LEVERAGE part 2')
			mean_returns = returns_30[returns_30 != 0].mean()*100
			risk = returns_30[returns_30 != 0].std()
			SR = mean_returns/risk
			if SR > 6:
				self.Leverage += 1 
			elif SR > 3:
				self.Leverage += 0.50

			elif 1 > SR > 3:
				self.Leverage += 0
			elif -3 < SR < 1:
				self.Leverage -= 0.50
			elif SR < -3: 
				self.Leverage -= 1


			if self.Leverage > 4:
				self.Leverage = 4
			elif self.Leverage < 2:
				self.Leverage = 2


			print("last SR ", SR )
			print("FINAL Leverage",self.Leverage)
		else:
			self.Leverage = 2


	def scale_out():
		pass
	def Draw_indicators(self):
		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.ATR_switches)#,self.ATR_min_list)
		return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.leverages,self.open_value,self.ATR_switches)#,self.ATR_min_list)




