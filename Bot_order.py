
import pandas as pd
from Trade_Calcs import Trade_Calcs

class BotOrder(object):
	def __init__(self,currentPrice,date,entryVolume,stopLoss=None,target_sell = None,trail = None,status = None,Long = True,target2 = None,live_stop = None):
		self.status = "OPEN"
		if status != None:
			self.status = status
		self.entryPrice = currentPrice
		self.exitPrice = None
		self.entryVolume = entryVolume
		self.exitVolume = None
		self.entryDate = date 
		self.exitDate = None
		self.stoplossed = False
		self.stopLoss = None
		self.trade_length = 1
		self.maxPrice = self.entryPrice
		self.trail = trail
		self.target_reached = None
		self.target_sell = target_sell
		self.long = Long
		self.target2 = target2
		self.live_stop = live_stop
