from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class Sig_L(object):
	def __init__(self, period = 14400):

		self.fast = 5
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)
		self.period = period

		self.indicators = BotIndicators()
		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.numSimulTrades = 1
		self.margin = True

		self.volume_rank = np.array([])
		
		self.scale = 300/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		self.price_Levels = {}
		
	def tick_price_info(self,price_info):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}

		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])


		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		#updating Indicators		
		
		#print(self.closes[-1:][0])
		if len(self.closes)>3:
			self.price_Levels = self.indicators.sig_levels(self.price_Levels,self.closes[-4:-3][0],self.opens[-4:-3][0],self.closes[-3:-2][0],self.opens[-3:-2][0],self.closes[-2:-1][0],self.opens[-2:-1][0],self.closes[-1:][0],self.opens[-1:][0])
		#print(self.price_Levels)
		#self.volume_rank = np.append(self.volume_rank,self.indicators.volume_rank(self.volumes))
		#print(self.price_levels)
		#print(self.volume_rank)
		
	def evaluate_Open(self):
		action = False
		Long = None
		price = None
		date = int(self.dates[-1:])
		total = None
		stoploss = None
		market_order = 'zero'
		#market_order = False

		if len(self.closes)>3:
			if self.price_Levels[str(self.closes[-2:-1][0])] > 1 and self.closes[-1:] < self.opens[-1:]: # shorts
		 	
		 		action = True
		 		Long = False
		 		price = self.closes[-1:]
				
						
			if self.price_Levels[str(self.closes[-2:-1][0])] > 1 and self.closes[-1:] > self.opens[-1:]: # longs
				
				action = True
				Long = True
				price = self.closes[-1:]
				
		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade):

		action = False
		price = None
		date = int(self.dates[-1:])
		total = None
		amount = None
		market_order = 'zero'
		#market_order = False
		


		if trade.long == False and self.closes[-1:] > self.opens[-1:] and self.closes[-2:-1] > self.opens[-2:-1]:
			action = True
			price = self.closes[-1:]
			if market_order != 'zero':
				market_order = True
					
		if trade.long == True and self.closes[-1:] < self.opens[-1:] and self.closes[-2:-1] < self.opens[-2:-1]:
			action = True
			price = self.closes[-1:]
			if market_order != 'zero':
				market_order = True
		
		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		return #(self.mavs,self.mavs2)
