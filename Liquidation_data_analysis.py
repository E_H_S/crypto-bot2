from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

#Strategy_on UTC TIMES  [(1.5-2.5),(7.5-11),(17.5 - 19.5)]
		#sydney times.  [(11.5-12.5),(17.5-21),(3.5-5.5)]



		#low volitility [Monday]
		#medium volility[Thursday, Saturday,Sunday]
		#Tuesday-Friday tighten Saturday


# SMA_M uses 3 indicators and will require an imput perameter for each


class Liquidation_data_collection(object):
	def __init__(self,period = 300,startTime = None, endTime = None,p1 = None,p2 = None,p3 = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = False, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = True,anchored = True,open_value_candles = True,momentum = True)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		

		self.ATR = []
		self.ATR_14 = []
		self.stoplosses = []
		self.max_risks = []

		self.ob_index = None


		self.VPOC = []
		self.max_risk = 0.08

		self.buy_line1 = []
		self.sell_line1 = []
		self.buy_line2 = []
		self.sell_line2 = []
		self.buy_line3 = []
		self.sell_line3 = []
		self.buy_line4 = []
		self.sell_line4 = []
		self.buy_line5 = []
		self.sell_line5 = []
		self.buy_line6 = []
		self.sell_line6 = []
		
		self.Leverage = 4


		self.mid_line = [None,None]

		if p1 != None:
			self.drop_percent = p1
		else:
			self.drop_percent = 0.04
			#self.drop_percent = 0.02

		if p2 != None:
			self.stoploss = p2
		else:
			self.stoploss = 0.025

		if p3 != None:
			self.clip_size = p3
		else:
			self.clip_size = 1500


		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None

		self.leverages = []

		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None

		self.volitilitys =[]
		self.account_size = []
		self.latest_range = []

		self.open_value = np.array([])

		self.middle = None

		self.rolling_period = 24
		self.ATR_rolling_period = 288

		self.min_initial_take_profit_candles = 0
		self.min_initial_take_profit = 0


		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True



		self.b_1_ready = True
		self.b_2_ready = True
		self.b_3_ready = True

		self.five_m_dates = []

		self.s_1_ready = True
		self.s_2_ready = True
		self.s_3_ready = True

		self.strategy_start = None
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = []
		self.ATR_switches = []

		self.numSimulTrades = 3
		self.margin = True

		self.closing_line = []
		self.volume_profile = None

		self.OI_density = []
		self.trigger_date = None
		self.trigger_dates = []


		self.timer1 = 0
		self.timer2 = 0
		self.timer3 = 0
		self.timer4 = 0
		self.timer5 = 0
		self.timer6 = 0
		self.timer7 = 0
		self.timer8 = 0

		self.last_stoplossed_trade = None
		


		self.count10 = 0
		self.switch10 = False

		self.candle_count = 0
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades,bull_tokens,bear_tokens):
		

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.t1 = datetime.datetime.utcnow()

		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']
		self.t2 = datetime.datetime.utcnow()

		self.account_size.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])
		self.latest_range.append(bull_tokens[-1]*self.closes[-1] + bear_tokens[-1])



		

		#Update relevent indicators
		if np.mod(self.dates[-1].minute,5) == 0:
			self.five_m_dates.append(self.dates[-1])
			self.tick_indicators(trades)
		#print(trades)
		#if len(trades)> 0:
		#	print(trades[-1:][0].status)

	def tick_indicators(self,trades):
		#updating Indicators
		#print(self.strategy_start,self.dates[-1:])


		self.candle_count +=1
		if self.trigger_date is not None:
			self.trigger_date += 1
			self.trigger_dates.append(self.trigger_date)



		if len(trades)> 0:
			if trades[-1].status == "STOP LOSSED" and trades[-1] != self.last_stoplossed_trade and self.trigger_date is not None:
				if self.trigger_date > 12*2:
					if trades[-1].entryDate == self.dates[-2] or trades[-1].entryDate == self.dates[-3] or trades[-1].entryDate == self.dates[-4]  or trades[-1].entryDate == self.dates[-5] or trades[-1].entryDate == self.dates[-6]:
						self.ATR_switch = False
						print("ATR SWITCH DUE TO STOP LOSS")
						self.last_stoplossed_trade = trades[-1]


		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
		#if self.count10 > 12*48:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		

		self.t3 = datetime.datetime.utcnow()

		#if len(self.dates)>1:
		#	self.open_value = self.indicators.get_open_value(self.dates[-2:-1])
		self.ATR.append(self.indicators.avg_range(self.closes,self.highs,self.lows,int(self.ATR_rolling_period)))
		#self.ATR_14.append(self.indicators.avg_range(self.closes,self.highs,self.lows,288*7))
		if self.candle_count > 1*2:
			self.momentum = self.indicators.get_momentum(self.dates[-3])
			momentum_tweak = -(self.momentum[-1:].item())*4
		else:
			momentum_tweak = 0
	
		utc_time = self.dates[-1:][0] - datetime.timedelta(hours = 10)
		day = utc_time.weekday()
		hour = utc_time.hour

		if utc_time.minute >= 30:
			minutes = 0.5
		else:
			minutes = 0

		week_num = day*24 + hour + minutes
		#print(week_num)

		self.t4 = datetime.datetime.utcnow()

		#if 1 < week_num < 12 or 24.5 < week_num < 28 or 49 < week_num < 60 or 73 < week_num < 87.5 or 89 < week_num < 93 or 94 < week_num < 98.5 or 99.5 < week_num < 104.5 or 113 < week_num < 140.5 or 144.5 < week_num < 154.5:
		if 0 <= week_num <1.5 or 11.5 < week_num <49.5 or 66.5 < week_num <73.5 or 87 < week_num < 100 or 107 < week_num < 113.5 or 154 < week_num: 
			self.volitility = 'high'
		else:
			self.volitility = 'low'


		
		if momentum_tweak > 0.15:
			momentum_tweak = 0.15
		if momentum_tweak < -0.15:
			momentum_tweak = -0.15
		if self.volitility == 'high':
			self.volitilitys.append(1)
			# bpc1 = 0.80 + momentum_tweak
			# bpc2 = 0.85 + momentum_tweak
			# bpc3 = 0.90 + momentum_tweak

			# spc1 = 0.2 + momentum_tweak
			# spc2 = 0.15 + momentum_tweak
			# spc3 = 0.1 + momentum_tweak

			if momentum_tweak > 0:
				bpc1 = 0.74 + momentum_tweak
				bpc2 = 0.80 + momentum_tweak*(0.8)
				bpc3 = 0.86 + momentum_tweak*(2/3)

				spc1 = 0.26 + momentum_tweak
				spc2 = 0.2 + momentum_tweak*(0.8)
				spc3 = 0.14 + momentum_tweak*(2/3)
			elif momentum_tweak < 0:
				bpc1 = 0.74 + momentum_tweak
				bpc2 = 0.80 + momentum_tweak*(0.8)
				bpc3 = 0.86 + momentum_tweak*(2/3)

				spc1 = 0.26 + momentum_tweak
				spc2 = 0.2 + momentum_tweak*(0.8)
				spc3 = 0.14 + momentum_tweak*(2/3)




		elif self.volitility == 'low':
			self.volitilitys.append(0)
			# bpc1 = 0.80
			# bpc2 = 0.85
			# bpc3 = 0.90

			# spc1 = 0.2
			# spc2 = 0.15
			# spc3 = 0.1

			if momentum_tweak > 0:
				bpc1 = 0.74 + momentum_tweak
				bpc2 = 0.80 + momentum_tweak*(0.8)
				bpc3 = 0.86 + momentum_tweak*(2/3)

				spc1 = 0.26 + momentum_tweak
				spc2 = 0.2 + momentum_tweak*(0.8)
				spc3 = 0.14 + momentum_tweak*(2/3)
			elif momentum_tweak < 0:
				bpc1 = 0.74 + momentum_tweak
				bpc2 = 0.80 + momentum_tweak*(0.8)
				bpc3 = 0.86 + momentum_tweak*(2/3)

				spc1 = 0.26 + momentum_tweak
				spc2 = 0.2 + momentum_tweak*(0.8)
				spc3 = 0.14 + momentum_tweak*(2/3)


		self.t5 = datetime.datetime.utcnow()

		#if self.candle_count > 290:
		if self.ATR[-1] is not None:
			if len(self.open_value['max_delta_OI0'][-1:]) > 0:
				biggest_drop = self.open_value['MAX_MAX_delta_Event_OI'][-1:].item()
				
				if biggest_drop < -self.open_value['high'][-36:].max()*self.drop_percent and self.switch10 == False:# and abs(self.closes[-1:]-self.closes[-1:]) > self.closes[-1:]*0.035: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 = True
					#self.calculate_clip_size()
					self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					self.last_OV_high = self.open_value['high'][-36:].max()
					self.ATR_switch = True
					self.latest_range = []


					if self.ATR_switches[-1] == 0:# and trades[-3:-2][0].status == "CLOSED" and trades[-2:-1][0].status == "CLOSED" and trades[-1:][0].status == "CLOSED":
						self.trigger_date = 0
						if len(trades) == 0 or trades[-1].status == "CLOSED":#and trades[-2:-1].status == "CLOSED" and trades[-1:].status == "CLOSED":
							self.strategy_start = self.dates[-1]
					print('\n')
					print("NEW TRIGGER EVENT! " , self.dates[-1])
					print(biggest_drop)
					print("MIN ATR ", self.ATR_min)
					print('\n')

				if self.ATR[-1] != None:
					if min(self.ATR_switches[-12*24:]) == 1:
						if 114 < week_num < 154 or 74 < week_num < 87 or 49 < week_num < 58 or 1 < week_num < 12:
							if self.ATR[-1] < self.ATR_min*1 and self.ATR_switch == True:
								self.ATR_switch = False
								#self.strategy_start = None
								print('\n')
								print('FAILED ATR SWITCH ', self.dates[-1:])
								print('\n')
						else:
							if self.ATR[-1] < self.ATR_min*1 and self.ATR_switch == True:
								self.ATR_switch = False
								#self.strategy_start = None
								print('\n')
								print('FAILED ATR SWITCH ', self.dates[-1:])
								print('\n')		
					
				if self.open_value['MAX_MAX_delta_Lookback_OI'][-1:].item() > -self.open_value['high'][-36:].max()*self.drop_percent and self.ATR_switch == True:
				#if self.open_value['MAX_MAX_delta_Lookback_OI'][-1:].item() > -self.open_value['high'][-36:].max()*1 and self.ATR_switch == True:
					self.ATR_switch = False
					#self.strategy_start = None
					print('\n')
					print('5 day OV drop expired! ', self.dates[-1:])
					print('\n')

				#if self.open_value['high'][-1].item() > self.last_OV_high + self.open_value['high'][-6*180:].max()*self.drop_percent and self.ATR_switch == True:
				if self.open_value['high'][-1].item() > self.last_OV_high + self.open_value['high'][-6*180:].max()*1 and self.ATR_switch == True:
					self.ATR_switch = False
					#self.strategy_start = None
					print('\n')
					print('OVER THE OV MAX ', self.dates[-1:])
					print('OV MAX ',self.last_OV_high + self.open_value['high'][-36:].max()*self.drop_percent)
					print("LAST OI ",self.open_value['close'][-1].item())
					print('\n')
			
			
		self.t6 = datetime.datetime.utcnow()
		
		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2],candle = True)


		self.t7 = datetime.datetime.utcnow()

		if self.ATR_switch == False:
			self.ATR_switches.append(0)	
		elif self.ATR_switch == True:
			self.ATR_switches.append(1)
		
		self.ATR_min_list.append(self.ATR_min)
		self.leverages.append(self.Leverage)
		self.stoplosses.append(self.stoploss)
		self.max_risks.append(self.max_risk)

			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		
		total = self.account_size[-1]*self.Leverage/3

		# if bp == self.buy_price1:
		# 	total = total*1.5
		# elif bp == self.buy_price2:
		# 	total = total
		# elif bp == self.buy_price3:
		# 	total = total*0.5
		
		# if sp == self.sell_price1:
		# 	total = total*1.5
		# elif sp == self.sell_price2:
		# 	total = total
		# elif sp == self.sell_price3:
		# 	total = total*0.5



		# if self.strategy_start is not None:
		# 	if (self.dates[-1] - self.strategy_start ).total_seconds()/3600 < 12:
		# 		total = total*0.5
		# 		#print('3x clip')
		# 		#print((self.strategy_start - self.dates[-1]).total_seconds()/3600)
		# 	elif (self.dates[-1] - self.strategy_start ).total_seconds()/3600 < 24:
		# 		total = total
		# 		#print('2x clip')
		# 	else:
		# 		total = total*1.5
		# 		#print('1.5x clip')
		# else:
		# 	total = total


		stoploss = None
		#market_order = 'zero'
		market_order = False

		spread_scaler = 1

	
		if len(self.dates) > 2000000:
			
			if  self.ATR_switch == True and sum(self.ATR_switches[-12*6:]) == 12*6:
				
				if sp != None and self.closing_line[-1] != None:
					# if (sp - self.closing_line[-1])/sp < 0.007:
					# 	self.closing_line[-1] = self.volume_profile[self.volume_profile['percent']<0.50].index[-1]
					if self.highs[-1] > sp*spread_scaler and self.closes[-2] < sp*spread_scaler and (sp - self.closing_line[-1])/sp < 0.025:
						action = True
						Long = False
						price = sp*spread_scaler
						stoploss = price*(1+self.stoploss)
				

				if bp != None and self.closing_line[-1] != None:
					# if (self.closing_line[-1] - bp)/bp < 0.007:
					# 	self.closing_line[-1] = self.volume_profile[self.volume_profile['percent']<0.50].index[-1]

					if self.lows[-1] < bp/spread_scaler and self.closes[-2] > bp/spread_scaler  and (self.closing_line[-1] - bp)/self.closing_line[-1] < 0.025:
						action = True
						Long = True
						price = bp/spread_scaler
						stoploss = price/(1+self.stoploss)


		if action == True:
			if Long == True:
				if bp == self.buy_price1:
					self.b_1_ready = False
				elif bp == self.buy_price2:
					self.b_2_ready = False
				elif bp == self.buy_price3:
					self.b_3_ready = False

			if Long == False:
				if sp == self.sell_price1:
					self.s_1_ready = False
				elif sp == self.sell_price2:
					self.s_2_ready = False
				elif sp == self.sell_price3:
					self.s_3_ready = False

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for Trade in trades:
				avg_open +=Trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:

		if trade.trade_length == 2:
			trade.take_profit_percentage = (trade.entryPrice - self.closing_line[-2])/self.closes[-2]
			if self.trigger_dates[0] > 30:
				trade.trigger_date = self.trigger_dates[0]
			elif self.trigger_dates[-1] > 30:
				trade.trigger_date = self.trigger_dates[-1]
			elif self.trigger_dates[-2] > 30:
				trade.trigger_date = self.trigger_dates[-2]
			elif self.trigger_dates[-3] > 30:
				trade.trigger_date = self.trigger_dates[-3]
			trade.entry_ATR = self.ATR[-2]


		if trade.long == True:
			#print((trade.entryPrice - self.closes[-2])/trade.entryPrice)
			if (trade.entryPrice - self.closes[-2])/trade.entryPrice < -0.025:
				pass
				#print("NEW CLOSING LINE!",self.dates[-1])
				#self.closing_line[-1] = self.volume_profile[self.volume_profile['percent']<0.85].index[-1]

			if self.highs[-1] > self.closing_line[-1] and trade.trade_length > 1:
				action = True
				price = self.closing_line[-1]
					

		if self.ATR_switch == False and trade.long == True:
			if self.highs[-2] < self.stoploss_sell and self.highs[-1] > self.stoploss_sell:
				action = True
				price = self.stoploss_sell
			elif self.highs[-2] > self.stoploss_sell and self.highs[-1] > self.stoploss_sell:
				action = True
				price = self.closes[-1]
		
					
		if trade.long == False:
			#print((trade.entryPrice - self.closes[-2])/trade.entryPrice)
			if (trade.entryPrice - self.closes[-2])/trade.entryPrice < -0.025:
				pass
				#print("NEW CLOSING LINE!",self.dates[-1])
				#self.closing_line[-1] = self.volume_profile[self.volume_profile['percent']<0.50].index[-1]

			if self.lows[-1] < self.closing_line[-1] and trade.long == False and trade.trade_length > 1:
				action = True
				price = self.closing_line[-1]
			
				
		if self.ATR_switch == False and trade.long == False:
			if self.lows[-2] > self.stoploss_buy and self.lows[-1] < self.stoploss_buy:
				action = True
				price = self.stoploss_buy
			elif self.lows[-2] < self.stoploss_buy and self.lows[-1] < self.stoploss_buy:
				action = True
				price = self.closes[-1]



		if action == True:
			self.b_1_ready = True
			self.b_2_ready = True
			self.b_3_ready = True
			self.s_1_ready = True
			self.s_2_ready = True
			self.s_3_ready = True

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def calculate_clip_size(self):
		print('CALCULATING LEVERAGE')
		returns_30 = pd.Series(self.latest_range)
		#print(returns_30)
		returns_30 = (returns_30.diff()/returns_30)
		#print(self.account_size)
		
		#print(len(returns_30[returns_30 != 0]))
		#print(returns_30)
		if len(returns_30[returns_30 != 0]) != 0 and self.ATR_14[-1] is not None:
			print('CALCULATING LEVERAGE part 2')
			mean_returns = returns_30[returns_30 != 0].mean()*100
			risk = returns_30[returns_30 != 0].std()
			SR = mean_returns/risk
			if SR > 10:
				self.max_risk += 0.04 
			elif SR > 5:
				self.max_risk += 0.015

			elif 3 > SR > 5:
				self.max_risk += 0
			elif -5 < SR < 3:
				self.max_risk -= 0.02
			elif SR < -5: 
				self.max_risk -= 0.06



			if self.max_risk > 0.2:
				self.max_risk = 0.2
			elif self.max_risk < 0.04:
				self.max_risk = 0.04


			stoploss_required = (0.04 + self.ATR_14[-1]*8)
			self.stoploss = stoploss_required
			self.Leverage = self.max_risk/stoploss_required
			if self.Leverage > 4:
				self.Leverage = 4
			elif self.Leverage < 0.5:
				self.Leverage = 0.5

			print("STOP_LOSS ",stoploss_required)
			print("MAX RISK ",self.max_risk)
			print("last SR ", SR )
			print("FINAL Leverage",self.Leverage)
		else:
			self.Leverage = 2




	def calc_OI_density(self):
		vp_width = ((self.volume_profile.index[0] - self.volume_profile.index[-1])/self.closes[-1])/self.ATR[-1]
		OI_min = self.open_value[-self.trigger_date:]['low'].min()
		OI_cum = ((self.open_value['close'][-1:].item() - OI_min)/self.open_value['close'][-1:].item())*100
		#print(OI_cum)
		#print(vp_width)
		if OI_cum/vp_width < 2:
			self.OI_density.append(OI_cum/vp_width)
		else:
			self.OI_density.append(None)
		#print(OI_cum)
		#print(vp_width)
		#print(OI_cum/vp_width)
		#print('\n')
	def Draw_indicators(self):

		print("ALL INDICATOR LENGTHS")
		print(len(self.VPOC))
		print(len(self.buy_line1))
		print(len(self.buy_line2))
		print(len(self.buy_line3))
		print(len(self.sell_line1))
		print(len(self.sell_line2))
		print(len(self.sell_line3))
		print(len(self.ATR))
		print(len(self.open_value))
		print(len(self.ATR_switches))
		print(len(self.volitilitys))
		print(len(self.closing_line))
		print(len(self.dates))
		print(self.candle_count)
		print(len(self.five_m_dates))



		#return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.stoplosses,self.open_value,self.max_risks,self.leverages,self.max_risks,self.five_m_dates)#,self.ATR_min_list)
		return (self.VPOC,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.open_value,self.ATR_switches,self.OI_density,self.closing_line,self.five_m_dates)#,self.ATR_min_list)