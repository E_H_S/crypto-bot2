from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class VPOC(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= False,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = True)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None


		self.VPOC = np.array([None,None]*5)


		self.buy_line1 = [None,None]
		self.sell_line1 = [None,None]
		self.buy_line2 = [None,None]
		self.sell_line2 = [None,None]
		self.buy_line3 = [None,None]
		self.sell_line3 = [None,None]
		self.buy_line4 = [None,None]
		self.sell_line4 = [None,None]
		self.buy_line5 = [None,None]
		self.sell_line5 = [None,None]
		self.buy_line6 = [None,None]
		self.sell_line6 = [None,None]
		

		self.mid_line = [None,None]



		self.prices = [(None,None),(None,None),(None,None)]
		self.buy_price1 = None
		self.buy_price2 = None
		self.buy_price3 = None
		self.buy_price4 = None
		self.buy_price5 = None
		self.buy_price6 = None



		self.sell_price1 = None
		self.sell_price2 = None
		self.sell_price3 = None
		self.sell_price4 = None
		self.sell_price5 = None
		self.sell_price6 = None




		self.middle = None


		# self.b_1_ready = True
		# self.b_2_ready = True
		# self.b_3_ready = True
		# self.s_1_ready = True
		# self.s_2_ready = True
		# self.s_3_ready = True


		self.b_1_ready = False
		self.b_2_ready = False
		self.b_3_ready = False
		self.b_4_ready = False
		self.b_5_ready = False
		self.b_6_ready = False


		self.s_1_ready = False
		self.s_2_ready = False
		self.s_3_ready = False

		self.s_4_ready = False
		self.s_5_ready = False
		self.s_6_ready = False
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = np.array([])

		self.ATR_switches = np.array([])

		self.numSimulTrades = 6
		self.margin = True

		self.count10 = 0
		self.switch10 = False
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = np.array(price_info['weighted_average'])
		self.highs = np.array(price_info['highs'])
		self.lows = np.array(price_info['lows'])
		self.opens = np.array(price_info['opens'])
		self.closes = np.array(price_info['closes'])
		self.volumes = np.array(price_info['volumes'])
		self.dates = np.array(price_info['dates'])
		self.qv = np.array(price_info['qv'])

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		#updating Indicators


		if len(self.sell_line3)> 5:
			if self.sell_line3[-4:-3][0] != None:
				#print(self.highs[-1:])
				#print(self.sell_line3[-1:])
				if self.closes[-2:-1] > self.sell_line6[-2:-1][0] and self.closes[-3:-2] > self.sell_line6[-3:-2][0] and self.closes[-4:-3] > self.sell_line6[-4:-3][0]:
					self.s_1_ready = True
					self.s_2_ready = True
					self.s_3_ready = True
				elif self.closes[-2:-1] < self.buy_line6[-2:-1][0] and self.closes[-3:-2] < self.buy_line6[-3:-2][0] and self.closes[-4:-3] < self.buy_line6[-4:-3][0]:
					self.b_1_ready = True
					self.b_2_ready = True
					self.b_3_ready = True



		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')

		

		self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))
		
	


		if len(self.ATR) > 290:
			if len(self.open_value['max_delta_OI'][-1:]) > 0:

			
				
				if self.open_value['max_delta_OI'][-1:].item() < -self.open_value['open_interest'][-6*180:].max()*0.07 and self.switch10 == False: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 == True
					self.ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					# if self.ATR_switch == False:
					# 	self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					# elif self.ATR_switch == True:
					# 	if ATR_min < self.ATR_min:
					# 		self.ATR_min = ATR_min

					self.last_OV_high = self.open_value['open_interest'][-6*180:].max()
					self.ATR_switch = True

					print('\n')
					print("NEW TRIGGER EVENT! " , self.dates[-1:])
					print(self.open_value['max_delta_OI'][-1:].item())
					print("MIN ATR ", self.ATR_min)
					print('\n')
					


				if self.ATR[-1:] != None:
					if self.ATR_switches[-12*24:].min() == 1: 
						if self.ATR[-1:] < self.ATR_min*1 and self.ATR_switch == True:
							self.ATR_switch = False
							print('\n')
							print('FAILED ATR SWITCH ', self.dates[-1:])
							print('\n')
						
					
				if self.open_value['max_delta_lookback_OI'][-1:].item() > -self.open_value['open_interest'][-6*180:].max()*0.07 and self.ATR_switch == True:
					self.ATR_switch = False
					print('\n')
					print('5 day OV drop expired! ', self.dates[-1:])
					print('\n')



				if self.open_value['open_interest'][-1].item() > self.last_OV_high + self.open_value['open_interest'][-6*180:].max()*0.07 and self.ATR_switch == True:

					self.ATR_switch = False
					print('\n')
					print('OVER THE OV MAX ', self.dates[-1:])
					print('OV MAX ',self.last_OV_high + self.open_value['open_interest'][-6*180:].max()*0.07)
					print("LAST OI ",self.open_value['open_interest'][-1].item())
					print('\n')
			
			

			






		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2:-1])
		
		if len(self.dates)> 10:
			self.volume_profile,self.index = self.indicators.get_volume_profile(self.dates[-1:],self.index)

			
			self.open_value = self.indicators.get_open_value(self.dates[-2:-1])
			

			if len(self.dates) > 12*24:

				try:
					self.VPOC = np.append(self.VPOC,self.volume_profile[self.volume_profile['percent']<0.50].index[-1:][0])
				except:
					self.VPOC = np.append(self.VPOC,self.volume_profile[self.volume_profile['percent']>0.50].index[0])
				try:
					self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.20].index[-1:][0]
					self.sell_price2 = self.volume_profile[self.volume_profile['percent']<0.15].index[-1:][0]
					self.sell_price3 = self.volume_profile[self.volume_profile['percent']<0.10].index[-1:][0]
					self.sell_price4 = self.volume_profile[self.volume_profile['percent']<0.8].index[-1:][0]
					self.sell_price5 = self.volume_profile[self.volume_profile['percent']<0.05].index[-1:][0]
					self.sell_price6 = self.volume_profile[self.volume_profile['percent']<0.02].index[-1:][0]
					#self.sell_price3 = self.volume_profile.index[0]

					self.buy_price1 = self.volume_profile[self.volume_profile['percent']>0.80].index[0]
					self.buy_price2 = self.volume_profile[self.volume_profile['percent']>0.85].index[0]
					self.buy_price3 = self.volume_profile[self.volume_profile['percent']>0.90].index[0]
					self.buy_price4 = self.volume_profile[self.volume_profile['percent']>0.92].index[0]
					self.buy_price5 = self.volume_profile[self.volume_profile['percent']>0.95].index[0]
					self.buy_price6 = self.volume_profile[self.volume_profile['percent']>0.98].index[0]
					#self.buy_price3 = self.volume_profile.index[-1:][0]
				except:
					print(self.volume_profile)
					#self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.2].index[-1:][0]
					self.sell_price1 = self.volume_profile[self.volume_profile['percent']<0.20].index[-1:][0]
					self.sell_price2 = self.volume_profile[self.volume_profile['percent']<0.15].index[-1:][0]
					self.sell_price3 = self.volume_profile[self.volume_profile['percent']<0.13].index[-1:][0]
					self.sell_price4 = self.volume_profile.index[2]
					self.sell_price5 = self.volume_profile.index[1]
					self.sell_price6 = self.volume_profile.index[0]


					self.buy_price1 = self.volume_profile[self.volume_profile['percent']>0.80].index[0]
					self.buy_price2 = self.volume_profile[self.volume_profile['percent']>0.85].index[0]
					self.buy_price3 = self.volume_profile[self.volume_profile['percent']>0.87].index[0]
					self.buy_price4 = self.volume_profile.index[-3:-2][0]
					self.buy_price5 = self.volume_profile.index[-2:-1][0]
					self.buy_price6 = self.volume_profile.index[-1:][0]

			else:
				self.VPOC = np.append(self.VPOC,None)


			
		self.mid_line.append(self.middle)

		self.buy_line1.append(self.buy_price1)
		self.sell_line1.append(self.sell_price1)
		self.buy_line2.append(self.buy_price2)
		self.sell_line2.append(self.sell_price2)
		self.buy_line3.append(self.buy_price3)
		self.sell_line3.append(self.sell_price3)

		self.buy_line4.append(self.buy_price1)
		self.sell_line4.append(self.sell_price1)
		self.buy_line5.append(self.buy_price2)
		self.sell_line5.append(self.sell_price2)
		self.buy_line6.append(self.buy_price3)
		self.sell_line6.append(self.sell_price3)

		self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3),(self.buy_price4,self.sell_price4),(self.buy_price5,self.sell_price5),(self.buy_price6,self.sell_price6)]
				

		if self.ATR_switch == False:
			self.ATR_switches = np.append(self.ATR_switches,0)
			
		elif self.ATR_switch == True:
			self.ATR_switches = np.append(self.ATR_switches,1)
		self.ATR_min_list = np.append(self.ATR_min_list,self.ATR_min)
		#self.OV_cutoff_list = 
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 500
		stoploss = None
		#market_order = 'zero'
		market_order = False


		if bp == self.buy_price1 and self.b_1_ready == False:
			bp = None
		elif bp == self.buy_price2 and self.b_2_ready == False:
			bp = None
		elif bp == self.buy_price3 and self.b_3_ready == False:
			bp = None
		elif bp == self.buy_price4 and self.b_4_ready == False:
			bp = None
		elif bp == self.buy_price5 and self.b_5_ready == False:
			bp = None
		elif bp == self.buy_price6 and self.b_6_ready == False:
			bp = None


		if sp == self.sell_price1 and self.s_1_ready == False:
			sp = None
		elif sp == self.sell_price2 and self.s_2_ready == False:
			sp = None
		elif sp == self.sell_price3 and self.s_3_ready == False:
			sp = None
		elif sp == self.sell_price4 and self.s_4_ready == False:
			sp = None
		elif sp == self.sell_price5 and self.s_5_ready == False:
			sp = None
		elif sp == self.sell_price6 and self.s_6_ready == False:
			sp = None



		if len(self.dates) > 2:
			if len(self.open_value['max_delta_OI'])> 0:
				#print(self.open_value['max_delta'])

				if self.open_value['max_delta_OI'][-1:].isnull().item() == False:
					#if self.open_value['max_delta'][-1:].item() < -8000 and self.ATR[-1:] > 0.0009 :
					if  self.ATR_switch == True :
						
						if sp != None:
							if self.highs[-1:] > sp and self.closes[-2:-1] < sp and sp > self.VPOC[-1:]*1.000125:
								action = True
								Long = False
								price = sp
								#stoploss = self.highs[-288*2:].max()*1.02
								
						if bp != None:

							if self.lows[-1:] < bp and self.closes[-2:-1] > bp and bp < self.VPOC[-1:]/1.000125:
								action = True
								Long = True
								price = bp
								#stoploss = self.lows[-288*2:].min()/1.02



		if action == True:
			if Long == True:
				if bp == self.buy_price1:
					self.b_1_ready = False
				elif bp == self.buy_price2:
					self.b_2_ready = False
				elif bp == self.buy_price3:
					self.b_3_ready = False

			if Long == False:
				if sp == self.sell_price1:
					self.s_1_ready = False
				elif sp == self.sell_price2:
					self.s_2_ready = False
				elif sp == self.sell_price3:
					self.s_3_ready = False


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:



		if self.highs[-1:] > self.VPOC[-1:] and trade.long == True:
		#if self.highs[-1:] > self.sell_price1 and trade.long == True:
			action = True
			price = self.VPOC[-1:]
			#price = self.sell_price1
		
					


		#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
		if self.lows[-1:] < self.VPOC[-1:] and trade.long == False:
		#if self.lows[-1:] < self.buy_price1 and trade.long == False:
			action = True
			price = self.VPOC[-1:]
			#price = self.buy_price1
		
		



		if action == True:
			self.b_1_ready = False
			self.b_2_ready = False
			self.b_3_ready = False
			self.s_1_ready = False
			self.s_2_ready = False
			self.s_3_ready = False

		# if action == True:
		# 	self.b_1_ready = True
		# 	self.b_2_ready = True
		# 	self.b_3_ready = True
		# 	self.s_1_ready = True
		# 	self.s_2_ready = True
		# 	self.s_3_ready = True

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):
		return (self.VPOC,self.buy_line2,self.sell_line2,self.buy_line4,self.sell_line4,self.buy_line6,self.sell_line6,self.ATR,self.open_value,self.ATR_switches)#,self.ATR_min_list)


