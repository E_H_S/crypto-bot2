import random 
import numpy as np 

def three_sided_dice(): # random returns one of the following intergers 0,1,2
	x = np.random.random() 
	result = None
	if x < 1/3:
		result = 0
	elif 1/3 < x < 2/3:
		result = 1
	elif 2/3 < x:
		result = 2
	return result

##This is a script to prove that switching in the Monte Hall problem leads to a 66.66% win rate

interations = 10000
wins = np.zeros(interations)



for n in range(interations):

	players_guess = three_sided_dice() #Constant player chooses a random door
	car_location = three_sided_dice() #Car is hidden behind a random door

	#doors is a object where each list represents a door.
	#The first element of the inner list represents the players guess. A 1 in this position reprsents the picked door.
	# 1 in the second element position represents the location of the car
	doors = [[0,0],[0,0],[0,0]]
 
	#print(doors)
	#Use the randomly generated door gueses to create the doors object outlined above.
	for x in range(3):
		for y in range (2):
			if x == players_guess and y == 0:
				doors[x][y] = 1
			if x == car_location and y == 1:
				doors[x][y] = 1

	#print(doors)
	#radomly choose a door which was not guessed by the player and does not have the car.
	p = 0 #dummy variable
	while p == 0:
		rand = three_sided_dice()
		if doors[rand] == [0,0]:
			doors.pop(rand) # ejects this element from the doors object ie. The game show host opens the door without the car.
			p = 1
	#print(doors)

	#Finally now the contestant switches his guess for a extra 33.33% chance at winning the car.
	if doors[0][0] == 1:
		#print("check 1")
		doors[0][0]= 0
		doors[1][0] = 1
	elif doors[1][0]== 1:
		#print("check 2")
		doors[0][0]= 1
		doors[1][0] = 0	

	#print(doors)
	if doors[0][0] == 1 and doors[0][1] == 1 or doors[1][0]== 1 and doors[1][1] == 1:
		#print("Contestant wins!!!")
		wins[n] = 1

	else:
		#print("Contestant loses...")
		wins[n] = 0

total_wins = sum(wins)
win_percentage = total_wins/interations
print("win percentage over 1000 iteratations = "+str(win_percentage))




