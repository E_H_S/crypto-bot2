from botlog import BotLog
from botindicators import BotIndicators
from bottrade import BotTrade
import datetime
import numpy as np 
import pandas as pd
#from poloniex import poloniex
import time

# SMA_M uses 3 indicators and will require an imput perameter for each


class WICKBACK_VPOC(object):
	def __init__(self,period = 300,startTime = None, endTime = None):

		#self.fast = 5
		self.fast = 6
		self.slow = 42
		self.longest_indicator = max(self.fast,self.slow)

		self.period = period
		self.limits = True
		self.index = 0
		self.ATR_min = 1000000000
		
		self.indicators = BotIndicators(orderbook= True,open_value = True, funding_rate = False,startTime = startTime ,endTime = endTime,volume_profile = False)
		

		
		#Inititalize inticator vectors
		self.mavs  = np.array([])
		self.mavs_delta = np.array([])
		self.mavs2  = np.array([])
		self.RSI = np.array([])
		self.ATR = np.array([])

		self.ob_index = None


		self.VPOC = np.array([None,None])
		self.vp_percentage =np.array([])


		self.buy_line1 = [None]
		self.sell_line1 = [None]
		self.buy_line2 = [None]
		self.sell_line2 = [None]
		self.buy_line3 = [None]
		self.sell_line3 = [None]
		self.buy_line4 = [None]
		self.sell_line4 = [None]
		self.buy_line5 = [None]
		self.sell_line5 = [None]
		self.buy_line6 = [None]
		self.sell_line6 = [None]
		

		self.mid_line = []

		self.prices = [(None,None),(None,None),(None,None)]
		




		self.middle = None


		
		
		self.max_value = 0
		self.last_OV_high = 1000000000000000
		self.ATR_switch = False
		
		self.ATR_min_list = np.array([])
		self.Min_depths = np.array([])

		self.ATR_switches = np.array([])

		self.numSimulTrades = 3
		self.margin = True

		self.count10 = 0
		self.switch10 = False
		
		self.scale = 14400/self.period
		self.longest_indicator = max(self.fast,self.slow)*self.scale
		
	def tick_price_info(self,price_info,trades):

		#self.price_info = {'candlestick':self.candlesticks,'dates':self.dates,'opens':self.openprices,'closes':self.closeprices,'weighted_average':self.priceAverage,'highs':self.highs,"lows":self.lows,'volumes':self.volumes,'qv':self.quoteVolumes}
		
		self.average_prices = price_info['weighted_average']
		self.highs = price_info['highs']
		self.lows = price_info['lows']
		self.opens = price_info['opens']
		self.closes = price_info['closes']
		self.volumes = price_info['volumes']
		self.dates = price_info['dates']
		self.qv = price_info['qv']

		#Update relevent indicators
		self.tick_indicators()

	def tick_indicators(self):
		#updating Indicators
		self.ATR = np.append(self.ATR,self.indicators.avg_range(self.closes,self.highs,self.lows,288))

		if len(self.dates)>1:
			self.open_value = self.indicators.get_open_value(self.dates[-2])

		if self.switch10 == True:
			self.count10 +=1

		if self.count10 > 12*3:
			self.switch10 = False
			self.count10 = 0
			print('SWITCH10 RESET')


		if len(self.ATR) > 290:
			if len(self.open_value['max_delta_OI'][-1:]) > 0:

			
				
				if self.open_value['max_delta_OI'][-1:].item() < -self.open_value['open_interest'][-6*180:].max()*0.07 and self.switch10 == False: #and self.open_value['max_delta'][-1:].item() != self.max_value:
					self.switch10 == True
					self.ATR_min =  min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					# if self.ATR_switch == False:
					# 	self.ATR_min = min(filter(lambda x: x is not None, self.ATR[-12*6:]))
					# elif self.ATR_switch == True:
					# 	if ATR_min < self.ATR_min:
					# 		self.ATR_min = ATR_min

					self.last_OV_high = self.open_value['open_interest'][-6*180:].max()
					self.ATR_switch = True

					print('\n')
					print("NEW TRIGGER EVENT! " , self.dates[-1:])
					print(self.open_value['max_delta_OI'][-1:].item())
					print("MIN ATR ", self.ATR_min)
					print('\n')
					


				if self.ATR[-1:] != None:
					if self.ATR_switches[-12*24:].min() == 1: 
						if self.ATR[-1:] < self.ATR_min*1 and self.ATR_switch == True:
							self.ATR_switch = False
							print('\n')
							print('FAILED ATR SWITCH ', self.dates[-1:])
							print('\n')
						
					
				if self.open_value['max_delta_lookback_OI'][-1:].item() > -self.open_value['open_interest'][-6*180:].max()*0.07 and self.ATR_switch == True:
					self.ATR_switch = False
					print('\n')
					print('5 day OV drop expired! ', self.dates[-1:])
					print('\n')



				if self.open_value['open_interest'][-1].item() > self.last_OV_high + self.open_value['open_interest'][-6*180:].max()*0.07 and self.ATR_switch == True:

					self.ATR_switch = False
					print('\n')
					print('OVER THE OV MAX ', self.dates[-1:])
					print('OV MAX ',self.last_OV_high + self.open_value['open_interest'][-6*180:].max()*0.07)
					print("LAST OI ",self.open_value['open_interest'][-1].item())
					print('\n')


		if len(self.dates)> 10:
			self.volume_profile,self.index = self.indicators.get_volume_profile(self.dates[-1:],self.index)
			if self.closes[-1:][0] >= self.volume_profile.index.max():
				self.vp_percentage = np.append(self.vp_percentage, 0)
			elif self.closes[-1:][0] <= self.volume_profile.index.min():
				self.vp_percentage = np.append(self.vp_percentage, 1)
			else:
				# print(self.volume_profile)
				# print(self.closes[-1:])
				# print(self.volume_profile[self.volume_profile.index >= self.closes[-1:][0]])
				# print(self.volume_profile[self.volume_profile.index >= self.closes[-1:][0]]['percent'].values[-1:].item())
				# print(self.volume_profile[self.volume_profile.index >= self.closes[-1:][0]]['percent'][-1:])
				self.vp_percentage = np.append(self.vp_percentage, self.volume_profile[self.volume_profile.index >= self.closes[-1:][0]]['percent'].values[-1:].item())






			
		
			print(self.volume_profile)
			print(self.closes[-1:])
			print(self.volume_profile[self.volume_profile.index > self.closes[-1:][0]])
			print(self.volume_profile[self.volume_profile.index > self.closes[-1:][0]]['percent'].values[-1:].item())
			print(self.volume_profile[self.volume_profile.index > self.closes[-1:][0]]['percent'][-1:])

			self.vp_percentage = np.append(self.vp_percentage, self.volume_profile[self.volume_profile.index > self.closes[-1:][0]]['percent'].values[-1:].item())
		else:
			self.vp_percentage = np.append(self.vp_percentage,None)


		#print(self.vp_percentage)
		# if self.ATR[-1:]!= None:
		# 	min_depth = (self.ATR[-1:][0]/0.0071)*30000000
		# 	max_depth = (self.ATR[-1:][0]/0.0071)*60000000

		# # 	if min_depth < 12500000:
		# # 		min_depth = 60500000
		# # 		max_depth = 90000000

		# 	print(min_depth)
		# 	print(max_depth)
		# else:
		min_depth = 7500000
		max_depth = 20000000

		self.Min_depths = np.append(self.Min_depths,min_depth)

		if len(self.dates[-2:-1])> 0:
			self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-2],self.ob_index)
			#self.last_orderbook,self.ob_index = self.indicators.get_orderbook(self.dates[-2:-1],None)
			self.buy_prices,self.sell_prices = self.indicators.n_largest_spread(self.last_orderbook,min_depth ,max_depth,3)
			try:
				self.buy_price1,self.buy_price2,self.buy_price3 = self.buy_prices['price']
				self.sell_price1,self.sell_price2,self.sell_price3 = self.sell_prices['price']
			except:
				self.buy_price1 = None
				self.buy_price2 = None
				self.buy_price3 = None
				self.sell_price1 = None
				self.sell_price2 = None
				self.sell_price3 = None


			self.prices = [(self.buy_price1,self.sell_price1),(self.buy_price2,self.sell_price2),(self.buy_price3,self.sell_price3)]

			self.buy_line1.append(self.buy_price1)
			self.sell_line1.append(self.sell_price1)

			self.buy_line2.append(self.buy_price2)
			self.sell_line2.append(self.sell_price2)

			self.buy_line3.append(self.buy_price3)
			self.sell_line3.append(self.sell_price3)

		if self.ATR_switch == False:
			self.ATR_switches = np.append(self.ATR_switches,0)
			
		elif self.ATR_switch == True:
			self.ATR_switches = np.append(self.ATR_switches,1)
		self.ATR_min_list = np.append(self.ATR_min_list,self.ATR_min)
			
			
			
	def evaluate_Open(self,bp = None,sp = None,trades = None):
		action = False
		Long = None
		price = None
		date = self.dates[-1:]
		total = 1000
		stoploss = None
		#market_order = 'zero'
		market_order = False


				
		# if sp != None and self.closes[-2:-1] != None and self.ATR_switch == True:
		# 	#if self.highs[-1:] > sp and self.closes[-2:-1] < sp and  0.20 < self.vp_percentage[-2:-1] < 0.80 and  0.20 < self.vp_percentage[-3:-2] < 0.80 and  0.20 < self.vp_percentage[-4:-3] < 0.80 and  0.20 < self.vp_percentage[-5:-4] < 0.80 and  0.2 < self.vp_percentage[-6:-5] < 0.80 and sp > self.closes[-2:-1]*1.0035: 
		# 	if self.highs[-1:] > sp and self.closes[-2:-1] < sp and sp > self.closes[-2:-1]*1.0035: 
			
		# 		action = True
		# 		Long = False
		# 		price = sp
		# 		stoploss = sp*1.01
				
		# if bp != None and self.closes[-2:-1] != None and self.ATR_switch == True:

		# 	if self.lows[-1:] < bp and self.closes[-2:-1] > bp  and bp < self.closes[-2:-1]/1.0035:
		# 		action = True
		# 		Long = True
		# 		price = bp
		# 		stoploss = bp/1.01


		

		return action,Long,price,date,total,self.margin,stoploss,market_order
	def evaluate_Close(self,trade,trades = None):
		

		if trades != None:
			avg_open = 0
			for trade in trades:
				avg_open +=trade.entryPrice
			avg_open = avg_open/len(trades)

		action = False
		price = None
		date = self.dates[-1:]
		total = None
		amount = None
		#market_order = 'zero'
		market_order = False
		
		#print(trade.trade_length)
		#print(date)

		#if self.highs[-1:] > trade.entryPrice*1.02 and trade.long == True:



		#if self.highs[-1:] > self.VPOC[-1:] and trade.long == True:
		#if trade.trade_length == 3 and trade.long == True:
		if self.highs[-1:] > trade.entryPrice*1.01  and trade.long == True:
		#if self.highs[-1:] > self.sell_price1 and trade.long == True:
			action = True
			price = self.closes[-1]
			price = trade.entryPrice*1.01
			#price = self.sell_price1
		
					


		#if self.lows[-1:] < trade.entryPrice/1.02 and trade.long == False:
		#if self.lows[-1:] < self.VPOC[-1:] and trade.long == False:
		#if self.lows[-1:] < self.buy_price1 and trade.long == False:
		#if trade.trade_length == 3 and trade.long == False:
		if self.lows[-1] < trade.entryPrice/1.01 and trade.long == False:
			action = True
			#price = self.closes[-1:]
			price = trade.entryPrice/1.01
			#price = self.buy_price1
		
		

		return action,price,total,date,amount,self.margin,market_order
				
	def tick_micro(self,candlestick,prices,lows,highs,opens,closes,dates):
		pass

	def add_position():
		pass

	def scale_out():
		pass
	def Draw_indicators(self):

		#self.vp_percentage = np.append(self.vp_percentage[self.vp_percentage == None],1 - self.vp_percentage[self.vp_percentage != None] )
		return (self.open_value,self.ATR_switches,)
		#(self.vp_percentage,self.buy_line1,self.sell_line1,self.buy_line2,self.sell_line2,self.buy_line3,self.sell_line3,self.ATR,self.Min_depths)
		#(self.VPOC,self.buy_line2,self.sell_line2,self.buy_line4,self.sell_line4,self.buy_line6,self.sell_line6,self.ATR,self.open_value,self.ATR_switches)#,self.ATR_min_list)


