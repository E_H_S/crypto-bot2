
##importing all required modules
import numpy as np
import sys, getopt
from botchart import BotChart
from botVis2 import BotVisual
from botVis_breakouts import BotVisual_breakouts


from BotStratLog import BotTradingLog
from creating_features import CF
#import modin.pandas as pd



#import calendar

from botlog import BotLog
from StratAnalyzer import Analyzer 
from Strategies import Strategies
from SMA_resistance import SMA_R
#from RAM_Database import RAM_DATABASE
import time
import datetime

from Strategy_compiler import Compiler
from Sig_levels import Sig_L
from Fishing_strategy import FISHY
from FundingRate import FR
import pandas as pd
#strategies
from Bollinger import Bolly_M
from SMA_MARGIN import SMA_M
from Renko_strat import renko_mom
from renko_range import renko_range
from Renko_strat_limit_only import Renko_Lim
from Range import Range
from Volume_prof_strategy import VPOC
from Wickfinder import Wicky	
from OI_change_strat import  OI_CHA_CHA
from bitmex_mom import Momentum
from Bitmex_bolli import Bolli
from Exhaustion import Exhaustion
from WICKBACKS_VPOC import WICKBACK_VPOC
from ATR_ONLY import ATR_ONLY
from Wicks_small import Wicks_small
from Backside_only import VPOC_BS
from Time_based_VPOC import VPOC_TIME
from Anchored_VPOC import VPOC_ANCHORED
from Momentum_v3 import VPOC_MOMENTUM
from Liquidity_Rejection import Liquid_Rejection
from VPOC_ANCHORED_ORDERBOOK import VPOC_ANCHORED_ORDERBOOK
from VPOC_ANCHORED_ORDERBOOK2 import VPOC_ANCHORED_ORDERBOOK2
from VPOC_BACKSIDE2 import VPOC_BS2
from VPOC2 import VPOC2
from VPOC_NA_MOM import VPOC_NA_MOM
from Anchored_VPOC_1m import VPOC_ANCHORED_1m
from Anchored_VPOC_v4 import VPOC_ANCHORED_v4
from VPOC_Preasure import VPOC_ANCHORED_P
from VPOC_ANCH_VERY_FAST import VPOC_ANCHORED_VF
from VPOC_diff import VPOC_diff
from Price_spreading import P_Spreading
from Price_spreading_1m import P_Spreading_1m
from Breakouts import Breakouts
from Market_maker_VPOC import M_M_VPOC
from Breakouts_part2 import Breakouts2
from Liquidation_data_analysis import Liquidation_data_collection
from ANCHORED_VPOC_ALWAYS_ON import VPOC_ANCHORED_ALWAYS_ON
from VPOC_ANCHORED_ACCUMULATION_FILTER import VPOC_ANCHORED_ACCUM
from VPOC_ANCH_LIQUIDITY_ONLY import VPOC_ANCHORED_LIQUIDITY
from botVis_Wick_in_Wickout import BotVisual_WIWO
from classic_market_making import classic_market_maker
from botvis_market_maker import BotVisual_market_maker



import query


import multiprocessing 



##Input perameters ##
#exchange = "poloniex"
exchange = 'Database1'

#candle_table = 'candles5m_XBT'
#candle_table = 'candles5m_XBT3'
#candle_table = 'candles1m_XBT4'
#candle_table = 'candles5m_XBT_JULY_AUG3'
#candle_table = 'candles1m_XBT_JULY_AUG3'
 

candle_table = 'XBT_5m_candles'


#orderbook_table = 'XBT_orderbook_May'
#candle_table = 'candles1h_XBT'
#candle_table = 'XBT_Futures_delta'
#candle_table = 'candles3'
				

pairs = ["BTC_ETH"]

#chart_style = 'renko'
chart_style = 'candlestick'
period = 60
#calendar.timegm()							#period of the candlesticks being queried, NOTE poloniex only excepts values of [300,900,1800,7200,14400,86400]
# startTime = time.mktime(datetime.datetime(2019,2,15,0,0,0).timetuple()) #local time
# endTime = time.mktime(datetime.datetime(2019,2,23,0,0,0).timetuple())	#local time

startTime 	= datetime.datetime(2019,9,15,0,0,0) #local time
endTime 	= datetime.datetime(2019,10,15,0,0,0)

#conver to UTC
# startTime = startTime - datetime.timedelta(hours = 11)
# endTime = endTime - datetime.timedelta(hours = 11)

collect_features = False

draw_pause = float(0.0) 											#Time pause between each chart update
Initital_BullVolume = float(0)										# 1 btc starting fund and float because you can only have a minimum of 1 satoshi
Initital_BearVolume = float(1000)
strategy = {} 		#dictionary with pair as key and value as strategy object
#Strategies = Strategies()
TradingLog = {}
Graph = {}
results = {}
if collect_features == False:
	features = CF()


#Paremeter Sweep Inputs
#Paremeter1 = list(np.linspace(0.05,0.1,16))
Paremeter1 = [0.085]
Paremeter2 = [1.06]
Paremeter3 = list(np.linspace(350,3000,8))


def Sweep(Paremeter1 = None,Paremeter2 = None,Paremeter3 = None):


	q = multiprocessing.Queue()
	Processes = []


	if Paremeter1 != None and Paremeter2 != None and Paremeter3 != None:
		for p1 in Paremeter1:
			for p2 in Paremeter2:
				for p3 in Paremeter3:
					Processes.append(multiprocessing.Process(target= main, args = (True,p1,p2,p3,q)))
	elif Paremeter1 != None and Paremeter2 != None:
		for p1 in Paremeter1:
			for p2 in Paremeter2:
				Processes.append(multiprocessing.Process(target= main, args = (True,p1,p2,None,q)))
	elif Paremeter1 != None:
		for p1 in Paremeter1:
				Processes.append(multiprocessing.Process(target= main, args = (True,p1,None,None,q)))

	total_tests = len(Processes)

	
	m = 0
	for p in Processes:
		print("STARTING BACKTEST "+str(m))
		print(str(len(Processes) - m) + ' remaining backtests')
		while True:
			if len(multiprocessing.active_children()) < 4:
				p.start()
				print("NEW BACKTEST STARTED")
				break
			else:
				pass
		m+= 1

	back_test_results = []

	#num_backtests = len(Paremeter1)*len(Paremeter2)*len(Paremeter3)

	while len(back_test_results) < total_tests:
		back_test_results.append(q.get())
		print("Scores added")

	df_scores = pd.DataFrame(back_test_results)

	df_scores.to_csv('VPOC_P_SWEEP_DATA.csv')



def RAM_Sweep(Paremeter1 = None,Paremeter2 = None,Paremeter3 = None):


	## 1. create and return the connection to the ram database
	RAM_DB = RAM_DATABASE()

	## 2. Start a loop of main funtion runs with unique parameters to test and the connection the RAM Database

	## 3. Let each loop through main append the results to csv

def main(parameter_sweep = False,p1 = None,p2 = None,p3 = None,q = None):
	
	chart = BotChart(exchange,pairs,period,startTime,endTime,
		chart_style = chart_style,
		backtest = False,
		download = False,
		data_set = 'XBT_USD 1m 2018 Bitmex.csv',
		candle_table = candle_table)
	
	#chart = BotChart(exchange,pairs,period,startTime,endTime,chart_style = chart_style,backtest = False,download = False,data_set = 'ETH_USD 1m 2018 Bitmex.csv') 
	count = 0 

	for pair in pairs:																			#Instatiate an object that queries for past price history from an exchange, between given dates, and exchange pairs.
		#strategy[str(pair)] = renko_mom(period = period)
		
		#strategy[str(pair)] =  VPOC_BS(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3) #times are in UNIX
		#strategy[str(pair)] =  classic_market_maker(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3) #times are in UNIX
		strategy[str(pair)] =  VPOC_MOMENTUM(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3) #times are in UNIX
		#strategy[str(pair)] =  Wicks_small(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3) #times are in UNIX
		#strategy[str(pair)] =  VPOC_ANCHORED(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3) #times are in UNIX
		#strategy[str(pair)] =  VPOC_ANCHORED_LIQUIDITY(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3)
		#strategy[str(pair)] =  Breakouts(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3)
		#strategy[str(pair)] =  Liquidation_data_collection(period = period,startTime = startTime, endTime = endTime,p1 = p1,p2=p2,p3=p3)

		TradingLog[str(pair)] = BotTradingLog(pair,period,strategy[str(pair)],
			starting_bull_volume = Initital_BullVolume,
			starting_bear_volume= Initital_BearVolume,
			live = False,
			backtest = True)											 #Instatiate a TradingLog object and the Number of tokens you wish to trade with in the currency of the Bear Token eg. BTC for the BTC/ETH exchange
		
		Graph[str(pair)] = BotVisual(chart_style = chart_style)
		#Graph[str(pair)] = BotVisual_market_maker(chart_style = chart_style)
		#Graph[str(pair)] = BotVisual_WIWO(chart_style = chart_style)
		#Graph[str(pair)] = BotVisual_breakouts(chart_style = chart_style)

	candles_allpairs = chart.getPoints()					#Candlesticks is a dictionary with key being the pair and values being a list of candlestick objects

	for pair in pairs:
		total_calcs = len(candles_allpairs[str(pair)])
		print("TOTAL CANDLES : " + str(len(candles_allpairs[str(pair)])))
		for candles in candles_allpairs[str(pair)]:			# A list of candlestick objects
			TradingLog[str(pair)].tick(candles)
			#Graph[str(pair)].draw(TradingLog[str(pair)],draw_pause)
			count += 1

			if np.remainder(count,1000) == 0:
				print('\n')
				print(str(round(count/total_calcs,2))+"%"+" complete ")
				print(datetime.datetime.fromtimestamp(time.time()))
				#print('Strategy total seconds : ' ,TradingLog[str(pair)].botcontroller.strategy_total_time)
				# print('Strategy timer1 : ' ,TradingLog[str(pair)].botcontroller.strategy.timer1)
				# print('Strategy timer2 : ' ,TradingLog[str(pair)].botcontroller.strategy.timer2)
				# print('Strategy timer3 ' ,TradingLog[str(pair)].botcontroller.strategy.timer3)
				# print('Strategy timer4 ' ,TradingLog[str(pair)].botcontroller.strategy.timer4)
				# print('Strategy timer5 ' ,TradingLog[str(pair)].botcontroller.strategy.timer5)
				# print('Strategy timer6 ' ,TradingLog[str(pair)].botcontroller.strategy.timer6)
				# print('Strategy timer7 ' ,TradingLog[str(pair)].botcontroller.strategy.timer7)
				# print('Strategy timer8 ' ,TradingLog[str(pair)].botcontroller.strategy.timer8)

				# print('Indicator timer1 ' ,TradingLog[str(pair)].botcontroller.strategy.indicators.timer1)
				# print('Indicator timer2 ' ,TradingLog[str(pair)].botcontroller.strategy.indicators.timer2)
				# print('Indicator timer3 ' ,TradingLog[str(pair)].botcontroller.strategy.indicators.timer3)


				# print('Strategy total seconds : ' ,TradingLog[str(pair)].botcontroller.strategy_total_time)
				# print('Controller total seconds : ' ,TradingLog[str(pair)].botcontroller.trade_controller_time)


		results[str(pair)] = Analyzer(TradingLog[str(pair)],pair,startTime,endTime)
		bt_data,trades =  results[str(pair)].full_analysis()

		if collect_features == True:
			features.create_DataFrame(
				bt_data,
				trades, 
				TradingLog[str(pair)].botcontroller.strategy.indicators.open_value_pairs,
				TradingLog[str(pair)].botcontroller.strategy.indicators.orderbooks_pair,
				TradingLog[str(pair)].botcontroller.strategy.indicators.funding_rate_pairs,
				TradingLog[str(pair)].botcontroller.strategy.indicators.volume_profile_pairs,
				TradingLog[str(pair)].botcontroller.strategy.indicators.one_m_candles_pairs
				)
		
		if parameter_sweep == False:	
			Graph[str(pair)].buildChart(TradingLog[str(pair)],results[str(pair)].df)

		if parameter_sweep == True:

			backtest_scores = {}
			backtest_scores['p1'] = p1
			backtest_scores['p2'] = p2
			backtest_scores['p3'] = p3
			backtest_scores['Final_return'] = results[str(pair)].final_return
			backtest_scores['Sharp_ratio'] = results[str(pair)].sharp_ratio
			backtest_scores['Ulcer_index'] = results[str(pair)].ulcer_index
			backtest_scores['num_trades'] = len(results[str(pair)].df_trades)
			backtest_scores['edge_before_commision'] = results[str(pair)].edge_before_commision
			q.put(backtest_scores)
			

		#Graph[str(pair)].equity_curve(results.df)
		
	
	#Portfolio = Compiler(results) #combines multiple trading pairs into one portfolio analysis benched against indexing the pairs
if __name__ == "__main__":
	#main(p1 = 8000000,p2 = 3000000000/4,p3 = 2*9333.8/1000000) # Single Run
	#main(p1 = 15000000,p2 = 243750000,p3 = 0.01026718) # Single Run
	main()
	#Sweep(Paremeter1 = Paremeter1,Paremeter2=Paremeter2,Paremeter3=Paremeter3) # Sweep Run



